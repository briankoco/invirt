FROM registry.fedoraproject.org/fedora:33

RUN dnf install -y procps kmod vim iproute gcc make glibc glibc-static gcc-gfortran

COPY build/invrun /
COPY build/invg.ko /
COPY build/install-invg.sh /

RUN mkdir /bench
COPY build/random_access /bench
COPY build/*.x /bench

RUN mkdir /tmp/invirt
COPY user/Makefile /tmp/invirt/Makefile
COPY user/shadow.o /tmp/invirt
COPY user/memory_map.o /tmp/invirt
COPY user/utils.o /tmp/invirt
COPY user/hashtable.o /tmp/invirt
COPY user/elfrw/libelfrw.a /tmp/invirt

#ENTRYPOINT /invrun 
