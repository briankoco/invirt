/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#ifndef __INVRUN_H__
#define __INVRUN_H__

#include <sys/user.h>
#include <invirt.h>

int
invirt_parse_loadable_elf_segments(const char    * exe_path,
                                   unsigned long   page_size,
                                   unsigned long * base_addr,
                                   unsigned long * size);

int
invirt_parse_elf_entry_point(const char    * exe_path,
                             unsigned long * entry_point);

#define SHADOW_HOST_MODE  0
#define SHADOW_GUEST_MODE 1

#endif /* __INVRUN_H__ */
