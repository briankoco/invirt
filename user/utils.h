/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#ifndef __UTILS_H__
#define __UTILS_H__

#ifndef __KERNEL__ 

/*
 * miscellaneous utilities
 */
#include <sys/select.h>
#include <sys/types.h>
#include <unistd.h>


/* toggle to disable syscall debugging print statements */
#define INVIRT_DEBUG_SYS

extern char   * inv_context;
extern FILE   * inv_out_fp;
extern FILE   * inv_err_fp;

#define __out(stream, fmt, args...)\
    do {\
        fprintf(stream, "%s: " fmt,\
            inv_context,\
            ##args);\
        fflush(stream);\
    } while(0)

#define out(fmt, args...) __out(inv_out_fp, fmt, ##args)
#define err(fmt, args...) __out(inv_err_fp, fmt, ##args)

#ifdef INVIRT_DEBUG_SYS
#define dbg(fmt, args...) __out(inv_out_fp, fmt, ##args)
#else
#define dbg(fmt, args...) do {} while (0)
#endif

void
invirt_init_context(const char * context);

ssize_t
invirt_write_data_fd(int          fd,
                     const char * data,
                     size_t       data_len);

ssize_t
invirt_read_data_fd(int     fd,
                    char  * data,
                    size_t  data_len);

int
invirt_query_uuid(char * uuid_str,
                  size_t uuid_str_len);

pid_t
invirt_query_kvm_pid(const char * uuid_str);


/*
 * fd handlers for event notification
 */
typedef int (*fd_handler_fn)(int, void *);
struct fd_handler {
    int fd;
    fd_handler_fn fn;
    void * priv_data;
};

struct notifier_internal;
typedef struct notifier_internal invirt_notifier_t;

invirt_notifier_t * 
invirt_notifier_create(void);

void
invirt_notifier_destroy(invirt_notifier_t * notifier);

int
invirt_notifier_remember_fd(invirt_notifier_t * notifier,
                            int                 fd,
                            fd_handler_fn       fn,
                            void              * priv_data);

int
invirt_notifier_forget_fd(invirt_notifier_t * notifier,
                          int                 fd);

struct fd_handler *
invirt_notifier_handler_of_fd(invirt_notifier_t * notifier,
                              int                 fd);

void
invirt_notifier_get_fd_info(invirt_notifier_t * notifier,
                            fd_set            * set,
                            int               * max_fd);

#endif /* __!KERNEL__ */
        
#endif /* __UTILS_H__ */
