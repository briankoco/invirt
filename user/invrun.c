/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

/*
 * invrun is a guest-side executable launcher.
 *
 * It first loads an ELF into a new process address space, and then mirrors its
 * address space before transfering program control to invd for co-native
 * execution. It then listens for system call requests from invd and issues
 * them on to the guest kernel.
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/user.h>

#include <invirt.h>

#include "invrun.h"
#include "memory_map.h"
#include "hashtable.h"
#include "utils.h"

#define INV_PID_TERMINATED  ((pid_t)0)
#define INV_INIT_HEAP_SIZE  ((1ULL << 30) + (1ULL << 21))
#define MIN_TEXT_START      (0x1000)

struct program_data {
    char * this_exe;

    /* shadow data */
    char * shadow_obj;
    char * shadow_exe;
    unsigned long shadow_text_start;
    pid_t shadow_pid;

    /* pipes for dup()ing shadow's stdout/stderr */
    int shadow_outfd[2];
    int shadow_errfd[2];
    int shadow_register_fd[2];
    FILE * shadow_outfp;
    FILE * shadow_errfp;

    /* target data */
    char * exe;
    pid_t pid;
    struct user_regs_struct regs;

    /* target argc/argv/envp */
    int argc;
    char ** argv;
    int envc;
    char ** envp;
    
    /* pipes for dup()ing target's stdin/stderr/stdout */
    int target_infd[2];
    int target_outfd[2];
    int target_errfd[2];
};


/* a set of globals for handling signals */
static invirt_notifier_t * notifier = NULL;
static int sigchld_pipe[2];
static bool terminate;

static int
exec_shadow(struct program_data * data,
            int                   mode)
{
    char * argv[7];

    /* setup argv */
    asprintf(&(argv[0]), "%s", data->shadow_exe);
    asprintf(&(argv[1]), "%d", mode);    
    asprintf(&(argv[2]), "%d", 0);
    asprintf(&(argv[3]), "%d", data->pid);
    asprintf(&(argv[4]), "%d", data->shadow_register_fd[0]);    
    asprintf(&(argv[5]), "%d", 0);

    argv[6] = NULL;

    /* let it rip */
    return execve(
        data->shadow_exe,
        argv,
        NULL
    );
}

static int
setup_shadow_pipes(struct program_data * data)
{
    int status;

    /* create pipes */
    status = pipe(data->shadow_outfd);
    if (status != 0) {
        fprintf(stderr, "Could not create pipe: %s\n", strerror(errno));
        goto out_out;
    }

    status = pipe(data->shadow_errfd);
    if (status != 0) {
        fprintf(stderr, "Could not create pipe: %s\n", strerror(errno));
        goto out_err;
    }

    status = pipe(data->shadow_register_fd);
    if (status != 0){
        fprintf(stderr, "Could not create pipe: %s\n", strerror(errno));
        goto out_reg;
    }

    return 0;

out_reg:
    close(data->shadow_errfd[0]);
    close(data->shadow_errfd[1]);

out_err:
    close(data->shadow_outfd[0]);
    close(data->shadow_outfd[1]);

out_out:
    return status; 
}


static int
setup_shadow(struct program_data * data)
{
    int status;
    ssize_t bytes;

    /* create the pipes */
    status = setup_shadow_pipes(data);
    if (status != 0)
        return status;
   
    /* fork/exec the shadow */
    status = -1;
    switch (data->shadow_pid = fork()) {
        case -1:
            fprintf(stderr, "Failed to fork shadow process: %s\n", 
                strerror(errno));
            return -1;

        case 0:
            close(data->shadow_outfd[0]);
            close(data->shadow_errfd[0]);
            close(data->shadow_register_fd[1]);

            /* dup2() to re-direct stdout/stderr to invrun */
            status = dup2(data->shadow_outfd[1], STDOUT_FILENO);
            if (status < 0) {
                fprintf(stderr, "Failed to dup2() STDOUT_FILENO: %s\n", 
                    strerror(errno));
                return status;
            }

            status = dup2(data->shadow_errfd[1], STDERR_FILENO);
            if (status < 0) {
                fprintf(stderr, "Failed to dup2() STDERR_FILENO: %s\n", 
                    strerror(errno));
                return status;
            }

            /* jump into shadow */
            exit(exec_shadow(data, INVIRT_INVG_SHADOW));
            assert(1 == 0);

        default:
            close(data->shadow_outfd[1]);
            close(data->shadow_errfd[1]);
            close(data->shadow_register_fd[0]);
            break;
    }

    /* write register context to shadow via pipe */
    bytes = invirt_write_data_fd(data->shadow_register_fd[1], (const char *)&(data->regs),
        sizeof(struct user_regs_struct)
    );
    close(data->shadow_register_fd[1]);

    if (bytes != sizeof(struct user_regs_struct)) {
        fprintf(stderr, "Failed to write register context to shadow\n");
        return -1;
    }

    return 0;
}

static int
target(struct program_data * data)
{
    int status;

    /* allow self to be ptrace'd */
    status = ptrace(PTRACE_TRACEME, 0, NULL, NULL);
    if (status != 0) {
        fprintf(stderr, "Failed to mark self as PTRACE-able: %s\n", 
            strerror(errno));
        exit(-1);
    }

    /* let it rip */
    return execve(data->exe, data->argv, data->envp);
}

#if 0
static int
parse_text_boundaries(pid_t target_pid,
                      uint64_t * text_section_start,
                      uint64_t * text_section_end)
{
    int status;
    struct memory_map target_map;

    status = invirt_parse_memory_map(target_pid, &target_map);
    if (status != 0)
        return status;

    *text_section_start = target_map.segments[0].start;
    *text_section_end   = target_map.segments[0].end;
    return 0;
}

static int
ptrace_single_step(pid_t pid)
{
    int status, t_status;

    status = ptrace(PTRACE_SINGLESTEP, pid, 0, 0);
    if (status) {
        fprintf(stderr, "Failed to single step process: %s\n", strerror(errno));
        return status;
    }

    waitpid(pid, &t_status, 0);
    return t_status;
}

/* assumes 64-bit registers */
static void
ptrace_instruction_pointer(pid_t      pid, 
                           uint64_t * rip)
{
    struct user_regs_struct regs;

    ptrace(PTRACE_GETREGS, pid, NULL, (void *)&regs);
    *rip = regs.rip;
}

static int
stop_target_in_text_section(struct program_data * data)
{
    int t_status, status;
    uint64_t text_section_start, text_section_end;
    
    waitpid(data->pid, &t_status, 0);
    if (WIFEXITED(t_status)) {
        fprintf(stderr, "Target process exited with status %d\n", 
                WEXITSTATUS(t_status));
        return -1;
    }

    if (WIFSTOPPED(t_status)){
        status = parse_text_boundaries(data->pid, &text_section_start, 
                &text_section_end);
        if (status)
            return status;
    }
    
    while (WIFSTOPPED(t_status)) {
        uint64_t rip;

        ptrace_instruction_pointer(data->pid, &rip);
        if (rip >= text_section_start && rip <= text_section_end) {
            printf("Stopped in TEXT SECTION RIP=0x%lx\n", rip);
            data->entry_point = rip;
            break;
        }

        t_status = ptrace_single_step(data->pid);
    }

    return 0;    
}
#endif

/*
 * waitpid() will return once the tracee invokes exec
 */
static int
stop_target_at_entry_point(struct program_data * data)
{
    int t_status, status;

    waitpid(data->pid, &t_status, 0);
    if (WIFEXITED(t_status)) {
        fprintf(stderr, "Target process exited with status %d\n", 
                WEXITSTATUS(t_status));
        return -1;
    }

    if (!WIFSTOPPED(t_status)) {
        fprintf(stderr, "Target did not stop on exec?\n");
        return -1;
    }

    status = ptrace(PTRACE_GETREGS, data->pid, NULL, (void *)&(data->regs));
    if (status == -1){
        fprintf(stderr, "Could not read target user_regs_struct: %s\n",
                strerror(errno));
        return -1;            
    }

    out("Target (%s) stopped at entry point (RIP=0x%lx, RSP=0x%lx)\n"
           " FS_BASE: 0x%lx, FS: 0x%x, GS_BASE: 0x%lx, GS: 0x%x\n",
        data->exe,
        (uint64_t)data->regs.rip,
        (uint64_t)data->regs.rsp,
        (uint64_t)data->regs.fs_base, (uint32_t)data->regs.fs,
        (uint64_t)data->regs.gs_base, (uint32_t)data->regs.gs
    );

    return 0;
}

static int
setup_target_pipes(struct program_data * data)
{
    int status;

    /* create pipes */
    status = pipe(data->target_infd);
    if (status != 0) {
        fprintf(stderr, "Could not create pipe: %s\n", strerror(errno));
        goto out_in;
    }

    status = pipe(data->target_outfd);
    if (status != 0) {
        fprintf(stderr, "Could not create pipe: %s\n", strerror(errno));
        goto out_out;
    }

    status = pipe(data->target_errfd);
    if (status != 0) {
        fprintf(stderr, "Could not create pipe: %s\n", strerror(errno));
        goto out_err;
    }

out_err:
    if (status != 0) {
        close(data->target_outfd[0]);
        close(data->target_outfd[1]);
    }

out_out:
    if (status != 0) {
        close(data->target_infd[0]);
        close(data->target_infd[1]);
    }

out_in:
    return status; 
}


static int
setup_target(struct program_data * data)
{
    int status;

    /* create the pipes */
    status = setup_target_pipes(data);
    if (status != 0)
        return status;

    /* fork/exec the target */
    switch (data->pid = fork()) {
        case -1:
            fprintf(stderr, "Failed to fork shadow process: %s\n", 
                strerror(errno));
            return -1;

        case 0:
            close(data->target_infd[1]);
            close(data->target_outfd[0]);
            close(data->target_errfd[0]);

            /* dup2() to re-direct stdin/stdout/stderr to invrun */
            status = dup2(data->target_infd[0], STDIN_FILENO);
            if (status < 0) {
                fprintf(stderr, "Failed to dup2() STDIN_FILENO: %s\n", 
                    strerror(errno));
                return status;
            }

            status = dup2(data->target_outfd[1], STDOUT_FILENO);
            if (status < 0) {
                fprintf(stderr, "Failed to dup2() STDOUT_FILENO: %s\n", 
                    strerror(errno));
                return status;
            }

            status = dup2(data->target_errfd[1], STDERR_FILENO);
            if (status < 0) {
                fprintf(stderr, "Failed to dup2() STDERR_FILENO: %s\n", 
                    strerror(errno));
                return status;
            }

            close(data->target_infd[0]);
            close(data->target_outfd[1]);
            close(data->target_errfd[1]);

            /* run target application */
            exit(target(data));
            assert(1 == 0);

        default:
            close(data->target_infd[0]);
            close(data->target_outfd[1]);
            close(data->target_errfd[1]);
            break;
    }

    /* ptrace the child, installing a breakpoint at its entry point before
     * transfering control to the shadow
     */
    status = stop_target_at_entry_point(data);
    if (status != 0) {
        fprintf(stderr, "Failed to run target to its entry point\n");
        return 0;
    }

    return 0;
}

/*
 * Determine where the shadow's text segment should be initialized
 * We walk through the loaded target processes' address space to find 
 * a contiguous region that is aligned based on the given 'alignment' 
 * and which has a size of at least 'extent' bytes
 */
static int
derive_text_start(struct memory_map * target_map,
                  unsigned long       extent,
                  unsigned long       alignment,
                  unsigned long     * base_addr)
{
    int i;
    unsigned long last_end, start;

    last_end = MIN_TEXT_START;

    for (i = 0; i < target_map->nr_segments; i++) {
        struct memory_segment * seg = &(target_map->segments[i]);

        /* room between this and previous seg? */
        start = PAGE_ALIGN_UP(last_end, alignment);

        /* seg->start can actually be less than 'start' which was page aligned
         * up */
        if (seg->start < start)
            continue;

        if (seg->start - start >= extent) {
            *base_addr = start;
            return 0;
        }

        last_end = seg->end;
    }

    return -1;
}

static int
invrun_invoke_makefile(const char  * target_elf,
                       unsigned long text_start,
                       bool          prelink)
{
    int st;
    char * cmd;

    asprintf(&cmd, "make -s -f " INVIRT_TMP_DIR  "/Makefile");

    if (prelink)
        asprintf(&cmd, "INVIRT_TARGET_ELF=%s %s shadow_prelink", target_elf, 
            cmd);
    else
        asprintf(&cmd, "INVIRT_TARGET_ELF=%s INVIRT_TEXTSTART=%lx %s shadow",
            target_elf, text_start, cmd);

    st = system(cmd);
    free(cmd);

    if (st != 0)
        fprintf(stderr, "make failed: %s\n", strerror(errno));

    return st;
}

/*
 * This function is called by the parent process to determine information
 * needed to link the shadow executable
 */
static int
link_shadow(struct program_data * data)
{
    int status;
    struct memory_map target_map;
    unsigned long base_addr, size;
    char * tmp_elf = NULL;

    /* parse target memory map */
    status = invirt_parse_memory_map(data->pid, &target_map);
    if (status != 0)
        return status;

    asprintf(&tmp_elf, INVIRT_TMP_DIR "invg-shadow-prelink-%d", data->pid);

    /* compile the shadow to a temporary executable -- we need this to 
     * determine the size of its PT_LOAD'able segments
     */
    status = invrun_invoke_makefile(tmp_elf, 0, true);
    if (status != 0) {
        fprintf(stderr, "Could not prelink shadow\n");
        goto out;
    }

    /* determine size and extent of the shadow's own PT_LOAD segments */
    status = invirt_parse_loadable_elf_segments(
        tmp_elf,
        PAGE_SIZE,
        &base_addr,
        &size
    );

    if (status != 0) {
        fprintf(stderr, "Could not query PT_LOAD segments from prelink ELF\n");
        goto out;
    }

    /* find space in the target's address space to put this, plus some space
     * for the initial heap which is conventionally placed after the PT_LOAD
     * segments
     */
    size += INV_INIT_HEAP_SIZE;

    status = derive_text_start(
        &target_map, 
        size, 
        PAGE_SIZE_2MB,
        &(data->shadow_text_start)
    );
    if (status != 0) {
        fprintf(stderr, "Could not find free region in target memory map?\n");
        goto out;
    }

    /* finally, link the shadow */
    asprintf(&(data->shadow_exe), INVIRT_TMP_DIR "invg-shadow-%d", data->pid);

    status = invrun_invoke_makefile(data->shadow_exe, data->shadow_text_start, 
        false);
    if (status != 0) {
        fprintf(stderr, "Could not link shadow\n");

        free(data->shadow_exe);
        data->shadow_exe = NULL;

        goto out;
    }

out:
    if (tmp_elf) {
        unlink(tmp_elf);
        free(tmp_elf);
    }

    return status;
}



/*
 * Callback routines for handling event processing in the invrun process
 */

/*
 * data available on shadow's stdout 
 */
static int
handle_shadow_stdout(int    fd,
                     void * priv_data)
{
    char data[PAGE_SIZE];
    ssize_t bytes;
    size_t total_bytes;

    for (total_bytes = 0; ; total_bytes += bytes) {
        /* read out the data */
        bytes = invirt_read_data_fd(fd, data, PAGE_SIZE-1);
        if (bytes <= 0)
            return bytes;

        data[bytes] = '\0';

        printf("%s", data);
        fflush(stdout);
    }

    assert(1 == 0);
    return -1;
}

/*
 * data avilable on shadow's stderr
 */
static int
handle_shadow_stderr(int    fd,
                     void * priv_data)
{
    char data[PAGE_SIZE];
    ssize_t bytes;
    size_t total_bytes;

    for (total_bytes = 0; ; total_bytes += bytes) {
        /* read out the data */
        bytes = invirt_read_data_fd(fd, data, PAGE_SIZE-1);
        if (bytes <= 0)
            return bytes;

        data[bytes] = '\0';

        printf("%s", data);
        fflush(stdout);
    }

    assert(1 == 0);
    return -1;
}

/*
 * data available on target's stdout
 */
static int
handle_target_stdout(int    fd,
                     void * priv_data)
{
    char data[PAGE_SIZE];
    ssize_t bytes, bytes_written;
    size_t total_bytes;

    for (total_bytes = 0; ; total_bytes += bytes) {
        /* read out the data */
        bytes = invirt_read_data_fd(fd, data, PAGE_SIZE-1);
        if (bytes <= 0)
            return bytes;

        data[bytes] = '\0';
        bytes_written = invirt_write_data_fd(STDOUT_FILENO, data, bytes);
        assert(bytes_written == bytes);
    }

    assert(1 == 0);
    return -1;
}

/*
 * data available on target's stderr
 */
static int
handle_target_stderr(int    fd,
                     void * priv_data)
{
    char data[PAGE_SIZE];
    ssize_t bytes, bytes_written;
    size_t total_bytes;

    for (total_bytes = 0; ; total_bytes += bytes) {
        /* read out the data */
        bytes = invirt_read_data_fd(fd, data, PAGE_SIZE-1);
        if (bytes <= 0)
            return bytes;

        data[bytes] = '\0';
        bytes_written = invirt_write_data_fd(STDERR_FILENO, data, bytes);
        assert(bytes_written == bytes);
    }

    assert(1 == 0);
    return -1;
}

/*
 * data available on stdin 
 */
static int
handle_stdin(int    fd,
             void * priv_data)
{
    char data[PAGE_SIZE];
    ssize_t bytes, bytes_written;
    size_t total_bytes;

    struct program_data * p_data = (struct program_data *)priv_data;

    for (total_bytes = 0; ; total_bytes += bytes) {
        /* read out the data */
        bytes = invirt_read_data_fd(fd, data, PAGE_SIZE-1);
        if (bytes <= 0)
            return bytes;

        data[bytes] = '\0';
        bytes_written = invirt_write_data_fd(p_data->target_infd[1], data, bytes);
        assert(bytes_written == bytes);
    }

    assert(1 == 0);
    return -1;
}

/*
 * teardown pipes and other state associated with
 * the target
 */
static void
teardown_target(struct program_data * data)
{
    close(data->target_infd[1]);
    close(data->target_outfd[0]);
    close(data->target_errfd[0]);

    invirt_notifier_forget_fd(notifier, STDIN_FILENO);
    invirt_notifier_forget_fd(notifier, data->target_outfd[0]);
    invirt_notifier_forget_fd(notifier, data->target_errfd[0]);
}

/*
 * kill a running target process
 */
static void
kill_and_teardown_target(struct program_data * data)
{
    kill(data->pid, SIGKILL);
    waitpid(data->pid, NULL, 0);

    teardown_target(data);
}

/*
 * teardown pipes and other state associated with
 * the shadow
 */
static void
teardown_shadow(struct program_data * data)
{
    close(data->shadow_outfd[0]);
    close(data->shadow_errfd[0]);

    invirt_notifier_forget_fd(notifier, data->shadow_outfd[0]);
    invirt_notifier_forget_fd(notifier, data->shadow_errfd[0]);

    unlink(data->shadow_exe);
}

/*
 * kill a running shadow process
 */
static void
kill_and_teardown_shadow(struct program_data * data)
{
    kill(data->shadow_pid, SIGTERM);
    waitpid(data->shadow_pid, NULL, 0);

    teardown_shadow(data);
}


/*
 * we received sigchld 
 */
static int
handle_sigchld(int    fd,
               void * priv_data)
{
    struct program_data * p_data = (struct program_data *)priv_data;
    pid_t pid;
    char byte;
    int ex_status;

    /* consume byte from self pipe */
    assert(read(fd, &byte, sizeof(char)) == sizeof(char));
    assert(byte == 'c');

    /* see if target is down */
    pid = waitpid(p_data->pid, &ex_status, WNOHANG);
    if (pid == p_data->pid) {
        if (WIFEXITED(ex_status)) {
            out("target exited with status %d\n", WEXITSTATUS(ex_status));

            teardown_target(p_data);
            terminate = true;
        }

        p_data->pid = INV_PID_TERMINATED;
    }

    /* see if shadow is down */
    pid = waitpid(p_data->shadow_pid, &ex_status, WNOHANG);
    if (pid == p_data->shadow_pid) {

        if (WIFEXITED(ex_status)) {
            if (p_data->pid != INV_PID_TERMINATED)
                /*
                fprintf(stderr, "Shadow exited while target still running "
                    " -- tearing down target\n");
                */

            teardown_shadow(p_data);
            terminate = true;
        }

        p_data->shadow_pid = INV_PID_TERMINATED;
    }

    return 0;
}

/*
 * signal handlers 
 */
static void
handle_sigchld_signal(int         sig,
                      siginfo_t * siginfo,
                      void      * ucontext)
{
    /* write to self pipe to defer the heavy processing we can't do here */
    assert(write(sigchld_pipe[1], "c", sizeof(char)) == sizeof(char));
}

static void
handle_sigint_signal(int sig)
{
    terminate = true;
}

static int
mark_fd_nonblocking(int fd)
{
    int flags = fcntl(fd, F_GETFL);
    if (!(flags & O_NONBLOCK))
        return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
    return 0;
}

static int
mark_fds_nonblocking(struct program_data * data)
{
    return (
        (mark_fd_nonblocking(data->shadow_outfd[0])       != 0) ||
        (mark_fd_nonblocking(data->shadow_errfd[0])       != 0) ||
        (mark_fd_nonblocking(data->target_outfd[0])       != 0) ||
        (mark_fd_nonblocking(data->target_errfd[0])       != 0) ||
        (mark_fd_nonblocking(STDIN_FILENO)                != 0) ||
        (mark_fd_nonblocking(sigchld_pipe[0])             != 0)
    );

}
static int
monitor_fds(struct program_data * data)
{
    int status;

    /* first, mark our read-side pipe fds as non-blocking */
    status = mark_fds_nonblocking(data);
    if (status != 0) {
        fprintf(stderr, "Failed to mark file descriptors with O_NONBLOCK\n");
        return -1;
    }
    
    status = invirt_notifier_remember_fd(notifier, data->shadow_outfd[0], 
            handle_shadow_stdout, data);
    if (status != 0) {
        fprintf(stderr, "Failed to track fd\n");
        goto out_1;
    }
    
    status = invirt_notifier_remember_fd(notifier, data->shadow_errfd[0], 
            handle_shadow_stderr, data);
    if (status != 0) {
        fprintf(stderr, "Failed to track fd\n");
        goto out_2;
    }

    status = invirt_notifier_remember_fd(notifier, data->target_outfd[0], 
            handle_target_stdout, data);
    if (status != 0) {
        fprintf(stderr, "Failed to track fd\n");
        goto out_3;
    }

    status = invirt_notifier_remember_fd(notifier, data->target_errfd[0], 
            handle_target_stderr, data);
    if (status != 0) {
        fprintf(stderr, "Failed to track fd\n");
        goto out_4;
    }

    status = invirt_notifier_remember_fd(notifier, STDIN_FILENO, 
            handle_stdin, data);
    if (status != 0) {
        fprintf(stderr, "Failed to track fd\n");
        goto out_5;
    }

    status = invirt_notifier_remember_fd(notifier, sigchld_pipe[0], 
            handle_sigchld, data);
    if (status != 0) {
        fprintf(stderr, "Failed to track fd\n");
        goto out_6;
    }

    return 0;
    
out_6:
    invirt_notifier_forget_fd(notifier, STDIN_FILENO);

out_5:
    invirt_notifier_forget_fd(notifier, data->target_errfd[0]);

out_4:
    invirt_notifier_forget_fd(notifier, data->target_outfd[0]);

out_3:
    invirt_notifier_forget_fd(notifier, data->shadow_errfd[0]);

out_2:
    invirt_notifier_forget_fd(notifier, data->shadow_outfd[0]);

out_1:
    return -1;
}

static int
setup_sigchld_handler(void)
{
    struct sigaction sa;
    int status;

    /* first, setup a self pipe */
    status = pipe(sigchld_pipe);
    if (status != 0) {
        fprintf(stderr, "Failed to create pipe: %s\n", strerror(errno));
        return -1;
    }

    sa.sa_sigaction = &handle_sigchld_signal;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_NOCLDSTOP + SA_SIGINFO;

    status = sigaction(SIGCHLD, &sa, NULL);
    if (status != 0) {
        fprintf(stderr, "sigaction failed: %s\n", strerror(errno));

        close(sigchld_pipe[0]);
        close(sigchld_pipe[1]);

        return -1;
    }

    return 0;
}

static int
setup_sigint_handler(void)
{
    struct sigaction sa;
    int status;

    sa.sa_handler = &handle_sigint_signal;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_NOCLDSTOP;

    status = sigaction(SIGINT, &sa, NULL);
    if (status != 0) {
        fprintf(stderr, "sigaction failed: %s\n", strerror(errno));
        return -1;
    }

    return 0;
}

static void
usage(FILE  * out_stream,
      char ** argv)
{
    fprintf(out_stream, 
        "Usage: %s [OPTIONS] <exe> <arguments ...>\n"
        "    -h (--help)        : print help and exit\n"
        "    -x (--exclude-env) : do not inherit current environment in"
                " target process\n",
        *argv
    );
    fflush(out_stream);
}

static int
setup_env(struct program_data * data,
          char               ** envp)
{
    char ** new_envp;
    int i, envc;
    
    /* determine number of entries in new envp */
    if (envp == NULL) {
        envc = 1;
    } else {
        for (i = 0; ; i++) {
            if (envp[i] == NULL)
                break;
        }

        envc = i + 1;
    }

    new_envp = malloc(sizeof(char *) * (envc + 1));
    if (new_envp == NULL) {
        fprintf(stderr, "Could not allocate envp: %s\n", strerror(errno));
        return -1;
    }

    /* copy in source env, if requested */
    for (i = 0; i < envc - 1; i++) {
        asprintf(&(new_envp[i]), "%s", envp[i]);
    }

    /* 
     * Add in LD_BIND_NOW to ensure dynamically linked libraries are pulled in
     * at program startup time, to force as much overhead forward as possible
     */
    asprintf(&(new_envp[i]), "LD_BIND_NOW=1");

    /* add null truncator */
    new_envp[i+1] = NULL;

    data->envp = new_envp;
    return 0;
}

static int
parse_cmd_line(int                   argc,
               char               ** argv,
               char               ** envp,
               struct program_data * data)
{

    int status, opt_off, opt_index, i;
    bool inherit_env = true;

    struct option long_options[] =
    {
        {"help",        no_argument,        0,  'h'},
        {"exclude-env", no_argument,        0,  'x'},
        {0,             0,                  0,  0}
    };

    opterr = 0; // quell error messages

    memset(data, 0, sizeof(struct program_data));

    while (1) {
        int c;
        
        c = getopt_long_only(argc, argv, "+hx", long_options, &opt_index);
        if (c == -1)
            break;

        switch (c) {
            case 'h':
                usage(stdout, argv);
                exit(EXIT_SUCCESS);

            case 'x':
                inherit_env = false;
                break;

            case '?':
            default:
                fprintf(stderr, "Error: invalid command line option\n");
                usage(stderr, argv);
                return -1;
        }
    }

    /* optind is the location in argv of the first non-option argument */
    opt_off = optind;
    if ((argc - opt_off) < 1) {
        usage(stderr, argv);
        return -1;
    }


    /* setup argc/argv/envp */
    {
        data->this_exe = argv[0];
        data->exe = argv[opt_off];
        data->argc = argc - opt_off;

        assert(data->argc >= 1);

        data->argv = malloc(sizeof(char *) * (data->argc + 1));
        if (data->argv == NULL) {
            fprintf(stderr, "Out of memory\n");
            data->argc = 0;
            return -1;
        }

        for (i = 0; opt_off < argc; i++, opt_off++) {
            data->argv[i] = argv[opt_off];
        }
        data->argv[i] = NULL;

        status = setup_env(data, (inherit_env) ? envp : NULL);
        if (status != 0) {
            fprintf(stderr, "Error: could not initialize target environment\n");
            free(data->argv);
            data->argv = NULL;
            data->argc = 0;
            return -1;
        }
    }

    return 0;
}

int
main(int     argc,
     char ** argv,
     char ** envp)
{
    int status;
    struct program_data * data;

    invirt_init_context("invrun");

    data = malloc(sizeof(struct program_data));
    if (data == NULL) {
        fprintf(stderr, "Out of memory\n");
        exit(EXIT_FAILURE);
    }

    memset(data, 0, sizeof(struct program_data));
    data->shadow_pid = INV_PID_TERMINATED;
    data->pid = INV_PID_TERMINATED;

    /* parse cmd line to generate program data */
    status = parse_cmd_line(argc, argv, envp, data);
    if (status != 0)
        goto out;

    /* start the target and intercept it at its entry point */
    status = setup_target(data);
    if (status != 0) {
        fprintf(stderr, "Could not setup target process\n");
        goto out;
    }

    /* link the shadow */
    status = link_shadow(data);
    if (status != 0) {
        fprintf(stderr, "Could not link shadow program\n");
        goto out;
    }

    /* start the guest shadow */
    status = setup_shadow(data);
    if (status != 0) {
        fprintf(stderr, "Could not setup shadow process\n");
        goto out;
    }

    /* do some other miscellaneous post-processing */
    {
        /* create invirt notifier */
        notifier = invirt_notifier_create();
        if (notifier == NULL) {
            fprintf(stderr, "Could not create notifier\n");
            goto out;
        }

        /* catch SICHLD */
        status = setup_sigchld_handler();
        if (status != 0) {
            fprintf(stderr, "Could not setup SIGCHLD handler\n");
            goto out;
        }

        /* catch SIGINT */
        status = setup_sigint_handler();
        if (status != 0) {
            fprintf(stderr, "Could not setup SIGINT handler\n");
            goto out;
        }

        /* track various fds that signal events */
        status = monitor_fds(data);
        if (status != 0) {
            fprintf(stderr, "Could not monitor file descriptors\n");
            goto out;
        }
    }

    /* core event processing loop */
    terminate = false;
    while (!terminate) {
        struct fd_handler * handler;
        int i, active_fds, max_fd;
        fd_set rdset;

        invirt_notifier_get_fd_info(notifier, &rdset, &max_fd);
        active_fds = select(max_fd + 1, &rdset, NULL, NULL, NULL);
        if (active_fds < 0) {
            if (errno == EINTR)
                continue;

            fprintf(stderr, "Failed to select: %s\n", strerror(errno));
            status = -1;
            break;
        }

        for (i = 0; i <= max_fd && active_fds > 0; i++) {
            if (FD_ISSET(i, &rdset)) {
                --active_fds;

                handler = invirt_notifier_handler_of_fd(notifier, i);
                assert(handler != NULL);
                assert(handler->fd == i);

                status = handler->fn(i, handler->priv_data);
                if (status != 0)
                    fprintf(stderr, "Error in fd handler (fd=%d)\n", i);

                /* if we handled a sigchld, don't keep processing */
                if (i == sigchld_pipe[0]) {
                    terminate = true;
                    break;
                }
            }
        }
    }

out:
    /* teardown target/shadow if either is still running */
    if (data->pid != INV_PID_TERMINATED)
        kill_and_teardown_target(data);

    if (data->shadow_pid != INV_PID_TERMINATED)
        kill_and_teardown_shadow(data);

    exit((status == 0) ? EXIT_SUCCESS : EXIT_FAILURE);
}
