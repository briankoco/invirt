/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

/*
 * miscellaneous utilities
 */


#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <invirt.h>

#include "utils.h"
#include "hashtable.h"

char * inv_context;
FILE * inv_out_fp = NULL;
FILE * inv_err_fp = NULL;

static void
ensure_tmp(void)
{
    const char * path = "/tmp/invirt";
    int ret;

    ret = mkdir(path, 0777);
    if (ret != 0) {
        assert(errno == EEXIST);
    }
}

static void
init_out_stream(void)
{
    char out_path[64];

    ensure_tmp();
    snprintf(out_path, 64, "/tmp/invirt/%s.stdout", inv_context);
    inv_out_fp = fopen(out_path, "w");
    assert(inv_out_fp != NULL);
}

static void
init_err_stream(void)
{
    char err_path[64];

    ensure_tmp();
    snprintf(err_path, 64, "/tmp/invirt/%s.stderr", inv_context);
    inv_err_fp = fopen(err_path, "w");
    assert(inv_err_fp != NULL);
}

void
invirt_init_context(const char * context)
{
    asprintf(&inv_context, "%s", context);
    init_out_stream();
    init_err_stream();
}

/*
 * utility to write 'data_len' bytes to an fd
 */
ssize_t
invirt_write_data_fd(int          fd,
                     const char * data,
                     size_t       data_len)
{
    size_t bytes_written;
    ssize_t cur_bytes;

    for (bytes_written = 0; bytes_written < data_len; bytes_written += cur_bytes) {
        cur_bytes = write(fd, data+bytes_written, data_len-bytes_written);
        
        if (cur_bytes < 0) {
            if (errno == EINTR) {
                cur_bytes = 0;
                continue;
            }

            if (errno == EAGAIN || errno == EWOULDBLOCK)
                break;
            
            err("failed to write to fd: %s\n", strerror(errno));
            return -1;
        }

        if (cur_bytes == 0)
            break;
    }

    return bytes_written;
}


/*
 * utility to read up to 'data_len' bytes from an fd 
 */
ssize_t
invirt_read_data_fd(int     fd,
                    char  * data,
                    size_t  data_len)
{
    size_t bytes_read;
    ssize_t cur_bytes;

    for (bytes_read = 0; bytes_read < data_len; bytes_read += cur_bytes) {
        cur_bytes = read(fd, data+bytes_read, data_len-bytes_read);
        
        if (cur_bytes < 0) {
            if (errno == EINTR) {
                cur_bytes = 0;
                continue;
            }

            if (errno == EAGAIN || errno == EWOULDBLOCK)
                break;
            
            err("failed to read from fd: %s\n", strerror(errno));
            return -1;
        }

        if (cur_bytes == 0)
            break;
    }

    return bytes_read;
}

/*
 * utility to query our system-uuid via proc filesystem
 */
static int
__invirt_query_uuid_procfs(char * uuid_str,
                           size_t uuid_str_len)
{
    FILE * fp;

    fp = fopen(INVIRT_PROC_PATH_UUID, "r");
    if (fp == NULL) {
        err("failed to open " INVIRT_PROC_PATH_UUID ": %s\n",  strerror(errno));
        return -1;
    }

    fgets(uuid_str, uuid_str_len, fp);

    fclose(fp);
    return 0;
}

int
invirt_query_uuid(char * uuid_str,
                  size_t uuid_str_len)
{
    return __invirt_query_uuid_procfs(uuid_str, uuid_str_len);
}

pid_t
invirt_query_kvm_pid(const char * uuid_str)
{
    DIR * dir;
    char * cmdline;
    pid_t pid = -1;

    /* read cmd line */
    cmdline = malloc(4096);
    if (cmdline == NULL) {
        err("out of memory\n");
        return -1;
    }
    cmdline[4095] = '\0';

    dir = opendir("/proc");
    if (dir == NULL) {
        err("failed to open /proc: %s\n", strerror(errno));
        free(cmdline);
        return -1;
    }

    while (pid == -1) {
        struct dirent * ent;
        FILE * fp;
        int fd, bytes;
        char comm[512], * c;

        ent = readdir(dir);
        if (ent == NULL)
            break;

        if (ent->d_type != DT_DIR)
            continue;

        /* read comm */
        snprintf(comm, 501, "/proc/%s/comm", ent->d_name);
        fp = fopen(comm, "r");
        if (fp == NULL) {
            continue;
        }

        fscanf(fp, "%511s", comm);
        fclose(fp);

        if (strcmp(comm, "qemu-system-x86") != 0)
            continue;

        snprintf(comm, 501, "/proc/%s/cmdline", ent->d_name);
        fd = open(comm, O_RDONLY);
        if (fd < 0) {
            err("failed to open /proc/%s/cmdline: %s\n", ent->d_name, strerror(errno));
            break;
        }

        bytes = read(fd, cmdline, 4096);

        /* read cmdline */
        for (c = cmdline; c < cmdline+bytes && pid == -1;) {
            if (strcmp(c, "-uuid") == 0) {
                while (*c++);
                /* uuid is 8-4-4-4-12 chars total, 24 numbers plus 4 dashes == 28 characters */
                if (strncmp(uuid_str, c, 28) == 0) {
                    pid = atoi(ent->d_name);
                    break;
                }
            }

            /* go to next string */
            while (*c++);
        }
    }

    closedir(dir);
    free(cmdline);

    return pid;
}

/*
 * fd notifier handling
 */
struct notifier_internal {
    struct hashtable * fd_to_handler_table;
    fd_set fdset;
    int max_fd;
};

static unsigned int
__fd_hash_fn(uintptr_t key)
{
    return invirt_hash_ptr(key);
}

static int
__fd_eq_fn(uintptr_t key1,
           uintptr_t key2)
{
    return (key1 == key2);
}

static void
__recalculate_max_fd(invirt_notifier_t * notifier)
{
    int i;

    for (i = notifier->max_fd - 1; i >= 0; i--) {
        if (FD_ISSET(i, &(notifier->fdset))) {
            notifier->max_fd = i;
            return;
        }
    }

    /* no fds left */
    notifier->max_fd = 0;
}

invirt_notifier_t * 
invirt_notifier_create(void)
{
    struct notifier_internal * notifier;

    notifier = malloc(sizeof(struct notifier_internal));
    if (notifier == NULL)
        return NULL;

    notifier->fd_to_handler_table = invirt_create_htable(
        0,
        __fd_hash_fn,
        __fd_eq_fn
    );
    if (notifier->fd_to_handler_table == NULL) {
        free(notifier);
        return NULL;
    }

    FD_ZERO(&(notifier->fdset));
    notifier->max_fd = 0;
    
    return notifier;
}

void
invirt_notifier_destroy(invirt_notifier_t * notifier)
{
    invirt_free_htable(notifier->fd_to_handler_table, 1, 0); 
    free(notifier);
}

int
invirt_notifier_remember_fd(invirt_notifier_t * notifier,
                            int                 fd,
                            fd_handler_fn       fn,
                            void              * priv_data)
{
    struct fd_handler * handler;
    int status;

    status = invirt_htable_search(notifier->fd_to_handler_table, fd);
    if (status != 0) {
        err("invirt_notifier: already monitoring fd %d", fd);
        return -1;
    }

    handler = malloc(sizeof(struct fd_handler));
    if (handler == NULL)
        return -1;

    handler->fd = fd;
    handler->fn = fn;
    handler->priv_data = priv_data;

    status = invirt_htable_insert(
        notifier->fd_to_handler_table, 
        (uintptr_t)fd, 
        (uintptr_t)handler
    );
    if (status == 0) {
        err("internal htable error\n");
        free(handler);
        return -1;
    }

    FD_SET(fd, &(notifier->fdset));

    if (fd > notifier->max_fd)
        notifier->max_fd = fd;

    return 0;
}

int
invirt_notifier_forget_fd(invirt_notifier_t * notifier,
                          int                 fd)
{
    struct fd_handler * handler;

    handler = (struct fd_handler *)invirt_htable_remove(
        notifier->fd_to_handler_table, 
        (uintptr_t)fd, 
        0
    );
    if (handler == NULL)
        return -1;

    FD_CLR(fd, &(notifier->fdset));
    free(handler);

    if (fd == notifier->max_fd)
        __recalculate_max_fd(notifier);

    return 0;
}


struct fd_handler *
invirt_notifier_handler_of_fd(invirt_notifier_t * notifier,
                              int                 fd)
{

    return (struct fd_handler *)invirt_htable_search(
        notifier->fd_to_handler_table, 
        fd
    );
}

void
invirt_notifier_get_fd_info(invirt_notifier_t * notifier,
                            fd_set            * set,
                            int               * max_fd)
{
    memcpy(set, &(notifier->fdset), sizeof(fd_set));
    *max_fd = notifier->max_fd;
}
