/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

/*
 * invd is a host-side daemon process responsible for bootstrapping
 * the host-side execution of invirt programs
 */


#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <signal.h>
#include <unistd.h>
#include <stdarg.h>
#include <getopt.h>
#include <fcntl.h>

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <invirt.h>
#include <invd.h>

#include "shadow.h"
#include "hashtable.h"
#include "utils.h"

#define __invd_out(stream, fmt, args...)\
    do {\
        fprintf(stream, "invd: " fmt, ##args);\
        fflush(stream);\
    } while (0)

#define invd_out(fmt, args...) __invd_out(stdout, fmt, ##args)
#define invd_err(fmt, args...) __invd_out(stderr, fmt, ##args)


#define IP_ADDR_LEN     32
#define HOST_DBG_VMID   65535
#define TGID_NONE       -1

/*
 * flags governing how invd thread should complete
 * processing of incoming socket commands
 */
#define INVD_FLAG_CLOSE_FD  0x1
#define INVD_FLAG_WRITE_RC  0x2

#define INVD_FLAGS_NORMAL       (INVD_FLAG_CLOSE_FD | INVD_FLAG_WRITE_RC)
#define INVD_FLAGS_NEW_TARGET   INVD_FLAG_WRITE_RC


/* program state we maintain for locally created shadows */
struct program_data {
    struct user_regs_struct regs; /* register contents to preload */
    char * invh_exe; /* host exe */
    pid_t  invh_tgid; /* host tgid */
    pid_t  target_tgid; /* guest target tgid */
    pid_t  tracer_tgid; /* guest tracer tgid */
    char * target_uuid; /* guest uuid */
    int    conn_fd; /* socket fd to guest shadow */
};


/* globals */
static invirt_notifier_t * invd_notifier;
static int invd_sigchld_pipe[2];
static char invd_ip_addr[IP_ADDR_LEN];
static int invd_ip_port;
static int invd_do_exit = 0;

/* map tgid to program_data structure */
static struct hashtable * tgid_to_program_table;

static void
usage(FILE  * out_stream,
      char ** argv)
{
    fprintf(out_stream,
        "Usage: %s [OPTIONS]\n"
        "\t -h (--help)  : print help and exit\n"
        "\t -p (--port=) : listen on port (default: %d)\n",
        *argv, INVIRT_INVD_IP_PORT
    );
    fflush(out_stream);
}

static int
parse_cmd_line(int     argc,
               char ** argv)
{
    int opt_off;

    opterr = 0; // quell error messages

    invd_ip_port = INVIRT_INVD_IP_PORT;
    strncpy(invd_ip_addr, INVIRT_INVD_IP_ADDR, IP_ADDR_LEN);

    while (1) {
        int c, option_index;
        struct option long_options[] =
        {
            {"help",    no_argument,        0,  'h'},
            {"ip",      required_argument,  0,  'i'},
            {"port",    required_argument,  0,  'p'},
            {0, 0, 0, 0},
        };

        c = getopt_long_only(argc, argv, "+hi:p:", long_options, &option_index);

        if (c == -1)
            break;

        switch (c) {
            case 'h':
                usage(stdout, argv);
                exit(EXIT_SUCCESS);

            case 'i':
                strncpy(invd_ip_addr, optarg, IP_ADDR_LEN);
                break;

            case 'p':
                invd_ip_port = atoi(optarg);
                break;

            case '?':
            default:
                invd_err("invalid command line option\n");
                usage(stderr, argv);
                return -1;
        }
    }

    /* optind is the location in argv of the first non-option argument */
    opt_off = optind;
    if ((argc - opt_off) != 0) {
        usage(stderr, argv);
        return -1;
    }

    return 0;
}

static uint32_t
invd_int_hash_fn(uintptr_t key)
{
    return invirt_hash_ptr(key);
}

static int
invd_int_eq_fn(uintptr_t key1,
              uintptr_t key2)
{
    return (key1 == key2);
}

#define invd_tgid_hash_fn   invd_int_hash_fn
#define invd_tgid_eq_fn     invd_int_eq_fn

static int
__remember_tgid(pid_t                 tgid,
                struct program_data * data)
{
    int status;

    status = invirt_htable_search(tgid_to_program_table, tgid);
    if (status != 0)
        return -1;

    status = invirt_htable_insert(tgid_to_program_table, (uintptr_t)tgid,
            (uintptr_t)data);
    if (status == 0)
        return -1;

    return 0;
}

static int
__forget_tgid(pid_t tgid)
{
    struct program_data * data;
    int status;

    data = (struct program_data *)invirt_htable_remove(
        tgid_to_program_table,
        (uintptr_t)tgid,
        0
    );

    if (data) {
        free(data);
        status = 0;
    } else {
        status = 1;
    }

    return status;
}

static struct program_data *
__program_data_of_tgid(pid_t tgid)
{
    return (struct program_data *)invirt_htable_search(
        tgid_to_program_table,
        (uintptr_t)tgid
    );
}

static void
handle_sigint_sigterm_signal(int sig)
{
    invd_do_exit = 1;
    invd_out("Got %s -- setting invd_do_exit\n",
        (sig == SIGINT) ? "SIGINT" : "SIGTERM"
    );
}

static int
handle_sigchld_fd(int    fd,
                  void * priv_data)
{
    struct program_data * data;
    ssize_t bytes;
    pid_t pid;
    int exit_st;

    assert(fd == invd_sigchld_pipe[0]);

    bytes = invirt_read_data_fd(
        fd,
        (char *)&pid,
        sizeof(pid_t)
    );
    if (bytes != sizeof(pid_t)) {
        invd_err("could not read pid from self pipe!!\n");
        return -1;
    }

    waitpid(pid, &exit_st, WNOHANG);
    if (WIFEXITED(exit_st)) {
        invd_out("tgid %d exited with status %d\n", pid,
            WEXITSTATUS(exit_st));
    } else if (WIFSIGNALED(exit_st)) {
        invd_out("tgid %d killed on signal %d (%s)\n", pid,
            WTERMSIG(exit_st), strsignal(WTERMSIG(exit_st)));
    } else {
        invd_out("tgid %d eited for unknown reason\n", pid);
    }

    data = __program_data_of_tgid(pid);

#if 0
    /* HACK: complete invd request */
    {
        struct invd_request req;

        req.msg_type = INVD_NEW_TARGET;
        req.rc = INVD_OK;

        invirt_write_data_fd(
            data->conn_fd,
            (const char *)&req,
            sizeof(struct invd_request)
        );
    }
#endif
    unlink(data->invh_exe);

    /* notify the guest that this process is donezo */
    close(data->conn_fd);

    assert(__forget_tgid(pid) == 0);

    return 0;
}

static void
handle_sigchld_signal(int         sig,
                      siginfo_t * siginfo,
                      void      * ucontext)
{
    ssize_t bytes;

    /*
     * write to self pipe to defer heavy processing that we
     * can't do in signal context
     */
    bytes = invirt_write_data_fd(
        invd_sigchld_pipe[1],
        (const char *)&(siginfo->si_pid),
        sizeof(pid_t)
    );
    if (bytes != sizeof(pid_t))
        invd_err("could not write pid to self pipe!!\n");
}

static int
__install_signal_handler(int    signum,
                         bool   siginfo,
                         void * handler)
{
    struct sigaction sa;
    int status;

    sigemptyset(&sa.sa_mask);

    if (siginfo) {
        sa.sa_flags = SA_NOCLDSTOP | SA_SIGINFO;
        sa.sa_sigaction = (void (*)(int, siginfo_t *, void *))handler;
    } else {
        sa.sa_flags = SA_NOCLDSTOP;
        sa.sa_handler = (void (*)(int))handler;
    }

    status = sigaction(signum, &sa, NULL);
    if (status != 0) {
        invd_err("sigaction failed: %s\n", strerror(errno));
        return status;
    }

    return 0;
}

static int
install_sigint_handler(void)
{
    return __install_signal_handler(
        SIGINT,
        false,
        (void *)handle_sigint_sigterm_signal
    );
}

static int
install_sigterm_handler(void)
{
    return __install_signal_handler(
        SIGTERM,
        false,
        (void *)handle_sigint_sigterm_signal
    );
}

static int
install_sigchld_handler(void)
{
    return __install_signal_handler(
        SIGCHLD,
        true,
        (void *)handle_sigchld_signal
    );
}

static int
install_signal_handlers(void)
{
    return (
        install_sigint_handler() ||
        install_sigterm_handler() ||
        install_sigchld_handler()
    );
}

static inline char *
invd_msg_type_to_str(invd_msg_t msg_type)
{
    switch (msg_type) {
        /*
        case INVD_NEW_VM: return "INVD_NEW_VM";
        case INVD_DEL_VM: return "INVD_DEL_VM";
        case INVD_GET_VMID: return "INVD_GET_VMID";
        */
        case INVD_NEW_TARGET: return "INVD_NEW_TARGET";
        case INVD_DEL_TARGET: return "INVD_DEL_TARGET";
        default: return "INVD_UNKOWN_TYPE";
    }
}

static inline char *
invd_rc_to_str(int rc)
{
    switch (rc) {
        case INVD_OK: return "INVD_OK";
        case INVD_ERR: return "INVD_ERR";
        case INVD_ERR_BAD_TYPE: return "INVD_ERR_BAD_TYPE";
        default: return "INVD_UNKNOWN_RC";
    }
}

static int
__exec_shadow(struct program_data * data,
              int                   register_fd)
{
    char * argv[7];

    /* setup argv */
    asprintf(&(argv[0]), "%s", data->invh_exe);
    asprintf(&(argv[1]), "%d", INVIRT_INVH_SHADOW);
    asprintf(&(argv[2]), "%s", data->target_uuid);
    asprintf(&(argv[3]), "%d", data->tracer_tgid);
    asprintf(&(argv[4]), "%d", register_fd);
    asprintf(&(argv[5]), "%d", data->conn_fd);

    argv[6] = NULL;

    /* let it rip */
    return execve(
        data->invh_exe,
        argv,
        NULL
    );
}

/*
 * setup the host shadow
 */
static int
__setup_shadow(struct program_data * data)
{
    /* TODO:
     * (1) fork
     *
     * (2) a. child:
     *      i. invh module initialization
     *          - register tgid with system call interception
     *          - pass socket information to kernel
     *     ii. write return code to the parent, which will
     *         complete the NEW_TARGET command
     *    iii. load target address space and jmp to entry point
     *
     *     b. parent:
     *      i. wait for return code from child (also setup SIGCHLD)
     *     ii. on success, setup data structures to track it
     *    iii. write RC to invrun shadow
     */

    int status, pipefd[2];
    ssize_t bytes;

    status = pipe(pipefd);
    if (status != 0) {
        invd_err("failed to create pipe: %s\n", strerror(errno));
        return -1;
    }

    status = -1;
    switch (data->invh_tgid = fork()) {
         case -1:
            invd_err("failed to fork shadow process: %s\n",
                    strerror(errno));
            return -1;

        case 0:
            close(pipefd[1]);
            exit(__exec_shadow(data, pipefd[0]));
            assert(1 == 0);

        default:
            close(pipefd[0]);
            break;
    }

    bytes = invirt_write_data_fd(
        pipefd[1],
        (const char *)&(data->regs),
        sizeof(struct user_regs_struct)
    );
    if (bytes != sizeof(struct user_regs_struct))
        invd_err("failed to write user register context\n");

    status = __remember_tgid(data->invh_tgid, data);
    if (status != 0) {
        invd_err("failed to update tgid hashtable\n");
        /* TODO: kill child */
        return -1;
    }

    return 0;
}

/*
 * New invirt target has been spawned in the VM
 *
 * The VM's context uuid is 'uuid_str' and the guest target tgid is 'vm_target_tgid'
 *
 * We will fork off a new process to execute the host shadow context for this
 * target
 */
static int
__handle_request_new_target(const char              * uuid_str,
                            pid_t                     vm_target_tgid,
                            pid_t                     vm_tracer_tgid,
                            struct user_regs_struct * regs,
                            const char              * payload,
                            size_t                    payload_len,
                            int                       new_conn_fd)
{
    ssize_t bytes;
    int shadow_fd, status;
    char * tmp_elf = NULL;
    struct program_data * data;

    data = malloc(sizeof(struct program_data));
    if (data == NULL) {
        invd_err("failed to allocate program_data: %s\n", strerror(errno));
        return -1;
    }

    memcpy(&(data->regs), regs, sizeof(struct user_regs_struct));
    asprintf(&(data->invh_exe), INVIRT_TMP_DIR "invh-shadow-%d", vm_target_tgid);
    asprintf(&(data->target_uuid), "%s", uuid_str);
    data->invh_tgid = TGID_NONE;
    data->target_tgid = vm_target_tgid;
    data->tracer_tgid = vm_tracer_tgid;
    data->conn_fd = new_conn_fd;

    shadow_fd = open(data->invh_exe, O_WRONLY | O_CREAT, 0775);
    if (shadow_fd == -1) {
        invd_err("could not create %s: %s\n", tmp_elf, strerror(errno));
        goto err_free;
    }

    status = ftruncate(shadow_fd, payload_len);
    if (status != 0) {
        invd_err("failed to truncate %s: %s\n", tmp_elf, strerror(errno));
        goto err;
    }

    bytes = invirt_write_data_fd(shadow_fd, payload, payload_len);
    if (bytes != payload_len) {
        invd_err("failed to write payload to %s: %s\n", tmp_elf, strerror(errno));
        goto err;
    }

    free(tmp_elf);
    close(shadow_fd);

    status = __setup_shadow(data);
    if (status != 0)
        goto err;

    invd_out("created invh shadow for target:\n"
             "  uuid:        %s\n"
             "  target tgid: %d\n"
             "  tracer tgid: %d\n",
        uuid_str, vm_target_tgid, vm_tracer_tgid
    );

    return INVD_OK;

err:
    close(shadow_fd);
    unlink(tmp_elf);
err_free:
    free(data->invh_exe);
    free(data);

    return INVD_ERR;
}

static int
__handle_request_del_target(const char * uuid_str,
                            pid_t        vm_target_tgid)
{
    return INVD_OK;
}

static unsigned long
__handle_request(struct invd_request * req,
                 const char          * payload,
                 size_t                payload_len,
                 int                   new_conn_fd)
{
    unsigned long flags = INVD_FLAGS_NORMAL;

    switch (req->msg_type) {
        case INVD_NEW_TARGET:
            req->rc = __handle_request_new_target(
                req->new_target.uuid_str,
                req->new_target.vm_target_tgid,
                req->new_target.vm_tracer_tgid,
                &(req->new_target.regs),
                payload,
                payload_len,
                new_conn_fd
            );

            /* this prevents us from closing new_conn_fd on return, as this
             * socket is needed in the host shadow
             */
            if (req->rc == INVD_OK)
                flags = INVD_FLAGS_NEW_TARGET;

            break;

        case INVD_DEL_TARGET:
            req->rc = __handle_request_del_target(
                req->del_target.uuid_str,
                req->del_target.vm_target_tgid
            );
            break;

        default:
            invd_err("invalid request (%d)\n", req->msg_type);
            req->rc = INVD_ERR_BAD_TYPE;
            break;
    }

    invd_out("request %s .... %s\n",
        invd_msg_type_to_str(req->msg_type),
        invd_rc_to_str(req->rc)
    );

    return flags;
}

static int
__handle_socket_request(int    connect_fd,
                        void * priv_data)
{
    struct sockaddr_in client_addr;
    struct invd_request req;
    socklen_t client_addr_len;
    int new_conn_fd;
    ssize_t bytes;
    unsigned long flags;

    size_t payload_len = 0;
    char * payload = NULL;

    client_addr_len = sizeof(struct sockaddr_in);
    new_conn_fd = accept(connect_fd, (struct sockaddr *)&client_addr, &client_addr_len);
    if (new_conn_fd < 0) {
        invd_err("could not accept connection request\n");
        return -1;
    }

    bytes = invirt_read_data_fd(new_conn_fd, (char *)&req,
            sizeof(struct invd_request));
    if (bytes != sizeof(struct invd_request)) {
        invd_err("could not read invd_request (ret=%ld)\n", bytes);
        return -1;
    }

    if (req.payload_len > 0) {
        payload = malloc(sizeof(char) * req.payload_len);
        if (payload == NULL) {
            invd_err("could not allocate memory for payload: %s\n",
                    strerror(errno));
            goto err_payload;
        }

        payload_len = req.payload_len;
        bytes = invirt_read_data_fd(new_conn_fd, payload, payload_len);
        if (bytes != payload_len) {
            invd_err("could not read payload in invd_request (ret=%ld)\n",
                    bytes);
            free(payload);
            goto err_payload;
        }
    }

    flags = __handle_request(&req, payload, payload_len, new_conn_fd);

    if (flags & INVD_FLAG_WRITE_RC) {
        bytes = invirt_write_data_fd(new_conn_fd, (const char *)&req,
                sizeof(struct invd_request));
        if (bytes != sizeof(struct invd_request)) {
            invd_err("could not write invd_request\n");
            return -1;
        }
    }

    if (flags & INVD_FLAG_CLOSE_FD)
        close(new_conn_fd);

    return 0;

err_payload:
    req.rc = INVD_ERR_PAYLOAD;
    bytes = invirt_write_data_fd(new_conn_fd, (const char *)&req,
            sizeof(struct invd_request));
    if (bytes != sizeof(struct invd_request))
        invd_err("could not write invd_request\n");

    close(new_conn_fd);

    return -1;
}

static int
invd_setup(void)
{
    int connect_fd, status, option;
    struct sockaddr_in serv_addr;

    /* Initialize global data structures */
    connect_fd = 0;
    status = -1;

    invd_notifier = invirt_notifier_create();
    if (invd_notifier == NULL) {
        invd_err("could not create fd notifier\n");
        goto out_notifier;
    }

    /* Create notifier, and create/track self pipe to handle sigchld events */
    {
        status = pipe(invd_sigchld_pipe);
        if (status != 0) {
            invd_err("could not create pipe: %s\n", strerror(errno));
            goto out_pipe;
        }

        /* mark the read side non blocking */
        status = fcntl(
            invd_sigchld_pipe[0],
            F_SETFL,
            fcntl(invd_sigchld_pipe[0], F_GETFL) | O_NONBLOCK
        );
        if (status != 0) {
            invd_err("could not mark pipe nonblocking: %s\n", strerror(errno));
            goto out_pipe;
        }

        status = invirt_notifier_remember_fd(invd_notifier,
                invd_sigchld_pipe[0], handle_sigchld_fd, NULL);
        if (status != 0) {
            invd_err("could not track sigchld fd\n");
            goto out_remember;
        }
    }

    /* Create hashtables */
    {
        tgid_to_program_table = invirt_create_htable(0, invd_tgid_hash_fn,
                invd_tgid_eq_fn);
        if (tgid_to_program_table == NULL) {
            invd_err("could not create hashtable\n");
            goto out_tgid;
        }
    }

    /* Setup connection socket */
    {
        connect_fd = socket(AF_INET, SOCK_STREAM, 0);
        if (connect_fd < 0) {
            invd_err("could not create socket: %s\n", strerror(errno));
            goto out_socket;
        }

        /* Make the socket address reusable */
        option = 1;
        status = setsockopt(connect_fd, SOL_SOCKET, SO_REUSEADDR, &option,
                sizeof(int));
        if (status < 0) {
            invd_err("could not set reusable socket address: %s\n",
                    strerror(errno));
            goto out_setup;
        }

        memset(&serv_addr, 0, sizeof(struct sockaddr_in));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = inet_addr(invd_ip_addr);
        serv_addr.sin_port = htons(invd_ip_port);

        status = bind(connect_fd, (struct sockaddr *)&serv_addr,
            sizeof(struct sockaddr_in));
        if (status < 0) {
            invd_err("could not bind address to socket: %s\n", strerror(errno));
            goto out_setup;
        }

        status = listen(connect_fd, 10);
        if (status < 0) {
            invd_err("could not configure passive socket: %s\n", strerror(errno));
            goto out_setup;
        }

        status = fcntl(connect_fd, F_SETFL, O_NONBLOCK);
        if (status < 0) {
            invd_err("could not mark socket fd as non-blocking: %s\n",
                    strerror(errno));
            goto out_setup;
        }

        status = invirt_notifier_remember_fd(invd_notifier, connect_fd,
                __handle_socket_request, NULL);
        if (status < 0) {
            invd_err("could not track fd\n");
            goto out_setup;
        }
    }

    /* return value is the new sock fd */
    status = connect_fd;

out_setup:
    if (status != connect_fd)
        close(connect_fd);
out_socket:
    if (status != connect_fd)
        invirt_free_htable(tgid_to_program_table, 1, 0);
out_tgid:
    if (status != connect_fd)
        invirt_notifier_forget_fd(invd_notifier, invd_sigchld_pipe[0]);
out_remember:
    if (status != connect_fd) {
        close(invd_sigchld_pipe[0]);
        close(invd_sigchld_pipe[1]);
    }
out_pipe:
    if (status != connect_fd)
        invirt_notifier_destroy(invd_notifier);
out_notifier:
    return status;
}

static void
write_ip_info(void)
{
    FILE * f;

    f = fopen(INVD_IP_INFO, "w");
    if (f == NULL) {
        invd_err("failed to open " INVD_IP_INFO ": %s\n", strerror(errno));
        return;
    }

    fprintf(f, "%s:%d\n", invd_ip_addr, invd_ip_port);
    fclose(f);
}

static void
del_ip_info(void)
{
    unlink(INVD_IP_INFO);
}

static int
invd(void)
{
    int connect_fd, status;

    connect_fd = invd_setup();
    if (connect_fd < 0)
        return connect_fd;

    /*
     * Core event processing loop.
     *
     * Wait for three things:
     *  i.   new connections on connect_fd
     *  ii.  system call request from /dev/invd
     *  iii. system call return from socket fd
     */
    invd_do_exit = 0;
    status = 0;

    write_ip_info();

    invd_out("listening for AF_INET connections on %s:%d\n",
        invd_ip_addr, invd_ip_port);

    while (!invd_do_exit) {
        struct fd_handler * handler = NULL;
        fd_set rdset;
        int i, active_fds, max_fd;

        invirt_notifier_get_fd_info(invd_notifier, &rdset, &max_fd);

        active_fds = select(max_fd + 1, &rdset, NULL, NULL, NULL);
        if (active_fds < 0) {
            if (errno == EINTR)
                continue;

            invd_err("failed to 'select' among fds: %s\n", strerror(errno));
            status = -1;
            invd_do_exit = 1;
            break;
        }

        for (i = 0; i <= max_fd && active_fds > 0; i++) {
            if (FD_ISSET(i, &rdset)) {

                --active_fds;

                handler = invirt_notifier_handler_of_fd(invd_notifier, i);
                if (handler == NULL) {
                    if (FD_ISSET(i, &rdset))
                        invd_err("no handler associated with fd\n");
                    continue;
                }

                assert(handler->fd == i);
                (void)handler->fn(i, handler->priv_data);
            }
        }
    }

    invd_out("caught exit signal. Tearing down ...\n");

    /* TODO: do we really need to clean anything up??; we're about to exit */
#if 0
    invirt_notifier_forget_fd(invd_notifier, connect_fd);
    invirt_notifier_destroy(invd_notifier);
    close(connect_fd);
#endif

    del_ip_info();

    return status;
}

int
main(int     argc,
     char ** argv,
     char ** envp)
{
    int status;

    status = parse_cmd_line(argc, argv);
    if (status != 0)
        return status;

    status = install_signal_handlers();
    if (status != 0)
        return status;

    status = invd();
    exit((status == 0) ? EXIT_SUCCESS : EXIT_FAILURE);
}
