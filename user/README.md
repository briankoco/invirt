# inVirt userspace utilities

* invrun

Guest-side executable used to launch an ELF into a co-native environment
and listen for its system calls and signals

* invd

Host-side daemon used to listen for new invrun invocations
