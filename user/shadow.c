/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdarg.h>
#include <getopt.h>
#include <fcntl.h>
#include <sched.h>

#include <sys/user.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

#include <invirt.h>
#include <invirt_vcall.h>
#include <invd.h>

#include "memory_map.h"
#include "shadow.h"
#include "hashtable.h"
#include "utils.h"

static void
handle_sigsegv_sigbus_signal(int         sig,
                             siginfo_t * si,
                             void      * unused)
{
    ucontext_t * context = unused;

    assert(sig == SIGSEGV || sig == SIGBUS);

    err("received %s at:\n"
        "    CR2:     0x%lx\n"
        "\n"
        "  General purpose registers:\n"
        "    R8:      0x%llx\n"
        "    R9:      0x%llx\n"
        "    R10:     0x%llx\n"
        "    R11:     0x%llx\n"
        "    R12:     0x%llx\n"
        "    R13:     0x%llx\n"
        "    R14:     0x%llx\n"
        "    R15:     0x%llx\n"
        "    RDI:     0x%llx\n"
        "    RSI:     0x%llx\n"
        "    RBP:     0x%llx\n"
        "    RDX:     0x%llx\n"
        "    RAX:     0x%llx\n"
        "    RCX:     0x%llx\n"
        "    RSP:     0x%llx\n"
        "    RIP:     0x%llx\n"
        "    EFL:     0x%llx\n"
        "    CSGSFS:  0x%llx\n"
        "    ERR:     0x%llx\n"
        "    TRAPNO:  0x%llx\n"
        "    OLDMASK: 0x%llx\n"
        "    CR2:     0x%llx\n",
        (sig == SIGSEGV) ? "SIGSEGV" : "SIGBUS",
        (unsigned long)si->si_addr,
        context->uc_mcontext.gregs[REG_R8],
        context->uc_mcontext.gregs[REG_R9],
        context->uc_mcontext.gregs[REG_R10],
        context->uc_mcontext.gregs[REG_R11],
        context->uc_mcontext.gregs[REG_R12],
        context->uc_mcontext.gregs[REG_R13],
        context->uc_mcontext.gregs[REG_R14],
        context->uc_mcontext.gregs[REG_R15],
        context->uc_mcontext.gregs[REG_RDI],
        context->uc_mcontext.gregs[REG_RSI],
        context->uc_mcontext.gregs[REG_RBP],
        context->uc_mcontext.gregs[REG_RDX],
        context->uc_mcontext.gregs[REG_RAX],
        context->uc_mcontext.gregs[REG_RCX],
        context->uc_mcontext.gregs[REG_RSP],
        context->uc_mcontext.gregs[REG_RIP],
        context->uc_mcontext.gregs[REG_EFL],
        context->uc_mcontext.gregs[REG_CSGSFS],
        context->uc_mcontext.gregs[REG_ERR],
        context->uc_mcontext.gregs[REG_TRAPNO],
        context->uc_mcontext.gregs[REG_OLDMASK],
        context->uc_mcontext.gregs[REG_CR2]
    );


    while (1) {}
    _exit(1);
}

static int
install_sigsegv_handler(void)
{
    struct sigaction sa;
    int status;

    sa.sa_sigaction = &handle_sigsegv_sigbus_signal;
    sa.sa_flags = SA_NOCLDSTOP | SA_SIGINFO;
    sigemptyset(&sa.sa_mask);

    status = sigaction(SIGSEGV, &sa, NULL);
    if (status != 0) {
        err("sigaction failed: %s\n", strerror(errno));
        return status;
    }

    return 0;
}

static int
install_sigbus_handler(void)
{
    struct sigaction sa;
    int status;

    sa.sa_sigaction = &handle_sigsegv_sigbus_signal;
    sa.sa_flags = SA_NOCLDSTOP | SA_SIGINFO;
    sigemptyset(&sa.sa_mask);

    status = sigaction(SIGBUS, &sa, NULL);
    if (status != 0) {
        err("sigaction failed: %s\n", strerror(errno));
        return status;
    }

    return 0;
}

static int
install_signal_handlers(void)
{
    return (
        install_sigsegv_handler() ||
        install_sigbus_handler()
    );
}


/**********************************
 *     END shared shadow code
 *********************************/



/**********************************
 * BEGIN invg_shadow specific code
 *********************************/

static int
invg_init(void)
{
    int fd;

    fd = open(INVG_DEVICE_PATH, O_RDWR);
    if (fd == -1) {
        err("could not open " INVG_DEVICE_PATH ": %s\n",
                strerror(errno));
        return -1;
    }

    return fd;
}

/*
 * Setup a shadow context in our address space
 */
static inline int __attribute__((always_inline))
map_local_target(pid_t target_pid,
                 int   invg_fd)
{
    struct memory_map self_map, target_map;
    int status;

    memset(&self_map, 0, sizeof(struct memory_map));
    memset(&target_map, 0, sizeof(struct memory_map));

    /* parse target map */
    status = invirt_parse_memory_map(target_pid, &target_map);
    if (status != 0) {
        err("could not parse target process memory map\n");
        return -1;
    }

    /* parse own map */
    status = invirt_parse_memory_map(getpid(), &self_map);
    if (status != 0) {
        err("could not parse local process memory map\n");
        return -1;
    }

    /* TODO: perform stack relocation if we see that it collides with target
     * address space
     */

    /* map the target */
    status = invirt_mmap_memory_map(&target_map, invg_fd, &self_map);
    if (status != 0) {
        err("could not map target memory segments\n");
        return -1;
    }

    /*
    out("Successfully mapped target segments. Dumping memory map\n");
    invirt_print_memory_map(&self_map);
    */

    return 0;
}

#if 0
static int
find_ip_addr(int      s,
             char   * ip_addr,
             size_t   ip_addr_len)
{
    int status, i, nr_iface, i1, i2, i3;
    struct ifreq reqs[16];
    struct ifconf conf = {
        .ifc_len = sizeof(struct ifreq)*16,
        .ifc_req = reqs
    };

    s = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);

    status = ioctl(s, SIOCGIFCONF, &conf);
    if (status < 0) {
        err("ioctl(SIOCGICONF) failed: %s\n", strerror(errno));
        return -1;
    }

    nr_iface = conf.ifc_len/sizeof(struct ifreq);
    sscanf(INVIRT_IP_PREFIX, "%d.%d.%d", &i1, &i2, &i3);

    for (i = 0; i < nr_iface; i++) {
        struct ifreq * req = &(reqs[i]);
        char ip_addr_found[32];
        int a1, a2, a3;

        /* get IP address in character format */
        inet_ntop(AF_INET, 
            &(((struct sockaddr_in *)&(req->ifr_addr))->sin_addr),
            ip_addr_found, 
            32
        );

        /* parse and test for equality */
        sscanf(ip_addr_found, "%d.%d.%d.%*s", &a1, &a2, &a3);

        if ((a1 == i1) && (a2 == i2) && (a3 == i3)) {
            strncpy(ip_addr, ip_addr_found, ip_addr_len);
            return 0;
        }
    }

    return -1;
}

static int
create_listener(int * port)
{
    int s, status, option;
    char ip_addr[32] = { 0 };
    struct sockaddr_in serv_addr;
    socklen_t socklen;

    s = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
    if (s < 0) {
        err("could not open socket: %s\n", strerror(errno));
        return -1;
    }

    /* make socket reusable */
    option = 1;
    status = setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(int));
    if (status < 0) {
        err("could not set reusable socket address: %s\n", strerror(errno));
        goto out_opt;
    }

    /* find ip addr */
    status = find_ip_addr(s, ip_addr, 32);
    if (status != 0) {
        err("could not determine local IP address: %s\n", strerror(errno));
        goto out_ip;
    }

    memset(&serv_addr, 0, sizeof(struct sockaddr_in));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip_addr);
    serv_addr.sin_port = htons(0);

    /* let kernel choose a port */
    status = bind(s, (struct sockaddr *)&serv_addr, 
            sizeof(struct sockaddr_in));
    if (status < 0) {
        err("could not bind address to socket: %s\n", strerror(errno));
        goto out_bind;
    }

    /* determine what port we were assigned */
    socklen = sizeof(struct sockaddr_in);
    status = getsockname(s, (struct sockaddr *)&serv_addr, &socklen); 
    if (status < 0) {
        err("getsockname failed: %s\n", strerror(errno));
        goto out_sockname;
    }

    *sock_fd = s;
    *port = ntohs(serv_addr.sin_port);

    return 0;

out_sockname:
out_bind:
out_ip:
out_opt:
    close(s);
    return -1;
}
#endif

static int
__send_recv_invd_request(struct invd_request * req,
                         const char          * payload,
                         size_t                data_len,
                         bool                  close_fd,
                         int                 * sock_fd_p)
{
    int sock_fd, status;
    ssize_t bytes;
    struct sockaddr_in serv_addr;
    
    sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_fd < 0) {
        err("could not create socket: %s\n", strerror(errno));
        return -1;
    }

    memset(&serv_addr, 0, sizeof(struct sockaddr_in));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(INVIRT_INVD_IP_ADDR);
    serv_addr.sin_port = htons(INVIRT_INVD_IP_PORT);

    status = connect(sock_fd, (struct sockaddr *)&serv_addr,
            sizeof(struct sockaddr_in));
    if (status != 0) {
        err("could not connect to invd: %s\n", strerror(errno));
        close(sock_fd);
        return -1;
    }

    bytes = invirt_write_data_fd(sock_fd, (const char *)req, 
            sizeof(struct invd_request));
    if (bytes != sizeof(struct invd_request)) {
        err("could not write invd_request to invd\n");
        close(sock_fd);
        return -1;
    }

    if (data_len > 0) {
        bytes = invirt_write_data_fd(sock_fd, payload, data_len);
        if (bytes != data_len) {
            err("could not write payload to invd\n");
            close(sock_fd);
            return -1;
        }
    }

    bytes = invirt_read_data_fd(sock_fd, (char *)req,
            sizeof(struct invd_request));
    if (bytes != sizeof(struct invd_request)) {
        err("could not read invd_request from invd\n");
        close(sock_fd);
        return -1;
    }

    if (close_fd)
        close(sock_fd);
    else if (sock_fd_p)
        *sock_fd_p = sock_fd;

    if (req->rc != INVD_OK)
        err("invd returned error code: %d\n", req->rc);

    return (req->rc == INVD_OK) ? 0 : -1;
}

static int
invd_setup(const char              * exe,
           pid_t                     target_pid,
           struct user_regs_struct * regs,
           int                     * sock_fd)
{
    char uuid_str[INVD_UUID_STR_LEN] = { 0 };
    int status;
    void * exe_data;
    size_t exe_len;

    /* determine our context's UUID */
    status = invirt_query_uuid(uuid_str, INVD_UUID_STR_LEN);
    if (status != 0) {
        err("failed to query system uuid; run <invirt>/mod/install.sh\n");
        return -1;
    }

    dbg("found UUID=%s\n", uuid_str);

    /* load our local executable into memory */
    {
        int fd;
        struct stat st;

        fd = open(exe, O_RDONLY);
        if (fd == -1) {
            err("failed to open %s: %s\n", exe, strerror(errno));
            return -1;
        }

        status = fstat(fd, &st);
        if (status != 0) {
            err("failed to stat %s: %s\n", exe, strerror(errno));
            close(fd);
            return -1;
        }

        exe_len = st.st_size;
        exe_data = mmap(NULL, exe_len, PROT_READ, MAP_SHARED, fd, 0);
        if (exe_data == MAP_FAILED) {
            err("failed to mmap %s: %s\n", exe, strerror(errno));
            close(fd);
            return -1;
        }
    }

    /* send new target message */
    {
        struct invd_request req = {
            .msg_type = INVD_NEW_TARGET,
            .rc = INVD_ERR,
            .new_target = {
                .vm_tracer_tgid = getpid(),
                .vm_target_tgid = target_pid,
            },
            .payload_len = exe_len
        };

        strncpy(req.new_target.uuid_str, uuid_str, INVD_UUID_STR_LEN);
        memcpy(
            (void *)&(req.new_target.regs), 
            (const void *)regs, 
            sizeof(struct user_regs_struct)
        );

        status = __send_recv_invd_request(&req, exe_data, exe_len, false, sock_fd);
        if (status != 0) {
            err("INVD_NEW_TARGET request failed\n");
            return -1;
        }
    }

    return 0;
}

static inline int __attribute__((always_inline))
invg_shadow(pid_t        target_pid,
            int          register_fd,
            const char * exe)
{
    int status, sock_fd, invg_fd, target_fd;
    struct user_regs_struct regs;
    ssize_t bytes;

    /* FIXME: hack to be removed */
    sleep(1);

    /* read register contents in from pipe */
    bytes = invirt_read_data_fd(
        register_fd, 
        (char *)&regs, 
        sizeof(struct user_regs_struct)
    );
    close(register_fd);

    if (bytes != sizeof(struct user_regs_struct)) {
        err("failed to read register context\n");
        return -1;
    }

    /* setup invg driver */
    invg_fd = invg_init();
    if (invg_fd < 0)
        return -1;

    /* tell invg the target tid */ 
    target_fd = ioctl(invg_fd, INVG_IOC_ATTACH_TID, target_pid);
    if (target_fd < 0) {
        err("could not issue INVG_IOC_SET_TARGET_TID ioctl to " INVG_DEVICE_PATH
                ": %s\n", strerror(errno));
        return -1;
    }

    /* setup local memory mirror */
    status = map_local_target(target_pid, target_fd);
    if (status != 0) {
        err("could not mirror address space\n");
        return status;
    }

    /*
     * XXX: from this point forward, our original VVAR/VDSO mappings are nuked.
     * So don't invoke:
     *   - clock_gettime
     *   - gettimeofday
     *   - getcpu
     *   - time
     *
     * if you must use them, do so through syscall()
     */

    /* connect to invd, sending data about the target context */
    status = invd_setup(exe, target_pid, &regs, &sock_fd);
    if (status != 0) {
        err("invd_setup failed\n");
        return status;
    }

    /*
     * sock_fd is an open socket connection; sit on it and listen for 
     * system calls
     */
    
    {
        struct memory_map local_map;

        status = invirt_parse_memory_map(getpid(), &local_map);
        if (status != 0) {
            err("could not parse local memory map\n");
            return -1;
        }

        invirt_print_memory_map(&local_map);
    }
    out("%d: listening for VCALLs\n", getpid());

    /* start monitoring sock_fd */

    while (1) {
        ssize_t bytes;
        struct invirt_vcall vcall;
        struct invg_ioc_fault ioc_fault;

        bytes = invirt_read_data_fd(
            sock_fd,
            (char *)&vcall,
            sizeof(struct invirt_vcall)
        );

        if (bytes <= 0) {
            out("invh hung up; host is done\n");
            break;
        }

        switch (vcall.msg_type) {
        case INVIRT_VCALL_SYSCALL:
            dbg("IN syscall %lu\n", vcall.syscall.nr);

            /* invoke the syscall */
            vcall.syscall.rc = syscall(
                vcall.syscall.nr,
                vcall.syscall.arg0,
                vcall.syscall.arg1,
                vcall.syscall.arg2,
                vcall.syscall.arg3,
                vcall.syscall.arg4,
                vcall.syscall.arg5
            );

            dbg("OUT syscall %lu = %lu (0x%lx)\n", vcall.syscall.nr, 
                vcall.syscall.rc, vcall.syscall.rc);

            bytes = invirt_write_data_fd(
                sock_fd,
                (char *)&vcall,
                sizeof(struct invirt_vcall)
            );
            if (bytes != sizeof(struct invirt_vcall)) {
                err("failed to write syscall rc\n");
            }

            break;

        case INVIRT_VCALL_PAGE_FAULT:
            dbg("Received PF request at HVA 0x%lx\n", vcall.page_fault.hva);

            ioc_fault.vaddr = vcall.page_fault.hva;
            ioc_fault.vmf_flags = vcall.page_fault.pf_flags;
            ioc_fault.sock_fd = sock_fd;

            /* 
             * fault in pages
             *
             * if successful, this will communicate the pages back to invh
             * directly in the kernel
             *
             * otherwise, we send an error response ourselves
             */
            //status = ioctl(target_fd, INVG_IOC_FAULT_PAGE, (char *)&ioc_fault);
            status = ioctl(target_fd, INVG_IOC_FAULT_VMA, (char *)&ioc_fault);
            if (status != 0) {
                err("INVG_IOC_FAULT_PAGE failed: %s\n", strerror(errno));
                vcall.page_fault.rc = -errno;

                bytes = invirt_write_data_fd(
                    sock_fd,
                    (char *)&vcall,
                    sizeof(struct invirt_vcall)
                );
                if (bytes != sizeof(struct invirt_vcall)) {
                    err("failed to write vcall result\n");
                }
            }

            break;

        default:
            err("received invalid syscall msg_type: %d\n", vcall.msg_type);
            break;
        }
    }
    
    return 0;
}

/**********************************
 *  END invg_shadow specific code
 *********************************/


/**********************************
 * BEGIN invh_shadow specific code
 *********************************/

#define PROC_MMAP_MIN_ADDR  "/proc/sys/vm/mmap_min_addr"
//#define INVH_MMAP_END       0x7fffffffffff
#define INVH_MMAP_END       0x7ffffffff000
#define INVH_MAX_MMAP_SIZE  (1ULL << 63)

#define SET_R(x, val)\
    ({\
        asm("mov %0, %%"x\
          :\
          : "m" (val)\
          :);\
    })

#define SET_R8(val)  SET_R("r8", val)
#define SET_R9(val)  SET_R("r9", val)
#define SET_R10(val) SET_R("r10", val)
#define SET_R11(val) SET_R("r11", val)
#define SET_R12(val) SET_R("r12", val)
#define SET_R13(val) SET_R("r13", val)
#define SET_R14(val) SET_R("r14", val)
#define SET_R15(val) SET_R("r15", val)
#define SET_RAX(val) SET_R("rax", val)
#define SET_RBX(val) SET_R("rbx", val)
#define SET_RCX(val) SET_R("rcx", val)
#define SET_RDX(val) SET_R("rdx", val)
#define SET_RSI(val) SET_R("rsi", val)
#define SET_RDI(val) SET_R("rdi", val)
#define SET_RBP(val) SET_R("rbp", val)
#define SET_RSP(val) SET_R("rsp", val)

#define JMP_ENTRY(rip)\
    ({\
        asm("jmp *%0"\
            :\
            : "m" (rip)\
            :\
        );\
    })


#define MEM_BARRIER() asm("" : : : "memory");


/* 
 * This is purposely _NOT_ on the stack.  We load RIP from this struct AFTER
 * we've moved RSP to the target's entry point.
 */
static volatile struct user_regs_struct __off_stack_regs;

static unsigned long
__find_min_mmap_addr(void)
{
    FILE * fp;
    unsigned long min_addr;

    fp = fopen(PROC_MMAP_MIN_ADDR, "r");
    if (fp == NULL) {
        err("could not open %s: %s\n", PROC_MMAP_MIN_ADDR, strerror(errno));
        return PAGE_SIZE;
    }

    fscanf(fp, "%lu", &min_addr);
    fclose(fp);
    return min_addr;
}

static int
__mmap_area(int           fd,
            unsigned long start,
            unsigned long end)
{
    unsigned long addr, len;
    void * mmap_addr;

    for (addr = start; addr < end; addr += len) {
        len = end - addr;
        if (len > INVH_MAX_MMAP_SIZE)
            len = INVH_MAX_MMAP_SIZE;

        mmap_addr = mmap(
            (void *)addr,
            len,
            PROT_READ | PROT_WRITE | PROT_EXEC,
            MAP_SHARED | MAP_FIXED | MAP_NORESERVE,
            fd,
            0
        );
        if (mmap_addr == MAP_FAILED) {
            err("could not mmap region [0x%lx, 0x%lx): %s\n",
                addr, addr+len, strerror(errno));
            return -1;
        }
    }

    return 0;
}

/*
 * mmap our entire address space to /dev/invh, allowing us
 * to catch page faults in kernel and forward to invg
 */
static int
invh_mmap_address_space(int invh_fd)
{
    int status, i;
    unsigned long min_mmap, rsv_start, rsv_end, last_end = 0;
    struct memory_map local_map;

    status = invirt_parse_memory_map(getpid(), &local_map);
    if (status != 0) {
        err("could not parse local memory map\n");
        return -1;
    }

    min_mmap = __find_min_mmap_addr();

    /* reserve everything that's not mapped to us already */
    for (i = 0; i < local_map.nr_segments; i++) {
        struct memory_segment * seg = &(local_map.segments[i]);

        /* is there a gap between segs? */
        if (seg->start > last_end) {
            rsv_start = last_end;
            rsv_end   = seg->start;

            if (rsv_start < min_mmap)
                rsv_start = min_mmap;

            if (rsv_end > INVH_MMAP_END)
                rsv_end = INVH_MMAP_END;

            status = __mmap_area(invh_fd, rsv_start, rsv_end);
            if (status != 0)
                return -1;
        }

        last_end = seg->end;
    }

    /* map from last segment to end of address space */
    rsv_start = last_end;
    rsv_end = INVH_MMAP_END;

    status = __mmap_area(invh_fd, rsv_start, rsv_end);
    if (status != 0)
        return -1;

    /* re-parse and print the resulting mmaps */
    invirt_parse_memory_map(getpid(), &local_map);
    invirt_print_memory_map(&local_map);

    return 0;
}

static int
invh_enable_coredump(void)
{
    FILE * fp;
    unsigned long flags;

    fp = fopen("/proc/self/coredump_filter", "w");
    if (fp == NULL) {
        err("could not open /proc/self/coredump_filter");
        return -1;
    }

    fprintf(fp, "0x7f"); 
    fclose(fp);

    fp = fopen("/proc/self/coredump_filter", "r");
    if (fp == NULL) {
        err("could not open /proc/self/coredump_filter");
        return -1;
    }

    fscanf(fp, "%lx", &flags);
    fclose(fp);

    if (flags != 0x7f) {
        err("Failed to update coredump_filter\n");
        return -1;
    }

    return 0;
}

static int
invh_init(const char * tracer_uuid,
          pid_t        tracer_pid,
          int          conn_fd)
{
    int fd, mapfd, status;
    pid_t kvm_pid;
    struct invh_ioc_traceme tracee;

    /* find KVM pid associated with this uuid */
    kvm_pid = invirt_query_kvm_pid(tracer_uuid);
    if (kvm_pid == -1) {
        err("could not find KVM pid associated with VM uuid %s\n", tracer_uuid);
        return -1;
    }

    out("found KVM pid %d for VM uuid %s\n", kvm_pid, tracer_uuid);

    /* enable core dump of file-backed mappings */
    status = invh_enable_coredump();
    if (status != 0)
        return -1;

    fd = open(INVH_DEVICE_PATH, O_RDWR);
    if (fd == -1) {
        err("could not open " INVH_DEVICE_PATH ": %s\n", strerror(errno));
        return -1;
    }

    mapfd = ioctl(fd, INVH_IOC_GET_MAPFD, 0);
    if (mapfd < 0 ){
        err("could not allocate mapfd from invh driver\n");
        close(fd);
        return -1;
    }

    status = invh_mmap_address_space(mapfd);
    if (status != 0) {
        err("could not reserve address space\n");
        close(fd);
        return -1;
    }
    
    strncpy(tracee.uuid_str, tracer_uuid, INVD_UUID_STR_LEN);
    tracee.tracer_pid = tracer_pid;
    tracee.kvm_pid = kvm_pid;
    tracee.mode = INVH_IOC_TRACEME_SOCKET;
    tracee.socket.conn_fd = conn_fd;

    status = ioctl(fd, INVH_IOC_TRACEME, (char *)&tracee);
    if (status != 0) {
        err("ioctl(INVH_IOC_TRACEME) failed: %s\n", strerror(errno));
        close(fd);
        return -1;
    }

    return 0;
}

static inline int __attribute__((always_inline))
invh_shadow(const char * tracer_uuid,
            pid_t        tracer_pid,
            int          register_fd,
            int          conn_fd)
{
    int status;
    ssize_t bytes;
    cpu_set_t mask;

    /* XXX pin self to cpu 0 for the time being */
    CPU_ZERO(&mask);
    CPU_SET(0, &mask);
    status = sched_setaffinity(0, sizeof(cpu_set_t), &mask);
    if (status != 0) {
        err("failed to set CPU affinity\n");
        return -1;
    }
    out("%d: pinned self to CPU 0\n", getpid());

    /* read register contents in from pipe */
    bytes = invirt_read_data_fd(
        register_fd, 
        (char *)&__off_stack_regs, 
        sizeof(struct user_regs_struct)
    );
    close(register_fd);

    if (bytes != sizeof(struct user_regs_struct)) {
        err("failed to read register context\n");
        return -1;
    }

    /* init invh driver */
    status = invh_init(tracer_uuid, tracer_pid, conn_fd);
    if (status != 0) {
        err("failed to init invh driver\n");
        return -1;
    }

    out("%d: initializing invh for uuid:%s,tgid:%d -- restoring registers, jumping to 0x%lx\n",
        getpid(), tracer_uuid, tracer_pid, (unsigned long)__off_stack_regs.rip
    );

    /* instantiate saved register context */
    SET_R8(__off_stack_regs.r8);    
    SET_R9(__off_stack_regs.r9);    
    SET_R10(__off_stack_regs.r10);   
    SET_R11(__off_stack_regs.r11);   
    SET_R12(__off_stack_regs.r12);   
    SET_R13(__off_stack_regs.r13);   
    SET_R14(__off_stack_regs.r14);   
    SET_R15(__off_stack_regs.r15);   
    SET_RAX(__off_stack_regs.rax);
    SET_RBX(__off_stack_regs.rbx);   
    SET_RCX(__off_stack_regs.rcx);   
    SET_RDX(__off_stack_regs.rdx);
    SET_RSI(__off_stack_regs.rsi);
    SET_RDI(__off_stack_regs.rdi);
    SET_RSP(__off_stack_regs.rsp);
    SET_RBP(__off_stack_regs.rbp);

    /* jump to the target's entry point address */
    MEM_BARRIER();
    JMP_ENTRY(__off_stack_regs.rip);

    return 0;
}

/**********************************
 *  END invh_shadow specific code
 *********************************/


int
main(int     argc,
     char ** argv,
     char ** envp)
{
    char * exe, * uuid;
    int status, register_fd, conn_fd, mode;
    pid_t tracer_pid;

    if (argc != 6) {
        fprintf(stderr, "shadow expects 6 arguments\n");
        return -1;
    }
 
    exe = argv[0];
    mode = atoi(argv[1]);
    uuid = argv[2];
    tracer_pid = atoi(argv[3]);
    register_fd = atoi(argv[4]);
    conn_fd = atoi(argv[5]);

    switch (mode) {
        case INVIRT_INVG_SHADOW:
            invirt_init_context("invg");
            break;

        case INVIRT_INVH_SHADOW:
            invirt_init_context("invh");
            break;

        default:
            err("invalid mode provided to shadow\n");
            return -1;
    }

    status = install_signal_handlers();
    if (status != 0) 
        return -1;

    out("pid:%d, tid:%d: starting %s shadow process\n",
        getpid(),
        gettid(),
        (mode == INVIRT_INVG_SHADOW) ? "invg" : "invh"
    );

    return (mode == INVIRT_INVG_SHADOW)
        ? invg_shadow(tracer_pid, register_fd, exe)
        : invh_shadow((const char *)uuid, tracer_pid, register_fd, conn_fd);
}
