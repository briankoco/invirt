#!/usr/bin/env python2

"""
  This file is part of the invirt project.
  
  This is free software.  You are permitted to use, redistribute, and
  modify it as specified in the file "LICENSE.md".
"""

"""
  invd.py:
    Support to send data to invd through a socket
"""

import os
import errno
import sys
import json
import socket
import uuid
import ctypes

# invd configuration
INVIRT_TMP_PATH     = "/tmp/invirt"
INVD_IP_INFO        = INVIRT_TMP_PATH + "/invd-ip-info.txt"
INVD_UUID_STR_LEN = (8+4+4+4+12+4+1)

# these are defined in invd.h
INVD_NEW_VM     = 0
INVD_DEL_VM     = 1
INVD_GET_VMID   = 2
INVD_NEW_TARGET = 3
INVD_DEL_TARGET = 4

INVD_NR_REQS    = 5

class INVD_NEW_VM_STRUCT(ctypes.Structure):
    _fields_ = [
        ("uuid_str", ctypes.c_char*INVD_UUID_STR_LEN),
        ("vmid", ctypes.c_int)
    ]

class INVD_DEL_VM_STRUCT(ctypes.Structure):
    _fields_ = [
        ("vmid", ctypes.c_int)
    ]

class INVD_GET_VMID_STRUCT(ctypes.Structure):
    _fields_ = [
        ("uuid_str", ctypes.c_char*INVD_UUID_STR_LEN),
        ("vmid", ctypes.c_int)
    ]

class INVD_NEW_TARGET_STRUCT(ctypes.Structure):
    _fields_ = [
        ("vmid", ctypes.c_int),
        ("vm_target_tgid", ctypes.c_int)
    ]

class INVD_DEL_TARGET_STRUCT(ctypes.Structure):
    _fields_ = [
        ("vmid", ctypes.c_int),
        ("vm_target_tgid", ctypes.c_int)
    ]

class INVD_REQUEST_UNION(ctypes.Union):
    _fields_ = [
        ("new_vm", INVD_NEW_VM_STRUCT),
        ("del_vm", INVD_DEL_VM_STRUCT),
        ("get_vmid", INVD_GET_VMID_STRUCT),
        ("new_target", INVD_NEW_TARGET_STRUCT),
        ("del_target", INVD_DEL_TARGET_STRUCT)
    ]

class INVD_REQUEST(ctypes.Structure):
    _anonymous_ = ("u",)
    _pack_ = 1
    _fields_ = [
        ("msg_type", ctypes.c_int),
        ("rc", ctypes.c_int),
        ("u", INVD_REQUEST_UNION)
    ]

def __invd_ip_info():
    if not os.path.isfile(INVD_IP_INFO):
        print >> sys.stderr, "%s does not exist -- cannot connect to invd" \
            % INVD_IP_INFO
        return (None, None)

    with open(INVD_IP_INFO, "r") as f:
        addr, port = f.readlines()[0].split(':')

    return (addr, int(port))

def __get_socket():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    addr, port = __invd_ip_info()
    if addr is None:
        return -1

    s.connect((addr, port))
    return s

def __send_recv_msg(payload):
    s = __get_socket()
    s.send(payload)

    return INVD_REQUEST.from_buffer_copy(
        s.recv(ctypes.sizeof(INVD_REQUEST))
    )

def invd_new_vm(uuid_str, vmid):
    inner_u = INVD_REQUEST_UNION()
    inner_u.new_vm = INVD_NEW_VM_STRUCT(uuid_str, vmid)
    payload = INVD_REQUEST(
        ctypes.c_int(INVD_NEW_VM), -1, inner_u
    )

    resp = __send_recv_msg(payload)
    return int(resp.rc)

def invd_del_vm(vmid):
    inner_u = INVD_REQUEST_UNION()
    inner_u.del_vm = INVD_DEL_VM_STRUCT(ctypes.c_int(vmid))
    payload = INVD_REQUEST(
        ctypes.c_int(INVD_DEL_VM), -1, inner_u
    )

    resp = __send_recv_msg(payload)
    return int(resp.rc)

def invd_get_vmid(uuid_str):
    inner_u = INVD_REQUEST_UNION()
    inner_u.get_vmid = INVD_GET_VMID_STRUCT(uuid_str, ctypes.c_int(-1))
    payload = INVD_REQUEST(
        ctypes.c_int(INVD_GET_VMID), -1, inner_u
    )

    resp = __send_recv_msg(payload)
    return int(resp.rc), int(resp.get_vmid.vmid)

