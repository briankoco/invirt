# Various scripts to setup invirt environments

* bridge.sh
    - Create a virtual network for needed for guest<->host communication

* invd.sh
    - Start/stop the host-side invd daemon

* invirt-kvm.py
    - Utility to create/destroy KVM VMs with invirt support
    - Use of invirt-kvm.py requires 2 configuration files describing your VM
      and hardware. See 'sample-configs' for examples on how to generate these
      files

## Authors

* **Brian Kocoloski**
