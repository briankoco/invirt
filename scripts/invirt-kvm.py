#!/usr/bin/env python3

"""
  This file is part of the invirt project.
  
  This is free software.  You are permitted to use, redistribute, and
  modify it as specified in the file "LICENSE.md".
"""

"""
  invirt-kvm.py:
    Launch/destroy an invirtable KVM VM
"""

import os
import errno
import sys
import getopt
import json
import subprocess
import signal
import socket
import uuid
import ctypes

# import invd functions
CUR_PATH = os.path.dirname(os.path.abspath(__file__)) 
sys.path.insert(0, CUR_PATH + "/extern")
from invd import invd_new_vm, invd_del_vm, invd_get_vmid

# bridge configuration
INVIRT_BR_NAME   = "ibr0"
INVIRT_TAP_NAME  = "itap0"

# invd configuration
INVIRT_TMP_PATH     = "/tmp/invirt"
INVIRT_IP_INFO      = INVIRT_TMP_PATH + "/invd-ip-info.dat"

HUGETLBFS_PREFIX    = "/sys/devices/system/node"
HUGETLBFS_POSTFIX   = "/nr_hugepages"



def HUGETLBFS_PAGE_SIZE_TO_PATH(page_size_kb):
    return "hugepages-%dkB" % page_size_kb

def HUGETLBFS_PATH(numa_node, page_size_kb):
    return HUGETLBFS_PREFIX + \
        "/node%d" % numa_node + \
        "/hugepages/%s" % HUGETLBFS_PAGE_SIZE_TO_PATH(page_size_kb) + \
        HUGETLBFS_POSTFIX

def usage(argv, out_f=sys.stderr, exit=None):
    print("Usage: %s [OPTIONS] <COMMAND> <vm.json> [<topology.json>]" % argv[0], file=out_f)
    print("   -h (--help)  : print help and exit", file=out_f)
    print("\nCommands:", file=out_f)
    print("  boot: boot VM specified in <vm.json>", file=out_f)
    print("    (requires topology.json)", file=out_f)
    print("  destroy: destroy VM specified in <vm.json>", file=out_f)

    if exit is not None:
        sys.exit(exit)


def parse_cmd_line(argc, argv):
    opts = []
    args = []

    try:
        opts, args = getopt.getopt(argv[1:], "h", ["help"])
    except getopt.GetoptError as err:
        print >> sys.stderr(err)
        usage(argv, exit=1)

    for o, a in opts:
        if o in ("-h", "--help"):
            usage(argv, out_f=sys.stdout, exit=0)

        else:
            usage(argv, exit=1)

    if len(args) < 2:
        usage(argv, exit=1)

    command = args[0]
    return command, args[1:]



def issue_system_cmd(cmd_list):
    cmd = ' '.join(cmd_list)
    ret = os.system(cmd)

    if ret != 0:
        print("The following command failed: '%s" % cmd)
    return ret

def pidfile_of_vm(vm_info):
    return "%s/vm-%d.pid" % (INVIRT_TMP_PATH, vm_info['id'])

def pid_of_vm(pid_file):
    cmd = ["cat", "%s" % pid_file]
    pid = subprocess.check_output(cmd)
    return int(pid)

"""
    Generate a mapping from VCPU to PCPU

    Two requirements:
        (1) groups of vcpus on the same virtual numa socket should be laid out sequentially:
            i.e., put 0,1,2,3 etc. on the same socket, rather than 0,2,4 and 1,3,5 on separate sockets

        (2) hyperthreads are always contiguous. i.e, 0,1, 2,3, are hyperthreads, not 0,24, 1,26, etc.

    params:
        pcpu_list: list of host OS core IDs where the guest will run 
        topo_info: toplogy of host processors, including reverse mapping of host OS core
            IDS to socket/core/thread physical info

    algorithm:
        determine number of sockets,cores_per_socket,threads_per_core that the guest will see
        based on the pcpu_list (must match exactly the host cpu config)

        walk sequentially through vcpu range from 0 to nr_cpus -1, mapping based on the two
        requirements above

    returns dict with:
        "num_cpus"
        "num_sockets",
        "num_cores_per_socket",
        "num_hw_threads_per_core",
        "vcpu_to_pcpu map"

"""
def generate_cpu_mapping(topo_info, pcpu_list):
    processor_info = topo_info["processor_info"]
    os_core_map    = topo_info["os_core_map"]

    # Determine number of sockets/cores/threads based on the pcpu list
    vcpu_topology = {}
    for pcpu in pcpu_list:
        core_info = os_core_map[pcpu]

        socket    = core_info["socket"]
        core      = core_info["core"]
        hw_thread = core_info["hw_thread"]

        if socket not in vcpu_topology:
            vcpu_topology[socket] = {}

        if core not in vcpu_topology[socket]:
            vcpu_topology[socket][core] = []

        vcpu_topology[socket][core].append(pcpu)


    # Ensure topology is uniform
    last_cores_per_socket = None
    last_threads_per_core = None
    for s in vcpu_topology:
        for c in vcpu_topology[s]:
            threads_per_core = len(vcpu_topology[s][c])
            if last_threads_per_core is not None and\
               last_threads_per_core != threads_per_core:
               print("Detected non-uniform CPU topology. Bailing")
               sys.exit(2)

            last_threads_per_core = threads_per_core

        cores_per_socket = len(vcpu_topology[s].keys())
        if last_cores_per_socket is not None and\
           last_cores_per_socket != cores_per_socket:
           print("Detected non-uniform CPU topology. Bailing")
           sys.exit(2)

        last_cores_per_socket = cores_per_socket


    # Topology is sane. Just walk the topo and map the VCPUs to PCPUs
    vcpu_to_pcpu = {}
    vcpu_off = 0

    for s in sorted(vcpu_topology):
        for c in sorted(vcpu_topology[s]):
            for pcpu in sorted(vcpu_topology[s][c]):
                vcpu_to_pcpu[vcpu_off] = pcpu
                vcpu_off += 1

    return {
        "num_cpus"                : vcpu_off,
        "num_sockets"             : len(vcpu_topology.keys()),
        "num_cores_per_socket"    : last_cores_per_socket,
        "num_hw_threads_per_core" : last_threads_per_core,
        "vcpu_to_pcpu_map"        : vcpu_to_pcpu 
    }


def build_hugetlbfs_cmd(vm_info, cpu_mapping):
    hugetlbfs_info = vm_info["hugetlbfs"]

    # Ensure enough nodes are provided for each numa socket that the VM will execute on
    numa_nodes = hugetlbfs_info["numa_nodes"]
    
    # We have a guarantee that that first n/m CPUs are socket 0, the next n/m are socket 1,
    # etc., where n is number of vcpus and m is number of numa nodes
    cpus_per_socket = cpu_mapping["num_cpus"] / cpu_mapping["num_sockets"]

    # Calculate size in bytes for each numa socket
    bytes_per_socket = long(hugetlbfs_info["memory_size_mb"] * (1024*1024))

    # Build the string
    cmd = []

    cpu_off = 0
    for vsocket, hsocket in enumerate(sorted(numa_nodes)):
        cmd += [
        "-object",
        "memory-backend-file,id=ram-node%d,prealloc=yes,mem-path=/dev/hugepages/,size=%lu,host-nodes=%d,policy=bind" % (vsocket, bytes_per_socket, hsocket),
        "-numa",
        "node,nodeid=%d,cpus=%d-%d,memdev=ram-node%d" % (\
            vsocket, 
            cpu_off,
            cpu_off + cpus_per_socket-1,
            vsocket
        )]

        cpu_off += cpus_per_socket

    return cmd

def build_qemu_cmd_line(vm_info, cpu_mapping, uuid_str):
    vm_id = vm_info['id']

    cmd = []
    cmd += ["qemu-system-x86_64", "-enable-kvm", 
        "-cpu", "host,host-cache-info=on,l3-cache=on",
    	"-vnc", "localhost:%d"%vm_id, "-daemonize",
        "-uuid", uuid_str
    ]

    #"-vnc", "localhost:0", "-daemonize"
    #"-cpu", "host,kvm=off,host-cache-info=on,l3-cache=on",

    pidfile = pidfile_of_vm(vm_info)
    cmd += ["-pidfile", "%s" % pidfile]
    cmd += ["-name", "invirt-vm,debug-threads=on"]
    #cmd += ["-name", "vm-%d"]

    # add cpu topology
    cmd += ["-smp", "cpus=%d,cores=%d,threads=%d,sockets=%d" \
        % (
            cpu_mapping["num_cpus"],
            cpu_mapping["num_cores_per_socket"],
            cpu_mapping["num_hw_threads_per_core"],
            cpu_mapping["num_sockets"],
        )
    ]

    # add memory info
    cmd += ["-m", "%d" % vm_info["memory_size_mb"]]

    if "hugetlbfs" in vm_info:
        cmd += build_hugetlbfs_cmd(vm_info, cpu_mapping)

    if "cdrom" in vm_info:
        cmd += ["-boot", "d", "-cdrom", vm_info["cdrom"]]
    else:
        cmd += ["-boot", "c"]

    # add hard disks
    if "hda" in vm_info:
        cmd += ["-drive", "file=%s,if=virtio" % vm_info["hda"]]

    if "hdb" in vm_info:
        cmd += ["-drive", "file=%s,if=virtio" % vm_info["hdb"]]

    # Bridged virtual guest NIC
    cmd += ["-net", "nic,model=virtio"]
    cmd += ["-net", "tap,ifname=%s,script=no" % INVIRT_TAP_NAME]

    return cmd, pidfile

def pin_cpus(cpu_mapping, pid):
    cmd = []

    cmd += [CUR_PATH + "/extern/qemu_affinity.py", "-k"]

    # Now, using the cpu mapping, pin everything where we want it
    vcpu_to_pcpu_map = cpu_mapping["vcpu_to_pcpu_map"]
    for vcpu in vcpu_to_pcpu_map:
        cmd += ["%d:%d" % (vcpu, vcpu_to_pcpu_map[vcpu])]

    cmd += ["--", "%d" % pid]
    cmd += ["&>", "/dev/null"]

    return cmd


def __delete_tap(name):
    cmd = ["tunctl", "-d", name]
    return issue_system_cmd(cmd)

def __create_tap(name):
    cmd = ["tunctl", "-t", name]
    return issue_system_cmd(cmd)

def __bring_up_device(name):
    cmd = ["ifconfig", name, "up"]
    return issue_system_cmd(cmd)

def __bring_up_device_addr(name, addr, netmask):
    cmd = ["ifconfig", name, addr, "netmask", netmask, "up"] 
    return issue_system_cmd(cmd)

def create_tap(name):
    __delete_tap(name)
    ret = __create_tap(name)
    if ret != 0:
        __delete_tap(name)
        return ret

    # bring it up
    ret = __bring_up_device(name)
    if ret != 0:
        __delete_tap(name)

    return ret

# init TAP device needed for invrun/invd communication
def init_tap():

    # Find bridge
    try:
        
        
        br = brctl.getbr(INVIRT_BR_NAME)
    except pybrctl.BridgeException as e: 
        print("No bridge present --- did you run bridge.sh?")
        return -1
        br = brctl.addbr(INVIRT_BR_NAME)
        pass

    # Create or find TAP device
    ifs = br.getifs()
    if INVIRT_TAP_NAME not in ifs:
        # Create TAP device
        ret = create_tap(INVIRT_TAP_NAME)
        if ret != 0:
            return ret

        # add to bridge
        try:
            br.addif(INVIRT_TAP_NAME)
        except pybrctl.BridgeException as e:
            print("Failed to add TAP dev to bridge: %s" % e)
            return -1

    return 0

def __reserve_hugetlbfs_node(node, page_size_kb, mem_size_mb):
    path = HUGETLBFS_PATH(node, page_size_kb)
    nr_hugepages = (mem_size_mb * 1024*1024) / (page_size_kb * 1024)

    cmd = ["echo", "%d" % nr_hugepages, ">", path]
    ret = issue_system_cmd(cmd)
    if ret != 0:
        return ret

    # ensure we reserved what we requested
    cmd = ["cat", path]
    nr_hugepages_avl = int(subprocess.check_output(cmd))
    if nr_hugepages_avl != nr_hugepages:
        print("Tried to reserve %d hugepages on numa node %d" \
            ", but only %d could be reserved" % \
            (nr_hugepages, node, nr_hugepages_avl), \
            file=sys.stderr
        )

        return -1

    return 0

# setup hugetlbfs 
def setup_hugetlbfs(vm_info):
    if "hugetlbfs" not in vm_info:
        return 0

    hugetlbfs_info = vm_info["hugetlbfs"]
    try:
        numa_nodes = hugetlbfs_info["numa_nodes"]
        mem_size_mb = hugetlbfs_info["memory_size_mb"]
        page_size_kb = hugetlbfs_info["page_size_kb"]
    except KeyError as e:
        print("Invalid 'hugetlbfs' config")
        return -1

    for node in numa_nodes:
        ret =__reserve_hugetlbfs_node(node, page_size_kb, mem_size_mb)
        if ret != 0:
            return -1

    return 0


"""
   Sanity check the VM cfg for the VM
"""
def parse_vm_config(vm_json):
    try:
        with open(vm_json, "r") as f:
            vm_data = json.load(f)
    except IOError as e:
        print("Could not open file %s" % vm_json)
        return None
    except ValueError as e:
        print("Could not parse file %s" % vm_json)
        return None

    if  "id" not in vm_data or\
        "core_list" not in vm_data or\
        "memory_size_mb" not in vm_data or\
        "hda" not in vm_data:
        print("Invalid format for %s" % vm_json)
        return None

    return vm_data

"""
   Sanity check the topology
"""
def parse_topo_config(topo_json):
    try:
        with open(topo_json, "r") as f:
            topo_data = json.load(f)
    except IOError as e:
        print("Could not open file %s" % topo_json)
        return None
    except ValueError as e:
        print("Could not parse file %s" % topo_json)
        return None

    # Parse out the required config parameters
    try:
        processor_info = topo_data["processor_info"]

        if "num_processors" not in processor_info or\
            "num_sockets" not in processor_info or\
            "cores_per_socket" not in processor_info or\
            "hw_threads_per_core" not in processor_info:
            print("Invalid format for %s" % topo_json)
            sys.exit(1)

        local_topo_info = {}
        local_topo_info["processor_info"] = processor_info
        os_core_map = {}

        # Build a reverse map from os_core to everything else
        processor_list = topo_data["processor_list"] 
        for p in processor_list:
            assert p["os_core"] not in os_core_map
            os_core_map[p["os_core"]] = {
                "socket"    : p["socket"],
                "core"      : p["core"],
                "hw_thread" : p["hw_thread"]
            }

        local_topo_info["os_core_map"] = os_core_map

    except KeyError:
        print("Invalid format for %s" % topo_json)
        return None

    return local_topo_info


def destroy(vm_info):
    pidfile = pidfile_of_vm(vm_info)
    if not os.path.exists(pidfile):
        print("Pidfile does not exist -- cannot kill VM")
        return -1

    # Send message to invd
    vm_id = vm_info['id']
    st = invd_del_vm(vm_info['id'])
    if st != 0:
        print("invd DEL_VM command failed")
        return -1

    pid = pid_of_vm(pidfile)
    cmd = ["kill", "-s", "TERM", "%d" % pid]
    st = issue_system_cmd(cmd)
    if st != 0:
        print("Could not kill VM")
        return st

    os.remove(pidfile)
    
    print("VM 'vm-%d' killed" % vm_id)
    return 0

def boot(vm_info, topo_info):
    st = init_tap()
    if st != 0:
        return -1

    st = setup_hugetlbfs(vm_info)
    if st != 0:
        return -1

    cpu_mapping = generate_cpu_mapping(topo_info, vm_info["core_list"])
    uuid_str = str(uuid.uuid4())
    cmd, pid_file = build_qemu_cmd_line(vm_info, cpu_mapping, uuid_str)

    # Send message to invd
    st = invd_new_vm(uuid_str, vm_info['id'])
    if st != 0:
        print("invd NEW_VM command failed")
        return -1

    # Startup VM
    print("Starting VM .... (this may take a few seconds)")

    ret = issue_system_cmd(cmd)
    if ret != 0:
        print("Could not start VM")
        return -1

    pid = pid_of_vm(pid_file)
    cmd = pin_cpus(cpu_mapping, pid)
    ret = issue_system_cmd(cmd)
    if ret != 0:
        print("Could not set VM CPU affinity; " \
            "VM running but not pinned")

    vm_id = vm_info['id']
    print("VM 'vm-%d' running, with a VNC server at localhost:%d" % (vm_id, 5900+vm_id))
    print("You need to assign it an IP address on the '%s' virtual network " \
        "to be compatible with invirt" % INVIRT_BR_NAME)

    return 0


def main(argc, argv, envp):
    cmd, args = parse_cmd_line(argv, argv)

    if cmd != 'boot' and cmd != 'destroy':
        print("Unknown command: '%s'" % cmd)
        usage(argv, exit=1)

    vm_json = args[0]
    vm_info = parse_vm_config(vm_json)
    if vm_info is None:
        return -1

    if cmd == 'boot':
        if len(args) < 2:
            usage(argv, exit=1)

        topo_json = args[1]
        topo_info = parse_topo_config(topo_json)
        if topo_info is None:
            return -1

        return boot(vm_info, topo_info)
    else:
        return destroy(vm_info)

if __name__ == "__main__":
    argv = sys.argv
    argc = len(argv)
    envp = os.environ
    sys.exit(main(argc, argv, envp))
