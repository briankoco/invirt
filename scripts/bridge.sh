#!/bin/bash

source ../config.sh

if [ $# -ne 1 ]; then
    echo "Usage: $0 <up/down>"
    exit 1
fi
cmd=$1

if [ $EUID -ne 0 ]; then
    echo "Must be run as root"
    exit 1
fi

if [ "$cmd" = "up" ]; then
    ip link add $INVIRT_BR_NAME type bridge
    ip addr add $INVIRT_BR_IP_ADDR/24 dev $INVIRT_BR_NAME
    ip link set up dev $INVIRT_BR_NAME
elif [ "$cmd" = "down" ]; then
    ip addr flush dev $INVIRT_BR_NAME
    ip link set down dev $INVIRT_BR_NAME
    ip link delete dev $INVIRT_BR_NAME
else
    echo "Usage: $0 <up/down>"
fi
