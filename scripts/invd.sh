#!/bin/bash

INVIRT=$(pwd)/..

source ${INVIRT}/config.sh

INVD_PATH="${INVIRT_TMP_DIR}/invd"
INVD_OUT="${INVIRT_TMP_DIR}/invd.stdout"
INVD_ERR="${INVIRT_TMP_DIR}/invd.stderr"
PIDFILE="${INVIRT_TMP_DIR}/invd.pid"

if [ ! -f $INVD_PATH ]; then
    echo "invd not installed; invoke make in the top-level directory first"
    exit
fi

if [ $# -ne 1 ]; then
    echo "Usage: $0 <start/stop>"
    exit
fi

cmd=$1

if [ "$cmd" = "start" ]; then
    if [ -f $PIDFILE ]; then
        echo "$PIDFILE exists -- is invd already running?"
        exit
    fi

    # bail if invh is not running
    if [ ! -c /dev/invh ]; then
        echo "/dev/invh does not exist -- have you loaded invh.ko?"
        exit
    fi

    rm -f $INVD_OUT
    rm -f $INVD_ERR

    $INVD_PATH 1>$INVD_OUT 2>$INVD_ERR &
    echo "$!" > $PIDFILE

    echo "invd started"
    echo "invd output logged to $INVD_OUT and $INVD_ERR"
elif [ "$cmd" = "stop" ]; then
    if [ ! -f $PIDFILE ]; then
        echo "$PIDFILE does not exist -- is invd running?"
        exit
    fi

    pid=$(cat $PIDFILE)
    kill -s TERM $pid 2>/dev/null
    rm $PIDFILE

    echo "invd stopped"
    echo "invd output logged to $INVD_OUT and $INVD_ERR"
else
    echo "Usage: $0 <start/stop>"
    exit
fi
