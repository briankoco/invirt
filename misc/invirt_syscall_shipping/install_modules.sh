#!/bin/bash

# Remove modules
if [ -c /dev/syscall_hook ]; then
    sudo rmmod syscall_hook
fi

if [ -c /dev/invirt_buffer ]; then
    sudo rmmod invirt_buffer
fi

sudo insmod ./invirt_syscall_buffer/mod/invirt_buffer.ko
sudo chmod 666 /dev/invirt_buffer

sudo insmod ./invirt_syscall_hook/mod/syscall_hook.ko
sudo chmod 444 /dev/syscall_hook

echo 'Starting up Syscall Server'
./invirt_syscall_buffer/user/syscall_server 0
