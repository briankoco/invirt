#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

int main(){
    char buf[10];
    int dev = open("/dev/syscall_hook", O_RDONLY);
    if (dev == -1){
	puts("error open dev");
	return -1;
    }
    int fd = open("daniel.txt", O_RDONLY);
    if (fd == -1){
	puts("error open");
	return -1;
    }
    memset(buf, 0, 10);
    int ret = read(fd, buf, 6);
    if (ret != 6){
	puts("error partial read");
	return -1;
    }

    puts(buf);

    ret = close(fd);
    if (ret == -1){
	puts("error close");
	return -1;
    }

    ret = close(dev);
    if (ret == -1){
	puts("error close dev");
	return -1;
    }
    return 0;
}
