#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/ioctl.h>

#include <syscall_hook.h>

void
usage(char * s)
{
    printf("Usage: %s <program name> <vm#>\n", s);
}

int main(int argc, char ** argv){
    int pipefd[2];
    int dev;
    unsigned int vm;
    char * executable;
    pid_t cpid;
    char buf;
    long status;

    if (argc != 3){
	usage(argv[0]);
	return -1;
    }

    executable = argv[1];
    vm = atoi(argv[2]);
    
    dev = open(SYSCALL_HOOK_MODULE_PATH, O_RDONLY);
    if (dev == -1){
	puts("error open dev");
	return -1;
    }

    if (pipe(pipefd) == -1){
	puts("Error: pipe");
	return -1;
    }

    if ((cpid = fork()) == 0){
	//child
	close(pipefd[1]);
	read(pipefd[0], &buf, 1); //issue a blocking call...
	close(pipefd[0]);
	execv(executable, (char * const []){executable, NULL});
	puts("execv error");
	return -1;
    }

    if (cpid == -1){
	puts("Error: fork");
	return -1;
    }
    
    //parent
    printf("Child PID: %u\n", cpid);
    
    close(pipefd[0]);

    struct inv_proc child = (struct inv_proc){cpid, vm};

    status = ioctl(dev, HOOK_IO_ADD_PROC, &child);
    if (status != 0){
        puts("Error issuing IOCTL");
        return -1;
    }
    
    write(pipefd[1], "a", 1);
    close(pipefd[1]);

    int s;
    wait(&s); //sync
    
    status = close(dev);
    if (status == -1){
	puts("error close dev");
	return -1;
    }
    return 0;
}
