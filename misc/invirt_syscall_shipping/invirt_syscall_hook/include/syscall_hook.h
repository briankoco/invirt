#ifndef __SYSCALL_HOOK_H__
#define __SYSCALL_HOOK_H__
#include <linux/ioctl.h>

struct inv_proc {
    pid_t pid;
    unsigned int vm;
};

#define SYSCALL_HOOK_MAGIC 'l'
#define HOOK_IO_ADD_PROC _IOW(SYSCALL_HOOK_MAGIC, 1, struct inv_proc *)

#define SYSCALL_HOOK_MODULE_NAME "syscall_hook"
#define SYSCALL_HOOK_MODULE_PATH "/dev/" SYSCALL_HOOK_MODULE_NAME

#endif /*__SYSCALL_HOOK_H__*/
