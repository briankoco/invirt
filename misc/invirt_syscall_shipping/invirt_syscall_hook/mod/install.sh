#!/bin/bash

# Remove module                                                                                                                                                                                             
if [ -c /dev/syscall_hook ]; then
    sudo rmmod syscall_hook
fi

if [ -c /dev/invirt_buffer ]; then
    sudo insmod syscall_hook.ko
    sudo chmod 444 /dev/syscall_hook
else
    echo 'ERROR: /dev/invirt_buffer not found'
fi




