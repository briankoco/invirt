#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <path to syscall_64.tbl>"
    exit 1
fi


#F1='/home/daniel/linux-4.14.109/arch/x86/entry/syscalls/syscall_64.tbl' # find ~/linux/arch/x86/entry/syscalls -name "syscall_64.tbl"
F1=$1

./build_syscalls.py $F1 > invirt_syscalls.c
