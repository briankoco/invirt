#ifndef __INVIRT_SYSCALLS__H__
#define __INVIRT_SYSCALLS__H__

/*Exported from invirt_syscalls.c*/
void install_invirt_syscalls(void ** syscall_table);
void uninstall_invirt_syscalls(void ** syscall_table);
/**/

#endif /* __INVIRT_SYSCALLS__H__ */
