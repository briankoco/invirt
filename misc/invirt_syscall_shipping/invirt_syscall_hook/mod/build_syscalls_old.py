#!/usr/bin/env python

import sys
import re

entry_re      = re.compile("(\d+)\s+(?:64|common|x32)\s+(\w+)\s+(?:__x64_)?(\w*)")
func_proto_re = re.compile("asmlinkage\s+long\s+(\w+)(\(.*?\));", re.DOTALL)
args_re       = re.compile("\*?(\w+)[,)]")

syscall_name_to_number = {}
syscall_stubs = set()

unnamed_params = ['int,', 'int)', 'long,', 'long)', '*,', '*)', 'size_t,', 'size_t)', 'unsigned,', 'unsigned)']

invirt_syscall_template =\
"""\
#define INVIRT_PTR_CAST asmlinkage long (*){args_one_line}
static asmlinkage long
invirt_{func_name}{args}{{
//    if (is_invirt_process(current->tgid)){{ 
    //    printk("INTERCEPTED: {func_name} SYS_NUM: {syscall_num}\\n");
//    }}
    return ((INVIRT_PTR_CAST)original_syscalls[{syscall_num}])({arg_string});
}}
#undef INVIRT_PTR_CAST
"""

invirt_syscalls_c =\
"""\
#include <linux/kernel.h>
#include <linux/unistd.h>
#include <linux/syscalls.h>

#include <syscall_hook_core.h>

#define INV_NUM_SYSCALLS 400

static void * original_syscalls[INV_NUM_SYSCALLS];
"""

installer_func_prototype=\
"""\
void
install_invirt_syscalls(void ** syscall_table){
"""

uninstaller_func_prototype=\
"""\
void
uninstall_invirt_syscalls(void ** syscall_table){
"""

if __name__ == '__main__':
    f1 = sys.argv[1]
    f2 = sys.argv[2]
    with open(f1) as file1:
        for line in file1.readlines():
            match = entry_re.match(line)
            if match:
                if match.groups(3) != None:
                    syscall_name_to_number[match.group(3)] = match.group(1)
                else:
                    syscall_name_to_number['sys_' + match.group(2)] = match.group(1)
    invirt_syscalls = []
    with open(f2) as file2:
        text = file2.read()
        matches = func_proto_re.findall(text)
        for match in matches:
            func_name  = match[0]
            if func_name in syscall_stubs:
                continue
            syscall_stubs.add(func_name)
            args       = match[1]
            arg_names  = args_re.findall(args)
            arg_string = ','.join(arg_names)
            for word in unnamed_params:
                if word in args:
                    tokens = args.split(',')
                    tokens[-1] = tokens[-1].rstrip(')')
                    for idx, token in enumerate(tokens):
                        tokens[idx] += ' arg' + str(idx)
                    tokens[-1] += ')'
                    args = ','.join(tokens)
                    arg_names  = args_re.findall(args)
                    arg_string = ','.join(arg_names)
                    break
            if 'void' in arg_names:
                arg_string = ''
            if func_name in syscall_name_to_number:
                func_str = invirt_syscall_template.format(func_name=func_name, args=args,
                                                          arg_string=arg_string, syscall_num=syscall_name_to_number[func_name],
                                                          args_one_line = re.sub(r'\s+',' ',args.replace('\n', ' ')))
                invirt_syscalls.append((int(syscall_name_to_number[func_name]), func_str, 'invirt_' + func_name))
    invirt_syscalls = sorted(invirt_syscalls, key=lambda x: x[0])
    print invirt_syscalls_c
    for pair in invirt_syscalls:
        print pair[1]
    #NOW PRINT INSTALLER FUNC
    print installer_func_prototype
    for pair in invirt_syscalls:
        print "    original_syscalls[{0}] = syscall_table[{0}];".format(pair[0])
        print "    syscall_table[{0}]     = (void *){1};".format(pair[0], pair[2])
    print "}\n"
    #NOW PRINT UNINSTALLER FUNC
    print uninstaller_func_prototype
    for pair in invirt_syscalls:
        print "    syscall_table[{0}] = original_syscalls[{0}];".format(pair[0])
    print "}"
            
                        
