#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/unistd.h>
#include <asm/paravirt.h> /*for read+write_cr0*/
#include <linux/kallsyms.h> /*for kall_sym_lookup*/
#include <linux/miscdevice.h>
#include <linux/mm_types.h>
#include <linux/atomic.h>
#include <linux/sched.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/delay.h> /*msleep temp fix*/
#include <linux/uaccess.h>


#include <invirt_syscalls.h>
#include <syscall_hook.h>


static unsigned long ** syscall_table;

/*Descriptor For Invirt Managed Processes*/
struct invirt_thread_group {

    pid_t tgid;

    unsigned int vm;
    spinlock_t lock; /* lock protects flags */
    atomic_t refcnt;
    volatile int flags;

    /* linkage on gbl_tg_hashtable */
    struct list_head tg_hashnode;
};

#define INVIRT_FLAG_DESTROYING      0x00010 /* being destroyed */
#define INVIRT_FLAG_DESTROYED       0x00020 /* 'being destroyed' finished */
#define INVIRT_FLAG_FLUSHING        0x00040 /* being flushed */

/*Hash Table Stuff*/
struct invirt_hashlist * gbl_tg_hashtable;

struct invirt_hashlist {
    rwlock_t lock;
    struct list_head list;
} ____cacheline_aligned;

#define INVIRT_TG_HASHTABLE_SIZE 8

static int
invirt_tg_hashtable_index(pid_t tgid)
{
    return ((unsigned int)tgid % INVIRT_TG_HASHTABLE_SIZE);
}

static void
invirt_tg_ref(struct invirt_thread_group * tg)
{
    WARN_ON(atomic_read(&(tg->refcnt)) <= 0);
    atomic_inc(&(tg->refcnt));
}

static struct invirt_thread_group *
invirt_tg_ref_by_tgid_nolock_internal(pid_t tgid,
                                      int   index,
                                      int   return_destroying)
{
    struct invirt_thread_group * tg;

    list_for_each_entry(tg, &(gbl_tg_hashtable[index].list), tg_hashnode) {
        if (tg->tgid == tgid) {
            if ((tg->flags & INVIRT_FLAG_DESTROYING)  &&
                (return_destroying == 0))
            {
                continue;
            }

            invirt_tg_ref(tg);
            return tg;
        }
    }

    return ERR_PTR(-ENOENT);
}


static void
invirt_tg_deref(struct invirt_thread_group * tg)
{
    WARN_ON(atomic_read(&(tg->refcnt)) <= 0);
    if (atomic_dec_return(&(tg->refcnt)) != 0)
        return;

    WARN_ON(!(tg->flags & INVIRT_FLAG_DESTROYED));

    /* last put of this tg .. tear it down */
    kfree(tg);
}

static struct invirt_thread_group *
__invirt_tg_ref_by_tgid(pid_t tgid,
                        int   return_destroying,
                        int   do_lock)
{
    struct invirt_thread_group * tg;
    int index;
    unsigned long flags;

    index = invirt_tg_hashtable_index(tgid);

    if (do_lock)
        read_lock_irqsave(&(gbl_tg_hashtable[index].lock), flags);

    tg = invirt_tg_ref_by_tgid_nolock_internal(
        tgid,
        index,
        return_destroying
    );

    if (do_lock)
        read_unlock_irqrestore(&(gbl_tg_hashtable[index].lock), flags);

    return tg;
}

#define invirt_tg_ref_by_tgid(t)            __invirt_tg_ref_by_tgid(t, 0, 1)
#define invirt_tg_ref_by_tgid_all(t)        __invirt_tg_ref_by_tgid(t, 1, 1)
#define invirt_tg_ref_by_tgid_nolock(t)     __invirt_tg_ref_by_tgid(t, 0, 0)
#define invirt_tg_ref_by_tgid_nolock_all(t) __invirt_tg_ref_by_tgid(t, 1, 0)

static void
invirt_tg_not_destroyable(struct invirt_thread_group * tg)
{
    atomic_set(&(tg->refcnt), 1);
}

static void
invirt_tg_destroyable(struct invirt_thread_group * tg)
{
    invirt_tg_deref(tg);
}

/**/

/* We Export this function so our syscall hooks can use it to
*  Separate Invirt Syscalls, from non Invirt ones
*/
int
is_invirt_process(pid_t pid, unsigned int * vm)
{
    struct invirt_thread_group * tg;
    tg = invirt_tg_ref_by_tgid(pid);
    if (!IS_ERR(tg)) {
	*vm = tg->vm;
        invirt_tg_deref(tg);
        return 1;
    }
    return 0;
}

/*                                                                                                                                                                
 * user open of the invirt driver                                                                                                                                                                          
 */
static int
syscall_hook_open(struct inode * inodep,
                  struct file  * filp)
{
    /* struct invirt_thread_group * tg; */
    /* int index; */
    /* unsigned long flags; */

    /* /\* if this has already been done, return silently *\/ */
    /* tg = invirt_tg_ref_by_tgid(current->tgid); */
    /* if (!IS_ERR(tg)) { */
    /*     invirt_tg_deref(tg); */
    /*     return 0; */
    /* } */

    /* /\* create tg *\/ */
    /* tg = kzalloc(sizeof(struct invirt_thread_group), GFP_KERNEL); */
    /* if (tg == NULL) */
    /*     return -ENOMEM; */

    /* tg->tgid = current->tgid; */
    /* INIT_LIST_HEAD(&(tg->tg_hashnode)); */
    /* spin_lock_init(&(tg->lock)); */
    /* /\* mutex_init(&(tg->mutex)); *\/ */

    /* invirt_tg_not_destroyable(tg); */

    /* /\* add tg to its hashlist *\/ */
    /* index = invirt_tg_hashtable_index(tg->tgid); */
    /* write_lock_irqsave(&(gbl_tg_hashtable[index].lock), flags); */
    /* list_add_tail(&(tg->tg_hashnode), &(gbl_tg_hashtable[index].list)); */
    /* write_unlock_irqrestore(&(gbl_tg_hashtable[index].lock), flags); */

    return 0;
}

/*
 * Destroy an invirt_thread_group
 */
static void
invirt_destroy_tg(struct invirt_thread_group * tg)
{
    invirt_tg_destroyable(tg);
    //invirt_tg_deref(tg);
}

void
invirt_teardown_tg(struct invirt_thread_group * tg)
{
    unsigned long flags;
    int index;

    spin_lock_irqsave(&(tg->lock), flags);
    WARN_ON(tg->flags & INVIRT_FLAG_DESTROYING);
    tg->flags |= INVIRT_FLAG_DESTROYING;
    spin_unlock_irqrestore(&(tg->lock), flags);

    /* Remove tg structure from its hash list */
    index = invirt_tg_hashtable_index(tg->tgid);
    write_lock_irqsave(&(gbl_tg_hashtable[index].lock), flags);
    list_del_init(&tg->tg_hashnode);
    write_unlock_irqrestore(&(gbl_tg_hashtable[index].lock), flags);

    spin_lock_irqsave(&(tg->lock), flags);
    WARN_ON(tg->flags & INVIRT_FLAG_DESTROYED);
    tg->flags |= INVIRT_FLAG_DESTROYED;
    spin_unlock_irqrestore(&(tg->lock), flags);

    /* if (tg->target_tgid != 0) */
    /*     invirt_detach_target_tgid(tg); */

#if 0
    /*
     * We don't call invirt_destroy_tg() here. We can't call
     * mmu_notifier_unregister() when the stack started with a
     * mmu_notifier_release() callout or we'll deadlock in the kernel
     * MMU notifier code.  invirt_destroy_tg() will be called when the
     * close of /dev/invirt occurs as deadlocks are not possible then.
     */
    invirt_tg_deref(tg);
#endif
    invirt_destroy_tg(tg);
}

static int
syscall_hook_flush(struct file  * filp,
                   fl_owner_t     owner)
{
    struct invirt_thread_group * tg;
    unsigned long flags;

    /* During a call to fork() there is a check for whether the parent process
     * has any pending signals. If there are pending signals, then the fork
     * aborts, and the child process is removed before delivering the signal
     * and starting the fork again. In that case, we can end up here, but since
     * we're mid-fork, current is pointing to the parent's task_struct and not
     * the child's. This would cause us to remove the parent's invirt mappings
     * by accident. We check here whether the owner pointer we have is the same
     * as the current->files pointer. If it is, or if current->files is NULL,
     * then this flush really does belong to the current process. If they don't
     * match, then we return without doing anything since the child shouldn't
     * have a valid invirt_thread_group struct yet.
     */
    if (current->files && current->files != owner)
        return 0;

    /*
     * invirt_flush() can get called twice for thread groups which inherited
     * /dev/invirt: once for the inherited fd, once for the first explicit use
     * of /dev/invirt. If we don't find the tg via invirt_tg_ref_by_tgid() we
     * assume we are in this type of scenario and return silently.
     *
     * Note the use of _all: we want to get this even if it's
     * destroying/destroyed.
     */
    tg = invirt_tg_ref_by_tgid_all(current->tgid);
    if (IS_ERR(tg))
        return 0;

    /* 
     * Two threads could have called invirt_flush at about the same time, and
     * thus invirt_tg_ref_by_tgid could return the same tg in both threads.
     * Guard against this.
     */
    spin_lock_irqsave(&tg->lock, flags);
    if (tg->flags & INVIRT_FLAG_FLUSHING) {
        spin_unlock_irqrestore(&tg->lock, flags);
        return 0;
    }
    tg->flags |= INVIRT_FLAG_FLUSHING;
    spin_unlock_irqrestore(&tg->lock, flags);

    invirt_teardown_tg(tg);

    invirt_tg_deref(tg);

#if 0
    /* BJK: we can't remove the tg from the lookup list yet, because of 
     * this call stack is possible:
     *  invirt_flush() ->
     *    invirt_destroy_tg() ->
     *      invirt_mmu_notifier_unlink() ->
     *        invirt_mmu_release() 
     *          invirt_mmu_release() will need to query the tg from the
     *          lookup list.
     *
     * We need to take an extra ref of the tg, so that we can pull it from the
     * hashlist after we call invirt_destroy_tg() below.
     */      

    invirt_tg_ref(tg);

    invirt_destroy_tg(tg);

    invirt_tg_deref(tg);
#endif
    return 0;
}

static long
syscall_hook_ioctl(struct file * filp,
                   unsigned int  cmd,
                   unsigned long arg)
{
    long status;
    
    switch (cmd) {
        case HOOK_IO_ADD_PROC:
            if (arg == 0) {
                status = -EINVAL;
                break;
            }
	    {
		struct invirt_thread_group * tg;
		int index;
		unsigned long flags;
		struct inv_proc buf;
		
		copy_from_user(&buf, (void *)arg, sizeof(struct inv_proc));

		/* if this has already been done, return silently */
		tg = invirt_tg_ref_by_tgid(buf.pid);
		if (!IS_ERR(tg)) {
		    invirt_tg_deref(tg);
		    break;
		}

		/* create tg */
		tg = kzalloc(sizeof(struct invirt_thread_group), GFP_KERNEL);
		if (tg == NULL){
		    status = -ENOMEM;
		    break;
		}
		

		tg->tgid = buf.pid;
		tg->vm   = buf.vm;
		INIT_LIST_HEAD(&(tg->tg_hashnode));
		spin_lock_init(&(tg->lock));

		invirt_tg_not_destroyable(tg);

		/* add tg to its hashlist */
		index = invirt_tg_hashtable_index(tg->tgid);
		write_lock_irqsave(&(gbl_tg_hashtable[index].lock), flags);
		list_add_tail(&(tg->tg_hashnode), &(gbl_tg_hashtable[index].list));
		write_unlock_irqrestore(&(gbl_tg_hashtable[index].lock), flags);		
	    }
	    status = 0;
            break;
	    
        default:
            status = -ENOIOCTLCMD;
            break;
    }
            
    return status;
}

static struct file_operations
syscall_hook_fops =
{
    .open           = syscall_hook_open,
    .flush          = syscall_hook_flush,
    .unlocked_ioctl = syscall_hook_ioctl,
};

static struct miscdevice
dev_handle =
{
    .minor = MISC_DYNAMIC_MINOR,
    .name = SYSCALL_HOOK_MODULE_NAME,
    .fops = &syscall_hook_fops
};

static int __init
syscall_hook_init(void)
{
    int status, i;

    gbl_tg_hashtable = kzalloc(
        sizeof(struct invirt_hashlist) * INVIRT_TG_HASHTABLE_SIZE,
        GFP_KERNEL
    );
    if (gbl_tg_hashtable == NULL)
        return -ENOMEM;

    for (i = 0; i < INVIRT_TG_HASHTABLE_SIZE; i++) {
        rwlock_init(&(gbl_tg_hashtable[i].lock));
        INIT_LIST_HEAD(&(gbl_tg_hashtable[i].list));
    }

    status = misc_register(&dev_handle);
    if (status != 0) {
        kfree(gbl_tg_hashtable);
        return status;
    }
    
    syscall_table = (unsigned long **) kallsyms_lookup_name("sys_call_table");

    write_cr0( read_cr0() & ~0x10000 ); BUG_ON((read_cr0() & 0x10000));
    install_invirt_syscalls((void **)syscall_table);
    write_cr0( read_cr0() | 0x10000 ); BUG_ON(!(read_cr0() & 0x10000));

    printk(KERN_INFO "FINISHED LOADING HOOK MODULE\n");
    return 0;
}

static void __exit
syscall_hook_exit(void)
{
    misc_deregister(&dev_handle);
    kfree(gbl_tg_hashtable);
    
    write_cr0( read_cr0() & ~0x10000 ); BUG_ON((read_cr0() & 0x10000));
    uninstall_invirt_syscalls((void **)syscall_table);
    write_cr0( read_cr0() | 0x10000 ); BUG_ON(!(read_cr0() & 0x10000));

    msleep(60000);
    printk("FINISHED UNLOADING HOOK MODULE\n");
}

module_init(syscall_hook_init);
module_exit(syscall_hook_exit);

MODULE_LICENSE("GPL");
