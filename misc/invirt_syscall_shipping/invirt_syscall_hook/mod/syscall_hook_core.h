#ifndef __SYSCALL_HOOK_CORE_H__
#define __SYSCALL_HOOK_CORE_H__

int is_invirt_process(pid_t pid, unsigned int * vm);

#endif /* __SYSCALL_HOOK_CORE_H__ */
