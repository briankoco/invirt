#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <poll.h>
#include <stdlib.h>

#include <invirt_buffer.h>
#include <shipped_syscall.h>

#include <string.h> //strerror
#include <errno.h>

#define NUM_FDS 1
#define TIMEOUT 1000 /*ms*/

void
usage(char * program){
    printf("Usage: %s <vm#>\n", program);
}

int main(int argc, char ** argv){
    int status;
    unsigned int vm = 0;
    struct shipped_syscall buf[4];
    struct pollfd fds[NUM_FDS];

    if (argc != 2){
	usage(argv[0]);
	return -1;
    }

    vm = atoi(argv[1]);

    int dev = open(INV_BUFFER_MODULE_PATH, O_RDWR);
    if (dev == -1){
	puts("Error Opening Device");
	puts(strerror(errno));
	return -1;
    }

    status = ioctl(dev, INV_IOCSVM, vm);
    if (status != 0){
	puts("Error issuing IOCTL");
	return -1;
    }

    int ret;
    fds[0] = (struct pollfd){dev, POLLIN, 0};

    while (1){
	status = poll(fds, NUM_FDS, TIMEOUT);
	if (status == -1){
	    puts("Error Poll");
	    puts(strerror(errno));
	    //goto out
	}

	if (fds[0].revents & POLLIN){
	    ret = read(dev, buf, 1);
	    if (ret != 1)
		puts("error reading");
	    else{
		printf("TGID: %u SYSCALL NUM: %u args: %lu %lu %lu %lu %lu %lu\n",
		       buf[0].tgid, buf[0].syscall_number, buf[0].arg0, buf[0].arg1,
		       buf[0].arg2, buf[0].arg3, buf[0].arg4, buf[0].arg5);
	    }
	}
    }

    status = close(dev);
    if (status == -1){
	puts("Error close");
	return -1;
    }

    return 0;
}
