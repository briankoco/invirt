#ifndef __INVIRT_BUFFER_H__
#define __INVIRT_BUFFER_H__
#include <linux/ioctl.h>

#define INVIRT_BUFFER_MAGIC 'k'
#define INV_IOCSVM _IOW(INVIRT_BUFFER_MAGIC, 1, unsigned int)

#define INVIRT_SYSCALL_BUFFER_MODULE_NAME "invirt_buffer"
#define INV_BUFFER_MODULE_PATH "/dev/" INVIRT_SYSCALL_BUFFER_MODULE_NAME

#endif /*__INVIRT_BUFFER_H__*/
