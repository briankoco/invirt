#ifndef __SHIPPED_SYSCALL_H__
#define __SHIPPED_SYSCALL_H__

struct shipped_syscall {
    pid_t tgid;
    unsigned int  syscall_number;
    unsigned long arg0;
    unsigned long arg1;
    unsigned long arg2;
    unsigned long arg3;
    unsigned long arg4;
    unsigned long arg5;
};

#endif /* __SHIPPED_SYSCALL_H__ */
