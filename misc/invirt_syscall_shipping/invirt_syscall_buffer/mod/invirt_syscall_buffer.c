#include <linux/spinlock.h>
#include <linux/gfp.h>
#include <linux/wait.h>
#include <linux/sched.h>

#include <shipped_syscall.h>
#include <invirt_syscall_buffer.h>

#define INVIRT_BUFFER_PAGES_ORDER 1
#define INVIRT_BUFFER_PAGES (1 << INVIRT_BUFFER_PAGES_ORDER)

extern struct invirt_syscall_buffer * syscall_buffers [];

int
init_invirt_buffer(struct invirt_syscall_buffer * syscall_buffer){
    unsigned long flags;
    spin_lock_irqsave(&syscall_buffer->lock, flags);//lock
    init_waitqueue_head(&syscall_buffer->waitq);
    syscall_buffer->enqueud_elements = 0;
    syscall_buffer->head             = 0;
    syscall_buffer->shipped_syscall_buffer = (struct shipped_syscall *) __get_free_pages(GFP_KERNEL, INVIRT_BUFFER_PAGES_ORDER);
    if (!syscall_buffer->shipped_syscall_buffer){
	spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
	return -ENOMEM;
    }
    syscall_buffer->buffer_capacity = (INVIRT_BUFFER_PAGES * PAGE_SIZE) / sizeof(struct shipped_syscall);
    spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
    return 0;
}

int
uninit_invirt_buffer(struct invirt_syscall_buffer * syscall_buffer){
    unsigned long flags;
    spin_lock_irqsave(&syscall_buffer->lock, flags);//lock
    if (syscall_buffer->shipped_syscall_buffer){
	free_pages((unsigned long)syscall_buffer->shipped_syscall_buffer, INVIRT_BUFFER_PAGES_ORDER);
    }
    spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
    return 0;
}

int
invirt_buffer_get_num_enqueud(struct invirt_syscall_buffer * syscall_buffer){
    unsigned long flags;
    unsigned int n;
    spin_lock_irqsave(&syscall_buffer->lock, flags);//lock
    n = syscall_buffer->enqueud_elements;
    spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
    return n;
}

/*Unblock processes waiting in Read or Poll*/
static inline void
wake_buffer(struct invirt_syscall_buffer * sb)
{
    wake_up_interruptible(&sb->waitq);
}

/*Export this function for use in the hooking module*/
EXPORT_SYMBOL(invirt_add_to_buffer);
void
invirt_add_to_buffer(unsigned int vm,
		     pid_t tgid,
		     unsigned int syscall_number,
		     unsigned long arg0,
		     unsigned long arg1,
		     unsigned long arg2,
		     unsigned long arg3,
		     unsigned long arg4,
		     unsigned long arg5)
{
    struct invirt_syscall_buffer * sb;

    sb = syscall_buffers[vm];
    if (sb == NULL){
	printk(KERN_DEBUG "NO BUFFER FOR REQUESTED VM\n");
	return;
    }

    invirt_buffer_enqeue_syscall(sb,tgid,syscall_number,arg0,arg1,
				 arg2,arg3,arg4,arg5);
    wake_buffer(sb);
}

int
invirt_buffer_enqeue_syscall(struct invirt_syscall_buffer * syscall_buffer,
			     pid_t tgid,
			     unsigned int syscall_number,
			     unsigned long arg0,
			     unsigned long arg1,
			     unsigned long arg2,
			     unsigned long arg3,
			     unsigned long arg4,
			     unsigned long arg5)
{
    unsigned int tail;
    unsigned long flags;
    spin_lock_irqsave(&syscall_buffer->lock, flags);//lock
    
    WARN_ON(syscall_buffer->enqueud_elements >= syscall_buffer->buffer_capacity);
    
    tail = (syscall_buffer->head + syscall_buffer->enqueud_elements) % syscall_buffer->buffer_capacity;
    syscall_buffer->shipped_syscall_buffer[tail] = (struct shipped_syscall){tgid, syscall_number,
							    arg0, arg1, arg2, arg3, arg4, arg5};
    ++syscall_buffer->enqueud_elements;
    
    spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
    return 0;
}

void
invirt_buffer_deqeue_syscall(struct invirt_syscall_buffer * syscall_buffer,
			     struct shipped_syscall *shipped_syscall_buf)
{
    unsigned long flags;
    spin_lock_irqsave(&syscall_buffer->lock, flags);//lock
    
    WARN_ON(syscall_buffer->enqueud_elements < 0);
    
    if (syscall_buffer->enqueud_elements == 0){
	spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
	return;
    }
    
    *shipped_syscall_buf = syscall_buffer->shipped_syscall_buffer[syscall_buffer->head];
    syscall_buffer->head = (syscall_buffer->head + 1) % syscall_buffer->buffer_capacity;
    --syscall_buffer->enqueud_elements;
    
    spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
    return;
}

void
invirt_set_buffer_vm(struct invirt_syscall_buffer * syscall_buffer,
		     unsigned int vm)
{
    unsigned long flags;
    spin_lock_irqsave(&syscall_buffer->lock, flags);//lock
    syscall_buffer->vm = vm;
    spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
}

void
invirt_buffer_mark_destroying(struct invirt_syscall_buffer * syscall_buffer)
{
    unsigned long flags;
    spin_lock_irqsave(&syscall_buffer->lock, flags);//lock
    syscall_buffer->flags |= INVIRT_SYSCALL_BUFFER_DESTROYING;
    spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
}
