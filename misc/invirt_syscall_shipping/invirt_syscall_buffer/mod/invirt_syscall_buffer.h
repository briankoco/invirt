#ifndef __INVIRT_SYSCALL_BUFFER_H__
#define __INVIRT_SYSCALL_BUFFER_H__

#include <linux/wait.h>
#include <linux/spinlock.h>
#include <shipped_syscall.h>

struct invirt_syscall_buffer {
    spinlock_t lock;
    wait_queue_head_t waitq;
    volatile int flags;
    unsigned int vm;
    int enqueud_elements;
    struct shipped_syscall * shipped_syscall_buffer;
    unsigned int head;
    unsigned int buffer_capacity;
};

#define INVIRT_SYSCALL_BUFFER_DESTROYING 0x00000001

int init_invirt_buffer(struct invirt_syscall_buffer * syscall_buffer);
int uninit_invirt_buffer(struct invirt_syscall_buffer * syscall_buffer);
int invirt_buffer_empty(struct invirt_syscall_buffer * syscall_buffer);
int invirt_buffer_enqeue_syscall(struct invirt_syscall_buffer * syscall_buffer,
				 pid_t tgid,
				 unsigned int syscall_number,
				 unsigned long arg0,
				 unsigned long arg1,
				 unsigned long arg2,
				 unsigned long arg3,
				 unsigned long arg4,
				 unsigned long arg5);
void invirt_buffer_deqeue_syscall(struct invirt_syscall_buffer * syscall_buffer,
				  struct shipped_syscall *shipped_syscall_buf);
void invirt_set_buffer_vm(struct invirt_syscall_buffer * syscall_buffer, unsigned int vm);
void invirt_buffer_mark_destroying(struct invirt_syscall_buffer * syscall_buffer);
int invirt_buffer_get_num_enqueud(struct invirt_syscall_buffer * syscall_buffer);
void
invirt_add_to_buffer(unsigned int vm,
		     pid_t tgid,
		     unsigned int syscall_number,
		     unsigned long arg0,
		     unsigned long arg1,
		     unsigned long arg2,
		     unsigned long arg3,
		     unsigned long arg4,
		     unsigned long arg5);
    
#endif /* __INVIRT_SYSCALL_BUFFER_H__ */
