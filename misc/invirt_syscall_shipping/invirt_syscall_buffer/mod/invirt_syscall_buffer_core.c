#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/unistd.h>
#include <linux/miscdevice.h>
#include <linux/mm_types.h>
#include <linux/atomic.h>
#include <linux/sched.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/wait.h>
#include <linux/uaccess.h>
#include <linux/poll.h>

#include <invirt_buffer.h>
#include <invirt_syscall_buffer.h>
#include <shipped_syscall.h>

/*This should probably be a hashtable or other dynamically sized data structure*/
#define INVIRT_MAX_VMS 64
struct invirt_syscall_buffer * syscall_buffers [INVIRT_MAX_VMS];

static int
invirt_syscall_buffer_open(struct inode * inodep,
                           struct file  * filp)
{
    int status;
    struct invirt_syscall_buffer * sb;
    
    sb = kzalloc(sizeof(struct invirt_syscall_buffer), GFP_KERNEL);
    if (sb == NULL)
        return -ENOMEM;

    spin_lock_init(&sb->lock);
    status = init_invirt_buffer(sb);
    filp->private_data = sb;

    return status;
}

static int
invirt_syscall_buffer_release(struct inode * inode,
			      struct file  * filp)
{
    struct invirt_syscall_buffer * sb;
    int status;

    sb = filp->private_data;

    syscall_buffers[sb->vm] = 0; //this needs a lock
    invirt_buffer_mark_destroying(sb);
    status = uninit_invirt_buffer(sb);
    kfree(sb);
    
    
    return 0;
}

static long
invirt_syscall_buffer_ioctl(struct file * filp,
			    unsigned int  cmd,
			    unsigned long arg)
{
    struct invirt_syscall_buffer * sb;
    int status = 0;

    sb = filp->private_data;

    switch (cmd) {
    case INV_IOCSVM:
	invirt_set_buffer_vm(sb, (unsigned int)arg);
	syscall_buffers[(unsigned int)arg] = sb;
	break;
    default:
	status = -ENOIOCTLCMD;
	break;
    }
    
    return status;
}

static ssize_t
invirt_syscall_buffer_read(struct file * filp,
			   char __user * buf,
			   size_t len,
			   loff_t * offset)
{
    struct invirt_syscall_buffer * sb;
    struct shipped_syscall buffer[len];
    unsigned int enqueud_elements;
    ssize_t ret;
    unsigned long unwritten;

    sb = filp->private_data;

    while ((enqueud_elements = invirt_buffer_get_num_enqueud(sb)) == 0){
	if (filp->f_flags & O_NONBLOCK)
	    return -EAGAIN;

	printk(KERN_DEBUG "Read: Going to sleep\n");

	if (wait_event_interruptible(sb->waitq, (invirt_buffer_get_num_enqueud(sb) > 0)))
	    return -ERESTARTSYS;

	printk(KERN_DEBUG "Read: waking from sleep\n");
    }


    ret = len < enqueud_elements ? len : enqueud_elements;
    {
	int i;
	for (i = 0; i < ret; ++i){
	    invirt_buffer_deqeue_syscall(sb, buffer + i);
	}
    }
    unwritten = copy_to_user(buf, buffer, ret * sizeof(struct shipped_syscall));
    if (unwritten != 0){
	/*Should probably try rewriting in a loop...*/
	printk(KERN_ALERT "SHORT WRITE IN invirt_syscall_buffer_read");
    }
	
    return ret;
}

unsigned int
invirt_syscall_buffer_poll(struct file * filp,
			   struct poll_table_struct * pt)
{
    /*RACE CONDITIONS?*/
    struct invirt_syscall_buffer * sb;
    unsigned int mask = 0;
    unsigned int enqueud_elements;

    sb = filp->private_data;

    poll_wait(filp, &sb->waitq, pt); //This might be a data race, but sb->waitq is equipped with its own lock

    enqueud_elements = invirt_buffer_get_num_enqueud(sb);
    if (enqueud_elements > 0)
	mask |= POLLIN | POLLRDNORM; //READABLE
    
    return mask;
}

static struct file_operations
invirt_syscall_buffer_fops =
{
    .open           = invirt_syscall_buffer_open,
    .poll           = invirt_syscall_buffer_poll,
    .read           = invirt_syscall_buffer_read,
    .release        = invirt_syscall_buffer_release,
    .unlocked_ioctl = invirt_syscall_buffer_ioctl,
};

static struct miscdevice
dev_handle =
{
    .minor = MISC_DYNAMIC_MINOR,
    .name  = INVIRT_SYSCALL_BUFFER_MODULE_NAME,
    .fops  = &invirt_syscall_buffer_fops
};

static int __init
syscall_hook_init(void)
{
    int status;

    status = misc_register(&dev_handle);
    if (status != 0) {    
        return status;
    }

    printk(KERN_INFO "FINISHED LOADING SYSCALL BUFFER MODULE\n");
    return 0;
}

static void __exit
syscall_hook_exit(void)
{
    misc_deregister(&dev_handle);

    printk("FINISHED UNLOADING SYSCALL BUFFER MODULE\n");
}

module_init(syscall_hook_init);
module_exit(syscall_hook_exit);

MODULE_LICENSE("GPL");
