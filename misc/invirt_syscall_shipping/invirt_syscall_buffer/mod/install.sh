#!/bin/bash

# Remove module

if [ -c /dev/invirt_buffer ]; then
    sudo rmmod invirt_buffer
fi

# Load module - lookup addr of syscall table
sudo insmod invirt_buffer.ko
sudo chmod 666 /dev/invirt_buffer


