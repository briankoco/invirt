/* 
 * Simple program to perform a stack relocation on x84-64 architectures
 * (c) Brian Kocoloski, 2019
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

#ifndef __x86_64
#error Unsuported architecture
#endif

#define PROC_SELF_MAPS "/proc/self/maps"

#define GET_RSP()\
    ({\
        unsigned long rsp;\
        asm("mov %%rsp, %0" : "=rm" (rsp));\
        rsp;\
    })

#define MOV_RSP(t)\
    ({\
        unsigned long to = t;\
        asm("mov %0, %%rsp" : "=rm" (to));\
    })

#define MEM_BARRIER() asm("" : : : "memory");


/*
 * find location of stack using /proc/self/maps
 */
static int 
find_stack_params(unsigned long * stack_base,
                  unsigned long * stack_size)
{
    unsigned long start, end, off, ino;
    char line[4096];
    char buf[4096];
    char * tmp;
    int ret, rc;
    FILE * f;

    f = fopen(PROC_SELF_MAPS, "r");
    if (f == NULL) {
        fprintf(stderr, "Failed to open " PROC_SELF_MAPS ": %s\n", 
            strerror(errno));
        return -1;
    }

    ret = -1;
    while (1) {
        tmp = fgets(line, 4096, f);
        if (!tmp)
            break;

        buf[0] = '\0';
        rc = sscanf(line, "%lx-%lx %*s %lx %*s %ld %255s", &start, &end, &off, &ino, buf);
        if (rc < 0) {
            fprintf(stderr, "Couldn't parse " PROC_SELF_MAPS ": %s\n", 
                strerror(errno));
            break;
        }

        if (strcmp(buf, "[stack]") == 0) {
            *stack_base = end;
            *stack_size = end-start;
            ret = 0;
            break;
        }
    }

    fclose(f);
    return ret;
}

static int
alloc_new_stack(unsigned long stack_size,
                void       ** new_stack)
{
    return posix_memalign(new_stack, (1ULL << 12), stack_size);
}

static inline void __attribute__((always_inline))
relocate_stack(unsigned long stack_top,
               unsigned long stack_size,
               unsigned long rsp,
               unsigned long new_stack_top)
{
    unsigned long stack_bottom, new_stack_bottom, rsp_off, new_rsp;

    stack_bottom = stack_top + stack_size;
    new_stack_bottom = new_stack_top + stack_size;

    /* rsp_off is the rsp offset from the 'bottom' of the stack */
    rsp_off = stack_bottom - rsp;
    new_rsp = new_stack_bottom - rsp_off;

    /*
     * Copy existing stack contents, which is everything from rsp_off to the
     * 'bottom' of the stack
     */
    memcpy(
        (void *)(new_stack_bottom - rsp_off),
        (void *)(stack_bottom - rsp_off),
        rsp_off
    ); 

    /* Move RSP to new location */
    MOV_RSP(new_rsp);
}

static void
print_stack(unsigned long stack_top,
            unsigned long stack_size,
            unsigned long rsp)
{
    unsigned long stack_bottom = stack_top + stack_size;
    unsigned long rsp_off      = stack_bottom - rsp;

    printf("Stack location:\n"
           "       0x%lx ---\n"
           "       0x%lx\n"
           "  RSP: 0x%lx (offset: 0x%lx)\n",
        stack_top, stack_bottom, rsp, rsp_off);

    printf("\n");
}

int 
main(int     argc,
     char ** argv,
     char ** envp)
{
    int status;
    unsigned long stack_base, stack_size, stack_top, new_stack_top;
    unsigned long old_rsp, new_rsp;

    /* find stack parameters */
    status = find_stack_params(&stack_base, &stack_size);
    if (status != 0) {
        fprintf(stderr, "Could not find stack base via /proc/self/maps\n");
        return -1;
    }
    stack_top = stack_base - stack_size;

    /* alloc new stack */
    status = alloc_new_stack(stack_size, (void **)&new_stack_top);
    if (status != 0) {
        fprintf(stderr, "Failed to allocate new stack: %s\n",
            strerror(errno));
        return -1;
    }

    /* 
     * Perform stack relocation
     *   - this function call __must__ be inlined
     *   - mem barriers to enable testing of function
     *     calls and data copies across stacks
     */
    {
        old_rsp = GET_RSP();
        printf("**** Before relocation ****\n");
        print_stack(stack_top, stack_size, old_rsp);

        MEM_BARRIER();
        relocate_stack(stack_top, stack_size, old_rsp, new_stack_top);
        MEM_BARRIER();

        new_rsp = GET_RSP();
        printf("\n**** After relocation ****\n");
        print_stack(new_stack_top, stack_size, new_rsp);
    }

    /* See if something on the stack survived the transition */
    {
        unsigned long argv_off, new_argv;

        argv_off = (unsigned long)argv - old_rsp;
        new_argv = new_rsp + argv_off;

        printf("old argv[0] = %s\n", argv[0]);
        printf("new argv[0] = %s\n", ((char **)new_argv)[0]);
    }

    return 0;
}
