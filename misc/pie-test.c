/* 
 * Simple program to test "-pie" compatibility
 * (c) Brian Kocoloski, 2019
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

int gbl = 10;

int 
main(int     argc,
     char ** argv,
     char ** envp)
{
    printf("Hello world from pie-test (gbl=%d)\n", gbl);
    fflush(stdout);
    return 0;
}
