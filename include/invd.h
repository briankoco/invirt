/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#ifndef __INVD_H__
#define __INVD_H__

#define INVD_UUID_STR_LEN   (8+4+4+4+12+4+1)

/* userspace only include */
#ifndef __KERNEL__

#include "invirt.h"

#include <sys/user.h>

#define INVD_IP_INFO        INVIRT_TMP_DIR "/invd-ip-info.txt"

#define INVD_OK             0
#define INVD_ERR            -1
#define INVD_ERR_BAD_TYPE   -2 
#define INVD_ERR_PAYLOAD    -3

typedef enum {
    INVD_NEW_TARGET,
    INVD_DEL_TARGET,
    INVD_NR_REQS
} invd_msg_t;

struct invd_request {
    invd_msg_t msg_type;
    int rc;
    size_t payload_len;

    union {
        struct {
            char uuid_str[INVD_UUID_STR_LEN];
            pid_t vm_tracer_tgid;
            pid_t vm_target_tgid;
            struct user_regs_struct regs;
        } new_target;

        struct {
            char uuid_str[INVD_UUID_STR_LEN];
            pid_t vm_tracer_tgid;
            pid_t vm_target_tgid;
        } del_target;
    };
};

#endif /* ! __KERNEL__ */

#endif /* __INVD_H__ */
