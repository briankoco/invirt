/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#ifndef __INVIRT_VCALL_H__
#define __INVIRT_VCALL_H__

/* interface for host <-> guest socket communication */

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#include "invirt.h"

typedef enum {
    INVIRT_VCALL_SYSCALL = 0,
    INVIRT_VCALL_PAGE_FAULT,
    INVIRT_VCALL_MMU_NOT,
    INVIRT_VCALL_NR_MSGS
} invirt_sysmsg_t;

typedef enum {
    INVIRT_MMU_NOT_INIT = 0,
    INVIRT_MMU_NOT_INVALIDATE,
    INVIRT_MMU_NR_MSGS
} invirt_mmumsg_t;

struct invirt_vcall {
    invirt_sysmsg_t msg_type;
    char uuid_str[INVD_UUID_STR_LEN];
    pid_t tracer_tgid;

    union {
        struct {
            unsigned long arg0;
            unsigned long arg1;
            unsigned long arg2;
            unsigned long arg3;
            unsigned long arg4;
            unsigned long arg5;
            unsigned long nr;
            long rc;
        } syscall;

        struct {
            unsigned long hva; /* input+output*/
            unsigned int pf_flags; /* input */
            unsigned long nr_pages; /* output */
            unsigned long pte_flags; /* output */
            long rc; /* output */
        } page_fault;

        struct {
            invirt_mmumsg_t msg_type;
            unsigned long vm_start;
            unsigned long nr_pages;
            int destroying; /* is the guest-side tg going away? */
            int rc;
        } mmu_not;
    };
};

#endif /* __INVIRT_VCALL_H__ */
