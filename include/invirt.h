/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#ifndef __INVIRT_H__
#define __INVIRT_H__

#ifndef __x86_64
#error Invirt only runs on x86_64 architectures
#endif

#include "invirt_config.h"
#include "invd.h"

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#define __INVIRT_PROC_NAME          "invirt"
#define __INVIRT_PROC_UUID_NAME     "system-uuid"
#define __INVIRT_PROC_VMADM_NAME    "vmadm"

#define INVG_MODULE_NAME "invg"
#define INVG_DEVICE_PATH "/dev/" INVG_MODULE_NAME
#define INVH_MODULE_NAME "invh"
#define INVH_DEVICE_PATH "/dev/" INVH_MODULE_NAME

/* ioctl codes */
#define INVG_IOC_ATTACH_TID         0x1000
#define INVG_IOC_DETACH_TID         0x1001
#define INVG_IOC_FAULT_PAGE         0x1002
#define INVG_IOC_FAULT_VMA          0x1003

#define INVH_IOC_GET_MAPFD          0x1000
#define INVH_IOC_TRACEME            0x1001
#define INVH_IOC_TRACEME_SOCKET     0x1

/* ioctl data structures */
struct invg_ioc_fault {
    /* in params */
    uint64_t vaddr;
    uint32_t vmf_flags;
    uint32_t sock_fd;

    /* out params */
    uint64_t base_vaddr;
    uint64_t paddr;
    uint64_t page_size;
    uint64_t pt_flags;
};

struct invh_ioc_traceme {
    char uuid_str[INVD_UUID_STR_LEN];
    pid_t tracer_pid;
    pid_t kvm_pid;
    int mode;

    struct {
        int conn_fd;
    } socket;
} __attribute__((packed));

/* proc codes + data structures */
#define INVH_PROC_VMADM_CREATE  0x1
#define INVH_PROC_VMADM_DELETE  0x2

struct invh_proc_vmadm {
    int cmd;
    char uuid_str[INVD_UUID_STR_LEN];
    int vmid;
} __attribute__((packed));

#ifndef __KERNEL__

#define PAGE_SIZE_4KB               (1ULL << 12)
#define PAGE_SIZE_2MB               (1ULL << 21)
#define PAGE_SIZE_1GB               (1ULL << 30)

#ifndef PAGE_SIZE
#define PAGE_SIZE                   PAGE_SIZE_4KB
#endif

#define PAGE_MASK_PS(ps)            (~(ps - 1)) 
#define PAGE_ALIGN_DOWN(addr, ps)	(addr & PAGE_MASK_PS(ps))
#define PAGE_ALIGN_UP(addr, ps)		((addr + (ps - 1)) & PAGE_MASK_PS(ps))

#define INVIRT_INVG_SHADOW          10
#define INVIRT_INVH_SHADOW          11

#define INVIRT_PROC_PATH            "/proc/" __INVIRT_PROC_NAME
#define INVIRT_PROC_PATH_UUID       INVIRT_PROC_PATH "/" __INVIRT_PROC_UUID_NAME 
#define INVIRT_PROC_PATH_VMADM      INVIRT_PROC_PATH "/" __INVIRT_PROC_VMADM_NAME

#endif /* ! __KERNEL__ */


#endif /* __INVIRT_H__ */
