#!/bin/bash

source ../config.sh

config_h="invirt_config.h"

cat <<EOT > $config_h
/* this is an auto-generated file. do not modify */

#ifndef __INVIRT_CONFIG_H__ 
#define __INVIRT_CONFIG_H__

EOT

envs=$(env | grep ^INVIRT_)

for e in ${envs[@]}; do
    k=$(echo "$e" | cut -d '=' -f 1)
    v=$(echo "$e" | cut -d '=' -f 2)

    if [[ $v =~ ^-?[0-9]+$ ]]; then
        echo "#define $k $v" >> $config_h
    else
        echo "#define $k \"$v\"" >> $config_h
    fi
done

echo >> $config_h
echo "#endif /* __INVIRT_CONFIG_H__ */" >> $config_h
