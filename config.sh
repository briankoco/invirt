#!/bin/bash

# invirt config
export INVIRT_TMP_DIR="/tmp/invirt"
export INVIRT_BR_NAME="ibr0"
export INVIRT_IP_PREFIX="10.88.0"
export INVIRT_BR_IP_ADDR="${INVIRT_IP_PREFIX}.1"
export INVIRT_BR_NETMASK="255.255.255.0"

# invd config
export INVIRT_INVD_IP_ADDR="${INVIRT_BR_IP_ADDR}"
export INVIRT_INVD_IP_PORT=8192
export INVIRT_INVD_IP_PORT_KERN=8193
