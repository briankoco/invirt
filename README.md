# invirt

invirt is a research prototype OS using virtualization to provide high
performance, security and isolation in cloud-based multi-tenant environments.

## Authors

* **Brian Kocoloski**
* **Daniel Zahka**
