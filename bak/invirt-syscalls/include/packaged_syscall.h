#ifndef __PACKAGED_SYSCALL_H__
#define __PACKAGED_SYSCALL_H__

struct packaged_syscall {
    unsigned long id; /*key*/
    pid_t tgid;
    unsigned int  syscall_number;
    unsigned long arg0;
    unsigned long arg1;
    unsigned long arg2;
    unsigned long arg3;
    unsigned long arg4;
    unsigned long arg5;
};

struct invirt_returned_syscall {
    unsigned long id; /*key*/
    unsigned long flags;
    unsigned long ret;
};

#define PACKAGED_SYSCALL_ACTIVE 0xABCDABCD

#endif /* __PACKAGED_SYSCALL_H__ */
