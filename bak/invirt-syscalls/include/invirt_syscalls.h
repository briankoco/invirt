#ifndef __INVIRT_SYSCALLS_H__
#define __INVIRT_SYSCALLS_H__
#include <linux/ioctl.h>

/*IOCTL*/
struct inv_ioc_ret_syscall_arg {
    pid_t tgid;
    unsigned long id; /* Routing # */
    unsigned long ret;
};

#define INVIRT_SYSCALLS_MAGIC 'k'
#define INV_IOC_ATTACH_PID  _IOW(INVIRT_SYSCALLS_MAGIC, 1, pid_t)
#define INV_IOC_RET_SYSCALL _IOW(INVIRT_SYSCALLS_MAGIC, 2, struct inv_ioc_ret_syscall_arg *)
/**/

#define INVIRT_SYSCALLS_HOOK_NODE_NAME "invirt_syscalls_hook"
#define INVIRT_SYSCALLS_HOOK_NODE_PATH "/dev/" INVIRT_SYSCALLS_HOOK_NODE_NAME 

#define INVIRT_SYSCALLS_BUFFER_NODE_NAME "invirt_syscalls_buffer"
#define INVIRT_SYSCALLS_BUFFER_NODE_PATH "/dev/" INVIRT_SYSCALLS_BUFFER_NODE_NAME 

#endif /*__INVIRT_SYSCALLS_H__*/
