#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/ioctl.h>

#include <sys/syscall.h>

#include <invirt_syscalls.h>

void
usage(char * s)
{
    printf("Usage: %s <program name>\n", s);
}

int main(int argc, char ** argv){
    int dev;
    char * executable;
    long status;

    if (argc != 2){
	usage(argv[0]);
	return -1;
    }

    executable = argv[1];

    printf("PID: %u\n", getpid());
        
    dev = open(INVIRT_SYSCALLS_HOOK_NODE_PATH, O_RDONLY); //should block
    if (dev == -1){
	puts("error open dev");
	return -1;
    }

    printf("PID: %u\n", syscall(39));

    return 0;    
}
