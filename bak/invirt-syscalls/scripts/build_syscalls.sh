#!/bin/bash

F1='/home/daniel/linux-4.14.109/arch/x86/entry/syscalls/syscall_64.tbl' # find ~/linux/arch/x86/entry/syscalls -name "syscall_64.tbl"
F2='../mod/syscall_hooks.c'

echo "USING FILE ${F1}"
echo "TO GENERATE"
echo "${F2}" 

./build_syscalls.py $F1 > $F2
