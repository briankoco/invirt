#!/usr/bin/env python

import sys
import re

entry_re = re.compile("(\d+)\s+(?:64|common)\s+(\w+)\s+(?:__x64_)?(\w*)")

invirt_syscall_template =\
"""\
static asmlinkage long
{func_name}(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){{
    if (is_invirt_process(current->tgid)){{ 
        return invirt_do_syscall(current->tgid,{syscall_num},arg0,arg1,arg2,arg3,arg4,arg5);
    }}
    return ((INVIRT_PTR_CAST)original_syscalls[{syscall_num}])(arg0, arg1, arg2, arg3, arg4, arg5);
}}
"""

invirt_syscalls_c =\
"""\
#include <linux/kernel.h>
#include <linux/unistd.h>
#include <linux/sched.h>

#include <invirt_syscalls_core.h>
#include <invirt_syscalls_buffer.h>

#define INV_NUM_SYSCALLS 400

static void * original_syscalls[INV_NUM_SYSCALLS];

#define INVIRT_PTR_CAST asmlinkage long (*) (long arg0, long arg1, long arg2,long arg3, long arg4, long arg5)

"""

undef_ptr_cast = "#undef INVIRT_PTR_CAST\n"

installer_func_prototype=\
"""\
void
install_invirt_syscalls(void ** syscall_table){
"""

uninstaller_func_prototype=\
"""\
void
uninstall_invirt_syscalls(void ** syscall_table){
"""

if __name__ == '__main__':
    f1 = sys.argv[1]
    invirt_syscalls = []
    with open(f1) as file1:
        for line in file1.readlines():
            match = entry_re.match(line)
            if match:
                syscall_num = match.group(1)
                func_name   = 'invirt_sys_' + syscall_num
                func_str = invirt_syscall_template.format(syscall_num = syscall_num, func_name = func_name)
                invirt_syscalls.append((syscall_num, func_str, func_name))
    invirt_syscalls = sorted(invirt_syscalls, key=lambda x: int(x[0]))
    print invirt_syscalls_c
    for pair in invirt_syscalls:
        print pair[1]
    print undef_ptr_cast
    #NOW PRINT INSTALLER FUNC
    print installer_func_prototype
    for pair in invirt_syscalls:
        print "    original_syscalls[{0}] = syscall_table[{0}];".format(pair[0])
        print "    syscall_table[{0}]     = (void *){1};".format(pair[0], pair[2])
    print "}\n"
    #NOW PRINT UNINSTALLER FUNC
    print uninstaller_func_prototype
    for pair in invirt_syscalls:
        print "    syscall_table[{0}] = original_syscalls[{0}];".format(pair[0])
    print "}"
            
                        
