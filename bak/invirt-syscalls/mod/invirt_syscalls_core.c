#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/unistd.h>
#include <asm/paravirt.h> /*for read+write_cr0*/
#include <linux/kallsyms.h> /*for kall_sym_lookup*/
#include <linux/miscdevice.h>
#include <linux/mm_types.h>
#include <linux/atomic.h>
#include <linux/sched.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/delay.h> /*msleep temp fix*/
#include <linux/uaccess.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include <linux/cdev.h>
#include <linux/spinlock.h>
#include <linux/sched/signal.h>

#include <syscall_hooks.h> //for hook and unhook functions
#include <invirt_syscalls.h> //device path + ioctl
#include <invirt_syscalls_buffer.h>
#include <packaged_syscall.h>

/*The address of the syscall table provided by kall_sym_lookup*/
static unsigned long ** syscall_table;

/*Processes that open dev/invirt_syscall_hook will sleep on this queue
  until invirt_buffer_exists(current->tgid) is true

  whenever a new buffer is created by /dev/invirt_syscall_buffer ioctl
  INV_IOC_ATTACH_PID, the queue is woken up
*/
static DECLARE_WAIT_QUEUE_HEAD(invirt_buffer_readerq);


/*We use a hashtable to keep track of all processes that are managed by invirt
  and require syscall interception. All of the table internals are the same as
  those used in the other /dev/invirt device.

  Nodes are added to the table from /dev/invirt_syscalls's ioctl cmd: INV_IOC_ATTACH_PID
*/

/*Hash Table Stuff*/
struct invirt_hashlist * gbl_sb_hashtable;

struct invirt_hashlist {
    rwlock_t lock;
    struct list_head list;
} ____cacheline_aligned;

#define INVIRT_SB_HASHTABLE_SIZE 8

static int
invirt_sb_hashtable_index(pid_t tgid)
{
    return ((unsigned int)tgid % INVIRT_SB_HASHTABLE_SIZE);
}

static void
invirt_sb_ref(struct invirt_syscall_buffer * sb)
{
    WARN_ON(atomic_read(&sb->refcnt) <= 0);
    atomic_inc(&sb->refcnt);
}

static struct invirt_syscall_buffer *
invirt_sb_ref_by_tgid_nolock_internal(pid_t tgid,
                                      int   index,
                                      int   return_destroying)
{
    struct invirt_syscall_buffer * sb;

    list_for_each_entry(sb, &(gbl_sb_hashtable[index].list), sb_hashnode) {
        if (sb->tgid == tgid) {
            if ((sb->flags & INVIRT_FLAG_DESTROYING)  &&
                (return_destroying == 0))
            {
                continue;
            }

            invirt_sb_ref(sb);
            return sb;
        }
    }

    return ERR_PTR(-ENOENT);
}

static void
invirt_sb_deref(struct invirt_syscall_buffer * sb)
{
    WARN_ON(atomic_read(&sb->refcnt) <= 0);
    if (atomic_dec_return(&sb->refcnt) != 0)
        return;

    WARN_ON(!(sb->flags & INVIRT_FLAG_DESTROYED));

    printk(KERN_INFO "freeing buffer for pid: %d\n", sb->tgid);
    
    /* last put of this sb .. tear it down */
    uninit_invirt_buffer(sb);
    kfree(sb);
}

static struct invirt_syscall_buffer *
__invirt_sb_ref_by_tgid(pid_t tgid,
                        int   return_destroying,
                        int   do_lock)
{
    struct invirt_syscall_buffer * sb;
    int index;
    unsigned long flags;

    index = invirt_sb_hashtable_index(tgid);

    if (do_lock)
        read_lock_irqsave(&(gbl_sb_hashtable[index].lock), flags);

    sb = invirt_sb_ref_by_tgid_nolock_internal(
        tgid,
        index,
        return_destroying
    );

    if (do_lock)
        read_unlock_irqrestore(&(gbl_sb_hashtable[index].lock), flags);

    return sb;
}

#define invirt_sb_ref_by_tgid(t)            __invirt_sb_ref_by_tgid(t, 0, 1)
#define invirt_sb_ref_by_tgid_all(t)        __invirt_sb_ref_by_tgid(t, 1, 1)
#define invirt_sb_ref_by_tgid_nolock(t)     __invirt_sb_ref_by_tgid(t, 0, 0)
#define invirt_sb_ref_by_tgid_nolock_all(t) __invirt_sb_ref_by_tgid(t, 1, 0)

static void
invirt_sb_not_destroyable(struct invirt_syscall_buffer * sb)
{
    atomic_set(&(sb->refcnt), 1);
}

static void
invirt_sb_destroyable(struct invirt_syscall_buffer * sb)
{
    invirt_sb_deref(sb);
}

static void
invirt_destroy_sb(struct invirt_syscall_buffer * sb)
{
    invirt_sb_destroyable(sb);
}

static void
invirt_teardown_sb(struct invirt_syscall_buffer * sb)
{
    unsigned long flags;
    int index;

    spin_lock_irqsave(&(sb->lock), flags);
    WARN_ON(sb->flags & INVIRT_FLAG_DESTROYING);
    sb->flags |= INVIRT_FLAG_DESTROYING;
    spin_unlock_irqrestore(&(sb->lock), flags);

    /* Remove sb structure from its hash list */
    index = invirt_sb_hashtable_index(sb->tgid);
    write_lock_irqsave(&(gbl_sb_hashtable[index].lock), flags);
    list_del_init(&sb->sb_hashnode);
    write_unlock_irqrestore(&(gbl_sb_hashtable[index].lock), flags);

    spin_lock_irqsave(&(sb->lock), flags);
    WARN_ON(sb->flags & INVIRT_FLAG_DESTROYED);
    sb->flags |= INVIRT_FLAG_DESTROYED;
    spin_unlock_irqrestore(&(sb->lock), flags);

    invirt_destroy_sb(sb); /* -1 for hash table ref removal */
}
/**/


/*This function gets called from all of the syscall hooks.*/
unsigned long
invirt_do_syscall(pid_t tgid,
		  unsigned int syscall_number,
		  unsigned long arg0,
		  unsigned long arg1,
		  unsigned long arg2,
		  unsigned long arg3,
		  unsigned long arg4,
		  unsigned long arg5)
{
    /* struct invirt_thread_group * tg; */
    struct invirt_syscall_buffer * sb;
    unsigned long id;
    unsigned long ret;

    sb = invirt_sb_ref_by_tgid(tgid);
    if (IS_ERR(sb)) {
	WARN_ON(1);
	printk(KERN_INFO "Trying add to untracked buffer\n");
        return 0; //should really just blow up or something
    }

    id = invirt_buffer_enqeue_syscall(sb,tgid,syscall_number,arg0,arg1,
				 arg2,arg3,arg4,arg5);
    printk(KERN_DEBUG "Enqueing Syscall: tgid: %d sys_num: %u id: %lu\n", tgid, syscall_number, id);

    /*Tell the buffer to ship this up*/
    wake_up_interruptible(&sb->waitq);

    while (invirt_syscall_returned(sb, id) == 0){
	if (wait_event_interruptible(sb->syscall_waitq, (invirt_syscall_returned(sb, id) == 1))){
 	    /* WARN_ON(1); */
	    /* printk(KERN_EMERG "WOKEN BY SIGNAL!!\n"); */
	    /*Sometimes we get pending signals and I haven't figured out 
              what they are exactly or how to deal with them*/
	    if (signal_pending(current)){
		printk(KERN_EMERG "PENDING SIGNAL\n");
		invirt_sb_deref(sb);
		return EINTR;
	    }
	}
    }

    ret = invirt_read_returned(sb, id);

    printk(KERN_DEBUG "Return Syscall: tgid: %d sys_num: %d id: %ld ret: %ld\n", tgid, syscall_number, id, ret);
    
    invirt_sb_deref(sb); /* -1 for local ref */

    return ret;
}

/* We Export this function so our syscall hooks can use it to
 * Separate Invirt Syscalls, from non Invirt ones
 */
int
is_invirt_process(pid_t pid)
{
    struct invirt_syscall_buffer * sb;
    unsigned long flags;
    int ret;
    
    sb = invirt_sb_ref_by_tgid(pid);
    if (!IS_ERR(sb)) {
	spin_lock_irqsave(&sb->lock, flags);
	ret = sb->flags & INVIRT_FLAG_ACTIVE;
	spin_unlock_irqrestore(&sb->lock, flags);
        invirt_sb_deref(sb);
        return ret;
    }
    return 0;
}

void
invirt_deactivate_buffer(pid_t pid)
{
    struct invirt_syscall_buffer * sb;
    unsigned long flags;
    
    sb = invirt_sb_ref_by_tgid(pid);
    if (!IS_ERR(sb)) {
	spin_lock_irqsave(&sb->lock, flags);
	sb->flags &= ~INVIRT_FLAG_ACTIVE;
	spin_unlock_irqrestore(&sb->lock, flags);
        invirt_sb_deref(sb);
    }
}

static int
invirt_buffer_exists(pid_t pid)
{
    struct invirt_syscall_buffer * sb;
    sb = invirt_sb_ref_by_tgid(pid);
    if (!IS_ERR(sb)) {
        invirt_sb_deref(sb);
        return 1;
    }
    return 0;
}

/*fops for hooking node*/

static int
invirt_syscalls_hook_release(struct inode * inode,
                             struct file  * filp)
{
    struct invirt_syscall_buffer * sb;
    unsigned long flags;

    /* Apparently this only gets called once, when the last copy of the 
     * Descriptor is closed...*/

    sb = invirt_sb_ref_by_tgid(current->tgid);

    spin_lock_irqsave(&sb->lock, flags);
    sb->flags &= ~INVIRT_FLAG_ACTIVE;
    sb->flags |=  INVIRT_FLAG_HANGUP;
    spin_unlock_irqrestore(&sb->lock, flags);

    wake_up_interruptible(&sb->waitq);
    
    invirt_sb_deref(sb);    /* -1 for local ref */

    return 0;
}

static int
invirt_syscalls_hook_open(struct inode * inodep,
			  struct file  * filp)
{
    //Data race? two descriptors for the same process could be added
    //to the table if someone decides to call this twice in a row
    //with the same pid....
    struct invirt_syscall_buffer * sb;
    unsigned long flags;

    printk(KERN_INFO "Invirt hook dev opened by tgid: %d\n", current->tgid);
    
    while ( invirt_buffer_exists(current->tgid) == 0 ){
	printk(KERN_INFO "syscall hook open going to sleep...\n");

	if (wait_event_interruptible(invirt_buffer_readerq, (invirt_buffer_exists(current->tgid) == 1)))
	    return -ERESTARTSYS; /*Here is another place where maybe dealing with pending signals is necessary*/

	printk(KERN_INFO "syscall hook open waking from sleep...\n");
    }

    sb = invirt_sb_ref_by_tgid(current->tgid);
    
    /*Set Flag to Active*/
    spin_lock_irqsave(&sb->lock, flags);
    sb->flags |= INVIRT_FLAG_ACTIVE;
    spin_unlock_irqrestore(&sb->lock, flags);

    invirt_sb_deref(sb); /* -1 for local ref */
            
    return 0;    
}

static long
invirt_syscalls_hook_ioctl(struct file * filp,
                   unsigned int  cmd,
                   unsigned long arg)
{
    long status;
    
    switch (cmd) {
    default:
	status = -ENOIOCTLCMD;
	break;
    }
            
    return status;
}

static struct file_operations
invirt_syscalls_hook_fops =
{
    .owner          = THIS_MODULE,
    .release        = invirt_syscalls_hook_release,
    .open           = invirt_syscalls_hook_open,
    .unlocked_ioctl = invirt_syscalls_hook_ioctl,
};
/*End fops for hooking node*/

/*fops for buffer node*/
 
static long
invirt_syscalls_buffer_ioctl(struct file * filp,
                             unsigned int  cmd,
                             unsigned long arg)
{
    long status = 0;
    
    switch (cmd) {
    case INV_IOC_ATTACH_PID:
	{
	    struct invirt_syscall_buffer * sb;
	    int index;
	    unsigned long flags;

	    /* Return if pid already exists in data structure*/
	    sb = invirt_sb_ref_by_tgid((pid_t) arg);
	    if ( !IS_ERR(sb) ){
		invirt_sb_deref(sb);
		status = -1; //not supporting this behavior right now
		break;
	    }
	    
	    /* create buffer */
	    sb = kzalloc(sizeof(struct invirt_syscall_buffer), GFP_KERNEL);
	    if (sb == NULL){
		status = -ENOMEM;
		break;
	    }

	    /* Init buffer intnernals */
	    status = init_invirt_buffer(sb, (pid_t) arg);
	    if (status){
		kfree(sb);
		break;
	    }

	    invirt_sb_not_destroyable(sb); /* +1 for hash table ref */

	    index = invirt_sb_hashtable_index(sb->tgid);
	    write_lock_irqsave(&(gbl_sb_hashtable[index].lock), flags);
	    list_add_tail(&(sb->sb_hashnode), &(gbl_sb_hashtable[index].list)); /*node is now live*/
	    write_unlock_irqrestore(&(gbl_sb_hashtable[index].lock), flags);
    
	    filp->private_data = sb;
	    invirt_sb_ref(sb); /* +1 for private_data ref */

	    printk(KERN_INFO "buffer for pid: %d is available\n", (pid_t) arg);

	    /*Wake up proc that blocked in open("/dev/invirt_syscall_hook"), now that buffer
              is ready*/
	    wake_up_interruptible(&invirt_buffer_readerq);
	}
	break;
    case INV_IOC_RET_SYSCALL:
	{
	    struct invirt_syscall_buffer * sb;
	    struct inv_ioc_ret_syscall_arg rs;

	    copy_from_user(&rs, (void*) arg, sizeof(struct inv_ioc_ret_syscall_arg));

	    sb = invirt_sb_ref_by_tgid(rs.tgid);
	    if ( IS_ERR(sb) ){
		WARN_ON(1);
		printk(KERN_EMERG "INV_IOC_RET_SYSCALL: Buffer does not exist\n");
		status = -1;
		break;
	    }

	    printk(KERN_INFO "Preparing to return tgid: %d ret: %lu id %lu\n", rs.tgid, rs.ret, rs.id);
	    invirt_add_returned(sb, rs.id, rs.ret);

	    invirt_sb_deref(sb); /*-1 for local ref*/
	}
	break;
    default:
	status = -ENOIOCTLCMD;
	break;
    }
            
    return status;
}
 
static ssize_t
invirt_syscalls_buffer_read(struct file * filp,
  			    char __user * buf,
			    size_t len,
			    loff_t * offset)
{
    struct invirt_syscall_buffer * sb;
    struct packaged_syscall buffer[len];
    unsigned int enqueud_elements;
    ssize_t ret;
    unsigned long unwritten;

    /* I've actually broken the read API here... len should always be bytes requested, and ret should be
     bytes written to the buffer. In this function both are in terms of struct packaged_syscalls...*/
    
    sb = filp->private_data;
    if (sb == NULL){
	printk(KERN_INFO "TRYING TO read FROM NULL BUFFER\n");
	return 1;
    }
    
    while ((enqueud_elements = invirt_buffer_get_num_enqueud(sb)) == 0){
	if (filp->f_flags & O_NONBLOCK)
	    return -EAGAIN;

	if (wait_event_interruptible(sb->waitq, (invirt_buffer_get_num_enqueud(sb) > 0)))
	    return -ERESTARTSYS; /*another place where dealing w/ pending signals may be necessary*/
    }

    ret = len < enqueud_elements ? len : enqueud_elements;
    {
	int i;
	for (i = 0; i < ret; ++i){
	    invirt_buffer_deqeue_syscall(sb, buffer + i);
	}
    }
    unwritten = copy_to_user(buf, buffer, ret * sizeof(struct packaged_syscall));
    if (unwritten != 0){
	/*Should probably try rewriting in a loop...*/
	printk(KERN_ALERT "SHORT WRITE IN invirt_syscall_buffer_read");
    }

    return ret;
}

static unsigned int
invirt_syscalls_buffer_poll(struct file * filp,
			    struct poll_table_struct * pt)
{
    /*RACE CONDITIONS?*/
    struct invirt_syscall_buffer * sb;
    unsigned int mask = 0;
    unsigned int enqueud_elements;
    unsigned long flags;

    sb = filp->private_data;
    if (sb == NULL){
	printk(KERN_INFO "TRYING TO poll FROM NULL BUFFER\n");
	return 1;
    }
    
    poll_wait(filp, &sb->waitq, pt); //This might be a data race, but sb->waitq is equipped with its own lock

    enqueud_elements = invirt_buffer_get_num_enqueud(sb);
    if (enqueud_elements > 0)
	mask |= POLLIN | POLLRDNORM; //READABLE

    spin_lock_irqsave(&sb->lock, flags);
    if (enqueud_elements == 0 && (sb->flags & INVIRT_FLAG_HANGUP))
	mask |= POLLHUP; //EOF
    spin_unlock_irqrestore(&sb->lock, flags);

    return mask;
}

static int
invirt_syscalls_buffer_release(struct inode * inode,
                               struct file  * filp)
{
    struct invirt_syscall_buffer * sb;

    /* Apparently this only gets called once, when the last copy of the 
     * Descriptor is closed...*/

    printk(KERN_INFO "buffer release called\n");
    
    /* sb = invirt_sb_ref_by_tgid(); */
    /* if (IS_ERR(sb)){ */
    /* 	/\*I'm guessing the first IOCTL call was never made */
    /* 	 *or failed*\/ */
    /* 	return -1; */
    /* } */

    sb = filp->private_data;
    if (!IS_ERR(sb)){
	invirt_sb_deref(sb); /* -1 for private_data ref */
    } else {
	WARN_ON(1);
	/*I'm guessing the first IOCTL call was never made
    	 *or failed*/
	return -1;
    } 

    invirt_teardown_sb(sb); /* -1 for hash table ref  */
    /* invirt_sb_deref(sb);    /\* -1 for local ref *\/ */

    printk(KERN_INFO "releasing buffer for pid: %d\n", current->tgid);
    
    return 0;
}

static int
invirt_syscalls_buffer_open(struct inode * inodep,
			    struct file  * filp)
{
    filp->private_data = 0;
    return 0;
}

static struct file_operations
invirt_syscalls_buffer_fops =
{
    .owner          = THIS_MODULE,
    .open           = invirt_syscalls_buffer_open,
    .release        = invirt_syscalls_buffer_release,
    .read           = invirt_syscalls_buffer_read,
    .poll           = invirt_syscalls_buffer_poll,
    .unlocked_ioctl = invirt_syscalls_buffer_ioctl,
};
/*End fops for buffer node*/

#define INV_FIRST_MINOR 0
#define INV_DEV_NODE_COUNT 2
#define INV_DEV_NAME "invirt_syscalls"

static dev_t first_dev;
static struct cdev *hook_cdev, *buffer_cdev;


static int __init
invirt_syscalls_init(void)
{
    int status, i;

    /* init hash table*/
    gbl_sb_hashtable = kzalloc(
        sizeof(struct invirt_hashlist) * INVIRT_SB_HASHTABLE_SIZE,
        GFP_KERNEL
    );
    if (gbl_sb_hashtable == NULL)
        return -ENOMEM;

    for (i = 0; i < INVIRT_SB_HASHTABLE_SIZE; i++) {
        rwlock_init(&(gbl_sb_hashtable[i].lock));
        INIT_LIST_HEAD(&(gbl_sb_hashtable[i].list));
    }

    /* device registration */
    status = alloc_chrdev_region(&first_dev, INV_FIRST_MINOR,
				 INV_DEV_NODE_COUNT, INV_DEV_NAME);
    if (status){
	printk(KERN_INFO "FAILED TO ALLOCATE DEV NUMBERS");
	goto error_a;
    }

    hook_cdev = cdev_alloc();
    hook_cdev->owner = THIS_MODULE;
    hook_cdev->ops = &invirt_syscalls_hook_fops;
    
    buffer_cdev = cdev_alloc();
    buffer_cdev->owner = THIS_MODULE;
    buffer_cdev->ops = &invirt_syscalls_buffer_fops;

    
    status = cdev_add(hook_cdev, MKDEV(MAJOR(first_dev), MINOR(first_dev)), 1);
    if (status){
	printk(KERN_INFO "cdev add hook dev failed\n");
	goto error_b;
    }

    status = cdev_add(buffer_cdev, MKDEV(MAJOR(first_dev), MINOR(first_dev) + 1), 1);
    if (status){
	printk(KERN_INFO "cdev add buffer dev failed\n");
	goto error_c;
    }

    /* syscall table overwrite */
    syscall_table = (unsigned long **) kallsyms_lookup_name("sys_call_table");

    write_cr0( read_cr0() & ~0x10000 ); BUG_ON((read_cr0() & 0x10000));
    install_invirt_syscalls((void **)syscall_table);
    write_cr0( read_cr0() | 0x10000 ); BUG_ON(!(read_cr0() & 0x10000));

    printk(KERN_INFO "FINISHED LOADING HOOK MODULE\n");
    return 0;

 error_c:
    cdev_del(hook_cdev);
 error_b:
    unregister_chrdev_region(first_dev, INV_DEV_NODE_COUNT);
 error_a:
    kfree(gbl_sb_hashtable);

    return status;
}

static void __exit
invirt_syscalls_exit(void)
{
    cdev_del(hook_cdev);
    cdev_del(buffer_cdev);
    unregister_chrdev_region(first_dev, INV_DEV_NODE_COUNT);
    
    kfree(gbl_sb_hashtable);
    
    write_cr0( read_cr0() & ~0x10000 ); BUG_ON((read_cr0() & 0x10000));
    uninstall_invirt_syscalls((void **)syscall_table);
    write_cr0( read_cr0() | 0x10000 ); BUG_ON(!(read_cr0() & 0x10000));

    msleep(100000);
    printk("FINISHED UNLOADING HOOK MODULE\n");
}

module_init(invirt_syscalls_init);
module_exit(invirt_syscalls_exit);

MODULE_LICENSE("GPL");
