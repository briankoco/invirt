#include <linux/kernel.h>
#include <linux/unistd.h>
#include <linux/sched.h>

#include <invirt_syscalls_core.h>
#include <invirt_syscalls_buffer.h>

#define INV_NUM_SYSCALLS 400

static void * original_syscalls[INV_NUM_SYSCALLS];

#define INVIRT_PTR_CAST asmlinkage long (*) (long arg0, long arg1, long arg2,long arg3, long arg4, long arg5)


static asmlinkage long
invirt_sys_0(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,0,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[0])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_1(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,1,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[1])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_2(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,2,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[2])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_3(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,3,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[3])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_4(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,4,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[4])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_5(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,5,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[5])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_6(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,6,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[6])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_7(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,7,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[7])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_8(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,8,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[8])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_9(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,9,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[9])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_10(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,10,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[10])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_11(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,11,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[11])(arg0, arg1, arg2, arg3, arg4, arg5);
}

/*sys_brk*/
static asmlinkage long
invirt_sys_12(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        /* return invirt_do_syscall(current->tgid,12,arg0,arg1,arg2,arg3,arg4,arg5); */
    }
    return ((INVIRT_PTR_CAST)original_syscalls[12])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_13(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,13,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[13])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_14(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,14,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[14])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_15(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,15,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[15])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_16(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,16,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[16])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_17(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,17,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[17])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_18(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,18,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[18])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_19(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,19,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[19])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_20(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,20,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[20])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_21(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,21,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[21])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_22(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,22,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[22])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_23(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,23,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[23])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_24(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,24,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[24])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_25(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,25,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[25])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_26(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,26,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[26])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_27(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,27,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[27])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_28(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,28,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[28])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_29(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,29,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[29])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_30(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,30,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[30])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_31(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,31,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[31])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_32(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,32,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[32])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_33(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,33,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[33])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_34(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,34,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[34])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_35(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,35,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[35])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_36(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,36,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[36])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_37(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,37,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[37])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_38(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,38,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[38])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_39(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,39,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[39])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_40(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,40,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[40])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_41(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,41,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[41])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_42(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,42,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[42])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_43(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,43,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[43])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_44(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,44,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[44])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_45(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,45,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[45])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_46(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,46,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[46])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_47(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,47,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[47])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_48(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,48,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[48])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_49(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,49,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[49])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_50(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,50,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[50])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_51(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,51,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[51])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_52(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,52,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[52])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_53(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,53,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[53])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_54(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,54,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[54])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_55(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,55,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[55])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_56(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,56,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[56])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_57(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,57,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[57])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_58(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,58,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[58])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_59(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,59,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[59])(arg0, arg1, arg2, arg3, arg4, arg5);
}

/*sys_exit*/
static asmlinkage long
invirt_sys_60(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        /* return invirt_do_syscall(current->tgid,60,arg0,arg1,arg2,arg3,arg4,arg5); */
	printk("sys_exit intercepted\n");
	invirt_deactivate_buffer(current->tgid);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[60])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_61(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,61,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[61])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_62(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,62,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[62])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_63(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,63,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[63])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_64(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,64,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[64])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_65(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,65,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[65])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_66(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,66,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[66])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_67(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,67,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[67])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_68(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,68,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[68])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_69(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,69,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[69])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_70(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,70,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[70])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_71(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,71,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[71])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_72(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,72,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[72])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_73(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,73,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[73])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_74(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,74,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[74])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_75(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,75,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[75])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_76(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,76,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[76])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_77(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,77,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[77])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_78(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,78,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[78])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_79(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,79,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[79])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_80(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,80,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[80])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_81(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,81,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[81])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_82(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,82,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[82])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_83(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,83,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[83])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_84(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,84,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[84])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_85(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,85,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[85])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_86(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,86,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[86])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_87(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,87,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[87])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_88(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,88,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[88])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_89(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,89,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[89])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_90(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,90,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[90])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_91(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,91,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[91])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_92(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,92,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[92])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_93(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,93,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[93])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_94(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,94,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[94])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_95(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,95,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[95])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_96(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,96,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[96])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_97(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,97,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[97])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_98(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,98,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[98])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_99(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,99,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[99])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_100(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,100,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[100])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_101(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,101,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[101])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_102(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,102,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[102])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_103(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,103,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[103])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_104(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,104,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[104])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_105(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,105,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[105])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_106(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,106,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[106])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_107(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,107,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[107])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_108(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,108,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[108])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_109(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,109,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[109])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_110(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,110,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[110])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_111(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,111,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[111])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_112(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,112,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[112])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_113(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,113,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[113])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_114(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,114,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[114])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_115(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,115,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[115])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_116(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,116,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[116])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_117(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,117,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[117])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_118(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,118,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[118])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_119(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,119,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[119])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_120(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,120,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[120])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_121(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,121,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[121])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_122(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,122,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[122])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_123(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,123,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[123])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_124(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,124,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[124])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_125(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,125,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[125])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_126(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,126,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[126])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_127(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,127,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[127])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_128(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,128,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[128])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_129(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,129,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[129])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_130(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,130,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[130])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_131(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,131,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[131])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_132(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,132,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[132])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_133(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,133,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[133])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_134(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,134,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[134])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_135(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,135,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[135])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_136(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,136,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[136])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_137(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,137,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[137])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_138(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,138,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[138])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_139(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,139,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[139])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_140(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,140,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[140])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_141(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,141,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[141])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_142(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,142,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[142])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_143(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,143,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[143])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_144(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,144,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[144])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_145(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,145,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[145])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_146(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,146,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[146])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_147(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,147,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[147])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_148(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,148,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[148])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_149(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,149,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[149])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_150(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,150,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[150])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_151(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,151,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[151])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_152(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,152,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[152])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_153(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,153,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[153])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_154(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,154,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[154])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_155(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,155,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[155])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_156(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,156,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[156])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_157(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,157,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[157])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_158(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,158,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[158])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_159(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,159,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[159])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_160(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,160,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[160])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_161(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,161,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[161])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_162(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,162,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[162])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_163(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,163,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[163])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_164(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,164,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[164])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_165(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,165,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[165])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_166(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,166,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[166])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_167(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,167,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[167])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_168(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,168,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[168])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_169(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,169,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[169])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_170(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,170,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[170])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_171(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,171,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[171])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_172(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,172,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[172])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_173(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,173,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[173])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_174(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,174,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[174])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_175(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,175,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[175])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_176(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,176,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[176])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_177(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,177,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[177])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_178(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,178,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[178])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_179(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,179,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[179])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_180(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,180,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[180])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_181(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,181,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[181])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_182(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,182,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[182])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_183(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,183,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[183])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_184(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,184,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[184])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_185(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,185,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[185])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_186(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,186,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[186])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_187(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,187,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[187])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_188(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,188,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[188])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_189(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,189,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[189])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_190(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,190,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[190])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_191(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,191,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[191])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_192(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,192,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[192])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_193(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,193,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[193])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_194(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,194,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[194])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_195(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,195,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[195])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_196(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,196,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[196])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_197(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,197,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[197])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_198(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,198,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[198])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_199(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,199,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[199])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_200(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,200,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[200])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_201(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,201,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[201])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_202(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,202,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[202])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_203(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,203,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[203])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_204(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,204,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[204])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_205(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,205,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[205])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_206(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,206,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[206])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_207(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,207,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[207])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_208(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,208,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[208])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_209(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,209,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[209])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_210(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,210,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[210])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_211(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,211,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[211])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_212(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,212,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[212])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_213(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,213,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[213])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_214(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,214,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[214])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_215(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,215,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[215])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_216(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,216,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[216])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_217(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,217,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[217])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_218(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,218,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[218])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_219(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,219,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[219])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_220(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,220,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[220])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_221(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,221,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[221])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_222(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,222,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[222])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_223(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,223,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[223])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_224(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,224,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[224])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_225(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,225,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[225])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_226(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,226,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[226])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_227(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,227,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[227])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_228(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,228,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[228])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_229(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,229,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[229])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_230(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,230,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[230])(arg0, arg1, arg2, arg3, arg4, arg5);
}

/*sys_exit_group*/
static asmlinkage long
invirt_sys_231(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        /* return invirt_do_syscall(current->tgid,231,arg0,arg1,arg2,arg3,arg4,arg5); */
	printk("sys_exit_group intercepted\n");
	invirt_deactivate_buffer(current->tgid);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[231])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_232(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,232,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[232])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_233(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,233,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[233])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_234(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,234,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[234])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_235(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,235,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[235])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_236(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,236,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[236])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_237(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,237,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[237])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_238(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,238,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[238])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_239(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,239,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[239])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_240(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,240,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[240])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_241(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,241,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[241])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_242(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,242,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[242])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_243(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,243,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[243])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_244(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,244,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[244])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_245(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,245,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[245])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_246(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,246,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[246])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_247(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,247,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[247])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_248(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,248,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[248])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_249(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,249,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[249])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_250(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,250,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[250])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_251(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,251,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[251])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_252(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,252,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[252])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_253(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,253,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[253])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_254(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,254,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[254])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_255(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,255,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[255])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_256(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,256,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[256])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_257(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,257,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[257])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_258(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,258,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[258])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_259(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,259,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[259])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_260(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,260,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[260])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_261(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,261,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[261])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_262(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,262,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[262])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_263(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,263,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[263])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_264(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,264,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[264])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_265(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,265,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[265])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_266(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,266,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[266])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_267(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,267,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[267])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_268(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,268,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[268])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_269(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,269,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[269])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_270(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,270,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[270])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_271(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,271,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[271])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_272(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,272,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[272])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_273(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,273,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[273])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_274(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,274,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[274])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_275(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,275,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[275])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_276(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,276,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[276])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_277(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,277,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[277])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_278(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,278,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[278])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_279(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,279,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[279])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_280(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,280,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[280])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_281(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,281,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[281])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_282(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,282,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[282])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_283(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,283,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[283])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_284(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,284,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[284])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_285(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,285,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[285])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_286(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,286,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[286])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_287(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,287,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[287])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_288(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,288,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[288])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_289(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,289,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[289])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_290(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,290,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[290])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_291(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,291,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[291])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_292(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,292,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[292])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_293(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,293,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[293])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_294(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,294,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[294])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_295(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,295,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[295])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_296(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,296,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[296])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_297(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,297,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[297])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_298(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,298,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[298])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_299(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,299,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[299])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_300(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,300,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[300])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_301(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,301,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[301])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_302(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,302,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[302])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_303(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,303,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[303])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_304(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,304,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[304])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_305(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,305,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[305])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_306(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,306,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[306])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_307(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,307,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[307])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_308(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,308,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[308])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_309(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,309,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[309])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_310(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,310,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[310])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_311(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,311,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[311])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_312(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,312,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[312])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_313(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,313,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[313])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_314(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,314,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[314])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_315(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,315,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[315])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_316(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,316,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[316])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_317(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,317,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[317])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_318(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,318,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[318])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_319(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,319,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[319])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_320(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,320,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[320])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_321(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,321,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[321])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_322(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,322,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[322])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_323(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,323,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[323])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_324(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,324,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[324])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_325(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,325,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[325])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_326(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,326,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[326])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_327(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,327,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[327])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_328(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,328,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[328])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_329(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,329,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[329])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_330(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,330,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[330])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_331(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,331,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[331])(arg0, arg1, arg2, arg3, arg4, arg5);
}

static asmlinkage long
invirt_sys_332(long arg0, long arg1, long arg2,long arg3, long arg4, long arg5){
    if (is_invirt_process(current->tgid)){ 
        return invirt_do_syscall(current->tgid,332,arg0,arg1,arg2,arg3,arg4,arg5);
    }
    return ((INVIRT_PTR_CAST)original_syscalls[332])(arg0, arg1, arg2, arg3, arg4, arg5);
}

#undef INVIRT_PTR_CAST

void
install_invirt_syscalls(void ** syscall_table){

    original_syscalls[0] = syscall_table[0];
    syscall_table[0]     = (void *)invirt_sys_0;
    original_syscalls[1] = syscall_table[1];
    syscall_table[1]     = (void *)invirt_sys_1;
    original_syscalls[2] = syscall_table[2];
    syscall_table[2]     = (void *)invirt_sys_2;
    original_syscalls[3] = syscall_table[3];
    syscall_table[3]     = (void *)invirt_sys_3;
    original_syscalls[4] = syscall_table[4];
    syscall_table[4]     = (void *)invirt_sys_4;
    original_syscalls[5] = syscall_table[5];
    syscall_table[5]     = (void *)invirt_sys_5;
    original_syscalls[6] = syscall_table[6];
    syscall_table[6]     = (void *)invirt_sys_6;
    original_syscalls[7] = syscall_table[7];
    syscall_table[7]     = (void *)invirt_sys_7;
    original_syscalls[8] = syscall_table[8];
    syscall_table[8]     = (void *)invirt_sys_8;
    original_syscalls[9] = syscall_table[9];
    syscall_table[9]     = (void *)invirt_sys_9;
    original_syscalls[10] = syscall_table[10];
    syscall_table[10]     = (void *)invirt_sys_10;
    original_syscalls[11] = syscall_table[11];
    syscall_table[11]     = (void *)invirt_sys_11;
    original_syscalls[12] = syscall_table[12];
    syscall_table[12]     = (void *)invirt_sys_12;
    original_syscalls[13] = syscall_table[13];
    syscall_table[13]     = (void *)invirt_sys_13;
    original_syscalls[14] = syscall_table[14];
    syscall_table[14]     = (void *)invirt_sys_14;
    original_syscalls[15] = syscall_table[15];
    syscall_table[15]     = (void *)invirt_sys_15;
    original_syscalls[16] = syscall_table[16];
    syscall_table[16]     = (void *)invirt_sys_16;
    original_syscalls[17] = syscall_table[17];
    syscall_table[17]     = (void *)invirt_sys_17;
    original_syscalls[18] = syscall_table[18];
    syscall_table[18]     = (void *)invirt_sys_18;
    original_syscalls[19] = syscall_table[19];
    syscall_table[19]     = (void *)invirt_sys_19;
    original_syscalls[20] = syscall_table[20];
    syscall_table[20]     = (void *)invirt_sys_20;
    original_syscalls[21] = syscall_table[21];
    syscall_table[21]     = (void *)invirt_sys_21;
    original_syscalls[22] = syscall_table[22];
    syscall_table[22]     = (void *)invirt_sys_22;
    original_syscalls[23] = syscall_table[23];
    syscall_table[23]     = (void *)invirt_sys_23;
    original_syscalls[24] = syscall_table[24];
    syscall_table[24]     = (void *)invirt_sys_24;
    original_syscalls[25] = syscall_table[25];
    syscall_table[25]     = (void *)invirt_sys_25;
    original_syscalls[26] = syscall_table[26];
    syscall_table[26]     = (void *)invirt_sys_26;
    original_syscalls[27] = syscall_table[27];
    syscall_table[27]     = (void *)invirt_sys_27;
    original_syscalls[28] = syscall_table[28];
    syscall_table[28]     = (void *)invirt_sys_28;
    original_syscalls[29] = syscall_table[29];
    syscall_table[29]     = (void *)invirt_sys_29;
    original_syscalls[30] = syscall_table[30];
    syscall_table[30]     = (void *)invirt_sys_30;
    original_syscalls[31] = syscall_table[31];
    syscall_table[31]     = (void *)invirt_sys_31;
    original_syscalls[32] = syscall_table[32];
    syscall_table[32]     = (void *)invirt_sys_32;
    original_syscalls[33] = syscall_table[33];
    syscall_table[33]     = (void *)invirt_sys_33;
    original_syscalls[34] = syscall_table[34];
    syscall_table[34]     = (void *)invirt_sys_34;
    original_syscalls[35] = syscall_table[35];
    syscall_table[35]     = (void *)invirt_sys_35;
    original_syscalls[36] = syscall_table[36];
    syscall_table[36]     = (void *)invirt_sys_36;
    original_syscalls[37] = syscall_table[37];
    syscall_table[37]     = (void *)invirt_sys_37;
    original_syscalls[38] = syscall_table[38];
    syscall_table[38]     = (void *)invirt_sys_38;
    original_syscalls[39] = syscall_table[39];
    syscall_table[39]     = (void *)invirt_sys_39;
    original_syscalls[40] = syscall_table[40];
    syscall_table[40]     = (void *)invirt_sys_40;
    original_syscalls[41] = syscall_table[41];
    syscall_table[41]     = (void *)invirt_sys_41;
    original_syscalls[42] = syscall_table[42];
    syscall_table[42]     = (void *)invirt_sys_42;
    original_syscalls[43] = syscall_table[43];
    syscall_table[43]     = (void *)invirt_sys_43;
    original_syscalls[44] = syscall_table[44];
    syscall_table[44]     = (void *)invirt_sys_44;
    original_syscalls[45] = syscall_table[45];
    syscall_table[45]     = (void *)invirt_sys_45;
    original_syscalls[46] = syscall_table[46];
    syscall_table[46]     = (void *)invirt_sys_46;
    original_syscalls[47] = syscall_table[47];
    syscall_table[47]     = (void *)invirt_sys_47;
    original_syscalls[48] = syscall_table[48];
    syscall_table[48]     = (void *)invirt_sys_48;
    original_syscalls[49] = syscall_table[49];
    syscall_table[49]     = (void *)invirt_sys_49;
    original_syscalls[50] = syscall_table[50];
    syscall_table[50]     = (void *)invirt_sys_50;
    original_syscalls[51] = syscall_table[51];
    syscall_table[51]     = (void *)invirt_sys_51;
    original_syscalls[52] = syscall_table[52];
    syscall_table[52]     = (void *)invirt_sys_52;
    original_syscalls[53] = syscall_table[53];
    syscall_table[53]     = (void *)invirt_sys_53;
    original_syscalls[54] = syscall_table[54];
    syscall_table[54]     = (void *)invirt_sys_54;
    original_syscalls[55] = syscall_table[55];
    syscall_table[55]     = (void *)invirt_sys_55;
    original_syscalls[56] = syscall_table[56];
    syscall_table[56]     = (void *)invirt_sys_56;
    original_syscalls[57] = syscall_table[57];
    syscall_table[57]     = (void *)invirt_sys_57;
    original_syscalls[58] = syscall_table[58];
    syscall_table[58]     = (void *)invirt_sys_58;
    original_syscalls[59] = syscall_table[59];
    syscall_table[59]     = (void *)invirt_sys_59;
    original_syscalls[60] = syscall_table[60];
    syscall_table[60]     = (void *)invirt_sys_60;
    original_syscalls[61] = syscall_table[61];
    syscall_table[61]     = (void *)invirt_sys_61;
    original_syscalls[62] = syscall_table[62];
    syscall_table[62]     = (void *)invirt_sys_62;
    original_syscalls[63] = syscall_table[63];
    syscall_table[63]     = (void *)invirt_sys_63;
    original_syscalls[64] = syscall_table[64];
    syscall_table[64]     = (void *)invirt_sys_64;
    original_syscalls[65] = syscall_table[65];
    syscall_table[65]     = (void *)invirt_sys_65;
    original_syscalls[66] = syscall_table[66];
    syscall_table[66]     = (void *)invirt_sys_66;
    original_syscalls[67] = syscall_table[67];
    syscall_table[67]     = (void *)invirt_sys_67;
    original_syscalls[68] = syscall_table[68];
    syscall_table[68]     = (void *)invirt_sys_68;
    original_syscalls[69] = syscall_table[69];
    syscall_table[69]     = (void *)invirt_sys_69;
    original_syscalls[70] = syscall_table[70];
    syscall_table[70]     = (void *)invirt_sys_70;
    original_syscalls[71] = syscall_table[71];
    syscall_table[71]     = (void *)invirt_sys_71;
    original_syscalls[72] = syscall_table[72];
    syscall_table[72]     = (void *)invirt_sys_72;
    original_syscalls[73] = syscall_table[73];
    syscall_table[73]     = (void *)invirt_sys_73;
    original_syscalls[74] = syscall_table[74];
    syscall_table[74]     = (void *)invirt_sys_74;
    original_syscalls[75] = syscall_table[75];
    syscall_table[75]     = (void *)invirt_sys_75;
    original_syscalls[76] = syscall_table[76];
    syscall_table[76]     = (void *)invirt_sys_76;
    original_syscalls[77] = syscall_table[77];
    syscall_table[77]     = (void *)invirt_sys_77;
    original_syscalls[78] = syscall_table[78];
    syscall_table[78]     = (void *)invirt_sys_78;
    original_syscalls[79] = syscall_table[79];
    syscall_table[79]     = (void *)invirt_sys_79;
    original_syscalls[80] = syscall_table[80];
    syscall_table[80]     = (void *)invirt_sys_80;
    original_syscalls[81] = syscall_table[81];
    syscall_table[81]     = (void *)invirt_sys_81;
    original_syscalls[82] = syscall_table[82];
    syscall_table[82]     = (void *)invirt_sys_82;
    original_syscalls[83] = syscall_table[83];
    syscall_table[83]     = (void *)invirt_sys_83;
    original_syscalls[84] = syscall_table[84];
    syscall_table[84]     = (void *)invirt_sys_84;
    original_syscalls[85] = syscall_table[85];
    syscall_table[85]     = (void *)invirt_sys_85;
    original_syscalls[86] = syscall_table[86];
    syscall_table[86]     = (void *)invirt_sys_86;
    original_syscalls[87] = syscall_table[87];
    syscall_table[87]     = (void *)invirt_sys_87;
    original_syscalls[88] = syscall_table[88];
    syscall_table[88]     = (void *)invirt_sys_88;
    original_syscalls[89] = syscall_table[89];
    syscall_table[89]     = (void *)invirt_sys_89;
    original_syscalls[90] = syscall_table[90];
    syscall_table[90]     = (void *)invirt_sys_90;
    original_syscalls[91] = syscall_table[91];
    syscall_table[91]     = (void *)invirt_sys_91;
    original_syscalls[92] = syscall_table[92];
    syscall_table[92]     = (void *)invirt_sys_92;
    original_syscalls[93] = syscall_table[93];
    syscall_table[93]     = (void *)invirt_sys_93;
    original_syscalls[94] = syscall_table[94];
    syscall_table[94]     = (void *)invirt_sys_94;
    original_syscalls[95] = syscall_table[95];
    syscall_table[95]     = (void *)invirt_sys_95;
    original_syscalls[96] = syscall_table[96];
    syscall_table[96]     = (void *)invirt_sys_96;
    original_syscalls[97] = syscall_table[97];
    syscall_table[97]     = (void *)invirt_sys_97;
    original_syscalls[98] = syscall_table[98];
    syscall_table[98]     = (void *)invirt_sys_98;
    original_syscalls[99] = syscall_table[99];
    syscall_table[99]     = (void *)invirt_sys_99;
    original_syscalls[100] = syscall_table[100];
    syscall_table[100]     = (void *)invirt_sys_100;
    original_syscalls[101] = syscall_table[101];
    syscall_table[101]     = (void *)invirt_sys_101;
    original_syscalls[102] = syscall_table[102];
    syscall_table[102]     = (void *)invirt_sys_102;
    original_syscalls[103] = syscall_table[103];
    syscall_table[103]     = (void *)invirt_sys_103;
    original_syscalls[104] = syscall_table[104];
    syscall_table[104]     = (void *)invirt_sys_104;
    original_syscalls[105] = syscall_table[105];
    syscall_table[105]     = (void *)invirt_sys_105;
    original_syscalls[106] = syscall_table[106];
    syscall_table[106]     = (void *)invirt_sys_106;
    original_syscalls[107] = syscall_table[107];
    syscall_table[107]     = (void *)invirt_sys_107;
    original_syscalls[108] = syscall_table[108];
    syscall_table[108]     = (void *)invirt_sys_108;
    original_syscalls[109] = syscall_table[109];
    syscall_table[109]     = (void *)invirt_sys_109;
    original_syscalls[110] = syscall_table[110];
    syscall_table[110]     = (void *)invirt_sys_110;
    original_syscalls[111] = syscall_table[111];
    syscall_table[111]     = (void *)invirt_sys_111;
    original_syscalls[112] = syscall_table[112];
    syscall_table[112]     = (void *)invirt_sys_112;
    original_syscalls[113] = syscall_table[113];
    syscall_table[113]     = (void *)invirt_sys_113;
    original_syscalls[114] = syscall_table[114];
    syscall_table[114]     = (void *)invirt_sys_114;
    original_syscalls[115] = syscall_table[115];
    syscall_table[115]     = (void *)invirt_sys_115;
    original_syscalls[116] = syscall_table[116];
    syscall_table[116]     = (void *)invirt_sys_116;
    original_syscalls[117] = syscall_table[117];
    syscall_table[117]     = (void *)invirt_sys_117;
    original_syscalls[118] = syscall_table[118];
    syscall_table[118]     = (void *)invirt_sys_118;
    original_syscalls[119] = syscall_table[119];
    syscall_table[119]     = (void *)invirt_sys_119;
    original_syscalls[120] = syscall_table[120];
    syscall_table[120]     = (void *)invirt_sys_120;
    original_syscalls[121] = syscall_table[121];
    syscall_table[121]     = (void *)invirt_sys_121;
    original_syscalls[122] = syscall_table[122];
    syscall_table[122]     = (void *)invirt_sys_122;
    original_syscalls[123] = syscall_table[123];
    syscall_table[123]     = (void *)invirt_sys_123;
    original_syscalls[124] = syscall_table[124];
    syscall_table[124]     = (void *)invirt_sys_124;
    original_syscalls[125] = syscall_table[125];
    syscall_table[125]     = (void *)invirt_sys_125;
    original_syscalls[126] = syscall_table[126];
    syscall_table[126]     = (void *)invirt_sys_126;
    original_syscalls[127] = syscall_table[127];
    syscall_table[127]     = (void *)invirt_sys_127;
    original_syscalls[128] = syscall_table[128];
    syscall_table[128]     = (void *)invirt_sys_128;
    original_syscalls[129] = syscall_table[129];
    syscall_table[129]     = (void *)invirt_sys_129;
    original_syscalls[130] = syscall_table[130];
    syscall_table[130]     = (void *)invirt_sys_130;
    original_syscalls[131] = syscall_table[131];
    syscall_table[131]     = (void *)invirt_sys_131;
    original_syscalls[132] = syscall_table[132];
    syscall_table[132]     = (void *)invirt_sys_132;
    original_syscalls[133] = syscall_table[133];
    syscall_table[133]     = (void *)invirt_sys_133;
    original_syscalls[134] = syscall_table[134];
    syscall_table[134]     = (void *)invirt_sys_134;
    original_syscalls[135] = syscall_table[135];
    syscall_table[135]     = (void *)invirt_sys_135;
    original_syscalls[136] = syscall_table[136];
    syscall_table[136]     = (void *)invirt_sys_136;
    original_syscalls[137] = syscall_table[137];
    syscall_table[137]     = (void *)invirt_sys_137;
    original_syscalls[138] = syscall_table[138];
    syscall_table[138]     = (void *)invirt_sys_138;
    original_syscalls[139] = syscall_table[139];
    syscall_table[139]     = (void *)invirt_sys_139;
    original_syscalls[140] = syscall_table[140];
    syscall_table[140]     = (void *)invirt_sys_140;
    original_syscalls[141] = syscall_table[141];
    syscall_table[141]     = (void *)invirt_sys_141;
    original_syscalls[142] = syscall_table[142];
    syscall_table[142]     = (void *)invirt_sys_142;
    original_syscalls[143] = syscall_table[143];
    syscall_table[143]     = (void *)invirt_sys_143;
    original_syscalls[144] = syscall_table[144];
    syscall_table[144]     = (void *)invirt_sys_144;
    original_syscalls[145] = syscall_table[145];
    syscall_table[145]     = (void *)invirt_sys_145;
    original_syscalls[146] = syscall_table[146];
    syscall_table[146]     = (void *)invirt_sys_146;
    original_syscalls[147] = syscall_table[147];
    syscall_table[147]     = (void *)invirt_sys_147;
    original_syscalls[148] = syscall_table[148];
    syscall_table[148]     = (void *)invirt_sys_148;
    original_syscalls[149] = syscall_table[149];
    syscall_table[149]     = (void *)invirt_sys_149;
    original_syscalls[150] = syscall_table[150];
    syscall_table[150]     = (void *)invirt_sys_150;
    original_syscalls[151] = syscall_table[151];
    syscall_table[151]     = (void *)invirt_sys_151;
    original_syscalls[152] = syscall_table[152];
    syscall_table[152]     = (void *)invirt_sys_152;
    original_syscalls[153] = syscall_table[153];
    syscall_table[153]     = (void *)invirt_sys_153;
    original_syscalls[154] = syscall_table[154];
    syscall_table[154]     = (void *)invirt_sys_154;
    original_syscalls[155] = syscall_table[155];
    syscall_table[155]     = (void *)invirt_sys_155;
    original_syscalls[156] = syscall_table[156];
    syscall_table[156]     = (void *)invirt_sys_156;
    original_syscalls[157] = syscall_table[157];
    syscall_table[157]     = (void *)invirt_sys_157;
    original_syscalls[158] = syscall_table[158];
    syscall_table[158]     = (void *)invirt_sys_158;
    original_syscalls[159] = syscall_table[159];
    syscall_table[159]     = (void *)invirt_sys_159;
    original_syscalls[160] = syscall_table[160];
    syscall_table[160]     = (void *)invirt_sys_160;
    original_syscalls[161] = syscall_table[161];
    syscall_table[161]     = (void *)invirt_sys_161;
    original_syscalls[162] = syscall_table[162];
    syscall_table[162]     = (void *)invirt_sys_162;
    original_syscalls[163] = syscall_table[163];
    syscall_table[163]     = (void *)invirt_sys_163;
    original_syscalls[164] = syscall_table[164];
    syscall_table[164]     = (void *)invirt_sys_164;
    original_syscalls[165] = syscall_table[165];
    syscall_table[165]     = (void *)invirt_sys_165;
    original_syscalls[166] = syscall_table[166];
    syscall_table[166]     = (void *)invirt_sys_166;
    original_syscalls[167] = syscall_table[167];
    syscall_table[167]     = (void *)invirt_sys_167;
    original_syscalls[168] = syscall_table[168];
    syscall_table[168]     = (void *)invirt_sys_168;
    original_syscalls[169] = syscall_table[169];
    syscall_table[169]     = (void *)invirt_sys_169;
    original_syscalls[170] = syscall_table[170];
    syscall_table[170]     = (void *)invirt_sys_170;
    original_syscalls[171] = syscall_table[171];
    syscall_table[171]     = (void *)invirt_sys_171;
    original_syscalls[172] = syscall_table[172];
    syscall_table[172]     = (void *)invirt_sys_172;
    original_syscalls[173] = syscall_table[173];
    syscall_table[173]     = (void *)invirt_sys_173;
    original_syscalls[174] = syscall_table[174];
    syscall_table[174]     = (void *)invirt_sys_174;
    original_syscalls[175] = syscall_table[175];
    syscall_table[175]     = (void *)invirt_sys_175;
    original_syscalls[176] = syscall_table[176];
    syscall_table[176]     = (void *)invirt_sys_176;
    original_syscalls[177] = syscall_table[177];
    syscall_table[177]     = (void *)invirt_sys_177;
    original_syscalls[178] = syscall_table[178];
    syscall_table[178]     = (void *)invirt_sys_178;
    original_syscalls[179] = syscall_table[179];
    syscall_table[179]     = (void *)invirt_sys_179;
    original_syscalls[180] = syscall_table[180];
    syscall_table[180]     = (void *)invirt_sys_180;
    original_syscalls[181] = syscall_table[181];
    syscall_table[181]     = (void *)invirt_sys_181;
    original_syscalls[182] = syscall_table[182];
    syscall_table[182]     = (void *)invirt_sys_182;
    original_syscalls[183] = syscall_table[183];
    syscall_table[183]     = (void *)invirt_sys_183;
    original_syscalls[184] = syscall_table[184];
    syscall_table[184]     = (void *)invirt_sys_184;
    original_syscalls[185] = syscall_table[185];
    syscall_table[185]     = (void *)invirt_sys_185;
    original_syscalls[186] = syscall_table[186];
    syscall_table[186]     = (void *)invirt_sys_186;
    original_syscalls[187] = syscall_table[187];
    syscall_table[187]     = (void *)invirt_sys_187;
    original_syscalls[188] = syscall_table[188];
    syscall_table[188]     = (void *)invirt_sys_188;
    original_syscalls[189] = syscall_table[189];
    syscall_table[189]     = (void *)invirt_sys_189;
    original_syscalls[190] = syscall_table[190];
    syscall_table[190]     = (void *)invirt_sys_190;
    original_syscalls[191] = syscall_table[191];
    syscall_table[191]     = (void *)invirt_sys_191;
    original_syscalls[192] = syscall_table[192];
    syscall_table[192]     = (void *)invirt_sys_192;
    original_syscalls[193] = syscall_table[193];
    syscall_table[193]     = (void *)invirt_sys_193;
    original_syscalls[194] = syscall_table[194];
    syscall_table[194]     = (void *)invirt_sys_194;
    original_syscalls[195] = syscall_table[195];
    syscall_table[195]     = (void *)invirt_sys_195;
    original_syscalls[196] = syscall_table[196];
    syscall_table[196]     = (void *)invirt_sys_196;
    original_syscalls[197] = syscall_table[197];
    syscall_table[197]     = (void *)invirt_sys_197;
    original_syscalls[198] = syscall_table[198];
    syscall_table[198]     = (void *)invirt_sys_198;
    original_syscalls[199] = syscall_table[199];
    syscall_table[199]     = (void *)invirt_sys_199;
    original_syscalls[200] = syscall_table[200];
    syscall_table[200]     = (void *)invirt_sys_200;
    original_syscalls[201] = syscall_table[201];
    syscall_table[201]     = (void *)invirt_sys_201;
    original_syscalls[202] = syscall_table[202];
    syscall_table[202]     = (void *)invirt_sys_202;
    original_syscalls[203] = syscall_table[203];
    syscall_table[203]     = (void *)invirt_sys_203;
    original_syscalls[204] = syscall_table[204];
    syscall_table[204]     = (void *)invirt_sys_204;
    original_syscalls[205] = syscall_table[205];
    syscall_table[205]     = (void *)invirt_sys_205;
    original_syscalls[206] = syscall_table[206];
    syscall_table[206]     = (void *)invirt_sys_206;
    original_syscalls[207] = syscall_table[207];
    syscall_table[207]     = (void *)invirt_sys_207;
    original_syscalls[208] = syscall_table[208];
    syscall_table[208]     = (void *)invirt_sys_208;
    original_syscalls[209] = syscall_table[209];
    syscall_table[209]     = (void *)invirt_sys_209;
    original_syscalls[210] = syscall_table[210];
    syscall_table[210]     = (void *)invirt_sys_210;
    original_syscalls[211] = syscall_table[211];
    syscall_table[211]     = (void *)invirt_sys_211;
    original_syscalls[212] = syscall_table[212];
    syscall_table[212]     = (void *)invirt_sys_212;
    original_syscalls[213] = syscall_table[213];
    syscall_table[213]     = (void *)invirt_sys_213;
    original_syscalls[214] = syscall_table[214];
    syscall_table[214]     = (void *)invirt_sys_214;
    original_syscalls[215] = syscall_table[215];
    syscall_table[215]     = (void *)invirt_sys_215;
    original_syscalls[216] = syscall_table[216];
    syscall_table[216]     = (void *)invirt_sys_216;
    original_syscalls[217] = syscall_table[217];
    syscall_table[217]     = (void *)invirt_sys_217;
    original_syscalls[218] = syscall_table[218];
    syscall_table[218]     = (void *)invirt_sys_218;
    original_syscalls[219] = syscall_table[219];
    syscall_table[219]     = (void *)invirt_sys_219;
    original_syscalls[220] = syscall_table[220];
    syscall_table[220]     = (void *)invirt_sys_220;
    original_syscalls[221] = syscall_table[221];
    syscall_table[221]     = (void *)invirt_sys_221;
    original_syscalls[222] = syscall_table[222];
    syscall_table[222]     = (void *)invirt_sys_222;
    original_syscalls[223] = syscall_table[223];
    syscall_table[223]     = (void *)invirt_sys_223;
    original_syscalls[224] = syscall_table[224];
    syscall_table[224]     = (void *)invirt_sys_224;
    original_syscalls[225] = syscall_table[225];
    syscall_table[225]     = (void *)invirt_sys_225;
    original_syscalls[226] = syscall_table[226];
    syscall_table[226]     = (void *)invirt_sys_226;
    original_syscalls[227] = syscall_table[227];
    syscall_table[227]     = (void *)invirt_sys_227;
    original_syscalls[228] = syscall_table[228];
    syscall_table[228]     = (void *)invirt_sys_228;
    original_syscalls[229] = syscall_table[229];
    syscall_table[229]     = (void *)invirt_sys_229;
    original_syscalls[230] = syscall_table[230];
    syscall_table[230]     = (void *)invirt_sys_230;
    original_syscalls[231] = syscall_table[231];
    syscall_table[231]     = (void *)invirt_sys_231;
    original_syscalls[232] = syscall_table[232];
    syscall_table[232]     = (void *)invirt_sys_232;
    original_syscalls[233] = syscall_table[233];
    syscall_table[233]     = (void *)invirt_sys_233;
    original_syscalls[234] = syscall_table[234];
    syscall_table[234]     = (void *)invirt_sys_234;
    original_syscalls[235] = syscall_table[235];
    syscall_table[235]     = (void *)invirt_sys_235;
    original_syscalls[236] = syscall_table[236];
    syscall_table[236]     = (void *)invirt_sys_236;
    original_syscalls[237] = syscall_table[237];
    syscall_table[237]     = (void *)invirt_sys_237;
    original_syscalls[238] = syscall_table[238];
    syscall_table[238]     = (void *)invirt_sys_238;
    original_syscalls[239] = syscall_table[239];
    syscall_table[239]     = (void *)invirt_sys_239;
    original_syscalls[240] = syscall_table[240];
    syscall_table[240]     = (void *)invirt_sys_240;
    original_syscalls[241] = syscall_table[241];
    syscall_table[241]     = (void *)invirt_sys_241;
    original_syscalls[242] = syscall_table[242];
    syscall_table[242]     = (void *)invirt_sys_242;
    original_syscalls[243] = syscall_table[243];
    syscall_table[243]     = (void *)invirt_sys_243;
    original_syscalls[244] = syscall_table[244];
    syscall_table[244]     = (void *)invirt_sys_244;
    original_syscalls[245] = syscall_table[245];
    syscall_table[245]     = (void *)invirt_sys_245;
    original_syscalls[246] = syscall_table[246];
    syscall_table[246]     = (void *)invirt_sys_246;
    original_syscalls[247] = syscall_table[247];
    syscall_table[247]     = (void *)invirt_sys_247;
    original_syscalls[248] = syscall_table[248];
    syscall_table[248]     = (void *)invirt_sys_248;
    original_syscalls[249] = syscall_table[249];
    syscall_table[249]     = (void *)invirt_sys_249;
    original_syscalls[250] = syscall_table[250];
    syscall_table[250]     = (void *)invirt_sys_250;
    original_syscalls[251] = syscall_table[251];
    syscall_table[251]     = (void *)invirt_sys_251;
    original_syscalls[252] = syscall_table[252];
    syscall_table[252]     = (void *)invirt_sys_252;
    original_syscalls[253] = syscall_table[253];
    syscall_table[253]     = (void *)invirt_sys_253;
    original_syscalls[254] = syscall_table[254];
    syscall_table[254]     = (void *)invirt_sys_254;
    original_syscalls[255] = syscall_table[255];
    syscall_table[255]     = (void *)invirt_sys_255;
    original_syscalls[256] = syscall_table[256];
    syscall_table[256]     = (void *)invirt_sys_256;
    original_syscalls[257] = syscall_table[257];
    syscall_table[257]     = (void *)invirt_sys_257;
    original_syscalls[258] = syscall_table[258];
    syscall_table[258]     = (void *)invirt_sys_258;
    original_syscalls[259] = syscall_table[259];
    syscall_table[259]     = (void *)invirt_sys_259;
    original_syscalls[260] = syscall_table[260];
    syscall_table[260]     = (void *)invirt_sys_260;
    original_syscalls[261] = syscall_table[261];
    syscall_table[261]     = (void *)invirt_sys_261;
    original_syscalls[262] = syscall_table[262];
    syscall_table[262]     = (void *)invirt_sys_262;
    original_syscalls[263] = syscall_table[263];
    syscall_table[263]     = (void *)invirt_sys_263;
    original_syscalls[264] = syscall_table[264];
    syscall_table[264]     = (void *)invirt_sys_264;
    original_syscalls[265] = syscall_table[265];
    syscall_table[265]     = (void *)invirt_sys_265;
    original_syscalls[266] = syscall_table[266];
    syscall_table[266]     = (void *)invirt_sys_266;
    original_syscalls[267] = syscall_table[267];
    syscall_table[267]     = (void *)invirt_sys_267;
    original_syscalls[268] = syscall_table[268];
    syscall_table[268]     = (void *)invirt_sys_268;
    original_syscalls[269] = syscall_table[269];
    syscall_table[269]     = (void *)invirt_sys_269;
    original_syscalls[270] = syscall_table[270];
    syscall_table[270]     = (void *)invirt_sys_270;
    original_syscalls[271] = syscall_table[271];
    syscall_table[271]     = (void *)invirt_sys_271;
    original_syscalls[272] = syscall_table[272];
    syscall_table[272]     = (void *)invirt_sys_272;
    original_syscalls[273] = syscall_table[273];
    syscall_table[273]     = (void *)invirt_sys_273;
    original_syscalls[274] = syscall_table[274];
    syscall_table[274]     = (void *)invirt_sys_274;
    original_syscalls[275] = syscall_table[275];
    syscall_table[275]     = (void *)invirt_sys_275;
    original_syscalls[276] = syscall_table[276];
    syscall_table[276]     = (void *)invirt_sys_276;
    original_syscalls[277] = syscall_table[277];
    syscall_table[277]     = (void *)invirt_sys_277;
    original_syscalls[278] = syscall_table[278];
    syscall_table[278]     = (void *)invirt_sys_278;
    original_syscalls[279] = syscall_table[279];
    syscall_table[279]     = (void *)invirt_sys_279;
    original_syscalls[280] = syscall_table[280];
    syscall_table[280]     = (void *)invirt_sys_280;
    original_syscalls[281] = syscall_table[281];
    syscall_table[281]     = (void *)invirt_sys_281;
    original_syscalls[282] = syscall_table[282];
    syscall_table[282]     = (void *)invirt_sys_282;
    original_syscalls[283] = syscall_table[283];
    syscall_table[283]     = (void *)invirt_sys_283;
    original_syscalls[284] = syscall_table[284];
    syscall_table[284]     = (void *)invirt_sys_284;
    original_syscalls[285] = syscall_table[285];
    syscall_table[285]     = (void *)invirt_sys_285;
    original_syscalls[286] = syscall_table[286];
    syscall_table[286]     = (void *)invirt_sys_286;
    original_syscalls[287] = syscall_table[287];
    syscall_table[287]     = (void *)invirt_sys_287;
    original_syscalls[288] = syscall_table[288];
    syscall_table[288]     = (void *)invirt_sys_288;
    original_syscalls[289] = syscall_table[289];
    syscall_table[289]     = (void *)invirt_sys_289;
    original_syscalls[290] = syscall_table[290];
    syscall_table[290]     = (void *)invirt_sys_290;
    original_syscalls[291] = syscall_table[291];
    syscall_table[291]     = (void *)invirt_sys_291;
    original_syscalls[292] = syscall_table[292];
    syscall_table[292]     = (void *)invirt_sys_292;
    original_syscalls[293] = syscall_table[293];
    syscall_table[293]     = (void *)invirt_sys_293;
    original_syscalls[294] = syscall_table[294];
    syscall_table[294]     = (void *)invirt_sys_294;
    original_syscalls[295] = syscall_table[295];
    syscall_table[295]     = (void *)invirt_sys_295;
    original_syscalls[296] = syscall_table[296];
    syscall_table[296]     = (void *)invirt_sys_296;
    original_syscalls[297] = syscall_table[297];
    syscall_table[297]     = (void *)invirt_sys_297;
    original_syscalls[298] = syscall_table[298];
    syscall_table[298]     = (void *)invirt_sys_298;
    original_syscalls[299] = syscall_table[299];
    syscall_table[299]     = (void *)invirt_sys_299;
    original_syscalls[300] = syscall_table[300];
    syscall_table[300]     = (void *)invirt_sys_300;
    original_syscalls[301] = syscall_table[301];
    syscall_table[301]     = (void *)invirt_sys_301;
    original_syscalls[302] = syscall_table[302];
    syscall_table[302]     = (void *)invirt_sys_302;
    original_syscalls[303] = syscall_table[303];
    syscall_table[303]     = (void *)invirt_sys_303;
    original_syscalls[304] = syscall_table[304];
    syscall_table[304]     = (void *)invirt_sys_304;
    original_syscalls[305] = syscall_table[305];
    syscall_table[305]     = (void *)invirt_sys_305;
    original_syscalls[306] = syscall_table[306];
    syscall_table[306]     = (void *)invirt_sys_306;
    original_syscalls[307] = syscall_table[307];
    syscall_table[307]     = (void *)invirt_sys_307;
    original_syscalls[308] = syscall_table[308];
    syscall_table[308]     = (void *)invirt_sys_308;
    original_syscalls[309] = syscall_table[309];
    syscall_table[309]     = (void *)invirt_sys_309;
    original_syscalls[310] = syscall_table[310];
    syscall_table[310]     = (void *)invirt_sys_310;
    original_syscalls[311] = syscall_table[311];
    syscall_table[311]     = (void *)invirt_sys_311;
    original_syscalls[312] = syscall_table[312];
    syscall_table[312]     = (void *)invirt_sys_312;
    original_syscalls[313] = syscall_table[313];
    syscall_table[313]     = (void *)invirt_sys_313;
    original_syscalls[314] = syscall_table[314];
    syscall_table[314]     = (void *)invirt_sys_314;
    original_syscalls[315] = syscall_table[315];
    syscall_table[315]     = (void *)invirt_sys_315;
    original_syscalls[316] = syscall_table[316];
    syscall_table[316]     = (void *)invirt_sys_316;
    original_syscalls[317] = syscall_table[317];
    syscall_table[317]     = (void *)invirt_sys_317;
    original_syscalls[318] = syscall_table[318];
    syscall_table[318]     = (void *)invirt_sys_318;
    original_syscalls[319] = syscall_table[319];
    syscall_table[319]     = (void *)invirt_sys_319;
    original_syscalls[320] = syscall_table[320];
    syscall_table[320]     = (void *)invirt_sys_320;
    original_syscalls[321] = syscall_table[321];
    syscall_table[321]     = (void *)invirt_sys_321;
    original_syscalls[322] = syscall_table[322];
    syscall_table[322]     = (void *)invirt_sys_322;
    original_syscalls[323] = syscall_table[323];
    syscall_table[323]     = (void *)invirt_sys_323;
    original_syscalls[324] = syscall_table[324];
    syscall_table[324]     = (void *)invirt_sys_324;
    original_syscalls[325] = syscall_table[325];
    syscall_table[325]     = (void *)invirt_sys_325;
    original_syscalls[326] = syscall_table[326];
    syscall_table[326]     = (void *)invirt_sys_326;
    original_syscalls[327] = syscall_table[327];
    syscall_table[327]     = (void *)invirt_sys_327;
    original_syscalls[328] = syscall_table[328];
    syscall_table[328]     = (void *)invirt_sys_328;
    original_syscalls[329] = syscall_table[329];
    syscall_table[329]     = (void *)invirt_sys_329;
    original_syscalls[330] = syscall_table[330];
    syscall_table[330]     = (void *)invirt_sys_330;
    original_syscalls[331] = syscall_table[331];
    syscall_table[331]     = (void *)invirt_sys_331;
    original_syscalls[332] = syscall_table[332];
    syscall_table[332]     = (void *)invirt_sys_332;
}

void
uninstall_invirt_syscalls(void ** syscall_table){

    syscall_table[0] = original_syscalls[0];
    syscall_table[1] = original_syscalls[1];
    syscall_table[2] = original_syscalls[2];
    syscall_table[3] = original_syscalls[3];
    syscall_table[4] = original_syscalls[4];
    syscall_table[5] = original_syscalls[5];
    syscall_table[6] = original_syscalls[6];
    syscall_table[7] = original_syscalls[7];
    syscall_table[8] = original_syscalls[8];
    syscall_table[9] = original_syscalls[9];
    syscall_table[10] = original_syscalls[10];
    syscall_table[11] = original_syscalls[11];
    syscall_table[12] = original_syscalls[12];
    syscall_table[13] = original_syscalls[13];
    syscall_table[14] = original_syscalls[14];
    syscall_table[15] = original_syscalls[15];
    syscall_table[16] = original_syscalls[16];
    syscall_table[17] = original_syscalls[17];
    syscall_table[18] = original_syscalls[18];
    syscall_table[19] = original_syscalls[19];
    syscall_table[20] = original_syscalls[20];
    syscall_table[21] = original_syscalls[21];
    syscall_table[22] = original_syscalls[22];
    syscall_table[23] = original_syscalls[23];
    syscall_table[24] = original_syscalls[24];
    syscall_table[25] = original_syscalls[25];
    syscall_table[26] = original_syscalls[26];
    syscall_table[27] = original_syscalls[27];
    syscall_table[28] = original_syscalls[28];
    syscall_table[29] = original_syscalls[29];
    syscall_table[30] = original_syscalls[30];
    syscall_table[31] = original_syscalls[31];
    syscall_table[32] = original_syscalls[32];
    syscall_table[33] = original_syscalls[33];
    syscall_table[34] = original_syscalls[34];
    syscall_table[35] = original_syscalls[35];
    syscall_table[36] = original_syscalls[36];
    syscall_table[37] = original_syscalls[37];
    syscall_table[38] = original_syscalls[38];
    syscall_table[39] = original_syscalls[39];
    syscall_table[40] = original_syscalls[40];
    syscall_table[41] = original_syscalls[41];
    syscall_table[42] = original_syscalls[42];
    syscall_table[43] = original_syscalls[43];
    syscall_table[44] = original_syscalls[44];
    syscall_table[45] = original_syscalls[45];
    syscall_table[46] = original_syscalls[46];
    syscall_table[47] = original_syscalls[47];
    syscall_table[48] = original_syscalls[48];
    syscall_table[49] = original_syscalls[49];
    syscall_table[50] = original_syscalls[50];
    syscall_table[51] = original_syscalls[51];
    syscall_table[52] = original_syscalls[52];
    syscall_table[53] = original_syscalls[53];
    syscall_table[54] = original_syscalls[54];
    syscall_table[55] = original_syscalls[55];
    syscall_table[56] = original_syscalls[56];
    syscall_table[57] = original_syscalls[57];
    syscall_table[58] = original_syscalls[58];
    syscall_table[59] = original_syscalls[59];
    syscall_table[60] = original_syscalls[60];
    syscall_table[61] = original_syscalls[61];
    syscall_table[62] = original_syscalls[62];
    syscall_table[63] = original_syscalls[63];
    syscall_table[64] = original_syscalls[64];
    syscall_table[65] = original_syscalls[65];
    syscall_table[66] = original_syscalls[66];
    syscall_table[67] = original_syscalls[67];
    syscall_table[68] = original_syscalls[68];
    syscall_table[69] = original_syscalls[69];
    syscall_table[70] = original_syscalls[70];
    syscall_table[71] = original_syscalls[71];
    syscall_table[72] = original_syscalls[72];
    syscall_table[73] = original_syscalls[73];
    syscall_table[74] = original_syscalls[74];
    syscall_table[75] = original_syscalls[75];
    syscall_table[76] = original_syscalls[76];
    syscall_table[77] = original_syscalls[77];
    syscall_table[78] = original_syscalls[78];
    syscall_table[79] = original_syscalls[79];
    syscall_table[80] = original_syscalls[80];
    syscall_table[81] = original_syscalls[81];
    syscall_table[82] = original_syscalls[82];
    syscall_table[83] = original_syscalls[83];
    syscall_table[84] = original_syscalls[84];
    syscall_table[85] = original_syscalls[85];
    syscall_table[86] = original_syscalls[86];
    syscall_table[87] = original_syscalls[87];
    syscall_table[88] = original_syscalls[88];
    syscall_table[89] = original_syscalls[89];
    syscall_table[90] = original_syscalls[90];
    syscall_table[91] = original_syscalls[91];
    syscall_table[92] = original_syscalls[92];
    syscall_table[93] = original_syscalls[93];
    syscall_table[94] = original_syscalls[94];
    syscall_table[95] = original_syscalls[95];
    syscall_table[96] = original_syscalls[96];
    syscall_table[97] = original_syscalls[97];
    syscall_table[98] = original_syscalls[98];
    syscall_table[99] = original_syscalls[99];
    syscall_table[100] = original_syscalls[100];
    syscall_table[101] = original_syscalls[101];
    syscall_table[102] = original_syscalls[102];
    syscall_table[103] = original_syscalls[103];
    syscall_table[104] = original_syscalls[104];
    syscall_table[105] = original_syscalls[105];
    syscall_table[106] = original_syscalls[106];
    syscall_table[107] = original_syscalls[107];
    syscall_table[108] = original_syscalls[108];
    syscall_table[109] = original_syscalls[109];
    syscall_table[110] = original_syscalls[110];
    syscall_table[111] = original_syscalls[111];
    syscall_table[112] = original_syscalls[112];
    syscall_table[113] = original_syscalls[113];
    syscall_table[114] = original_syscalls[114];
    syscall_table[115] = original_syscalls[115];
    syscall_table[116] = original_syscalls[116];
    syscall_table[117] = original_syscalls[117];
    syscall_table[118] = original_syscalls[118];
    syscall_table[119] = original_syscalls[119];
    syscall_table[120] = original_syscalls[120];
    syscall_table[121] = original_syscalls[121];
    syscall_table[122] = original_syscalls[122];
    syscall_table[123] = original_syscalls[123];
    syscall_table[124] = original_syscalls[124];
    syscall_table[125] = original_syscalls[125];
    syscall_table[126] = original_syscalls[126];
    syscall_table[127] = original_syscalls[127];
    syscall_table[128] = original_syscalls[128];
    syscall_table[129] = original_syscalls[129];
    syscall_table[130] = original_syscalls[130];
    syscall_table[131] = original_syscalls[131];
    syscall_table[132] = original_syscalls[132];
    syscall_table[133] = original_syscalls[133];
    syscall_table[134] = original_syscalls[134];
    syscall_table[135] = original_syscalls[135];
    syscall_table[136] = original_syscalls[136];
    syscall_table[137] = original_syscalls[137];
    syscall_table[138] = original_syscalls[138];
    syscall_table[139] = original_syscalls[139];
    syscall_table[140] = original_syscalls[140];
    syscall_table[141] = original_syscalls[141];
    syscall_table[142] = original_syscalls[142];
    syscall_table[143] = original_syscalls[143];
    syscall_table[144] = original_syscalls[144];
    syscall_table[145] = original_syscalls[145];
    syscall_table[146] = original_syscalls[146];
    syscall_table[147] = original_syscalls[147];
    syscall_table[148] = original_syscalls[148];
    syscall_table[149] = original_syscalls[149];
    syscall_table[150] = original_syscalls[150];
    syscall_table[151] = original_syscalls[151];
    syscall_table[152] = original_syscalls[152];
    syscall_table[153] = original_syscalls[153];
    syscall_table[154] = original_syscalls[154];
    syscall_table[155] = original_syscalls[155];
    syscall_table[156] = original_syscalls[156];
    syscall_table[157] = original_syscalls[157];
    syscall_table[158] = original_syscalls[158];
    syscall_table[159] = original_syscalls[159];
    syscall_table[160] = original_syscalls[160];
    syscall_table[161] = original_syscalls[161];
    syscall_table[162] = original_syscalls[162];
    syscall_table[163] = original_syscalls[163];
    syscall_table[164] = original_syscalls[164];
    syscall_table[165] = original_syscalls[165];
    syscall_table[166] = original_syscalls[166];
    syscall_table[167] = original_syscalls[167];
    syscall_table[168] = original_syscalls[168];
    syscall_table[169] = original_syscalls[169];
    syscall_table[170] = original_syscalls[170];
    syscall_table[171] = original_syscalls[171];
    syscall_table[172] = original_syscalls[172];
    syscall_table[173] = original_syscalls[173];
    syscall_table[174] = original_syscalls[174];
    syscall_table[175] = original_syscalls[175];
    syscall_table[176] = original_syscalls[176];
    syscall_table[177] = original_syscalls[177];
    syscall_table[178] = original_syscalls[178];
    syscall_table[179] = original_syscalls[179];
    syscall_table[180] = original_syscalls[180];
    syscall_table[181] = original_syscalls[181];
    syscall_table[182] = original_syscalls[182];
    syscall_table[183] = original_syscalls[183];
    syscall_table[184] = original_syscalls[184];
    syscall_table[185] = original_syscalls[185];
    syscall_table[186] = original_syscalls[186];
    syscall_table[187] = original_syscalls[187];
    syscall_table[188] = original_syscalls[188];
    syscall_table[189] = original_syscalls[189];
    syscall_table[190] = original_syscalls[190];
    syscall_table[191] = original_syscalls[191];
    syscall_table[192] = original_syscalls[192];
    syscall_table[193] = original_syscalls[193];
    syscall_table[194] = original_syscalls[194];
    syscall_table[195] = original_syscalls[195];
    syscall_table[196] = original_syscalls[196];
    syscall_table[197] = original_syscalls[197];
    syscall_table[198] = original_syscalls[198];
    syscall_table[199] = original_syscalls[199];
    syscall_table[200] = original_syscalls[200];
    syscall_table[201] = original_syscalls[201];
    syscall_table[202] = original_syscalls[202];
    syscall_table[203] = original_syscalls[203];
    syscall_table[204] = original_syscalls[204];
    syscall_table[205] = original_syscalls[205];
    syscall_table[206] = original_syscalls[206];
    syscall_table[207] = original_syscalls[207];
    syscall_table[208] = original_syscalls[208];
    syscall_table[209] = original_syscalls[209];
    syscall_table[210] = original_syscalls[210];
    syscall_table[211] = original_syscalls[211];
    syscall_table[212] = original_syscalls[212];
    syscall_table[213] = original_syscalls[213];
    syscall_table[214] = original_syscalls[214];
    syscall_table[215] = original_syscalls[215];
    syscall_table[216] = original_syscalls[216];
    syscall_table[217] = original_syscalls[217];
    syscall_table[218] = original_syscalls[218];
    syscall_table[219] = original_syscalls[219];
    syscall_table[220] = original_syscalls[220];
    syscall_table[221] = original_syscalls[221];
    syscall_table[222] = original_syscalls[222];
    syscall_table[223] = original_syscalls[223];
    syscall_table[224] = original_syscalls[224];
    syscall_table[225] = original_syscalls[225];
    syscall_table[226] = original_syscalls[226];
    syscall_table[227] = original_syscalls[227];
    syscall_table[228] = original_syscalls[228];
    syscall_table[229] = original_syscalls[229];
    syscall_table[230] = original_syscalls[230];
    syscall_table[231] = original_syscalls[231];
    syscall_table[232] = original_syscalls[232];
    syscall_table[233] = original_syscalls[233];
    syscall_table[234] = original_syscalls[234];
    syscall_table[235] = original_syscalls[235];
    syscall_table[236] = original_syscalls[236];
    syscall_table[237] = original_syscalls[237];
    syscall_table[238] = original_syscalls[238];
    syscall_table[239] = original_syscalls[239];
    syscall_table[240] = original_syscalls[240];
    syscall_table[241] = original_syscalls[241];
    syscall_table[242] = original_syscalls[242];
    syscall_table[243] = original_syscalls[243];
    syscall_table[244] = original_syscalls[244];
    syscall_table[245] = original_syscalls[245];
    syscall_table[246] = original_syscalls[246];
    syscall_table[247] = original_syscalls[247];
    syscall_table[248] = original_syscalls[248];
    syscall_table[249] = original_syscalls[249];
    syscall_table[250] = original_syscalls[250];
    syscall_table[251] = original_syscalls[251];
    syscall_table[252] = original_syscalls[252];
    syscall_table[253] = original_syscalls[253];
    syscall_table[254] = original_syscalls[254];
    syscall_table[255] = original_syscalls[255];
    syscall_table[256] = original_syscalls[256];
    syscall_table[257] = original_syscalls[257];
    syscall_table[258] = original_syscalls[258];
    syscall_table[259] = original_syscalls[259];
    syscall_table[260] = original_syscalls[260];
    syscall_table[261] = original_syscalls[261];
    syscall_table[262] = original_syscalls[262];
    syscall_table[263] = original_syscalls[263];
    syscall_table[264] = original_syscalls[264];
    syscall_table[265] = original_syscalls[265];
    syscall_table[266] = original_syscalls[266];
    syscall_table[267] = original_syscalls[267];
    syscall_table[268] = original_syscalls[268];
    syscall_table[269] = original_syscalls[269];
    syscall_table[270] = original_syscalls[270];
    syscall_table[271] = original_syscalls[271];
    syscall_table[272] = original_syscalls[272];
    syscall_table[273] = original_syscalls[273];
    syscall_table[274] = original_syscalls[274];
    syscall_table[275] = original_syscalls[275];
    syscall_table[276] = original_syscalls[276];
    syscall_table[277] = original_syscalls[277];
    syscall_table[278] = original_syscalls[278];
    syscall_table[279] = original_syscalls[279];
    syscall_table[280] = original_syscalls[280];
    syscall_table[281] = original_syscalls[281];
    syscall_table[282] = original_syscalls[282];
    syscall_table[283] = original_syscalls[283];
    syscall_table[284] = original_syscalls[284];
    syscall_table[285] = original_syscalls[285];
    syscall_table[286] = original_syscalls[286];
    syscall_table[287] = original_syscalls[287];
    syscall_table[288] = original_syscalls[288];
    syscall_table[289] = original_syscalls[289];
    syscall_table[290] = original_syscalls[290];
    syscall_table[291] = original_syscalls[291];
    syscall_table[292] = original_syscalls[292];
    syscall_table[293] = original_syscalls[293];
    syscall_table[294] = original_syscalls[294];
    syscall_table[295] = original_syscalls[295];
    syscall_table[296] = original_syscalls[296];
    syscall_table[297] = original_syscalls[297];
    syscall_table[298] = original_syscalls[298];
    syscall_table[299] = original_syscalls[299];
    syscall_table[300] = original_syscalls[300];
    syscall_table[301] = original_syscalls[301];
    syscall_table[302] = original_syscalls[302];
    syscall_table[303] = original_syscalls[303];
    syscall_table[304] = original_syscalls[304];
    syscall_table[305] = original_syscalls[305];
    syscall_table[306] = original_syscalls[306];
    syscall_table[307] = original_syscalls[307];
    syscall_table[308] = original_syscalls[308];
    syscall_table[309] = original_syscalls[309];
    syscall_table[310] = original_syscalls[310];
    syscall_table[311] = original_syscalls[311];
    syscall_table[312] = original_syscalls[312];
    syscall_table[313] = original_syscalls[313];
    syscall_table[314] = original_syscalls[314];
    syscall_table[315] = original_syscalls[315];
    syscall_table[316] = original_syscalls[316];
    syscall_table[317] = original_syscalls[317];
    syscall_table[318] = original_syscalls[318];
    syscall_table[319] = original_syscalls[319];
    syscall_table[320] = original_syscalls[320];
    syscall_table[321] = original_syscalls[321];
    syscall_table[322] = original_syscalls[322];
    syscall_table[323] = original_syscalls[323];
    syscall_table[324] = original_syscalls[324];
    syscall_table[325] = original_syscalls[325];
    syscall_table[326] = original_syscalls[326];
    syscall_table[327] = original_syscalls[327];
    syscall_table[328] = original_syscalls[328];
    syscall_table[329] = original_syscalls[329];
    syscall_table[330] = original_syscalls[330];
    syscall_table[331] = original_syscalls[331];
    syscall_table[332] = original_syscalls[332];
}
