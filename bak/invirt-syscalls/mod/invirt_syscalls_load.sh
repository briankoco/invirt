#!/bin/bash

module='invirt_syscalls'

hook_dev='invirt_syscalls_hook'
hook_mode='444'

buffer_dev='invirt_syscalls_buffer'
buffer_mode='666'

sudo insmod $module.ko || exit 1

major=$(awk "\$2==\"$module\" {print \$1}" /proc/devices)

#remove stale nodes
if [ -c /dev/$hook_dev ]; then
    sudo rm -f /dev/$hook_dev
fi
if [ -c /dev/$buffer_dev ]; then
    sudo rm -f /dev/$buffer_dev
fi

#make device nodes
sudo mknod /dev/$hook_dev c $major 0
sudo mknod /dev/$buffer_dev c $major 1

#change permissions
sudo chmod $hook_mode /dev/$hook_dev
sudo chmod $buffer_mode /dev/$buffer_dev 
