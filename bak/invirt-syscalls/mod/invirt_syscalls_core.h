#ifndef __INVIRT_SYSCALLS_CORE_H__
#define __INVIRT_SYSCALLS_CORE_H__

int is_invirt_process(pid_t pid);

void invirt_deactivate_buffer(pid_t pid);

unsigned long
invirt_do_syscall(pid_t tgid,
		  unsigned int syscall_number,
		  unsigned long arg0,
		  unsigned long arg1,
		  unsigned long arg2,
		  unsigned long arg3,
		  unsigned long arg4,
		  unsigned long arg5);

#endif /*__INVIRT_SYSCALLS_CORE_H__*/
