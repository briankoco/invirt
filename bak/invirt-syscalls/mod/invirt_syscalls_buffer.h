#ifndef __INVIRT_SYSCALLS_BUFFER_H__
#define __INVIRT_SYSCALLS_BUFFER_H__

#include <linux/wait.h>
#include <linux/spinlock.h>
#include <packaged_syscall.h>

struct invirt_syscall_buffer {
    spinlock_t lock; /**/
    wait_queue_head_t waitq; /*sleeping on Poll and Read until buffer is non-empty*/
    wait_queue_head_t syscall_waitq; /*sleeping in invirt_do_syscall, until return value is available*/
    volatile unsigned long counter; /*used for syscall id #'s*/
    volatile int flags;
    atomic_t refcnt;

    pid_t tgid;
    
    int enqueud_elements;
    struct packaged_syscall * packaged_syscall_buffer;
    unsigned int head;
    unsigned int buffer_capacity;

    spinlock_t cb_lock; /*completion buffer*/
    struct invirt_returned_syscall * completion_buffer;
    unsigned long cb_capacity;
    
    /* linkage on buffer_list */
    struct list_head sb_hashnode;
};

#define INVIRT_FLAG_DESTROYING      0x00010 /* being destroyed */
#define INVIRT_FLAG_DESTROYED       0x00020 /* 'being destroyed' finished */
#define INVIRT_FLAG_ACTIVE          0x00040
#define INVIRT_FLAG_HANGUP          0x00080

int init_invirt_buffer(struct invirt_syscall_buffer * syscall_buffer, pid_t pid);
int uninit_invirt_buffer(struct invirt_syscall_buffer * syscall_buffer);
int invirt_buffer_empty(struct invirt_syscall_buffer * syscall_buffer);
int invirt_buffer_enqeue_syscall(struct invirt_syscall_buffer * syscall_buffer,
				 pid_t tgid,
				 unsigned int syscall_number,
				 unsigned long arg0,
				 unsigned long arg1,
				 unsigned long arg2,
				 unsigned long arg3,
				 unsigned long arg4,
				 unsigned long arg5);
void invirt_buffer_deqeue_syscall(struct invirt_syscall_buffer * syscall_buffer,
				  struct packaged_syscall *packaged_syscall_buf);
void invirt_buffer_mark_destroying(struct invirt_syscall_buffer * syscall_buffer);
int invirt_buffer_get_num_enqueud(struct invirt_syscall_buffer * syscall_buffer);
int invirt_syscall_returned(struct invirt_syscall_buffer * sb, unsigned long id);
int invirt_read_returned(struct invirt_syscall_buffer * sb, unsigned long id);
void invirt_add_returned(struct invirt_syscall_buffer * sb, unsigned long id, unsigned long ret);

#endif /* __INVIRT_SYSCALLS_BUFFER_H__ */
