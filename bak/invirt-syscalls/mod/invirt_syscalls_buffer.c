#include <linux/spinlock.h>
#include <linux/gfp.h>
#include <linux/wait.h>
#include <linux/sched.h>

#include <packaged_syscall.h>
#include <invirt_syscalls_buffer.h>
#include <invirt_syscalls_core.h>

#define INVIRT_BUFFER_PAGES_ORDER 1
#define INVIRT_BUFFER_PAGES (1 << INVIRT_BUFFER_PAGES_ORDER)

#define COMPLETION_BUFFER_PAGES_ORDER 0
#define COMPLETION_BUFFER_PAGES (1 << COMPLETION_BUFFER_PAGES_ORDER)

/*
 * No Lock Required - sb should only ever be locally visible
 */
int
init_invirt_buffer(struct invirt_syscall_buffer * syscall_buffer, pid_t tgid){
    spin_lock_init(&syscall_buffer->lock);
    spin_lock_init(&syscall_buffer->cb_lock);
    init_waitqueue_head(&syscall_buffer->waitq);
    init_waitqueue_head(&syscall_buffer->syscall_waitq);
    INIT_LIST_HEAD(&syscall_buffer->sb_hashnode);

    syscall_buffer->tgid                    = tgid;
    syscall_buffer->enqueud_elements        = 0;
    syscall_buffer->head                    = 0;
    syscall_buffer->packaged_syscall_buffer = (struct packaged_syscall *) __get_free_pages(GFP_KERNEL, INVIRT_BUFFER_PAGES_ORDER);
    if (!syscall_buffer->packaged_syscall_buffer)
	return -ENOMEM;

    syscall_buffer->buffer_capacity = (INVIRT_BUFFER_PAGES * PAGE_SIZE) / sizeof(struct packaged_syscall);

    syscall_buffer->completion_buffer = (struct invirt_returned_syscall *) __get_free_pages(GFP_KERNEL, COMPLETION_BUFFER_PAGES_ORDER);
    if (!syscall_buffer->completion_buffer){
	free_pages((unsigned long)syscall_buffer->packaged_syscall_buffer, INVIRT_BUFFER_PAGES_ORDER);
	return -ENOMEM;
    }
    syscall_buffer->cb_capacity = (COMPLETION_BUFFER_PAGES * PAGE_SIZE) / sizeof(struct invirt_returned_syscall);
    syscall_buffer->counter = 0;
    
    return 0;
}

/*Check if completed syscall is on our return buffer*/
int
invirt_syscall_returned(struct invirt_syscall_buffer * sb,
			unsigned long id)
{
    int i;
    unsigned long flags;

    spin_lock_irqsave(&sb->cb_lock, flags);//lock
    for (i = 0; i < sb->cb_capacity; ++i){
	if (sb->completion_buffer[i].id == id &&
	    sb->completion_buffer[i].flags == PACKAGED_SYSCALL_ACTIVE){
	    spin_unlock_irqrestore(&sb->cb_lock, flags);//unlock
	    return 1;
	}
    }

    spin_unlock_irqrestore(&sb->cb_lock, flags);//unlock
    return 0;
}

/*Same as above, but read return status and mark the syscall as stale*/
int
invirt_read_returned(struct invirt_syscall_buffer * sb,
		     unsigned long id)
{
    int i;
    unsigned long ret;
    unsigned long flags;

    spin_lock_irqsave(&sb->cb_lock, flags);//lock
    for (i = 0; i < sb->cb_capacity; ++i){
	if (sb->completion_buffer[i].id == id &&
	    sb->completion_buffer[i].flags == PACKAGED_SYSCALL_ACTIVE){

	    ret = sb->completion_buffer[i].ret;
	    sb->completion_buffer[i].flags = 0;
	    spin_unlock_irqrestore(&sb->cb_lock, flags);//unlock

	    return ret;
	}
    }
    
    spin_unlock_irqrestore(&sb->cb_lock, flags);//unlock
    WARN_ON(1);
    return 0;
}

/*Called from ioctl: places returned syscall onto return buffer*/
void
invirt_add_returned(struct invirt_syscall_buffer * sb,
		    unsigned long id,
		    unsigned long ret)
{
    int i;
    unsigned long flags;

    printk(KERN_INFO "adding returned syscall\n");
    spin_lock_irqsave(&sb->cb_lock, flags);//lock
    for (i = 0; i < sb->cb_capacity; ++i){
	if (sb->completion_buffer[i].flags != PACKAGED_SYSCALL_ACTIVE){
	    sb->completion_buffer[i] = (struct invirt_returned_syscall){id,PACKAGED_SYSCALL_ACTIVE,ret};
	    spin_unlock_irqrestore(&sb->cb_lock, flags);//unlock

	    wake_up_interruptible(&sb->syscall_waitq);
	    printk(KERN_INFO "id: %lu returned - waking buffer\n", id);
	    return;
	}
    }
    
    spin_unlock_irqrestore(&sb->cb_lock, flags);//unlock
    WARN_ON(1);
}

/*
 * Frees the its pages, maybe do something to the flags as well....
 */
int
uninit_invirt_buffer(struct invirt_syscall_buffer * syscall_buffer){
    unsigned long flags;
    spin_lock_irqsave(&syscall_buffer->lock, flags);//lock
    if (syscall_buffer->packaged_syscall_buffer){
	free_pages((unsigned long)syscall_buffer->packaged_syscall_buffer, INVIRT_BUFFER_PAGES_ORDER);
    }
    if (syscall_buffer->completion_buffer){
	free_pages((unsigned long)syscall_buffer->completion_buffer, COMPLETION_BUFFER_PAGES_ORDER);
    }
    spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
    return 0;
}

int
invirt_buffer_get_num_enqueud(struct invirt_syscall_buffer * syscall_buffer){
    unsigned long flags;
    unsigned int n;
    spin_lock_irqsave(&syscall_buffer->lock, flags);//lock
    n = syscall_buffer->enqueud_elements;
    spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
    return n;
}

int
invirt_buffer_enqeue_syscall(struct invirt_syscall_buffer * syscall_buffer,
			     pid_t tgid,
			     unsigned int syscall_number,
			     unsigned long arg0,
			     unsigned long arg1,
			     unsigned long arg2,
			     unsigned long arg3,
			     unsigned long arg4,
			     unsigned long arg5)
{
    unsigned int tail;
    unsigned long flags;
    unsigned long id;
    spin_lock_irqsave(&syscall_buffer->lock, flags);//lock
    
    WARN_ON(syscall_buffer->enqueud_elements >= syscall_buffer->buffer_capacity);
    id = syscall_buffer->counter++; //overflow is ok - I think
    tail = (syscall_buffer->head + syscall_buffer->enqueud_elements) % syscall_buffer->buffer_capacity;
    syscall_buffer->packaged_syscall_buffer[tail] = (struct packaged_syscall){id, tgid, syscall_number,
							    arg0, arg1, arg2, arg3, arg4, arg5};
    ++syscall_buffer->enqueud_elements;
    
    spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
    return id;
}

void
invirt_buffer_deqeue_syscall(struct invirt_syscall_buffer * syscall_buffer,
			     struct packaged_syscall *packaged_syscall_buf)
{
    unsigned long flags;
    spin_lock_irqsave(&syscall_buffer->lock, flags);//lock
    
    WARN_ON(syscall_buffer->enqueud_elements < 0);
    
    if (syscall_buffer->enqueud_elements == 0){
	spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
	return;
    }
    
    *packaged_syscall_buf = syscall_buffer->packaged_syscall_buffer[syscall_buffer->head];
    syscall_buffer->head = (syscall_buffer->head + 1) % syscall_buffer->buffer_capacity;
    --syscall_buffer->enqueud_elements;
    
    spin_unlock_irqrestore(&syscall_buffer->lock, flags);//unlock
    return;
}
