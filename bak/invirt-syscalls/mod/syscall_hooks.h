#ifndef __SYSCALL_HOOKS__H__
#define __SYSCALL_HOOKS__H__

/*Exported from syscall_hooks.c*/
void install_invirt_syscalls(void ** syscall_table);
void uninstall_invirt_syscalls(void ** syscall_table);
/**/

#endif /*__SYSCALL_HOOKS__H__*/
