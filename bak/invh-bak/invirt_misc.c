/*
 * This file is part of the invirt project developed at Washington
 * University in St. Louis
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/dmi.h>
#include <linux/seq_file.h>

#include <invirt.h>
#include <invirt_priv.h>

static struct proc_dir_entry * invirt_proc_dir;
static struct proc_dir_entry * uuid_entry;

struct invirt_thread_group *
invirt_tg_ref_by_tgid_nolock_internal(pid_t tgid,
                                      int   index, 
                                      int   return_destroying)
{
    struct invirt_thread_group * tg;

    list_for_each_entry(tg, &(gbl_tg_hashtable[index].list), tg_hashnode) {
        if (tg->tgid == tgid) {
            if ((tg->flags & INVIRT_FLAG_DESTROYING)  &&
                (return_destroying == 0))
            {
                continue;
            }

            invirt_tg_ref(tg);
            return tg;
        }
    }

    return ERR_PTR(-ENOENT);
}


void
invirt_tg_deref(struct invirt_thread_group * tg)
{
    WARN_ON(atomic_read(&(tg->refcnt)) <= 0);
    if (atomic_dec_return(&(tg->refcnt)) != 0)
        return;

    WARN_ON(!(tg->flags & INVIRT_FLAG_DESTROYED));

    /* last put of this tg .. tear it down */
    kfree(tg);
}

static int
uuid_procfs_show(struct seq_file * file,
                 void            * private_data)
{ 
    const char * uuid = dmi_get_system_info(DMI_PRODUCT_UUID);
    seq_printf(file, "%s\n", uuid);
    return 0;
}

static int
uuid_procfs_open(struct inode * inodep,
                 struct file  * filp)
{
    return single_open(filp, uuid_procfs_show, NULL);
}

static int
uuid_procfs_release(struct inode * inodep,
                    struct file  * filp)
{
    return single_release(inodep, filp);
}

static struct file_operations
uuid_procfs_fops = 
{
    .owner = THIS_MODULE,
    .open = uuid_procfs_open,
    .read = seq_read,
    .llseek = seq_lseek,
    .release = uuid_procfs_release
};

int
invirt_setup_proc_entries(void)
{
    invirt_proc_dir = proc_mkdir(__INVIRT_PROC_NAME, NULL);
    if (invirt_proc_dir == NULL) {
        printk(KERN_ERR "Failed to create proc entry\n");
        return -1;
    }

    uuid_entry = proc_create_data(
        __INVIRT_PROC_UUID_NAME,
        0444,
        invirt_proc_dir,
        &uuid_procfs_fops,
        NULL
    );
    if (uuid_entry == NULL) {
        printk(KERN_ERR "Failed to create " __INVIRT_PROC_UUID_NAME
            " entry\n");
        remove_proc_entry(__INVIRT_PROC_NAME, NULL);
        return -1;
    }

    return 0;
}

void
invirt_destroy_proc_entries(void)
{
    remove_proc_entry(__INVIRT_PROC_UUID_NAME, invirt_proc_dir);
    remove_proc_entry(__INVIRT_PROC_NAME, NULL);
}
