#!/bin/bash

if [ -c /dev/invh ]; then
    rmmod invh
fi

insmod invh.ko invirt_debug=1
chmod 666 /dev/invh
