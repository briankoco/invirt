/*
 * This file is part of the invirt project developed at Washington
 * University in St. Louis
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#include <linux/kallsyms.h>
#include <linux/slab.h>
#include <linux/nospec.h>

#include <asm/unistd.h>
#include <asm/syscall.h>
#include <asm/special_insns.h>

#include <invirt_priv.h>

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,17,0)
#error "We do not yet support system call hooking for kernels 4.17 and up"
#endif


#define invirt_disable_wp() \
    write_cr0(read_cr0() & ~0x10000); WARN_ON((read_cr0() & 0x10000));\

#define invirt_enable_wp() \
    write_cr0(read_cr0() | 0x10000); WARN_ON(!(read_cr0() & 0x10000));



/* address of linux sys call table (queried via kallsyms) */
asmlinkage sys_call_ptr_t * lnx_sys_call_table = NULL;

/* a backup of the linux sys call table before we overwrite it */
asmlinkage sys_call_ptr_t * lnx_sys_call_table_bak = NULL;

/* atomic variable used to synchronize on_each_cpu calls */
static atomic_t invirt_smp_cnt = ATOMIC_INIT(0);

asmlinkage long 
invirt_ni_syscall(unsigned long arg0,
                  unsigned long arg1,
                  unsigned long arg2,
                  unsigned long arg3,
                  unsigned long arg4,
                  unsigned long arg5)
{
    return -ENOSYS;
}


#ifdef __SYSCALL_64
#undef __SYSCALL_64
#endif

/*
 * First pass: define invirt system call handlers 
 */
#define __SYSCALL_64(nr, srm, qual)\
    asmlinkage long invirt_sys_##nr(unsigned long arg0,\
                                    unsigned long arg1,\
                                    unsigned long arg2,\
                                    unsigned long arg3,\
                                    unsigned long arg4,\
                                    unsigned long arg5)\
    {\
        struct invirt_thread_group * tg;\
        long ret;\
        tg = invirt_tg_ref_by_tgid(current->tgid);\
        if (IS_ERR(tg))\
            return lnx_sys_call_table_bak[nr](arg0, arg1, arg2, arg3, arg4, arg5);\
        /* TODO: replace with call to invirt subsystem */\
        ret = lnx_sys_call_table_bak[nr](arg0, arg1, arg2, arg3, arg4, arg5);\
        invirt_tg_deref(tg);\
        printk(KERN_INFO "INVIRT SYS CALL %u\n", nr);\
        return ret;\
    }
#include <asm/syscalls_64.h>

/* 
 * Second pass: build the actual call table 
 */
#undef __SYSCALL_64
#define __SYSCALL_64(nr, sym, qual) [nr] = invirt_sys_##nr,
asmlinkage sys_call_ptr_t invirt_sys_call_table[__NR_syscall_max+1] = {
    [0 ... __NR_syscall_max] = &invirt_ni_syscall,
#include <asm/syscalls_64.h>
};


static void
__move_syscall_table(bool          install,
                     unsigned long smp_id)
{
    atomic_inc(&invirt_smp_cnt);

    if (smp_id != smp_processor_id())
        return;

    /* wait until all cpus have issued this cross call to ensure 
     * no one is using the sys call table 
     */
    while (atomic_read(&invirt_smp_cnt) < num_online_cpus());

    /* disable write protection checking */
    invirt_disable_wp();

    if (install) {
        /* backup the table */
        memcpy(
            (void *)lnx_sys_call_table_bak,
            (void *)lnx_sys_call_table,
            sizeof(sys_call_ptr_t) * (__NR_syscall_max + 1)
        );

        /* install our table */
        memcpy(
            (void *)lnx_sys_call_table,
            (void *)invirt_sys_call_table,
            sizeof(sys_call_ptr_t) * (__NR_syscall_max + 1)
        );
    } else {
        /* restore the backup */
        memcpy(
            (void *)lnx_sys_call_table,
            (void *)lnx_sys_call_table_bak,
            sizeof(sys_call_ptr_t) * (__NR_syscall_max + 1)
        );
    }

    /* re-enable write protection */
    invirt_enable_wp();
}

static void
install_invirt_syscall_table(void * data)
{
    return __move_syscall_table(true, (unsigned long)data);
}

static void
uninstall_invirt_syscall_table(void * data)
{
    return __move_syscall_table(false, (unsigned long)data);
}

int
invirt_hook_syscalls(void)
{
    unsigned long addr, smp_id;

    /* find system call table */
    addr = kallsyms_lookup_name("sys_call_table");
    if (addr == 0)
        return -1;
    lnx_sys_call_table = (sys_call_ptr_t *)addr;

    lnx_sys_call_table_bak = kmalloc(
        sizeof(sys_call_ptr_t) * (__NR_syscall_max + 1),
        GFP_KERNEL
    );
    if (lnx_sys_call_table_bak == NULL)
        return -ENOMEM;

    atomic_set(&invirt_smp_cnt, 0);

    /* Install our syscall table */
    smp_id = get_cpu();
    on_each_cpu(install_invirt_syscall_table, (void *)smp_id, 1); 
    put_cpu();

    return 0;
}

void
invirt_unhook_syscalls(void)
{
    unsigned long smp_id;

    atomic_set(&invirt_smp_cnt, 0);
    
    /* Re-install original syscall table */
    smp_id = get_cpu();
    on_each_cpu(uninstall_invirt_syscall_table, (void *)smp_id, 1); 
    put_cpu();

    kfree(lnx_sys_call_table_bak);
}
