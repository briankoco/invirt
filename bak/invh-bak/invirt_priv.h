/*
 * This file is part of the invirt project developed at Washington
 * University in St. Louis
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#ifndef __INVIRT_PRIV_H__
#define __INVIRT_PRIV_H__

#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/mm_types.h>
#include <linux/atomic.h>
#include <linux/sched.h>
#include <linux/list.h>

/* 
 * info we need to track for invirt programs
 */
struct invirt_thread_group {

    /* attacher's tgid */
    pid_t tgid;

    /* target info */
    pid_t target_tgid;
    struct task_struct * target_task;
    struct mm_struct * target_mm;

    /* mutex protects concurrent modifications to target info */
    /* lock protects flags */
    struct mutex mutex;
    spinlock_t lock;
    atomic_t refcnt;
    volatile int flags;

    /* linkage on gbl_tg_hashtable */
    struct list_head tg_hashnode;
};

/*
 * Attribute and state flags
 */
#define INVIRT_FLAG_DESTROYING      0x00010 /* being destroyed */
#define INVIRT_FLAG_DESTROYED       0x00020 /* 'being destroyed' finished */
#define INVIRT_FLAG_FLUSHING        0x00040 /* being flushed */

/* defined in invirt_core.c */
extern int invirt_debug;
extern struct invirt_hashlist * gbl_tg_hashtable;

/* defined in invirt_umem.c */
int invirt_setup_vma(struct invirt_thread_group *, struct vm_area_struct *);

/* defined in invirt_misc.c */
struct invirt_thread_group * invirt_tg_ref_by_tgid_nolock_internal(pid_t, int, int);
void invirt_tg_deref(struct invirt_thread_group *);
int invirt_setup_proc_entries(void);
void invirt_destroy_proc_entries(void);

/* defined in invirt_syscall.c */
int invirt_hook_syscalls(void);
void invirt_unhook_syscalls(void);


struct invirt_hashlist {
    rwlock_t lock;
    struct list_head list;
} ____cacheline_aligned;

#define INVIRT_TG_HASHTABLE_SIZE 8

static inline int
invirt_tg_hashtable_index(pid_t tgid)
{
    return ((unsigned int)tgid % INVIRT_TG_HASHTABLE_SIZE);
}

static inline void
invirt_tg_ref(struct invirt_thread_group * tg)
{
    WARN_ON(atomic_read(&(tg->refcnt)) <= 0);
    atomic_inc(&(tg->refcnt));
}

static inline struct invirt_thread_group *
__invirt_tg_ref_by_tgid(pid_t tgid,
                        int   return_destroying,
                        int   do_lock)
{
    struct invirt_thread_group * tg;
    int index;
    unsigned long flags;

    index = invirt_tg_hashtable_index(tgid);

    if (do_lock)
        read_lock_irqsave(&(gbl_tg_hashtable[index].lock), flags);

    tg = invirt_tg_ref_by_tgid_nolock_internal(
        tgid, 
        index, 
        return_destroying
    );

    if (do_lock)
        read_unlock_irqrestore(&(gbl_tg_hashtable[index].lock), flags);

    return tg;
}

#define invirt_tg_ref_by_tgid(t)            __invirt_tg_ref_by_tgid(t, 0, 1)
#define invirt_tg_ref_by_tgid_all(t)        __invirt_tg_ref_by_tgid(t, 1, 1)
#define invirt_tg_ref_by_tgid_nolock(t)     __invirt_tg_ref_by_tgid(t, 0, 0)
#define invirt_tg_ref_by_tgid_nolock_all(t) __invirt_tg_ref_by_tgid(t, 1, 0)


static inline void
invirt_tg_not_destroyable(struct invirt_thread_group * tg)
{
    atomic_set(&(tg->refcnt), 1);
}

static inline void
invirt_tg_destroyable(struct invirt_thread_group * tg)
{
    invirt_tg_deref(tg);
}

#endif /* __INVIRT_PRIV_H__ */
