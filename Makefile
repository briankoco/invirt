all: modules user bench ctr

build:
	mkdir -p build

.PHONY: modules
modules: build include
	cd mod/invg && make && mv invg.ko ../../build && cp install-invg.sh ../../build/install-invg.sh
	cd mod/invh && make && mv invh.ko ../../build && cp install.sh ../../build/install-invh.sh

.PHONY: modules_clean
modules_clean:
	cd mod/invg && make clean
	cd mod/invh && make clean

.PHONY: include_clean
include_clean:
	cd include && clean

.PHONY: clean
clean: modules_clean
	rm -rf build
	cd mod && make clean
	cd include && make clean

.PHONY: include
include:
	cd include && make

.PHONY: user
user: build include
	cd user && make && mv invrun ../build && mv invd ../build

.PHONY: bench
bench:
	gcc bench/random_access.c -o build/random_access
	cd bench/NPB3.4.1/NPB3.4-OMP && make suite && cp bin/*.x ../../../build

.PHONY: ctr
ctr: user
	sudo podman build -f Dockerfile -t docker.io/bkocolos/invg:latest
