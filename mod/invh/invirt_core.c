/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/tracepoint.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/dmi.h>
#include <linux/seq_file.h>
#include <linux/anon_inodes.h>

#include <linux/sched/mm.h>
#include <linux/sched/task.h>

#include <invirt.h>
#include <invirt_priv.h>

struct invirt_hashlist * gbl_th_hashtable;
struct invirt_hashlist * gbl_tracer_hashtable;

static struct proc_dir_entry * invirt_proc_dir;
static struct proc_dir_entry * uuid_proc_entry;

/* debugging */
int invirt_debug_level = 0;
module_param(invirt_debug_level, int, 0644);

/* location of sys_call_table in Kallsyms */
unsigned long kallsyms_sys_call_table = 0;
module_param(kallsyms_sys_call_table, ulong, 0644);

/* location of vm_list in Kallsyms */
unsigned long kallsyms_vm_list = 0;
module_param(kallsyms_vm_list, ulong, 0644);

/* location ov kvm_lock in Kallsyms */
unsigned long kallsyms_kvm_lock = 0;
module_param(kallsyms_kvm_lock, ulong, 0644);

static int
invirt_mapfd_mmap(struct file           * filp,
                  struct vm_area_struct * vma)
{
    struct invirt_thread * th;
    int status;

    th = invirt_th_ref_by_pid(current->pid);
    if (IS_ERR(th))
        return PTR_ERR(th);

    status = invirt_setup_vma(th, vma);

    invirt_th_deref(th);
    return status;
}

/* Device operations associaated with mmap fops */
static struct file_operations
invirt_mapfd_fops =
{
    .mmap = invirt_mapfd_mmap
};


static int
invirt_setup_tracee(struct invirt_thread    * th,
                    struct invh_ioc_traceme * traceme)
{
    int status, index;
    char * uuid_str;
    pid_t tracer_pid, kvm_pid;
    struct pid * pid;
    struct task_struct * task;
    struct mm_struct * mm;
    struct kvm * kvm;
    unsigned long flags;

    uuid_str   = traceme->uuid_str;
    tracer_pid = traceme->tracer_pid;
    kvm_pid    = traceme->kvm_pid;

    pid = find_get_pid(kvm_pid);
    if (IS_ERR(pid)) {
        status = PTR_ERR(pid);
        goto err_pid;
    }

    task = get_pid_task(pid, PIDTYPE_PID);
    if (IS_ERR(task)) {
        status = PTR_ERR(task);
        goto err_task;
    }

    mm = get_task_mm(task);
    if (IS_ERR(mm)) {
        status = PTR_ERR(mm);
        goto err_mm;
    }

    kvm = invirt_get_kvm_by_pid(kvm_pid);
    if (IS_ERR(kvm)) {
        status = PTR_ERR(kvm);
        goto err_kvm;
    }

    switch (traceme->mode) {
    case INVH_IOC_TRACEME_SOCKET:
        status = invirt_setup_tracee_socket(th, (const char *)uuid_str, tracer_pid, traceme->socket.conn_fd);
        break;

    default:
        status = -EINVAL;
        break;
    }

    if (status)
        goto err_socket;

    BUG_ON(strncmp(th->uuid_str, uuid_str, INVD_UUID_STR_LEN) != 0);
    BUG_ON(th->tracer_tgid != tracer_pid);

    /* remember the tracer */
    index = invirt_tracer_hashtable_index(uuid_str, tracer_pid);
    write_lock_irqsave(&(gbl_tracer_hashtable[index].lock), flags);
    list_add_tail(&(th->tracer_hashnode), &(gbl_tracer_hashtable[index].list));
    write_unlock_irqrestore(&(gbl_tracer_hashtable[index].lock), flags);

    th->kvm_pid  = kvm_pid;
    th->kvm_mm   = mm;
    th->kvm_task = task;
    th->kvm      = kvm;

    return 0;

err_socket:
    invirt_put_kvm(kvm);
err_kvm:
    mmput(mm);
err_mm:
    put_task_struct(task);
err_task:
    put_pid(pid);
err_pid:
    return status;
}

static void
invirt_teardown_tracee(struct invirt_thread * th)
{
    unsigned long flags;
    int mode, index;

    spin_lock_irqsave(&(th->lock), flags);
    mode = th->mode;
    spin_unlock_irqrestore(&(th->lock), flags);

    switch (mode) {
    case INVIRT_TRACEMODE_SOCKET:
        invirt_teardown_tracee_socket(th);
        break;

    default:
        return;
    }

    /* release various refs */
    invirt_put_kvm(th->kvm);
    mmput(th->kvm_mm);
    put_task_struct(th->kvm_task);
    put_pid(task_pid(th->kvm_task));

    /* remove tracer from lookup list */
    index = invirt_tracer_hashtable_index(th->uuid_str, th->tracer_tgid);
    write_lock_irqsave(&(gbl_tracer_hashtable[index].lock), flags);
    list_del_init(&th->tracer_hashnode);
    write_unlock_irqrestore(&(gbl_tracer_hashtable[index].lock), flags);
}

/*
 * user open of the invirt driver
 */
static int
invirt_dev_open(struct inode * inodep,
                struct file  * filp)
{
    struct invirt_thread * th;
    int index;
    unsigned long flags;

    /* if this has already been done, return silently */
    th = invirt_th_ref_by_pid(current->pid);
    if (WARN_ON(!IS_ERR(th))) {
        invirt_th_deref(th);
        return 0;
    }

    /* inc module refcnt */
    if (!try_module_get(THIS_MODULE))
        return -ENODEV;

    /* create th */
    th = kzalloc(sizeof(struct invirt_thread), GFP_KERNEL);
    if (th == NULL) {
        module_put(THIS_MODULE);
        return -ENOMEM;
    }

    th->pid = current->pid;
    th->mm = current->mm;
    mmget(th->mm);
    INIT_LIST_HEAD(&(th->th_hashnode));
    th->remote_extent_root = RB_ROOT;
    spin_lock_init(&(th->lock));
    mutex_init(&(th->mutex));

    invirt_th_not_destroyable(th);

    inv_dbg(1, "created thread pid:%d,tgid:%d\n", th->pid, current->tgid);

    /* add th to its hashlist */
    index = invirt_th_hashtable_index(th->pid);
    write_lock_irqsave(&(gbl_th_hashtable[index].lock), flags);
    list_add_tail(&(th->th_hashnode), &(gbl_th_hashtable[index].list));
    write_unlock_irqrestore(&(gbl_th_hashtable[index].lock), flags);

    return 0;
}

/*
 * Destroy an invirt_thread
 */
static void
invirt_destroy_th(struct invirt_thread * th)
{
    invirt_th_destroyable(th);
    //invirt_th_deref(th);
}

void
invirt_teardown_th(struct invirt_thread * th)
{
    unsigned long flags;
    int index;

    spin_lock_irqsave(&(th->lock), flags);
    WARN_ON(th->flags & INVIRT_FLAG_DESTROYING);
    th->flags |= INVIRT_FLAG_DESTROYING;
    spin_unlock_irqrestore(&(th->lock), flags);

    mutex_lock(&(th->mutex));
    invirt_release_remote_extents(th);
    mutex_unlock(&(th->mutex));

    invirt_teardown_tracee(th);

    /* Remove th structure from its hash list */
    index = invirt_th_hashtable_index(th->pid);
    write_lock_irqsave(&(gbl_th_hashtable[index].lock), flags);
    list_del_init(&th->th_hashnode);
    write_unlock_irqrestore(&(gbl_th_hashtable[index].lock), flags);

    spin_lock_irqsave(&(th->lock), flags);
    WARN_ON(th->flags & INVIRT_FLAG_DESTROYED);
    th->flags |= INVIRT_FLAG_DESTROYED;
    spin_unlock_irqrestore(&(th->lock), flags);

    mmput(th->mm);

    module_put(THIS_MODULE);

#if 0
    /*
     * We don't call invirt_destroy_th() here. We can't call
     * mmu_notifier_unregister() when the stack started with a
     * mmu_notifier_release() callout or we'll deadlock in the kernel
     * MMU notifier code.  invirt_destroy_th() will be called when the
     * close of /dev/invirt occurs as deadlocks are not possible then.
     */
    invirt_th_deref(th);
#endif
    invirt_destroy_th(th);
}

static int
invirt_dev_flush(struct file  * filp,
                 fl_owner_t     owner)
{
    struct invirt_thread * th;

    inv_dbg(1, "tid %d (tgid=%d) exiting\n", current->pid, current->tgid);

    /* During a call to fork() there is a check for whether the parent process
     * has any pending signals. If there are pending signals, then the fork
     * aborts, and the child process is removed before delivering the signal
     * and starting the fork again. In that case, we can end up here, but since
     * we're mid-fork, current is pointing to the parent's task_struct and not
     * the child's. This would cause us to remove the parent's invirt mappings
     * by accident. We check here whether the owner pointer we have is the same
     * as the current->files pointer. If it is, or if current->files is NULL,
     * then this flush really does belong to the current process. If they don't
     * match, then we return without doing anything since the child shouldn't
     * have a valid invirt_thread struct yet.
     */
    if (current->files && current->files != owner)
        return 0;

    /*
     * invirt_flush() can get called twice for thread groups which inherited
     * /dev/invirt: once for the inherited fd, once for the first explicit use
     * of /dev/invirt. If we don't find the th via invirt_th_ref_by_pid() we
     * assume we are in this type of scenario and return silently.
     *
     * Note the use of _all: we want to get this even if it's
     * destroying/destroyed.
     */
    th = invirt_th_ref_by_pid_all(current->pid);
    if (IS_ERR(th))
        return 0;

    invirt_teardown_th(th);

    invirt_th_deref(th);

#if 0
    /* BJK: we can't remove the th from the lookup list yet, because of
     * this call stack is possible:
     *  invirt_flush() ->
     *    invirt_destroy_th() ->
     *      invirt_mmu_notifier_unlink() ->
     *        invirt_mmu_release()
     *          invirt_mmu_release() will need to query the th from the
     *          lookup list.
     *
     * We need to take an extra ref of the th, so that we can pull it from the
     * hashlist after we call invirt_destroy_th() below.
     */

    invirt_th_ref(th);

    invirt_destroy_th(th);

    invirt_th_deref(th);
#endif
    return 0;
}

static long
invirt_dev_ioctl(struct file * filp,
                 unsigned int  cmd,
                 unsigned long arg)
{
    struct invirt_thread * th;
    int status;

    th = invirt_th_ref_by_pid(current->pid);
    if (IS_ERR(th))
        return PTR_ERR(th);

    switch (cmd) {
    case INVH_IOC_GET_MAPFD:
        status = anon_inode_getfd("[invh]", &invirt_mapfd_fops, NULL, O_RDWR | O_CLOEXEC);
        break;

    case INVH_IOC_TRACEME: {
        struct invh_ioc_traceme ktraceme;
        if (copy_from_user(&ktraceme, (char __user *)arg,
                sizeof(struct invh_ioc_traceme))) {
            status = -EFAULT;
            break;
        }

        status = invirt_setup_tracee(th, &ktraceme);
        break;
    }

    default:
        status = -ENOIOCTLCMD;
        break;
    }

    invirt_th_deref(th);
    return (long)status;
}

static struct file_operations
invirt_dev_fops =
{
    .open           = invirt_dev_open,
    .flush          = invirt_dev_flush,
    .unlocked_ioctl = invirt_dev_ioctl,
};

static struct miscdevice
dev_handle =
{
    .minor = MISC_DYNAMIC_MINOR,
    .name = INVH_MODULE_NAME,
    .fops = &invirt_dev_fops
};

static int
uuid_procfs_show(struct seq_file * file,
                 void            * private_data)
{
    const char * uuid = dmi_get_system_info(DMI_PRODUCT_UUID);
    seq_printf(file, "%s\n", uuid);
    return 0;
}

static int
uuid_procfs_open(struct inode * inodep,
                 struct file  * filp)
{
    return single_open(filp, uuid_procfs_show, NULL);
}

static int
uuid_procfs_release(struct inode * inodep,
                    struct file  * filp)
{
    return single_release(inodep, filp);
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,6,0)
static struct proc_ops
uuid_procfs_fops =
{
    .proc_open = uuid_procfs_open,
    .proc_read = seq_read,
    .proc_lseek = seq_lseek,
    .proc_release = uuid_procfs_release
};

#else
static struct file_operations
uuid_procfs_fops =
{
    .owner = THIS_MODULE,
    .open = uuid_procfs_open,
    .read = seq_read,
    .llseek = seq_lseek,
    .release = uuid_procfs_release
};
#endif

static int
invirt_setup_procfs(void)
{
    invirt_proc_dir = proc_mkdir(__INVIRT_PROC_NAME, NULL);
    if (invirt_proc_dir == NULL) {
        inv_err("Failed to create proc entry\n");
        return -1;
    }

    uuid_proc_entry = proc_create_data(
        __INVIRT_PROC_UUID_NAME,
        0444,
        invirt_proc_dir,
        &uuid_procfs_fops,
        NULL
    );
    if (uuid_proc_entry == NULL) {
        inv_err("failed to create " __INVIRT_PROC_UUID_NAME " entry\n");
        remove_proc_entry(__INVIRT_PROC_NAME, NULL);
        return -1;
    }

    return 0;
}

static void
invirt_teardown_procfs(void)
{
    remove_proc_entry(__INVIRT_PROC_UUID_NAME, invirt_proc_dir);
    remove_proc_entry(__INVIRT_PROC_NAME, NULL);
}

static int __init
invirt_init(void)
{
    int status, i;

    gbl_th_hashtable = kzalloc(
        sizeof(struct invirt_hashlist) * INVIRT_TH_HASHTABLE_SIZE,
        GFP_KERNEL
    );
    if (gbl_th_hashtable == NULL)
        return -ENOMEM;

    for (i = 0; i < INVIRT_TH_HASHTABLE_SIZE; i++) {
        rwlock_init(&(gbl_th_hashtable[i].lock));
        INIT_LIST_HEAD(&(gbl_th_hashtable[i].list));
    }

    gbl_tracer_hashtable = kzalloc(
        sizeof(struct invirt_hashlist) * INVIRT_TH_HASHTABLE_SIZE,
        GFP_KERNEL
    );
    if (gbl_tracer_hashtable == NULL)
        goto out_tracer;

    for (i = 0; i < INVIRT_TH_HASHTABLE_SIZE; i++) {
        rwlock_init(&(gbl_tracer_hashtable[i].lock));
        INIT_LIST_HEAD(&(gbl_tracer_hashtable[i].list));
    }

    /* enable mmu notifier callout handling */
    status = invirt_enable_mmunot();
    if (status != 0) {
        inv_err("failed to enable mmu notifiers\n");
        goto out_mmu;
    }

    /* enable system call interposition */
    status = invirt_enable_syscalls();
    if (status != 0) {
        inv_err("failed to enable system call interposition\n");
        goto out_syscalls;
    }

    /* setup procfs */
    status = invirt_setup_procfs();
    if (status != 0) {
        inv_err("failed to enable procfs\n");
        goto out_proc;
    }

    /* register misc devices */
    status = misc_register(&dev_handle);
    if (status != 0) {
        inv_err("failed to regiser misc device\n");
        goto out_misc;
    }

    inv_out("module installed successfully\n");

    return 0;

out_misc:
    invirt_teardown_procfs();
out_proc:
    invirt_disable_syscalls();
out_syscalls:
    invirt_disable_mmunot();
out_mmu:
    kfree(gbl_tracer_hashtable);
out_tracer:
    kfree(gbl_th_hashtable);
    return status;
}

static void __exit
invirt_exit(void )
{
    misc_deregister(&dev_handle);
    invirt_teardown_procfs();
    invirt_disable_syscalls();
    invirt_disable_mmunot();
    kfree(gbl_tracer_hashtable);
    kfree(gbl_th_hashtable);

    inv_out("module uninstalled successfully\n");
}

module_init(invirt_init);
module_exit(invirt_exit);

MODULE_LICENSE("GPL");
