/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

/*
 * This file provides memory management support for shadow memory regions in
 * the host shadow process.
 */

#include <linux/version.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/memory.h>
#include <linux/highmem.h>
#include <linux/slab.h>
#include <linux/atomic.h>
#include <linux/kvm_host.h>
#include <linux/rbtree.h>
#include <linux/pfn_t.h>
#include <linux/radix-tree.h>

#include <invirt_priv.h>

#include <asm/stacktrace.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,8,0)
#define mmap_read_lock(mm)   down_read(&((mm)->mmap_sem))
#define mmap_read_unlock(mm) up_read(&((mm)->mmap_sem))
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,17,0)
typedef int vm_fault_t;
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,11,0)
enum page_entry_size {
    PE_SIZE_PTE = 0,
    PE_SIZE_PMD,
    PE_SIZE_PUD,
};
#endif


#define GET_R(x, val)\
    ({\
        asm("mov %%"x", %%rax\n\t"\
            "mov %%rax, %0\n\t"\
            : "=m"(val) : : "%rax"\
        );\
    })

#define GET_CR2(val) GET_R("cr2", val)

#define INV_MASK(ps)                (~(ps - 1))
#define INV_ALIGN_DOWN(addr, ps)    (addr & INV_MASK(ps))
#define INV_ALIGN_UP(addr, ps)      ((addr + (ps - 1)) & INV_MASK(ps))


/*
 * Values in the radix tree cannot have these bits set:
 * - RADIX_TREE_ENTRY_MASK
 */
#define INV_PAGE_MASK             0x30
#define INV_PUD_PAGE              0x10
#define INV_PMD_PAGE              0x20

struct invirt_pfn_region {
    unsigned long first_pfn;
    unsigned long nr_pfns;
    struct list_head node;
};

struct invirt_pfn_range {
    unsigned long arch_flags;
    unsigned long total_size;  /* total size in bytes of underlying region */
    struct list_head regions;
};


/*
 * Each local page fault generates an invirt_remote_extent which corresponds
 * to a VMA in invg
 *
 * An extent's virtual address range maps one to one with a corresponding
 * remote extent in the invg shadow process. Depending on the operational mode,
 * an extent either maps to a single page or an entire guest VMA
 */
struct invirt_remote_extent {
    atomic_t nr_pages_refed;

    struct invirt_thread * th;
    struct vm_area_struct * vma;
    unsigned long vm_start;
    unsigned long * gpfn_list;
    unsigned long nr_pages;
    unsigned long pte_flags;

    /* radix tree stores gpa->hpa translations, with largest page size possible
     * for actually mapping each translation
     */
    struct radix_tree_root pgtable;

    /* rb-linkage */
    struct rb_node rb_node;
};

static int
__add_extent(struct rb_root              * root,
             struct invirt_remote_extent * new_extent)
{
    struct rb_node ** new    = &((root)->rb_node);
    struct rb_node  * parent = NULL;

    unsigned long new_start = new_extent->vm_start;
    unsigned long new_end   = new_start + (new_extent->nr_pages << PAGE_SHIFT);

    while (*new) {
        struct invirt_remote_extent * extent = container_of(*new, struct invirt_remote_extent, rb_node);
        unsigned long start = extent->vm_start;
        unsigned long end   = start + (extent->nr_pages << PAGE_SHIFT);

        parent = *new;
        if (new_end <= start)
            new = &((*new)->rb_left);
        else if (new_start >= end)
            new = &((*new)->rb_right);
        else
            return -EFAULT;
    }

    rb_link_node(&(new_extent->rb_node), parent, new);
    rb_insert_color(&(new_extent->rb_node), root);

    return 0;
}

/* find extent containing this address */
static struct invirt_remote_extent *
__find_containing_extent(struct rb_root * root,
                         unsigned long    start)
{
    struct rb_node * node;

    for (node = rb_first(root); node != NULL; node = rb_next(node)) {
        struct invirt_remote_extent * extent = container_of(node, struct invirt_remote_extent, rb_node);
        unsigned long extent_start = extent->vm_start;
        unsigned long extent_end   = extent_start + (extent->nr_pages << PAGE_SHIFT);

        if (start >= extent_start && start < extent_end)
            return extent;
    }

    return ERR_PTR(-ENOENT);
}

static void
__print_extents(struct rb_root * root)
{
    struct rb_node * node;
    struct rb_node * next;
    struct invirt_remote_extent * extent;

    inv_dbg(3, "printing remote extent tree\n");

    for (node = rb_first(root); node != NULL; node = next) {
        next = rb_next(node);
        extent = container_of(node, struct invirt_remote_extent, rb_node);
        inv_dbg(3, "  [0x%lx, 0x%lx)\n",
            extent->vm_start, extent->vm_start + (extent->nr_pages << PAGE_SHIFT)
        );
    }
}

static int
__invirt_follow_page(struct invirt_thread  * th,
                     struct vm_area_struct * target_vma,
                     struct vm_area_struct * vma,
                     unsigned long           fault_address,
                     unsigned long           vmf_flags,
                     struct page          ** page)
{
    unsigned long gup_flags;
    long rc;

    gup_flags = FOLL_POPULATE | FOLL_MLOCK | FOLL_GET;
    if (vma->vm_flags & VM_WRITE)
        gup_flags |= FOLL_WRITE;

    rc = get_user_pages_remote(
#if LINUX_VERSION_CODE < KERNEL_VERSION(5,9,0)
        th->kvm_task,
#endif
        th->kvm_mm,
        fault_address,
        1,
        gup_flags,
        page,
        NULL
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,10,0)
        , NULL
#endif
    );

    if (rc != 1)
        inv_err("get_user_pages failed (err=%ld)\n", rc);

    return (rc == 1) ? 0 : rc;
}

static int
__invirt_follow_pfn(struct task_struct    * task,
                    struct mm_struct      * mm,
                    struct vm_area_struct * vma,
                    unsigned long           fault_address,
                    unsigned long           vmf_flags,
                    unsigned long         * pfn)


{
    int status;

    status = follow_pfn(vma, fault_address, pfn);
    if (status != 0) {
        /* follow_pfn() just walks the PTs, it does not force faults -- so we
         * do so manually here
         */
        status = fixup_user_fault(
#if LINUX_VERSION_CODE < KERNEL_VERSION(5,9,0)
            task,
#endif
            mm,
            fault_address,
            (vmf_flags & FAULT_FLAG_WRITE) ? FAULT_FLAG_WRITE : 0,
            NULL
        );

        if (status != 0) {
            inv_err("fixup_user_fault() failed (err=%d)\n", status);
            return status;
        }

        status = follow_pfn(vma, fault_address, pfn);
        if (status != 0) {
            inv_err("follow_pfn() failed (err=%d)\n", status);
            return status;
        }
    }

    return 0;
}

static int
__invirt_gpfn_to_hpfn(struct invirt_remote_extent * extent,
                      struct vm_fault             * vmf,
                      unsigned long                 gpfn,
                      unsigned long               * hpfn)
{
    struct invirt_thread * th;
    struct vm_area_struct * kvm_vma;
    struct page * page;
    int status;
    unsigned long hva;

    th = extent->th;

    /* find HVA in kvm process where this memory lives */
    mutex_lock(&th->kvm->slots_lock);
    hva = gfn_to_hva(th->kvm, gpfn);
    mutex_unlock(&th->kvm->slots_lock);

    if (kvm_is_error_hva(hva)) {
        inv_err("failed to translate GFN to HVA\n");
        return -EFAULT;
    }

    /* find VMA in KVM process */
    mmap_read_lock(th->kvm_mm);

    kvm_vma = find_extend_vma(th->kvm_mm, hva);
    if (!kvm_vma || kvm_vma->vm_start > hva) {
        inv_err("could not find KVM VMA at HVA 0x%lx\n", hva);
        mmap_read_unlock(th->kvm_mm);
        return -EFAULT;
    }

    if (kvm_vma->vm_flags & (VM_IO | VM_PFNMAP)) {
        status = __invirt_follow_pfn(th->kvm_task, th->kvm_mm, kvm_vma, hva, vmf->flags, hpfn);
        if ((status == 0) && pfn_valid(*hpfn))
            get_page(pfn_to_page(*hpfn));
    } else {
        status = __invirt_follow_page(th, kvm_vma, extent->vma, hva, vmf->flags, &page);
        if (status == 0)
            *hpfn = page_to_pfn(page);
    }

    mmap_read_unlock(th->kvm_mm);

    /* if the pfn is valid, we refed it */
    if (status == 0 && pfn_valid(*hpfn))
        atomic_inc(&(extent->nr_pages_refed));

    return status;
}

static int
__invirt_add_pgtable_mapping(struct invirt_remote_extent * extent,
                             unsigned long                 gpfn,
                             unsigned long                 hpfn,
                             unsigned long                 num_pages,
                             unsigned long               * inserted)
{
    unsigned long gpa, hpa, page_size;
    int status;

    if (INV_PAGE_MASK & RADIX_TREE_ENTRY_MASK) {
        inv_err("INV_PAGE_MASK intersects RADIX_TREE_ENTRY_MASK!!\n");
        return -EFAULT;
    }

    gpa = PFN_PHYS(gpfn);
    hpa = PFN_PHYS(hpfn);

    /* determine gpa alignment */
    if (gpa == INV_ALIGN_DOWN(gpa, PUD_SIZE))
        page_size = PUD_SIZE;
    else if (gpa == INV_ALIGN_DOWN(gpa, PMD_SIZE))
        page_size = PMD_SIZE;
    else
        page_size = PAGE_SIZE;

    while (page_size > PAGE_SIZE) {
        if (hpa == INV_ALIGN_DOWN(hpa, page_size)) {
            /* hpa is aligned -- let's see if we have enough contiguous pgns to map with this size */
            if (num_pages >= (page_size >> PAGE_SHIFT))
                break;
        }

        /* fallthrough to lower size */
        page_size = page_size >> 9;
    }

    if (page_size == PUD_SIZE)
        hpa |= INV_PUD_PAGE;
    else if (page_size == PMD_SIZE)
        hpa |= INV_PMD_PAGE;

    BUG_ON(sizeof(void *) != sizeof(unsigned long));
    status = radix_tree_insert(&(extent->pgtable), gpa, (void *)hpa);
    if (status != 0) {
        inv_err("radix_tree insert failed: %d\n", status);
        return status;
    }

    *inserted = page_size >> PAGE_SHIFT;

    inv_dbg(3, "inserted %s mapping covering 0x%lx bytes from GPA [0x%lx,0x%lx) -> HPA [0x%lx, 0x%lx)\n",
        (page_size == PUD_SIZE) ? "PUD" :
        (page_size == PMD_SIZE) ? "PMD" :
        "PTE", *inserted << PAGE_SHIFT, gpa, gpa + (*inserted << PAGE_SHIFT), hpa, hpa + (*inserted << PAGE_SHIFT)
    );

    return 0;
}

static unsigned long
__invirt_del_pgtable_mapping(struct invirt_remote_extent * extent,
                             unsigned long                 gpfn,
                             unsigned long               * removed)
{
    unsigned long gpa, hpa, page_size;

    gpa = PFN_PHYS(gpfn);
    hpa = (unsigned long)radix_tree_delete(&(extent->pgtable), gpa);
    BUG_ON(hpa == 0);

    if (hpa & INV_PUD_PAGE)
        page_size = PUD_SIZE;
    else if (hpa & INV_PMD_PAGE)
        page_size = PMD_SIZE;
    else
        page_size = PAGE_SIZE;

    *removed = page_size >> PAGE_SHIFT;

    inv_dbg(3, "deleted %s mapping covering 0x%lx bytes from GPA [0x%lx,0x%lx) -> HPA [0x%lx, 0x%lx)\n",
        (page_size == PUD_SIZE) ? "PUD" :
        (page_size == PMD_SIZE) ? "PMD" :
        "PTE", *removed << PAGE_SHIFT, gpa, gpa + (*removed << PAGE_SHIFT), hpa, hpa + (*removed << PAGE_SHIFT)
    );

    /* return page aligned address */
    return PHYS_PFN(hpa);
}

static int
__invirt_add_pgtable_range(struct invirt_remote_extent * extent,
                           unsigned long                 gpfn,
                           unsigned long                 hpfn,
                           unsigned long                 num_pages)
{
    unsigned long added, removed, total, total_rm;
    int status;

    for (total = 0; total < num_pages; total += added) {
        status = __invirt_add_pgtable_mapping(
            extent,
            gpfn + total,
            hpfn + total,
            num_pages - total,
            &added
        );
        if (status != 0)
            goto remove;
    }

    return 0;

remove:
    for (total_rm = 0; total_rm < total; total_rm += removed)
        (void)__invirt_del_pgtable_mapping(extent, gpfn + total_rm, &removed);
    return status;
}

static void
__invirt_delete_pgtable(struct invirt_remote_extent * extent)
{
    unsigned long j, gpfn, hpfn, cur, removed;

    j = 0;
    while (j < extent->nr_pages) {
        gpfn = extent->gpfn_list[j];
        hpfn = __invirt_del_pgtable_mapping(extent, gpfn, &removed);

        for (cur = 0; cur < removed; cur++) {
            if (pfn_valid(hpfn + cur)) {
                put_page(pfn_to_page(hpfn + cur));
                atomic_dec(&(extent->nr_pages_refed));
            }
        }

        j += removed;
    }
}

/*
 * Convert a guest pfn list in the remote_extent to a set of host pfns, and
 * store the translation in a radix tree
 */
static int
__invirt_generate_pgtable(struct invirt_remote_extent * extent,
                          struct vm_fault             * vmf)
{
    unsigned long i, gpfn, hpfn, gbase, hbase, contig;
    int status;

    for (i = 0, contig = 0; i < extent->nr_pages; i++) {
        gpfn = extent->gpfn_list[i];

        /* convert to hpfn */
        status = __invirt_gpfn_to_hpfn(extent, vmf, gpfn, &hpfn);
        if (status != 0)
            goto err;

        inv_dbg(3, "converted 0x%lx:0x%lx\n", gpfn, hpfn);

        if (i == 0) {
            gbase = gpfn;
            hbase = hpfn;
        }

        /* if we discover host or guest discontiguity, commit to radix tree */
        if ((gpfn != (gbase + contig)) || (hpfn != (hbase + contig))) {
            status = __invirt_add_pgtable_range(extent, gbase, hbase, contig);
            if (status != 0)
               goto err;

            contig = 0;
            gbase = gpfn;
            hbase = hpfn;
        }

        contig++;
    }

    /* commit last contingent */
    if (contig > 0) {
        status = __invirt_add_pgtable_range(extent, gbase, hbase, contig);
        if (status != 0)
            goto err;
    }

    return 0;

err:
     /*
      * The range:
      *   [hbase, hbase+contig)
      * has been ref'd, but not added to the pgtable. Explicitly deref
      */
    for (i = 0; i < contig; i++) {
        hpfn = hbase + i;
        if (pfn_valid(hpfn)) {
            put_page(pfn_to_page(hpfn));
            atomic_dec(&(extent->nr_pages_refed));
        }
    }

    /* remove anything in the pgtable */
    __invirt_delete_pgtable(extent);

    return status;
}

static int
__invirt_fetch_pfns(struct invirt_thread         * th,
                    struct vm_fault              * vmf,
                    struct vm_area_struct        * vma,
                    unsigned long                  fault_address,
                    struct invirt_remote_extent ** extent_p)
{
    struct invirt_remote_extent * extent;
    unsigned long base_va, pte_flags, nr_pages;
    int status;

    /* generate remote_extent covering the provided range */
    extent = kmalloc(sizeof(struct invirt_remote_extent), GFP_KERNEL);
    if (extent == NULL)
        return -ENOMEM;

    /* need to release mmap sem and mutex to prevent deadlock if this
     * call results an an mmu notifier being sent down to us
     */
    mutex_unlock(&(th->mutex));
    mmap_read_unlock(th->mm);

    /* query gpa */
    status = invirt_send_page_fault(
        th,
        fault_address,
        vmf->flags,
        &base_va,
        &(extent->gpfn_list),
        &nr_pages,
        &pte_flags
    );

    mmap_read_lock(th->mm);
    mutex_lock(&(th->mutex));

    if (status != 0) {
        inv_err("remote page fault failed (err=%d)\n", status);
        //dump_stack();
        goto out_remote;
    }

    extent->th = th;
    extent->vma = vma;
    extent->vm_start = base_va;
    extent->nr_pages = nr_pages;
    extent->pte_flags = pte_flags;
    INIT_RADIX_TREE(&(extent->pgtable), GFP_KERNEL);

    atomic_set(&(extent->nr_pages_refed), 0);

    /* generate pgtable for the gpa->hpa mappings */
    status = __invirt_generate_pgtable(extent, vmf);
    if (status != 0)
        goto out_pgtable;

    /* save the extent */
    status = __add_extent(&(th->remote_extent_root), extent);
    if (status != 0)
        goto out_save;

    __print_extents(&(th->remote_extent_root));

    inv_dbg(2, "added remote extent for [0x%lx, 0x%lx)\n",
        extent->vm_start,
        extent->vm_start + (extent->nr_pages << PAGE_SHIFT)
    );

    *extent_p = extent;
    return 0;

out_save:
    __invirt_delete_pgtable(extent);
out_pgtable:
    vfree(extent->gpfn_list);
out_remote:
    kfree(extent);
    return status;
}

static bool
__invirt_walk_pgtable(struct invirt_remote_extent * extent,
                      unsigned long                 gpa,
                      unsigned long               * hpa_p,
                      unsigned long               * page_size_p)
{
    unsigned long page_size, hpa;

    /* try PUD */
    page_size = PUD_SIZE;
    hpa = (unsigned long)radix_tree_lookup(&(extent->pgtable), INV_ALIGN_DOWN(gpa, page_size));
    if (hpa && (hpa & INV_PUD_PAGE))
        goto found;

    /* try PMD */
    page_size = PMD_SIZE;
    hpa = (unsigned long)radix_tree_lookup(&(extent->pgtable), INV_ALIGN_DOWN(gpa, page_size));
    if (hpa && (hpa & INV_PMD_PAGE))
        goto found;

    /* try PTE */
    page_size = PAGE_SIZE;
    hpa = (unsigned long)radix_tree_lookup(&(extent->pgtable), gpa);
    if (hpa)
        goto found;

    return false;

found:
    /* clear out the lower order bits */
    *hpa_p       = (hpa >> PAGE_SHIFT) << PAGE_SHIFT;
    *page_size_p = page_size;
    return true;
}

static vm_fault_t
__invirt_vma_fault_pud(struct invirt_remote_extent * extent,
                       struct vm_fault             * vmf,
                       unsigned long                 fault_address)
{
#ifndef CONFIG_HAVE_ARCH_TRANSPARENT_HUGEPAGE_PUD
    return VM_FAULT_FALLBACK;
#else

    unsigned long gpfn, hpfn, pgoff, gpa, hpa, page_size;
    pfn_t pfn;
    bool found;
    int status;

    inv_dbg(3, "received PUD fault at 0x%lx\n", fault_address);

    pgoff = (fault_address - extent->vm_start) >> PAGE_SHIFT;
    gpfn = extent->gpfn_list[pgoff];

    /* walk the pagetable looking for a PUD mapping */
    gpa = INV_ALIGN_DOWN(PFN_PHYS(gpfn), PUD_SIZE);
    found = __invirt_walk_pgtable(extent, gpa, &hpa, &page_size);
    if (!found)
        return VM_FAULT_FALLBACK;

    switch (page_size) {
    case PUD_SIZE:
        break;
    case PMD_SIZE:
    case PAGE_SIZE:
        /* if we only find a PMD or PTE here, fallback */
        return VM_FAULT_FALLBACK;
    default:
        BUG_ON(1);
    }

    BUG_ON(hpa != INV_ALIGN_DOWN(hpa, PUD_SIZE));
    hpfn = PHYS_PFN(hpa);
    if (WARN_ON(!pfn_valid(hpfn)))
        inv_dbg(1, "hpfn 0x%lx is NOT valid\n", hpfn);

    pfn = pfn_to_pfn_t(hpfn);
    status = vmf_insert_pfn_pud(vmf, pfn, vmf->flags);
    if (status != VM_FAULT_NOPAGE) {
        inv_err("vm_insert_pfn_pmf failed (err=%d)\n", status);
        return VM_FAULT_SIGBUS;
    }

    inv_dbg(3, "mapped PUD page for fault at 0x%lx\n", fault_address);
    return VM_FAULT_NOPAGE;
#endif
}

static vm_fault_t
__invirt_vma_fault_pmd(struct invirt_remote_extent * extent,
                       struct vm_fault             * vmf,
                       unsigned long                 fault_address)
{
    unsigned long gpfn, hpfn, pgoff, gpa, hpa, page_size;
    pfn_t pfn;
    bool found;
    int status;

    inv_dbg(3, "received PMD fault at 0x%lx\n", fault_address);

    pgoff = (fault_address - extent->vm_start) >> PAGE_SHIFT;
    gpfn = extent->gpfn_list[pgoff];

    /* walk the pagetable looking for a PMD mapping */
    gpa = INV_ALIGN_DOWN(PFN_PHYS(gpfn), PMD_SIZE);
    found = __invirt_walk_pgtable(extent, gpa, &hpa, &page_size);
    if (!found)
        return VM_FAULT_FALLBACK;

    switch (page_size) {
    case PUD_SIZE:
        /* add offset into PUD */
        hpa += (gpa - INV_ALIGN_DOWN(gpa, PUD_SIZE));
        break;
    case PMD_SIZE:
        break;
    case PAGE_SIZE:
        /* if we only find a PTE here, fallback */
        return VM_FAULT_FALLBACK;
    default:
        BUG_ON(1);
    }

    BUG_ON(hpa != INV_ALIGN_DOWN(hpa, PMD_SIZE));
    hpfn = PHYS_PFN(hpa);
    if (WARN_ON(!pfn_valid(hpfn)))
        inv_dbg(1, "hpfn 0x%lx is NOT valid\n", hpfn);

    pfn = pfn_to_pfn_t(hpfn);
    status = vmf_insert_pfn_pmd(vmf, pfn, vmf->flags);
    if (status != VM_FAULT_NOPAGE) {
        inv_err("vm_insert_pfn_pmf failed (err=%d)\n", status);
        return VM_FAULT_SIGBUS;
    }

    inv_dbg(3, "mapped PMD page for fault at 0x%lx\n", fault_address);
    return VM_FAULT_NOPAGE;
}

static vm_fault_t
__invirt_vma_fault_pte(struct invirt_remote_extent * extent,
                       struct vm_fault             * vmf,
                       unsigned long                 fault_address)
{
    unsigned long gpfn, hpfn, pgoff, gpa, hpa, page_size;
    pfn_t pfn;
    bool found;
    int status;

    pgoff = (fault_address - extent->vm_start) >> PAGE_SHIFT;
    gpfn = extent->gpfn_list[pgoff];
    gpa = PFN_PHYS(gpfn);

    found = __invirt_walk_pgtable(extent, gpa, &hpa, &page_size);
    if (!found) {
        inv_err("could not convert gpfn 0x%lx to hpfn\n", gpfn);
        return VM_FAULT_SIGBUS;
    }

    switch (page_size) {
    case PUD_SIZE:
        /* add offset into PUD */
        hpa += (gpa - INV_ALIGN_DOWN(gpa, PUD_SIZE));
        break;
    case PMD_SIZE:
        /* add offset into PMD */
        hpa += (gpa - INV_ALIGN_DOWN(gpa, PMD_SIZE));
        break;
    case PAGE_SIZE:
        break;
    default:
        BUG_ON(1);
    }

    hpfn = PHYS_PFN(hpa);
    if (WARN_ON(!pfn_valid(hpfn)))
        inv_dbg(1, "hpfn 0x%lx which is NOT valid\n", hpfn);

    pfn = pfn_to_pfn_t(hpfn);
    status = vmf_insert_mixed(extent->vma, extent->vm_start + (pgoff << PAGE_SHIFT), pfn);
    if (status != VM_FAULT_NOPAGE) {
        inv_err("vm_insert_mixed failed (err=%d)\n", status);
        return VM_FAULT_SIGBUS;
    }

    inv_dbg(3, "mapped 1 page at 0x%lx\n", fault_address);
    return VM_FAULT_NOPAGE;

    /* FIXME: vma page prots are R+W+X -- may need to use prots from invg to
     * trigger mprotect, sigsegv, and such
     */

    /*
    status = remap_pfn_range(
        vma,
        extent->vm_start + (i << PAGE_SHIFT),
        hpfn,
        PAGE_SIZE,
        vma->vm_page_prot
    );
    */
}

static vm_fault_t
__invirt_vma_fault(struct invirt_thread  * th,
                   struct vm_fault       * vmf,
                   struct vm_area_struct * vma,
                   unsigned long           fault_address,
                   enum page_entry_size    pe_size)
{
    int status;
    uint64_t cr2;
    struct invirt_remote_extent * extent;
    vm_fault_t ret;

    GET_CR2(cr2);
    inv_dbg(3, "page fault at 0x%llx (fault_address=0x%lx)\n", cr2, fault_address);

    /* grab extent, or fetch it if we havn't covered this region yet */
    extent = __find_containing_extent(&(th->remote_extent_root), fault_address);
    if (IS_ERR(extent)) {
        status = __invirt_fetch_pfns(th, vmf, vma, fault_address, &extent);
        if (status != 0)
            return VM_FAULT_SIGBUS;
    }

    BUG_ON(fault_address < extent->vm_start);
    BUG_ON(fault_address >= (extent->vm_start + (extent->nr_pages << PAGE_SHIFT)));

    switch (pe_size) {
    case PE_SIZE_PUD:
        ret = __invirt_vma_fault_pud(extent, vmf, fault_address);
        break;
    case PE_SIZE_PMD:
        ret = __invirt_vma_fault_pmd(extent, vmf, fault_address);
        break;
    case PE_SIZE_PTE:
        ret = __invirt_vma_fault_pte(extent, vmf, fault_address);
        break;
    default:
        inv_err("received PF at pe_size:%d; cannot handle this size\n", pe_size);
        ret = VM_FAULT_SIGBUS;
        break;
    }

    return ret;
}

static vm_fault_t
do_invirt_vma_fault(struct vm_area_struct * vma,
                    struct vm_fault       * vmf,
                    enum page_entry_size    pe_size)
{
    unsigned long fault_address;

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,10,0)
    fault_address = PAGE_ALIGN(vmf->virtual_address);
#else
    fault_address = PAGE_ALIGN(vmf->address);
#endif

    {
        struct invirt_thread * th;
        vm_fault_t ret;

        th = invirt_th_ref_by_pid(current->pid);
        if (IS_ERR(th))
            return VM_FAULT_SIGBUS;

        mutex_lock(&(th->mutex));
        ret = __invirt_vma_fault(th, vmf, vma, fault_address, pe_size);
        mutex_unlock(&(th->mutex));

        invirt_th_deref(th);
        return ret;
    }
}

static vm_fault_t
invirt_vma_fault(
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,11,0)
                 struct vm_area_struct * vma,
#endif
                 struct vm_fault       * vmf)
{
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,11,0)
    struct vm_area_struct * vma = vmf->vma;
#endif
    return do_invirt_vma_fault(vma, vmf, PE_SIZE_PTE);
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,11,0)
static vm_fault_t
invirt_vma_huge_fault(struct vm_fault    * vmf,
                      enum page_entry_size pe_size)
{
    return do_invirt_vma_fault(vmf->vma, vmf, pe_size);
}
#endif


static int
invirt_vma_access(struct vm_area_struct * vma,
                  unsigned long           addr,
                  void                  * buf,
                  int                     len,
                  int                     write)
{
    unsigned long pfn;
    struct page * page;
    void * kaddr;
    int offset = addr & (PAGE_SIZE - 1);

    if (follow_pfn(vma, addr, &pfn))
        return -ENOMEM;

    page = pfn_to_page(pfn);
    kaddr = kmap(page);
    if (!kaddr)
        return -ENOMEM;

    if (write)
        copy_to_user_page(vma, page, addr, kaddr + offset, buf, len);
    else
        copy_from_user_page(vma, page, addr, buf, kaddr + offset, len);

    kunmap(page);
    return len;
}

static void
invirt_vma_open(struct vm_area_struct * vma)
{
}

static void
invirt_vma_close(struct vm_area_struct * vma)
{
    /* TODO: release any extents that overlap this vma */
    inv_dbg(2, "vma [0x%lx, 0x%lx) closed\n", vma->vm_start, vma->vm_end);
}

static struct vm_operations_struct
invirt_vma_ops =
{
    .open  = invirt_vma_open,
    .close = invirt_vma_close,
    .huge_fault = invirt_vma_huge_fault,
    .fault = invirt_vma_fault,
    .access = invirt_vma_access,
};

int
invirt_setup_vma(struct invirt_thread  * th,
                 struct vm_area_struct * vma)
{
    vma->vm_flags |= VM_IO | VM_DONTEXPAND | VM_MIXEDMAP | VM_HUGEPAGE;
    vma->vm_ops = &invirt_vma_ops;
    return 0;
}

static void
__invirt_release_remote_extent(struct invirt_thread        * th,
                               struct invirt_remote_extent * remote_extent)
{
    unsigned long length, start;

    /* deref all host pages */
    __invirt_delete_pgtable(remote_extent);

    /* zap PTEs */
    start  = remote_extent->vm_start;
    length = (remote_extent->nr_pages << PAGE_SHIFT);

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,18,0)
    WARN_ON(zap_vma_ptes(remote_extent->vma, start, length) != 0);
#else
    zap_vma_ptes(remote_extent->vma, start, length);
#endif

    WARN_ON(atomic_read(&(remote_extent->nr_pages_refed)) != 0);
}

static void
__invirt_release_remote_extents(struct invirt_thread * th,
                                unsigned long          start,
                                unsigned long          end)
{
    struct rb_node * node, * next;
    struct invirt_remote_extent * extent;
    unsigned long remote_start, remote_end;

    for (node = rb_first(&(th->remote_extent_root)); node != NULL; node = next) {
        next = rb_next(node);
        extent = container_of(node, struct invirt_remote_extent, rb_node);

        remote_start = extent->vm_start;
        remote_end   = remote_start + (extent->nr_pages << PAGE_SHIFT);

        if (end && remote_start >= end)
            break;
        else if (start && start >= remote_end)
            continue;

        __invirt_release_remote_extent(th, extent);

        inv_dbg(2, "released remote extent for [0x%lx, 0x%lx)\n",
            extent->vm_start,
            extent->vm_start + (extent->nr_pages << PAGE_SHIFT)
        );

        rb_erase(&(extent->rb_node), &(th->remote_extent_root));
        vfree(extent->gpfn_list);
        kfree(extent);
    }
}

void
invirt_release_remote_extents(struct invirt_thread * th)
{
    inv_dbg(1, "releasing all th %d remote extents\n", th->pid);
    __invirt_release_remote_extents(th, 0, 0);
}

int
invirt_release_page_range(struct invirt_thread * th,
                          unsigned long          start,
                          unsigned long          nr_pages)
{
    mmap_read_lock(th->mm);
    mutex_lock(&(th->mutex));
    __invirt_release_remote_extents(th, start, start + (nr_pages << PAGE_SHIFT));
    mutex_unlock(&(th->mutex));
    mmap_read_unlock(th->mm);

    return 0;
}
