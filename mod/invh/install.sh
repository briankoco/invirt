#!/bin/bash

if [ -c /dev/invh ]; then
    rmmod invh
fi

symbol_oct() {
    local s="$1"
    echo "obase=10; ibase=16; ${s^^}" | bc
}

symbol_kvm() {
    local name="$1"
    sym=$(grep "\[kvm\]" /proc/kallsyms | grep " $name" | cut -d " " -f 1)
    if [ -z $sym ]; then
        echo "Error: could not find [kvm] symbol '$sym' in /proc/kallsyms"
        exit 1
    fi
    echo $(symbol_oct $sym)
}

symbol() {
    local name="$1"
    sym=$(grep " $name$" /proc/kallsyms | cut -d " " -f 1)
    if [ -z $sym ]; then
        echo "Error: could not find symbol '$sym' in /proc/kallsyms"
        exit 1
    fi
    echo $(symbol_oct $sym)
}

sys_call_table=$(symbol "sys_call_table")
vm_list=$(symbol_kvm "vm_list")
kvm_lock=$(symbol_kvm "kvm_lock")

insmod build/invh.ko \
    invirt_debug_level=2 \
    kallsyms_sys_call_table=$sys_call_table \
    kallsyms_vm_list=$vm_list \
    kallsyms_kvm_lock=$kvm_lock

chmod 666 /dev/invh
