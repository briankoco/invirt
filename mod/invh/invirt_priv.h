/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#ifndef __INVIRT_PRIV_H__
#define __INVIRT_PRIV_H__

#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/mm_types.h>
#include <linux/atomic.h>
#include <linux/sched.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/kvm_host.h>

#include <invirt.h>
#include <invirt_common.h>
#include <invirt_version.h>

#define INVIRT_MAGIC 0x1234

/*
 * info we need to track for invh threads running invirt programs
 */
struct invirt_thread {
    atomic_t refcnt;

    /* process id == thread_id on Linux */
    pid_t pid;

    /* Qemu/KVM process stuff */
    pid_t kvm_pid;
    struct kvm * kvm;
    struct task_struct * kvm_task;
    struct mm_struct * kvm_mm;

    /* invg process that we're mirroring */
    char uuid_str[INVD_UUID_STR_LEN];
    pid_t tracer_tgid;

    struct mutex mutex;
    struct mm_struct * mm;

    /* lock protects flags */
    spinlock_t lock;
    volatile int flags;

    /* conn info */
    pid_t target_pid;
    int mode;

    /* cache of regs/nr for most recent syscall */
    unsigned int sys_nr;
    union {
        struct pt_regs sys_pt_regs;
        struct {
            unsigned long arg0;
            unsigned long arg1;
            unsigned long arg2;
            unsigned long arg3;
            unsigned long arg4;
            unsigned long arg5;
        } sys_regs;
    };

    /* multiplex different communication modes */
    union {
        struct {
            struct socket * invg_sock;
        } socket;
    };

    /* RB-tree of remote page extents faulted in to guest */
    struct rb_root remote_extent_root;

    /* linkage on gbl_th_hashtable */
    struct list_head th_hashnode;

    /* linkage on gbl_tracer_hashtable */
    struct list_head tracer_hashnode;
};

/*
 * Attribute and state flags
 */
#define INVIRT_FLAG_DESTROYING      0x00010 /* being destroyed */
#define INVIRT_FLAG_DESTROYED       0x00020 /* 'being destroyed' finished */
#define INVIRT_FLAG_TRACED          0x00100 /* being traced */

/*
 * Communication modes
 */
#define INVIRT_TRACEMODE_SOCKET    0x1      /* socket tracing */

/* defined in invirt_core.c */
extern unsigned long kallsyms_sys_call_table;
extern unsigned long kallsyms_vm_list;
extern unsigned long kallsyms_kvm_lock;
extern struct invirt_hashlist * gbl_th_hashtable;
extern struct invirt_hashlist * gbl_tracer_hashtable;

/* defined in invirt_syscall.c */
int invirt_enable_syscalls(void);
void invirt_disable_syscalls(void);
int invirt_get_traced_thread(struct invirt_thread **);
long invirt_do_syscall_pt_regs(struct invirt_thread *, unsigned int, const struct pt_regs *);
long invirt_do_syscall_args(struct invirt_thread *, unsigned int,
                            unsigned long, unsigned long, unsigned long,
                            unsigned long, unsigned long, unsigned long);
long invirt_do_syscall_local(struct invirt_thread * th);

/* defined in invirt_vcall_invh.c */
int invirt_setup_tracee_socket(struct invirt_thread *, const char *, pid_t, int);
void invirt_teardown_tracee_socket(struct invirt_thread *);

int invirt_send_syscall(struct invirt_thread *, unsigned int,
                        unsigned long, unsigned long, unsigned long,
                        unsigned long, unsigned long, unsigned long,
unsigned long *);
int invirt_send_page_fault(struct invirt_thread *, unsigned long,
            unsigned int, unsigned long * , unsigned long **,
            unsigned long *, unsigned long *);

/* defined in invirt_umem.c */
int invirt_setup_vma(struct invirt_thread *, struct vm_area_struct *);
int invirt_release_page_range(struct invirt_thread *, unsigned long, unsigned long);
void invirt_release_remote_extents(struct invirt_thread *);

/* defined in invirt_mmunot.c */
int invirt_enable_mmunot(void);
void invirt_disable_mmunot(void);

/* defined in invirt_kvm.c */
struct kvm * invirt_get_kvm_by_pid(pid_t);
void invirt_put_kvm(struct kvm *);

struct invirt_hashlist {
    rwlock_t lock;
    struct list_head list;
} ____cacheline_aligned;

#define INVIRT_TH_HASHTABLE_SIZE 8
#define INVIRT_VM_HASHTABLE_SIZE 8

static inline int
invirt_th_hashtable_index(pid_t pid)
{
    return ((unsigned int)pid % INVIRT_TH_HASHTABLE_SIZE);
}

static inline int
invirt_tracer_hashtable_index(const char * uuid,
                              pid_t        tgid)
{
    uint64_t idx;
    uint32_t uuid_hash;
    char c;

    uuid_hash = 0;
    while ((c = *uuid++))
        uuid_hash += c;

    idx = ((uint64_t)uuid_hash << 32) + tgid;
    return ((unsigned int)idx % INVIRT_TH_HASHTABLE_SIZE);
}

static inline void
invirt_th_ref(struct invirt_thread * th)
{
    WARN_ON(atomic_read(&(th->refcnt)) <= 0);
    atomic_inc(&(th->refcnt));
}

static inline void
invirt_th_deref(struct invirt_thread * th)
{
    WARN_ON(atomic_read(&(th->refcnt)) <= 0);
    if (atomic_dec_return(&(th->refcnt)) != 0)
        return;

    WARN_ON(!(th->flags & INVIRT_FLAG_DESTROYED));

    inv_dbg(3, "last deref of th %d\n", th->pid);

    /* last put of this th .. tear it down */
    kfree(th);
}

static inline struct invirt_thread *
__invirt_th_ref_by_pid_nolock_internal(pid_t pid,
                                       int   index,
                                       int   return_destroying)
{
    struct invirt_thread * th;

    list_for_each_entry(th, &(gbl_th_hashtable[index].list), th_hashnode) {
        if (th->pid == pid) {
            if ((th->flags & INVIRT_FLAG_DESTROYING)  &&
                (return_destroying == 0))
            {
                continue;
            }

            invirt_th_ref(th);
            return th;
        }
    }

    return ERR_PTR(-ESRCH);
}

static inline struct invirt_thread *
__invirt_th_ref_by_pid(pid_t pid,
                       int   return_destroying,
                       int   do_lock)
{
    struct invirt_thread * th;
    int index;
    unsigned long flags;

    index = invirt_th_hashtable_index(pid);

    if (do_lock)
        read_lock_irqsave(&(gbl_th_hashtable[index].lock), flags);

    th = __invirt_th_ref_by_pid_nolock_internal(
        pid,
        index,
        return_destroying
    );

    if (do_lock)
        read_unlock_irqrestore(&(gbl_th_hashtable[index].lock), flags);

    return th;
}

#define invirt_th_ref_by_pid(t)            __invirt_th_ref_by_pid(t, 0, 1)
#define invirt_th_ref_by_pid_all(t)        __invirt_th_ref_by_pid(t, 1, 1)
#define invirt_th_ref_by_pid_nolock(t)     __invirt_th_ref_by_pid(t, 0, 0)
#define invirt_th_ref_by_pid_nolock_all(t) __invirt_th_ref_by_pid(t, 1, 0)

static inline struct invirt_thread *
__invirt_th_ref_by_tracer_nolock_internal(const char * uuid_str,
                                          pid_t        tgid,
                                          int          index,
                                          int          return_destroying)
{
    struct invirt_thread * th;

    list_for_each_entry(th, &(gbl_tracer_hashtable[index].list), tracer_hashnode) {
        if ((strncmp(th->uuid_str, uuid_str, INVD_UUID_STR_LEN) == 0) && (th->tracer_tgid == tgid)) {
            if ((th->flags & INVIRT_FLAG_DESTROYING)  &&
                (return_destroying == 0))
            {
                continue;
            }

            invirt_th_ref(th);
            return th;
        }
    }

    return ERR_PTR(-ESRCH);
}

static inline struct invirt_thread *
__invirt_th_ref_by_tracer(const char * uuid_str,
                          pid_t        tgid,
                          int          return_destroying,
                          int          do_lock)
{
    struct invirt_thread * th;
    int index;
    unsigned long flags;

    index = invirt_tracer_hashtable_index(uuid_str, tgid);

    if (do_lock)
        read_lock_irqsave(&(gbl_tracer_hashtable[index].lock), flags);

    th = __invirt_th_ref_by_tracer_nolock_internal(
        uuid_str,
        tgid,
        index,
        return_destroying
    );

    if (do_lock)
        read_unlock_irqrestore(&(gbl_tracer_hashtable[index].lock), flags);

    return th;
}

#define invirt_th_ref_by_tracer(v, t)            __invirt_th_ref_by_tracer(v, t, 0, 1)
#define invirt_th_ref_by_tracer_all(v, t)        __invirt_th_ref_by_tracer(v, t, 1, 1)
#define invirt_th_ref_by_tracer_nolock(v, t)     __invirt_th_ref_by_tracer(v, t, 0, 0)
#define invirt_th_ref_by_tracer_nolock_all(v, t) __invirt_th_ref_by_tracer(v, t, 1, 0)


static inline void
invirt_th_not_destroyable(struct invirt_thread * th)
{
    atomic_set(&(th->refcnt), 1);
}

static inline void
invirt_th_destroyable(struct invirt_thread * th)
{
    invirt_th_deref(th);
}

#endif /* __INVIRT_PRIV_H__ */
