/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#include <linux/kallsyms.h>
#include <linux/slab.h>
#include <linux/nospec.h>
#include <linux/module.h>

#include <asm/unistd.h>
#include <asm/syscall.h>
#include <asm/special_insns.h>

#include <invirt_priv.h>

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,17,0)
#define SYSCALL_USE_PT_REGS
#endif

/* atomic variable used to synchronize on_each_cpu calls */
static atomic_t invirt_smp_cnt = ATOMIC_INIT(0);

/* address of linux sys call table (queried via kallsyms) */
asmlinkage sys_call_ptr_t * lnx_sys_call_table = NULL;

/* a backup of the linux sys call table before we overwrite it */
asmlinkage sys_call_ptr_t * lnx_sys_call_table_bak = NULL;


/*
 * Linux now prevents you from flipping the WP bit at runtime (for good
 * reason), so use our own utilities
 */

static void
invirt_write_cr0(uint64_t val)
{
    asm volatile("mov %0,%%cr0" : : "r" (val));
}

static uint64_t
invirt_read_cr0(void)
{
    uint64_t val;
    asm volatile("mov %%cr0,%0" : "=r" (val));
    return val;
}

#define invirt_disable_wp() \
    invirt_write_cr0(invirt_read_cr0() & ~0x10000); WARN_ON((invirt_read_cr0() & 0x10000));

#define invirt_enable_wp() \
    invirt_write_cr0(invirt_read_cr0() | 0x10000); WARN_ON(!(invirt_read_cr0() & 0x10000));


/*
 * Newer kernels put pt_regs as single syscall argument, while older
 * pass register contents on the stack
 */

#ifdef __SYSCALL_64
#undef __SYSCALL_64
#endif

#ifdef SYSCALL_USE_PT_REGS
long
invirt_do_syscall_pt_regs(struct invirt_thread * th,
                          unsigned int           nr,
                          const struct pt_regs * regs)
{
    long ret;
    int status;

	th->sys_nr = nr;
    memcpy(&(th->sys_pt_regs), regs, sizeof(struct pt_regs));

    /* call invirt subsystem */
    status = invirt_send_syscall(
        th, nr, regs->di, regs->si, regs->dx, regs->r10, regs->r8, regs->r9, &ret
    );
    if (status != 0) {
        inv_err("failed to issue invirt syscall %u (err=%d)\n", nr, status);
        ret = status;
        goto out;
    }

out:
    invirt_th_deref(th);
    return ret;

}

asmlinkage long
invirt_ni_syscall(const struct pt_regs * regs)
{
    return -ENOSYS;
}

/*
 * First pass: syscall prototypes.
 * These are defined in invirt_syscall_stubs.S
 */
#if LINUX_VERSION_CODE < KERNEL_VERSION(5,7,0)
#define __SYSCALL_64(nr, sym, qual) asmlinkage long invirt_sys_##nr(const struct pt_regs * regs);
#else
#define __SYSCALL_X32(nr, sym)
#define __SYSCALL_64(nr, sym) extern long invirt_sys_##nr(const struct pt_regs * regs);
#define __SYSCALL_COMMON __SYSCALL_64
#endif

#include <asm/syscalls_64.h>

#else
long
invirt_do_syscall_regs(struct invirt_thread * th,
                       unsigned int           nr,
                       unsigned long          arg0,
                       unsigned long          arg1,
                       unsigned long          arg2,
                       unsigned long          arg3,
                       unsigned long          arg4,
                       unsigned long          arg5)
{
    long ret;
    int status;

    th->sys_nr = nr;
    th->sys_regs.arg0 = arg0;
    th->sys_regs.arg1 = arg1;
    th->sys_regs.arg2 = arg2;
    th->sys_regs.arg3 = arg3;
    th->sys_regs.arg4 = arg4;
    th->sys_regs.arg5 = arg5;

    /* call invirt subsystem */
    status = invirt_send_syscall(
        th, nr, arg0, arg1, arg2, arg3, arg4, arg5, &ret
    );
    if (status != 0) {
        inv_err("failed to issue invirt syscall %u (err=%d)\n", nr, status);
        ret = status;
        goto out;
    }

out:
    invirt_th_deref(th);
    return ret;
}

asmlinkage long
invirt_ni_syscall(unsigned long arg0,
                  unsigned long arg1,
                  unsigned long arg2,
                  unsigned long arg3,
                  unsigned long arg4,
                  unsigned long arg5)
{
    return -ENOSYS;
}

/*
 * First pass: syscall prototypes.
 * These are defined in invirt_syscall_stubs.S
 */
#define __SYSCALL_64(nr, sym, qual)\
    asmlinkage long invirt_sys_##nr(unsigned long arg0,\
                                    unsigned long arg1,\
                                    unsigned long arg2,\
                                    unsigned long arg3,\
                                    unsigned long arg4,\
                                    unsigned long arg5);
#include <asm/syscalls_64.h>

#endif

/*
 * Second pass: build the table
 */
#undef __SYSCALL_64

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,7,0)
#define __SYSCALL_64(nr, sym, qual) [nr] = invirt_sys_##nr,
#define __SYSCALL_COMMON __SYSCALL_64
#else
#define __SYSCALL_64(nr, sym) [nr] = invirt_sys_##nr,
#endif

asmlinkage sys_call_ptr_t invirt_sys_call_table[__NR_syscall_max+1] = {
    [0 ... __NR_syscall_max] = &invirt_ni_syscall,
#include <asm/syscalls_64.h>
};

static void
__update_syscall_table(bool          install,
                       unsigned long smp_id)
{
    atomic_inc(&invirt_smp_cnt);

    /* all CPUs except the initiator have nothing else to do */
    if (smp_id != smp_processor_id())
        return;

    /* wait until all cpus have issued this cross call to ensure
     * no one is using the sys call table
     */
    while (atomic_read(&invirt_smp_cnt) < num_online_cpus());

    /* disable write protection checking */
    invirt_disable_wp();

    if (install) {
        /* backup the table */
        memcpy(
            (void *)lnx_sys_call_table_bak,
            (void *)lnx_sys_call_table,
            sizeof(sys_call_ptr_t) * (__NR_syscall_max + 1)
        );

        /* install our table */
        memcpy(
            (void *)lnx_sys_call_table,
            (void *)invirt_sys_call_table,
            sizeof(sys_call_ptr_t) * (__NR_syscall_max + 1)
        );
    } else {
        /* restore the backup */
        memcpy(
            (void *)lnx_sys_call_table,
            (void *)lnx_sys_call_table_bak,
            sizeof(sys_call_ptr_t) * (__NR_syscall_max + 1)
        );
    }

    /* re-enable write protection */
    invirt_enable_wp();
}

static void
install_invirt_syscall_table(void * data)
{
    return __update_syscall_table(true, (unsigned long)data);
}

static void
uninstall_invirt_syscall_table(void * data)
{
    return __update_syscall_table(false, (unsigned long)data);
}

static void
invirt_hook_syscalls(void)
{
    unsigned long smp_id;

    atomic_set(&invirt_smp_cnt, 0);

    /* Install our syscall table */
    smp_id = get_cpu();
    on_each_cpu(install_invirt_syscall_table, (void *)smp_id, 1);
    put_cpu();
}

static void
invirt_unhook_syscalls(void)
{
    unsigned long smp_id;

    atomic_set(&invirt_smp_cnt, 0);

    /* Re-install original syscall table */
    smp_id = get_cpu();
    on_each_cpu(uninstall_invirt_syscall_table, (void *)smp_id, 1);
    put_cpu();
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,7,0)
static void *
invirt_try_lookup_symbol(const char * symbol_name)
{
    return (void *)kallsyms_lookup_name(symbol_name);
}
#else
static void *
invirt_try_lookup_symbol(const char * symbol_name)
{
    inv_err("cannot query symbol '%s' as kallsyms_lookup_name is no longer exported\n", symbol_name);
    return NULL;
}
#endif

int
invirt_enable_syscalls(void)
{
    /* find system call table */
    if (kallsyms_sys_call_table != 0) {
        lnx_sys_call_table = (sys_call_ptr_t *)kallsyms_sys_call_table;
    } else {
        lnx_sys_call_table = (sys_call_ptr_t *)invirt_try_lookup_symbol("sys_call_table");
        if (lnx_sys_call_table == NULL)
            return -ENODEV;
    }

    lnx_sys_call_table_bak = kmalloc(
        sizeof(sys_call_ptr_t) * (__NR_syscall_max + 1),
        GFP_KERNEL
    );
    if (lnx_sys_call_table_bak == NULL)
        return -ENOMEM;

    invirt_hook_syscalls();

    inv_dbg(1, "hooked sys_call_table at 0x%lx\n", (unsigned long)lnx_sys_call_table);

    return 0;
}

void
invirt_disable_syscalls(void)
{
    invirt_unhook_syscalls();
    kfree(lnx_sys_call_table_bak);
}

int
invirt_get_traced_thread(struct invirt_thread ** th_p)
{
    struct invirt_thread * th;
    int is_traced = 1;
    unsigned long flags;

    /* does invirt thread exist? */
    th = invirt_th_ref_by_pid(current->pid);
    if (IS_ERR(th))
        return PTR_ERR(th);

    /* is invirt thread being traced? */
    spin_lock_irqsave(&(th->lock), flags);
    if (unlikely(!(th->flags & INVIRT_FLAG_TRACED)))
        is_traced = 0;
    spin_unlock_irqrestore(&(th->lock), flags);

#if 1
    if (unlikely(!is_traced)) {
        invirt_th_deref(th);
        return -ESRCH;
    }
#endif

    *th_p = th;
    return 0;
}

long
invirt_do_syscall_local(struct invirt_thread * th)
{
#ifdef SYSCALL_USE_PT_REGS
    return lnx_sys_call_table_bak[th->sys_nr](&(th->sys_pt_regs));
#else
    return lnx_sys_call_table_bak[th->sys_nr](
        th->sys_regs.arg0,
        th->sys_regs.arg1,
        th->sys_regs.arg1,
        th->sys_regs.arg2,
        th->sys_regs.arg3,
        th->sys_regs.arg4,
        th->sys_regs.arg5
    );
#endif
}
