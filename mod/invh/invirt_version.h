/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

/*
 * This file sets various preprocessor CONFIG_ settings based on the version of
 * the Linux kernel. Some individual files may still rely on their version checks,
 * but in general we try to keep most of this checking in here
 */

#include <linux/version.h>
#include <linux/kconfig.h>

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,17,0)
#define INVIRT_SYSCALL_USE_PT_REGS
#endif
