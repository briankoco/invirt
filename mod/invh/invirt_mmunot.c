/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/kthread.h>
#include <linux/kref.h>
#include <linux/idr.h>

#include <linux/file.h>
#include <linux/net.h>
#include <linux/inet.h>
#include <linux/skbuff.h>
#include <linux/wait.h>
#include <net/sock.h>

#include <invirt.h>
#include <invirt_vcall.h>
#include <invirt_priv.h>

typedef enum {
    MMU_RX_INITED = 0,
    MMU_RX_RUNNING,
    MMU_RX_EXITING,
    MMU_RX_EXITED
} mmu_rx_state_t;

struct mmu_rx_struct {
    struct task_struct * thread;
    struct socket * socket;

    int vmid;
    char uuid_str[INVD_UUID_STR_LEN];

    struct mutex mutex;
    bool should_exit;
    mmu_rx_state_t state;

    struct kref refcnt;
    struct list_head node;
};

/* master mmu_rx thread */
static struct mmu_rx_struct * master_rx;
static DEFINE_IDA(vmid_ida);

#define __mmu_out(loglevel, mmu_rx, fmt, args...)\
    do {\
        if (mmu_rx->vmid == -1)\
            printk(loglevel "[invh_mmu_master]: " fmt, ##args);\
        else\
            printk(loglevel "[invh_mmu_rx%d]: " fmt, mmu_rx->vmid, ##args);\
    } while (0)

#define mmu_out(mmu_rx, fmt, args...) __mmu_out(KERN_INFO, mmu_rx, fmt, ##args)
#define mmu_err(mmu_rx, fmt, args...) __mmu_out(KERN_ERR, mmu_rx, fmt, ##args)
#define mmu_dbg(mmu_rx, lvl, fmt, args...)\
    do {\
        if (lvl <= invirt_debug_level)\
            __mmu_out(KERN_INFO, mmu_rx, fmt, ##args);\
    } while(0)

static void
mmu_get(struct mmu_rx_struct * mmu_rx)
{
    kref_get(&(mmu_rx->refcnt));
}

static void
mmu_rx_last_put(struct kref * kref)
{
    struct mmu_rx_struct * mmu_rx = container_of(kref, struct mmu_rx_struct, refcnt);

    /* if necessary, shutdown has already happened */
    sock_release(mmu_rx->socket);
    mmu_dbg(mmu_rx, 3, "freeing mmu_rx\n");

    kfree(mmu_rx);
}

static void
mmu_put(struct mmu_rx_struct * mmu_rx)
{
    kref_put(&(mmu_rx->refcnt), mmu_rx_last_put);
}

static int
invirt_str_to_ipaddr(const char * str,
                     u8         * dst)
{
    /* this function returns 0 on failure ... */
    if (WARN_ON(in4_pton(str, -1, dst, -1, NULL) == 0))
        return -EIO;

    return 0;
}

static void
do_vcall_mmunot(struct mmu_rx_struct * mmu_rx,
                struct invirt_vcall  * vcall)
{
    struct invirt_thread * th;

    mmu_dbg(mmu_rx, 3, "received vcall msg_type:%d, tracer_tgid:%d\n", vcall->msg_type, vcall->tracer_tgid);

    vcall->mmu_not.rc = -EINVAL;

    if (strncmp(mmu_rx->uuid_str, vcall->uuid_str, INVD_UUID_STR_LEN) != 0) {
        mmu_err(mmu_rx, "received MMU invalidation from wrong tracer; uuid:%s, expected:%s\n", 
            vcall->uuid_str, mmu_rx->uuid_str);
        goto bad_uuid;
    }

    /* find thread */
    vcall->mmu_not.rc = -ESRCH;
    th = invirt_th_ref_by_tracer(mmu_rx->uuid_str, vcall->tracer_tgid);
    if (IS_ERR(th)) {
        if (!vcall->mmu_not.destroying) 
            mmu_err(mmu_rx, "could not find tracee with tracer_tgid=%d\n", vcall->tracer_tgid);
        goto bad_tgid;
    }

    mmu_dbg(mmu_rx, 2, "received MMU invalidation for page range: [0x%lx, 0x%lx)\n",
        vcall->mmu_not.vm_start,
        vcall->mmu_not.vm_start + (vcall->mmu_not.nr_pages << 12)
    );

    /* release pages */
    vcall->mmu_not.rc = invirt_release_page_range(th, vcall->mmu_not.vm_start, vcall->mmu_not.nr_pages);

    invirt_th_deref(th);
bad_tgid:
bad_uuid:
    return;
}

/*
 * core processing loop for mmu_notifier invalidation management
 */
static int
child_rx_fn(void * arg)
{
    struct mmu_rx_struct * mmu_rx = (struct mmu_rx_struct *)arg;
    struct invirt_vcall vcall;
    int status;

    mmu_dbg(mmu_rx, 2, "inited\n");

    while (1) {
        status = invirt_recv_vcall_socket(mmu_rx->socket, &vcall, false, true);
        if (kthread_should_stop())
            break;

        /* hang up */
        if (status != 0) {
            mmu_err(mmu_rx, "receive error (err=%d)\n", status);
            break;
        }

        if (vcall.msg_type != INVIRT_VCALL_MMU_NOT)
            mmu_err(mmu_rx, "malformed vcall: not of type INVIRT_VCALL_MMU_NOT\n");
        else
            do_vcall_mmunot(mmu_rx, &vcall);

        status = invirt_send_vcall_socket(mmu_rx->socket, &vcall);
        if (status != 0)
            mmu_err(mmu_rx, "send error (err=%d)\n", status);
    }

    kernel_sock_shutdown(mmu_rx->socket, SHUT_RDWR);

    /* if we were told to exit, then we need to wait for a kthread_stop() */
    mutex_lock(&(mmu_rx->mutex));
    while ((mmu_rx->state == MMU_RX_EXITING) && (!kthread_should_stop()))
        schedule();

    mmu_rx->state = MMU_RX_EXITED;
    mutex_unlock(&(mmu_rx->mutex));

    mmu_dbg(mmu_rx, 2, "exited\n");
    mmu_put(mmu_rx);

    return 0;
}

static int
master_do_accept(struct mmu_rx_struct * mmu_rx,
                 struct socket       ** new_socket,
                 int                  * new_vmid,
                 char                 * new_uuid,
                 size_t                 uuid_str_len)
{
    int status;
    struct socket * socket;
    struct invirt_vcall vcall;

    status = kernel_accept(mmu_rx->socket, &socket, 0);
    if (mmu_rx->should_exit) {
        if (WARN_ON(status == 0)) {
            kernel_sock_shutdown(socket, SHUT_RDWR);
            sock_release(socket);
        }
        return 0;
    }

    if (status < 0) {
        mmu_err(mmu_rx, "failed to accept connection (err=%d)\n", status);
        return status;
    }

    /* pull in the first packet to indicate which uuid this is for */
    status = invirt_recv_vcall_socket(socket, (void *)&vcall, false, true);
    if ((status != 0) ||
        (vcall.msg_type != INVIRT_VCALL_MMU_NOT) ||
        (vcall.mmu_not.msg_type != INVIRT_MMU_NOT_INIT))
    {
        mmu_err(mmu_rx, "malformed connection request\n");
        kernel_sock_shutdown(socket, SHUT_RDWR);
        sock_release(socket);
        return -EIO;
    }

    *new_socket = socket;
    strncpy(new_uuid, vcall.uuid_str, uuid_str_len);
    *new_vmid = ida_alloc(&vmid_ida, GFP_KERNEL);
    if (*new_vmid < 0) {
        mmu_err(mmu_rx, "failed to allocate id: %d\n", *new_vmid);
        kernel_sock_shutdown(socket, SHUT_RDWR);
        sock_release(socket);
        return -EIO;
    }

    return 0;
}

static void
master_stop_thread(struct mmu_rx_struct * node)
{
    if (node->state != MMU_RX_EXITED) {
        node->state = MMU_RX_EXITING;
        mutex_unlock(&(node->mutex));
        kthread_stop(node->thread);
    } else
        mutex_unlock(&(node->mutex));

    BUG_ON(node->state != MMU_RX_EXITED);
    list_del(&(node->node));
    mmu_put(node);
}

/*
 * thread fn for master thread which listens for incoming
 * mmu connections
 */
static int
master_mmu_rx_fn(void * arg)
{
    int status, vmid;
    struct socket * socket;
    struct mmu_rx_struct * node, * next;
    char uuid_str[INVD_UUID_STR_LEN];

    socket = NULL;
    vmid = -1;

    while (1) {
        status = master_do_accept(master_rx, &socket, &vmid, uuid_str, INVD_UUID_STR_LEN);
        if (master_rx->should_exit) {
            status = 0;
            break;
        }

        if (status != 0)
            continue;

        /* see if we already have a thread for this uuid */
        list_for_each_entry_safe(node, next, &(master_rx->node), node) {
            if (strncmp(node->uuid_str, uuid_str, INVD_UUID_STR_LEN) == 0) {
                mmu_dbg(master_rx, 1, "already have mmu_rx thread for uuid=%s; killing old thread\n", uuid_str);
                master_stop_thread(node);
                break;
            }
        }

        node = kzalloc(sizeof(struct mmu_rx_struct), GFP_KERNEL);
        if (node == NULL) {
            mmu_err(master_rx, "out of memory\n");
            kernel_sock_shutdown(socket, SHUT_RDWR);
            sock_release(socket);
            break;
        }

        kref_init(&(node->refcnt));
        mutex_init(&(node->mutex));
        node->socket = socket;
        node->vmid = vmid;
        node->state = MMU_RX_INITED;
        strncpy(node->uuid_str, uuid_str, INVD_UUID_STR_LEN);

        /* protect child with an elevated ref */
        mmu_get(node);

        node->thread = kthread_run(child_rx_fn, node, "mmu_rx_%d", vmid);
        if (IS_ERR(node->thread)) {
            mmu_err(master_rx, "could not create kernel thread\n");
            mmu_put(node);
            kernel_sock_shutdown(socket, SHUT_RDWR);
            break;
        }

        list_add_tail(&(node->node), &(master_rx->node));
    }

    /* stop/release all threads */
    list_for_each_entry_safe(node, next, &(master_rx->node), node) {
        master_stop_thread(node);
    }

    while (!kthread_should_stop())
        schedule();

    mmu_put(master_rx);

    return status;
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,8,0)
static int
invirt_set_socket_reuseaddr(struct socket * socket)
{
    int option, status;

    option = 1;
    status = kernel_setsockopt(socket, SOL_SOCKET, SO_REUSEADDR,
            (char *)&option, sizeof(int));
    if (status != 0)
        inv_err("failed to setsockopt\n");

    return status;
}
#else
static int
invirt_set_socket_reuseaddr(struct socket * socket)
{
    sock_set_reuseaddr(socket->sk);
    return 0;
}
#endif

int
invirt_enable_mmunot(void)
{
    struct sockaddr_in mmu_addr;
    struct socket * socket;
    struct task_struct * thread;
    int status;

    status = sock_create_kern(&init_net, AF_INET, SOCK_STREAM, 0, &socket);
    if (status != 0) {
        inv_err("failed to create socket\n");
        goto out_sock;
    }

    status = invirt_set_socket_reuseaddr(socket);
    if (status != 0) {
        inv_err("failed to make socket addr reusable\n");
        goto out_reuse;
    }

    mmu_addr.sin_family = AF_INET;
    mmu_addr.sin_port = htons(INVIRT_INVD_IP_PORT_KERN);

    status = invirt_str_to_ipaddr(
        INVIRT_INVD_IP_ADDR,
        (u8 *)&(mmu_addr.sin_addr.s_addr)
    );
    if (status != 0)
       goto out_ipaddr;

    status = kernel_bind(socket, (struct sockaddr *)&mmu_addr,
            sizeof(struct sockaddr_in));
    if (status != 0) {
        inv_err("failed to bind socket%s\n", (status == -EADDRNOTAVAIL) ? "- did you run scripts/bridge.sh?" : "");
        goto out_bind;
    }

    status = kernel_listen(socket, INT_MAX);
    if (status != 0) {
        inv_err("failed to set passive socket\n");
        goto out_listen;
    }

    status = -ENOMEM;
    master_rx = kzalloc(sizeof(struct mmu_rx_struct), GFP_KERNEL);
    if (master_rx == NULL) {
        inv_err("out of memory\n");
        goto out_kmalloc;
    }

    INIT_LIST_HEAD(&(master_rx->node));
    mutex_init(&(master_rx->mutex));
    kref_init(&(master_rx->refcnt));
    master_rx->socket = socket;
    master_rx->should_exit = false;
    master_rx->vmid = -1;

    /* keep an elevated ref to protect data while master is running */
    mmu_get(master_rx);

    thread = kthread_run(master_mmu_rx_fn, NULL, "invh_mmu_master");
    if (IS_ERR(thread)) {
        inv_err("could not create kernel thread\n");
        status = PTR_ERR(thread);
        goto out_kthread;
    }
    master_rx->thread = thread;

    return 0;

out_kthread:
    kernel_sock_shutdown(master_rx->socket, SHUT_RDWR);
    mmu_put(master_rx);
    return status;

out_kmalloc:
out_listen:
out_bind:
out_ipaddr:
out_reuse:
    sock_release(socket);
out_sock:
    return status;
}

void
invirt_disable_mmunot(void)
{
    master_rx->should_exit = true;
    mb();

    kernel_sock_shutdown(master_rx->socket, SHUT_RDWR);
    kthread_stop(master_rx->thread);
    mmu_put(master_rx);
}
