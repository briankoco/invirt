/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#include <linux/kernel.h>
#include <linux/file.h>
#include <linux/net.h>
#include <linux/skbuff.h>
#include <linux/wait.h>
#include <net/sock.h>

#include <asm/unistd_64.h>

#include <invirt.h>
#include <invirt_vcall.h>
#include <invirt_priv.h>

static inline void
check_syscall_noreturn(unsigned long nr,
                       unsigned long arg0)
{
    switch (nr) {
    case __NR_exit:
    case __NR_exit_group:
        do_exit(arg0);

    default:
        break;
    }
}

static int
send_vcall_socket(struct invirt_thread * th,
                  struct invirt_vcall  * vcall)
{
    int status;

    status = invirt_send_vcall_socket(th->socket.invg_sock, vcall);
    if (status != 0)
        return status;

    /* bail on syscalls that don't return */
    check_syscall_noreturn(vcall->syscall.nr, vcall->syscall.arg0);

    status = invirt_recv_vcall_socket(th->socket.invg_sock, vcall, false, false);
    if (status != 0)
        return status;

    return 0;
}

/*
 * We need to take action after execution of some system calls
 */
static int
post_process_syscall(struct invirt_thread * th,
                     struct invirt_vcall  * vcall)
{
    unsigned long orig_rc, new_rc;

    BUG_ON(vcall->msg_type != INVIRT_VCALL_SYSCALL);

    orig_rc = vcall->syscall.rc;
    new_rc = orig_rc;

    switch (vcall->syscall.nr) {
    case __NR_arch_prctl:
    case __NR_rt_sigaction:
//    case __NR_set_tid_address:
//    case __NR_set_robust_list:
//    case __NR_rt_sigprocmask:
//    case __NR_rt_sigreturn:
        inv_dbg(2, "issuing syscall %lu locally\n", vcall->syscall.nr);
        new_rc = invirt_do_syscall_local(th);
        break;

    default:
        break;
    }

    if (new_rc != orig_rc)
        inv_dbg(1, "syscall (nr=%lu) returned %ld on invg, but %ld locally\n", vcall->syscall.nr, orig_rc, new_rc);

    return 0;
}

int
invirt_send_syscall(struct invirt_thread * th,
                    unsigned int           nr,
                    unsigned long          arg0,
                    unsigned long          arg1,
                    unsigned long          arg2,
                    unsigned long          arg3,
                    unsigned long          arg4,
                    unsigned long          arg5,
                    unsigned long        * rc)
{
    struct invirt_vcall vcall = {
        .msg_type = INVIRT_VCALL_SYSCALL,
        .tracer_tgid = th->target_pid,
        .syscall = {
            .nr = nr,
            .arg0 = arg0,
            .arg1 = arg1,
            .arg2 = arg2,
            .arg3 = arg3,
            .arg4 = arg4,
            .arg5 = arg5
        }
    };
    int status;

    inv_dbg(3, "syscall[%u]  (IN): 0x%lx, 0x%lx, 0x%lx, 0x%lx, 0x%lx, 0x%lx\n",
        nr, arg0, arg1, arg2, arg3, arg4, arg5
    );

    /* Issue the syscall via vcall to invg */
    switch (th->mode) {
    case INVIRT_TRACEMODE_SOCKET:
        status = send_vcall_socket(th, &vcall);
        break;
    default:
        inv_err("invalid tracemode: %d\n", th->mode);
        status = -EIO;
    }

    if (status != 0) {
        inv_err("failed to send syscall via vcall (status=%d)\n", status);
        goto err_send;
    }

    /* Do post-procssing */
    status = post_process_syscall(th, &vcall);
    if (status != 0)
        goto err_postprocess;

    *rc = vcall.syscall.rc;

    inv_dbg(3, "syscall[%u] (OUT): 0x%lx\n", nr, *rc);

    return 0;

err_postprocess:
err_send:
    return status;
}

/* TODO: cache this damn page_list thing */

int
invirt_send_page_fault(struct invirt_thread * th,
                       unsigned long          hva,
                       unsigned int           fault_flags,
                       unsigned long        * base_va,
                       unsigned long       ** gpfn_list_p,
                       unsigned long        * nr_pages_p,
                       unsigned long        * pte_flags_p)
{
    int status;
    unsigned long * gpfn_list, len;

    struct invirt_vcall vcall = {
        .msg_type = INVIRT_VCALL_PAGE_FAULT,
        .tracer_tgid = th->target_pid,
        .page_fault = {
            .hva = hva,
            .pf_flags = fault_flags
        }
    };

    inv_dbg(3, "sending page fault for 0x%lx\n", hva);

    status = invirt_send_vcall_socket(th->socket.invg_sock, &vcall); 
    if (status != 0)
        return status;

    status = invirt_recv_vcall_socket(th->socket.invg_sock, &vcall, false, false);
    if (status != 0)
        return status;

    if (vcall.page_fault.rc != 0) {
        inv_err("page fault @invg failed (err=%ld)\n", vcall.page_fault.rc);
        return vcall.page_fault.rc;
    }

    if (WARN_ON(vcall.page_fault.nr_pages == 0))
        return -EIO;

    inv_dbg(3, "invg mapped fault with %lu pages\n", vcall.page_fault.nr_pages);

    status = -ENOMEM;
    len = sizeof(unsigned long) * vcall.page_fault.nr_pages;
    gpfn_list = vmalloc(len);
    if (gpfn_list == NULL) {
        inv_err("failed to vmalloc gpfn_list\n");
        goto out_vmalloc;
    }

    status = invirt_recv_data_socket(th->socket.invg_sock,  (void *)gpfn_list, len, false, false);
    if (status != len) {
        inv_err("failed to read gpa from socket (read %d out of %lu bytes)\n", status, len);
        status = -ENOMSG;
        goto out_read;
    }

    *base_va = vcall.page_fault.hva;
    *gpfn_list_p = gpfn_list;
    *nr_pages_p = vcall.page_fault.nr_pages;
    *pte_flags_p = vcall.page_fault.pte_flags;

    return 0;

out_vmalloc:
    /* read out one page to tell invg we can't complete this */
    {
        unsigned long gpa;
        (void)invirt_recv_data_socket(th->socket.invg_sock, (void *)&gpa,
                sizeof(unsigned long), false, false);
    }

out_read:
    vfree(gpfn_list);
    return status;
}

void
invirt_teardown_tracee_socket(struct invirt_thread * th)
{
    int status;
    unsigned long flags;
    struct socket * sock = th->socket.invg_sock;

    mutex_lock(&(th->mutex));

    spin_lock_irqsave(&(th->lock), flags);
    status = 0;
    if (WARN_ON(!(th->flags & INVIRT_FLAG_TRACED))) {
        spin_unlock_irqrestore(&(th->lock), flags);
        goto out;
    }
    spin_unlock_irqrestore(&(th->lock), flags);

    sockfd_put(sock);

    spin_lock_irqsave(&(th->lock), flags);
    th->flags &= ~INVIRT_FLAG_TRACED;
    th->mode = 0;
    spin_unlock_irqrestore(&(th->lock), flags);

out:
    mutex_unlock(&(th->mutex));
}

int
invirt_setup_tracee_socket(struct invirt_thread * th,
                           const char           * uuid_str,
                           pid_t                  target_pid,
                           int                    conn_fd)
{
    int status;
    unsigned long flags;
    struct socket * sock;

    mutex_lock(&(th->mutex));

    spin_lock_irqsave(&(th->lock), flags);
    status = 0;
    if (th->flags & INVIRT_FLAG_TRACED)
        status = -EBUSY;
    spin_unlock_irqrestore(&(th->lock), flags);

    if (status != 0)
        goto out;

    sock = sockfd_lookup(conn_fd, &status);
    if (sock == NULL)
        goto out;

    th->socket.invg_sock = sock;

    spin_lock_irqsave(&(th->lock), flags);
    th->flags |= INVIRT_FLAG_TRACED;
    th->tracer_tgid = target_pid;
    th->mode = INVIRT_TRACEMODE_SOCKET;
    strncpy(th->uuid_str, uuid_str, INVD_UUID_STR_LEN);
    spin_unlock_irqrestore(&(th->lock), flags);

    inv_dbg(1, "started tracing host thread %d by uuid:%s,tgid:%d\n", current->pid, uuid_str, target_pid);

    status = 0;

out:
    mutex_unlock(&(th->mutex));
    return status;
}
