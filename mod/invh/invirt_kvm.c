/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#include <linux/kvm_host.h>
#include <invirt_priv.h>

struct kvm *
invirt_get_kvm_by_pid(pid_t pid)
{
    struct mutex * m = (struct mutex *)kallsyms_kvm_lock;
    struct list_head * l = (struct list_head *)kallsyms_vm_list;
    struct kvm * k;

    mutex_lock(m);
    list_for_each_entry(k, l, vm_list) {
        if (k->userspace_pid == pid) {
            kvm_get_kvm(k);
            mutex_unlock(m);
            return k;
        }
    }
    mutex_unlock(m);
    return ERR_PTR(-ENODEV);
}

void
invirt_put_kvm(struct kvm * k)
{
    kvm_put_kvm(k);
}
