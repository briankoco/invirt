/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#ifndef __INVIRT_COMMON_H__
#define __INVIRT_COMMON_H__

#include <linux/mm_types.h>
#include <linux/net.h>
#include <linux/module.h>

#include <invirt.h>
#include <invirt_vcall.h>

extern int invirt_debug_level;

static inline char *
invirt_prefix(void)
{
    return THIS_MODULE->name;
}

#define __inv_out(klvl, fmt, args...)\
    do {\
        printk(klvl "%s: " fmt, invirt_prefix(), ##args);\
    } while (0)

#define inv_out(fmt, args...) __inv_out(KERN_INFO, fmt, ##args)
#define inv_err(fmt, args...) __inv_out(KERN_ERR, fmt, ##args)
#define inv_dbg(lvl, fmt, args...)\
    do {\
        if (lvl <= invirt_debug_level)\
            __inv_out(KERN_INFO, fmt, ##args);\
    } while (0)

/* defined in invirt_vcall.c */
int
invirt_send_data_socket(struct socket * socket,
                        void          * src,
                        unsigned long   len);


int
invirt_recv_data_socket(struct socket * socket,
                        void          * dst,
                        unsigned long   len,
                        bool            nonblocking,
                        bool            is_kthread);

static inline int
invirt_send_vcall_socket(struct socket       * socket,
                         struct invirt_vcall * vcall)
{
    int len = invirt_send_data_socket(socket, (void *)vcall, sizeof(struct invirt_vcall));
    return (len == sizeof(struct invirt_vcall)) ? 0 : -ENOMSG;
}

static inline int
invirt_recv_vcall_socket(struct socket       * socket,
                         struct invirt_vcall * vcall,
                         bool                  nonblocking,
                         bool                  is_kthread)
{
    int len = invirt_recv_data_socket(socket, (void *)vcall, sizeof(struct invirt_vcall), nonblocking, is_kthread);
    return (len == sizeof(struct invirt_vcall)) ? 0 : -ENOMSG;
}

/* defined in invirt_pts.c */
pte_t *
invirt_vaddr_to_pte_offset(struct mm_struct * mm,
                           unsigned long      vaddr,
                           unsigned long    * offset);

unsigned long
invirt_vaddr_to_pfn(struct mm_struct * mm,
                    unsigned long      vaddr);

#endif /* __INVIRT_COMMON_H__ */
