/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#include <linux/kernel.h>
#include <linux/file.h>
#include <linux/net.h>
#include <linux/skbuff.h>
#include <linux/wait.h>
#include <linux/kthread.h>
#include <net/sock.h>

#include <invirt.h>
#include <invirt_vcall.h>
#include <invirt_common.h>

static int
__send_data(struct socket * socket,
            void          * src,
            unsigned long   len)
{
    struct kvec iov = {
        .iov_base = src,
        .iov_len  = (__kernel_size_t)len
    };

    struct msghdr msg = {
        .msg_flags = 0,
        .msg_name = NULL,
        .msg_namelen = 0,
        .msg_control = 0,
        .msg_controllen = 0
    };

    return kernel_sendmsg(socket, &msg, &iov, 1, len);
}

int
invirt_send_data_socket(struct socket * socket,
                        void          * src,
                        unsigned long   len)
{
    int sent, total;

    total = 0;
    while (len > 0) {
        sent = __send_data(socket, src, len);

        if (sent < 0) {
            inv_err("kernel_sendmsg failed (err=%d)\n", sent);
            break;
        }

        if (sent == 0) {
            inv_dbg(2, "sent 0 bytes on a blocking socket\n");
            break;
        }

        len -= sent;
        src += sent;
        total += sent;
    }

    return total;
}


static int
__recv_data(struct socket * socket,
            void          * dst,
            unsigned long   len,
            bool            nonblocking,
            bool            is_kthread)
{
    struct sock * sk = socket->sk;
    int status;

    struct kvec iov =
    {
        .iov_base = dst,
        .iov_len  = (__kernel_size_t)len
    };

    struct msghdr msg =
    {
        .msg_flags = (nonblocking) ? MSG_DONTWAIT : 0,
        .msg_name  = NULL,
        .msg_namelen = 0,
        .msg_control = NULL,
        .msg_controllen = 0,
    };

    /* maintain ability to kthread_stop() a kernel thread to break it
     * from this receive
     */
    if (!nonblocking && is_kthread) {
        status = wait_event_interruptible(
            *(sk_sleep(sk)),
            (!skb_queue_empty(&sk->sk_receive_queue) || kthread_should_stop())
        );
        if (status)
            return status;

        /* See if we were woken due to kthread_stop() */
        if (kthread_should_stop())
            return 0;
    }

    return kernel_recvmsg(socket, &msg, &iov, 1, len, msg.msg_flags);
}

/*
 * recv data through an open socket
 */
int
invirt_recv_data_socket(struct socket * socket,
                        void          * dst,
                        unsigned long   len,
                        bool            nonblocking,
                        bool            is_kthread)
{
    int rcvd, total;

    total = 0;
    while (len > 0) {
        rcvd = __recv_data(socket, dst, len, nonblocking, is_kthread);

        if (rcvd < 0) {
            inv_err("kernel_recvmsg failed (err=%d)\n", rcvd);
            break;
        }

        /* receive nothing on a blocking socket --- break */
        if (rcvd == 0) {
            inv_dbg(2, "received 0 bytes on socket\n");
            break;
        }

        len -= rcvd;
        dst += rcvd;
        total += rcvd;
    }

    return total;
}
