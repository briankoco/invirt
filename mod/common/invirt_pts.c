/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

/*
 * This file provides basic page table walking utilities. Much of the 
 * code is adapted from xpmem: 
 *
 * https://gitlab.prognosticlab.org/prognosticlab/xpmem.git   
 */

#include <linux/version.h>
#include <linux/mm.h>
#include <linux/memory.h>
#include <linux/highmem.h>
#include <linux/slab.h>
#include <linux/atomic.h>

/* linux added 5-level paging in 4.11; fake the p4d level
 * in earlier kernels
 */
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,11,0)
#define p4d_t                   pgd_t
#define p4d_offset(pgd, vaddr)  pgd
#define p4d_present(p4d)        1
#endif

/* Not sure why this didn't show up until 4.12 ... */
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,12,0)
#define p4d_large(p4d)          0
#endif

typedef enum {
    HUGETLB_PTE_PGD,
    HUGETLB_PTE_P4D,
    HUGETLB_PTE_PUD,
    HUGETLB_PTE_PMD
} hugepte_type_t;


static inline pte_t *
invirt_huge_pte_offset(struct mm_struct * mm,
                       unsigned long      vaddr)
{
    pgd_t * pgd;
    p4d_t * p4d;
    pud_t * pud;
    pmd_t * pmd;

    pgd = pgd_offset(mm, vaddr);
    if (!pgd_present(*pgd))
        return NULL;

    p4d = p4d_offset(pgd, vaddr);
    if (!p4d_present(*p4d))
        return NULL;
    if (p4d_large(*p4d))
        return (pte_t *)p4d;

    pud = pud_offset(p4d, vaddr);
    if (!pud_present(*pud))
        return NULL;
    if (pud_large(*pud))
        return (pte_t *)pud;

    pmd = pmd_offset(pud, vaddr);
    return (pte_t *)pmd;
}

static inline pte_t *
invirt_hugetlb_pte(struct mm_struct * mm,
                   unsigned long      vaddr,
                   unsigned long    * offset,
                   hugepte_type_t     hugepte_type,
                   pte_t            * hugepte)
{
    struct vm_area_struct * vma;
    unsigned long address = 0, hps;
    pte_t * pte;

    vma = find_vma(mm, vaddr);
    if (!vma || vma->vm_start > vaddr)
        return NULL;

#ifdef CONFIG_HUGE_PAGE
    if (is_vm_hugetlb_page(vma)) {
        struct hstate * hs = hstate_vma(vma);
        hps = huge_page_size(hs);
        address = vaddr & huge_page_mask(hs);
    }
#endif
#ifdef CONFIG_TRANSPARENT_HUGEPAGE
    if (hugepte) {
        switch (hugepte_type) {
        case HUGETLB_PTE_PUD: /* 1GB THP */
            if (pud_trans_huge(*(pud_t *)hugepte)) {
                hps = PUD_SIZE;
                address = vaddr & PUD_MASK;
            }

            break;

        case HUGETLB_PTE_PMD: /* 2MB THP */
            if (pmd_trans_huge(*(pmd_t *)hugepte)) {
                hps = PMD_SIZE;
                address = vaddr & PMD_MASK;
            }

            break;

        case HUGETLB_PTE_PGD:
        case HUGETLB_PTE_P4D:
        default:
            /* Linux has no THP for pgd/p4d at the moment ... */
            break;
        }
    }
#endif

    /* should never be 0, as this function is only called if we 
     * found a huge pte somewhere
     */
    if (WARN_ON(address == 0))
        return NULL;

    /* offset into the pte */
    if (offset)
        *offset = (vaddr & (hps - 1)) & PAGE_MASK;

    pte = invirt_huge_pte_offset(mm, address);
    if (!pte || pte_none(*pte))
        return NULL;

    return pte;
}

pte_t *
invirt_vaddr_to_pte_offset(struct mm_struct * mm,
                           unsigned long      vaddr,
                           unsigned long    * offset)
{
    pgd_t * pgd;
    p4d_t * p4d;
    pud_t * pud;
    pmd_t * pmd;
    pte_t * pte;

    if (offset)
        *offset = 0;

    pgd = pgd_offset(mm, vaddr);
    if (!pgd_present(*pgd))
        return NULL;
    else if (pgd_large(*pgd))
        return invirt_hugetlb_pte(mm, vaddr, offset, HUGETLB_PTE_PGD, 
            (pte_t *)pgd);

    p4d = p4d_offset(pgd, vaddr);
    if (!p4d_present(*p4d))
        return NULL;
    else if (p4d_large(*p4d))
        return invirt_hugetlb_pte(mm, vaddr, offset, HUGETLB_PTE_P4D, 
            (pte_t *)pud);

    pud = pud_offset(p4d, vaddr);
    if (!pud_present(*pud))
        return NULL;
    else if (pud_large(*pud))
        return invirt_hugetlb_pte(mm, vaddr, offset, HUGETLB_PTE_PUD, 
            (pte_t *)pud);

    pmd = pmd_offset(pud, vaddr);
    if (!pmd_present(*pmd))
        return NULL;
    else if (pmd_large(*pmd))
        return invirt_hugetlb_pte(mm, vaddr, offset, HUGETLB_PTE_PMD, 
            (pte_t *)pmd);

    pte = pte_offset_map(pmd, vaddr);
    if (!pte_present(*pte))
        return NULL;

    return pte;
}

unsigned long
invirt_vaddr_to_pfn(struct mm_struct * mm,
                    unsigned long      vaddr)
{
    pte_t * pte;
    unsigned long offset, pfn;

    pte = invirt_vaddr_to_pte_offset(mm, vaddr, &offset);
    if (pte == NULL)
        return 0;

    pfn = pte_pfn(*pte) + (offset >> PAGE_SHIFT);
    pte_unmap(pte);

    return pfn;
}
