/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/tracepoint.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/dmi.h>
#include <linux/seq_file.h>
#include <linux/anon_inodes.h>

#include <invirt.h>
#include <invirt_priv.h>

struct invirt_hashlist * gbl_tg_hashtable;
static struct proc_dir_entry * invirt_proc_dir;
static struct proc_dir_entry * uuid_proc_entry;

/* serialize /dev/invg interactions */
static DEFINE_MUTEX(invirt_dev_mutex);

char * invirt_uuid;

/* debugging */
int invirt_debug_level = 0;
module_param(invirt_debug_level, int, 0644);

int invirt_show_uuid = 1;
module_param(invirt_show_uuid, int, 0644);

static long
invirt_target_ioctl(struct file * filp,
                    unsigned int  cmd,
                    unsigned long arg)
{
    struct invirt_thread_group * tg;
    struct socket * sock;
    struct invg_ioc_fault k_fault;
    int status;

    tg = invirt_tg_ref_by_tgid(invirt_get_tgid());
    if (IS_ERR(tg))
        return PTR_ERR(tg);

    switch (cmd) {
    case INVG_IOC_FAULT_PAGE:
    case INVG_IOC_FAULT_VMA:
        if (copy_from_user(&k_fault, (char __user *)arg,
                sizeof(struct invg_ioc_fault))) {
            status = -EFAULT;
            break;
        }

        sock = sockfd_lookup(k_fault.sock_fd, &status);
        if (sock == NULL) {
            status = -EINVAL;
            break;
        }

        status = invirt_do_remote_fault(
            tg,
            sock,
            (unsigned long)k_fault.vaddr,
            (unsigned int)k_fault.vmf_flags,
            (cmd == INVG_IOC_FAULT_PAGE) ? false : true
        );

        sockfd_put(sock);
        break;
    default:
        status = -ENOIOCTLCMD;
        break;
    }

    invirt_tg_deref(tg);
    return status;
}

static int
invirt_target_mmap(struct file           * filp,
                   struct vm_area_struct * vma)
{
    struct invirt_thread_group * tg;
    int status;

    tg = invirt_tg_ref_by_tgid(invirt_get_tgid());
    if (IS_ERR(tg))
        return PTR_ERR(tg);

    status = invirt_setup_vma(tg, vma);

    invirt_tg_deref(tg);
    return status;
}

/* Device ops associated with target attachments */
static struct file_operations
invirt_target_fops =
{
    .unlocked_ioctl = invirt_target_ioctl,
    .mmap           = invirt_target_mmap,
};

/*
 * Attach to a target.
 *
 * On success, returns a file descriptor with which to interact
 * with the target, including to mmap() its regions
 *
 * Returns <0 on error
 */
static int
invirt_attach_target_tgid(struct invirt_thread_group * tg,
                          pid_t                        target_tgid)
{
    struct pid * pid;
    struct task_struct * task;
    struct mm_struct * mm;
    int status;

    mutex_lock(&(tg->mutex));

    if (tg->target_tgid != 0) {
        status = -EBUSY;
        goto err_busy;
    }

    pid = find_get_pid(target_tgid);
    if (IS_ERR(pid)) {
        status = PTR_ERR(pid);
        goto err_find;
    }

    task = get_pid_task(pid, PIDTYPE_PID);
    if (IS_ERR(task)) {
        status = PTR_ERR(task);
        goto err_task;
    }

    mm = get_task_mm(task);
    if (IS_ERR(mm)) {
        status = PTR_ERR(mm);
        goto err_mm;
    }

    status = anon_inode_getfd("[invg]", &invirt_target_fops, NULL, O_RDWR | O_CLOEXEC);
    if (status < 0)
        goto err_fd;

    tg->target_mm   = mm;
    tg->target_task = task;
    tg->target_tgid = target_tgid;

    inv_dbg(1, "attached tgid %d to its target (target=%d)\n", tg->tgid, tg->target_tgid);

    goto out;

err_fd:
    mmput(mm);
err_mm:
    put_task_struct(task);
err_task:
    put_pid(pid);
err_find:
err_busy:
out:
    mutex_unlock(&(tg->mutex));
    return status;
}

static int
invirt_detach_target_tgid(struct invirt_thread_group * tg,
                          bool                         from_mmu)
{
    int status;

    mutex_lock(&(tg->mutex));

    if (tg->target_tgid == 0) {
        status = -EINVAL;
        goto out;
    }

    /* remove all faulted memory */
    invirt_release_remote_extents(tg);
    invirt_release_local_extents(tg, from_mmu);

    mmput(tg->target_mm);
    put_task_struct(tg->target_task);
    put_pid(task_pid(tg->target_task));

    inv_dbg(1, "detached tgid %d from its target (target=%d)\n", tg->tgid, tg->target_tgid);

    tg->target_tgid = 0;
    status = 0;

out:
    mutex_unlock(&(tg->mutex));
    return status;
}

static int
invirt_init_tg(void)
{
    struct invirt_thread_group * tg;
    int index, status;
    unsigned long flags;

    if (!try_module_get(THIS_MODULE))
        return -ENODEV;

    /* prevent duplicate opens */
    tg = invirt_tg_ref_by_tgid(invirt_get_tgid());
    if (!IS_ERR(tg)) {
        invirt_tg_deref(tg);
        status = -EBUSY;
        goto err_tg;
    }

    /* create tg */
    status = -ENOMEM;
    tg = kzalloc(sizeof(struct invirt_thread_group), GFP_KERNEL);
    if (tg == NULL)
        goto err_tg;

    tg->tgid = invirt_get_tgid();
    tg->mm = current->mm;
    INIT_LIST_HEAD(&(tg->gbl_hashnode));
    tg->local_extent_root = RB_ROOT;
    tg->remote_extent_root = RB_ROOT;
    spin_lock_init(&(tg->lock));
    mutex_init(&(tg->mutex));

    status = invirt_init_tg_mmunot(tg);
    if (status)
        goto err_mmu;

    /* add tg to its hashlist */
    index = invirt_tg_hashtable_index(tg->tgid);
    write_lock_irqsave(&(gbl_tg_hashtable[index].lock), flags);
    list_add_tail(&(tg->gbl_hashnode), &(gbl_tg_hashtable[index].list));
    write_unlock_irqrestore(&(gbl_tg_hashtable[index].lock), flags);

    invirt_tg_not_destroyable(tg);

    return 0;

err_mmu:
    kfree(tg);
err_tg:
    module_put(THIS_MODULE);
    return status;
}

/*
 * Destroy an invirt_thread_group
 */
static void
invirt_destroy_tg(struct invirt_thread_group * tg)
{
    invirt_deinit_tg_mmunot(tg);
    invirt_tg_destroyable(tg);
    invirt_tg_deref(tg);

    module_put(THIS_MODULE);
}

/*
 * Invoked from the mmu_notifier release callout
 */
void
invirt_teardown_tg(struct invirt_thread_group * tg)
{
    unsigned long flags;

    spin_lock_irqsave(&(tg->lock), flags);
    WARN_ON(tg->flags & INVIRT_FLAG_DESTROYING);
    tg->flags |= INVIRT_FLAG_DESTROYING;
    spin_unlock_irqrestore(&(tg->lock), flags);

    /* detach target tgid -- this will also release local and remote extents */
    if (tg->target_tgid != 0)
        invirt_detach_target_tgid(tg, true);

    spin_lock_irqsave(&(tg->lock), flags);
    WARN_ON(tg->flags & INVIRT_FLAG_DESTROYED);
    tg->flags |= INVIRT_FLAG_DESTROYED;
    spin_unlock_irqrestore(&(tg->lock), flags);

    /*
     * We don't call invirt_destroy_tg() here. We can't call
     * mmu_notifier_unregister() when the stack started with a
     * mmu_notifier_release() callout or we'll deadlock in the kernel
     * MMU notifier code.  invirt_destroy_tg() will be called when the
     * close of /dev/invirt occurs as deadlocks are not possible then.
     */
    invirt_tg_deref(tg);
    //invirt_destroy_tg(tg);
}

/*
 * user open of the invirt driver
 */
static int
invirt_dev_open(struct inode * inodep,
                struct file  * filp)
{
    int ret;

    mutex_lock(&invirt_dev_mutex);
    ret = invirt_init_tg();
    mutex_unlock(&invirt_dev_mutex);

    return ret;
}

static int
invirt_dev_flush(struct file  * filp,
                 fl_owner_t     owner)
{
    struct invirt_thread_group * tg;
    unsigned long flags;
    int index;

    /* During a call to fork() there is a check for whether the parent process
     * has any pending signals. If there are pending signals, then the fork
     * aborts, and the child process is removed before delivering the signal
     * and starting the fork again. In that case, we can end up here, but since
     * we're mid-fork, current is pointing to the parent's task_struct and not
     * the child's. This would cause us to remove the parent's invirt mappings
     * by accident. We check here whether the owner pointer we have is the same
     * as the current->files pointer. If it is, or if current->files is NULL,
     * then this flush really does belong to the current process. If they don't
     * match, then we return without doing anything since the child shouldn't
     * have a valid invirt_thread_group struct yet.
     */
    if (current->files && current->files != owner)
        return 0;

    /*
     * invirt_flush() can get called twice for thread groups which inherited
     * /dev/invirt: once for the inherited fd, once for the first explicit use
     * of /dev/invirt. If we don't find the tg via invirt_tg_ref_by_tgid() we
     * assume we are in this type of scenario and return silently.
     *
     * Note the use of _all: we want to get this even if it's
     * destroying/destroyed.
     */
    tg = invirt_tg_ref_by_tgid_all(invirt_get_tgid());
    if (IS_ERR(tg))
        return 0;

    /*
     * Two threads could have called invirt_flush at about the same time, and
     * thus invirt_tg_ref_by_tgid could return the same tg in both threads.
     * Guard against this.
     */
    spin_lock_irqsave(&tg->lock, flags);
    if (tg->flags & INVIRT_FLAG_FLUSHING) {
        spin_unlock_irqrestore(&tg->lock, flags);
        return 0;
    }
    tg->flags |= INVIRT_FLAG_FLUSHING;
    spin_unlock_irqrestore(&tg->lock, flags);

    /* We can't remove the tg from the lookup list yet, because
     * this call stack is possible:
     *  invirt_flush() ->
     *    invirt_destroy_tg() ->
     *      invirt_mmu_notifier_unlink() ->
     *        invirt_mmu_release()
     *          invirt_mmu_release() will need to query the tg from the
     *          lookup list.
     *
     * We need to take an extra ref of the tg, so that we can pull it from the
     * hashlist after we call invirt_destroy_tg() below.
     */

    invirt_tg_ref(tg);

    invirt_destroy_tg(tg);

    /* Remove tg structure from its hash list */
    index = invirt_tg_hashtable_index(tg->tgid);
    write_lock_irqsave(&(gbl_tg_hashtable[index].lock), flags);
    list_del_init(&tg->gbl_hashnode);
    write_unlock_irqrestore(&(gbl_tg_hashtable[index].lock), flags);

    invirt_tg_deref(tg);
    return 0;
}


static long
invirt_dev_ioctl(struct file * filp,
                 unsigned int  cmd,
                 unsigned long arg)
{
    struct invirt_thread_group * tg;
    int status;

    tg = invirt_tg_ref_by_tgid(invirt_get_tgid());
    if (IS_ERR(tg))
        return PTR_ERR(tg);

    switch (cmd) {
    case INVG_IOC_ATTACH_TID:
        if (arg == 0) {
            status = -EINVAL;
            break;
        }

        status = invirt_attach_target_tgid(tg, (pid_t)arg);
        break;

    case INVG_IOC_DETACH_TID:
        if (arg != 0) {
            status = -EINVAL;
            break;
        }

        status = invirt_detach_target_tgid(tg, false);
        break;

    default:
        status = -ENOIOCTLCMD;
        break;
    }

    invirt_tg_deref(tg);
    return status;
}

/* Device ops associated with /dev/invg */
static struct file_operations
invirt_dev_fops =
{
    .open           = invirt_dev_open,
    .flush          = invirt_dev_flush,
    .unlocked_ioctl = invirt_dev_ioctl,
};

static struct miscdevice
dev_handle =
{
    .minor = MISC_DYNAMIC_MINOR,
    .name = INVG_MODULE_NAME,
    .fops = &invirt_dev_fops
};

static int
uuid_procfs_show(struct seq_file * file,
                 void            * private_data)
{
    seq_printf(file, "%s\n", invirt_uuid);
    return 0;
}

static int
uuid_procfs_open(struct inode * inodep,
                 struct file  * filp)
{
    return single_open(filp, uuid_procfs_show, NULL);
}

static int
uuid_procfs_release(struct inode * inodep,
                    struct file  * filp)
{
    return single_release(inodep, filp);
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,6,0)
static struct proc_ops
uuid_procfs_fops =
{
    .proc_open = uuid_procfs_open,
    .proc_read = seq_read,
    .proc_lseek = seq_lseek,
    .proc_release = uuid_procfs_release
};
#else
static struct file_operations
uuid_procfs_fops =
{
    .owner = THIS_MODULE,
    .open = uuid_procfs_open,
    .read = seq_read,
    .llseek = seq_lseek,
    .release = uuid_procfs_release
};
#endif

static int
invirt_setup_procfs(void)
{
    invirt_proc_dir = proc_mkdir(__INVIRT_PROC_NAME, NULL);
    if (invirt_proc_dir == NULL) {
        inv_err("failed to create proc entry\n");
        return -1;
    }

    uuid_proc_entry = proc_create_data(
        __INVIRT_PROC_UUID_NAME,
        0444,
        invirt_proc_dir,
        &uuid_procfs_fops,
        NULL
    );
    if (uuid_proc_entry == NULL) {
        inv_err("failed to create " __INVIRT_PROC_UUID_NAME " entry\n");
        remove_proc_entry(__INVIRT_PROC_NAME, NULL);
        return -1;
    }

    return 0;
}

static void
invirt_teardown_procfs(void)
{
    remove_proc_entry(__INVIRT_PROC_UUID_NAME, invirt_proc_dir);
    remove_proc_entry(__INVIRT_PROC_NAME, NULL);
}

static int __init
invirt_init(void)
{
    int status, i;

    invirt_uuid = (char *)dmi_get_system_info(DMI_PRODUCT_UUID);

    status = -ENOMEM;
    gbl_tg_hashtable = kzalloc(
        sizeof(struct invirt_hashlist) * INVIRT_TG_HASHTABLE_SIZE,
        GFP_KERNEL
    );
    if (gbl_tg_hashtable == NULL)
        goto out_hashtable;

    for (i = 0; i < INVIRT_TG_HASHTABLE_SIZE; i++) {
        rwlock_init(&(gbl_tg_hashtable[i].lock));
        INIT_LIST_HEAD(&(gbl_tg_hashtable[i].list));
    }

    status = invirt_enable_mmunot();
    if (status != 0) {
        inv_err("failed to enable mmu notifiers\n");
        goto out_mmunot;
    }

    if (invirt_show_uuid) {
        status = invirt_setup_procfs();
        if (status != 0) {
            inv_err("failed to enable procfs\n");
            goto out_proc;
        }
    }

    status = misc_register(&dev_handle);
    if (status != 0) {
        inv_err("failed to register misc device\n");
        goto out_misc;
    }

    inv_out("module installed successfully\n");
    return 0;

out_misc:
    if (invirt_show_uuid)
        invirt_teardown_procfs();
out_proc:
    invirt_disable_mmunot();
out_mmunot:
    kfree(gbl_tg_hashtable);
out_hashtable:
    return status;
}

static void __exit
invirt_exit(void )
{
    misc_deregister(&dev_handle);
    if (invirt_show_uuid)
        invirt_teardown_procfs();
    invirt_disable_mmunot();
    kfree(gbl_tg_hashtable);

    inv_out("module uninstalled successfully\n");
}

module_init(invirt_init);
module_exit(invirt_exit);

MODULE_LICENSE("GPL");
