/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/mmu_notifier.h>

#include <linux/file.h>
#include <linux/net.h>
#include <linux/inet.h>
#include <linux/netdevice.h>
#include <linux/inetdevice.h>
#include <net/sock.h>

#include <invirt.h>
#include <invirt_vcall.h>
#include <invirt_priv.h>

/* connection socket */
static struct socket * invh_socket;

static int
invirt_send_vcall_socket_with_uuid(struct socket       * socket, 
                                   struct invirt_vcall * vcall,
                                   const char          * uuid_str)
{
    strncpy(vcall->uuid_str, uuid_str, INVD_UUID_STR_LEN);
    return invirt_send_vcall_socket(invh_socket, vcall);
}

static int
invirt_str_to_ipaddr(const char * str,
                     u8         * dst)
{
    if (WARN_ON(in4_pton(str, -1, dst, -1, NULL) == 0))
        return -EIO;

    return 0;
}


#if LINUX_VERSION_CODE < KERNEL_VERSION(5,3,0)

#define invirt_for_ifa(ifa, dev)        for_ifa(dev)
#define invirt_endfor_ifa(dev)          endfor_ifa(dev)

#else

#define invirt_for_ifa(ifa, idev)           \
    {                                       \
        const struct in_ifaddr *ifa;        \
        in_dev_for_each_ifa_rcu(ifa, idev) {

#define invirt_endfor_ifa(idev)             \
        }                                   \
    }

#endif

static int
invirt_find_local_ipaddr(__be32   src_addr,
                         __be32 * addr_found)
{
    struct net_device * netdev;
    struct in_device * idev;
    int status = -ENXIO;

    rtnl_lock();
    for_each_netdev(&init_net, netdev) {
        idev = in_dev_get(netdev);
        invirt_for_ifa(ifa, idev) {
            if (inet_ifa_match(src_addr, ifa)) {
                *addr_found = ifa->ifa_address;
                status = 0;
                in_dev_put(idev);
                goto out;
            }
        } invirt_endfor_ifa(idev);
        in_dev_put(idev);
    }
out:
    rtnl_unlock();
    return status;
}

static int
invirt_connect_invh(void)
{
    int status;
    struct sockaddr_in invh_mmu_addr, invg_addr;

    status = sock_create_kern(&init_net, AF_INET, SOCK_STREAM, 0, &invh_socket);
    if (status != 0) {
        inv_err("failed to create socket\n");
        return status;
    }

    invh_mmu_addr.sin_family = AF_INET;
    invh_mmu_addr.sin_port = htons(INVIRT_INVD_IP_PORT_KERN);

    status = invirt_str_to_ipaddr(
        INVIRT_INVD_IP_ADDR,
        (u8 *)&(invh_mmu_addr.sin_addr.s_addr)
    );
    if (status != 0) {
        sock_release(invh_socket);
        return -1;
    }

    invg_addr.sin_family = AF_INET;
    invg_addr.sin_port = 0;

    status = invirt_find_local_ipaddr(
        (__be32)invh_mmu_addr.sin_addr.s_addr,
        (__be32 *)&(invg_addr.sin_addr.s_addr)
    );
    if (status != 0) {
        inv_err("could not find local IP address\n");
        sock_release(invh_socket);
        return status;
    }

    /* bind */
    status = kernel_bind(invh_socket, (struct sockaddr *)&invg_addr,
        sizeof(struct sockaddr_in));
    if (status != 0) {
        inv_err("failed to bind local IP address\n");
        sock_release(invh_socket);
        return status;
    }

    /* connect */
    status = kernel_connect(invh_socket, (struct sockaddr *)&invh_mmu_addr,
        sizeof(struct sockaddr_in), 0);
    if (status != 0) {
        inv_err("failed to connect to invh_recv thread\n");
        sock_release(invh_socket);
        return status;
    }

    /* send connection message */
    {
        struct invirt_vcall mmu_not = {
            .msg_type = INVIRT_VCALL_MMU_NOT,
            .mmu_not = {
                .msg_type = INVIRT_MMU_NOT_INIT
            }
        };

        status = invirt_send_vcall_socket_with_uuid(invh_socket, &mmu_not, invirt_uuid);
        if (status != 0) {
            inv_err("failed to send mmu_not vcall\n");

            kernel_sock_shutdown(invh_socket, SHUT_RDWR);
            sock_release(invh_socket);

            return status;
        }
    }

    return 0;
}

static void
invirt_disconnect_invh(void)
{
    kernel_sock_shutdown(invh_socket, SHUT_RDWR);
    sock_release(invh_socket);
}


/*
 * Once we get here, we know we are allowed to block
 */
static void
do_invirt_mmu_invalidate_range(struct mmu_notifier * mn,
                               struct mm_struct    * mm,
                               unsigned long         start,
                               unsigned long         end)
{
    struct invirt_thread_group * tg;
    unsigned long nr_pages;
    int status;

    tg = container_of(mn, struct invirt_thread_group, mmu_not);
    invirt_tg_ref(tg);

    if (invirt_get_tgid() != tg->tgid) {
        inv_err("tg %d received callout from wrong tgid %d; ignoring\n", tg->tgid, invirt_get_tgid());
        invirt_tg_deref(tg);
        return;
    }

    nr_pages = (end - start + PAGE_SIZE - 1) >> PAGE_SHIFT;
    status = invirt_release_page_range(tg, start, nr_pages);
    if (status != 0)
        inv_err("failed to release page range (err=%d)\n", status);

    invirt_tg_deref(tg);
}


#if LINUX_VERSION_CODE < KERNEL_VERSION(4,19,0)
#define invirt_mmu_invalidate_range do_invirt_mmu_invalidate_range
#else

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,0,0)
static int
invirt_mmu_invalidate_range(struct mmu_notifier * mn,
                            struct mm_struct    * mm,
                            unsigned long         start,
                            unsigned long         end,
                            bool                  blockable)
{
#else
static int
invirt_mmu_invalidate_range(struct mmu_notifier             * mn,
                            const struct mmu_notifier_range * range)
{
#if LINUX_VERSION_CODE < KERNEL_VERSION(5,2,0)
    bool blockable = range->blockable;
#else
    bool blockable = mmu_notifier_range_blockable(range);
#endif
    struct mm_struct * mm = range->mm;
    unsigned long start = range->start;
    unsigned long end = range->end;

    inv_dbg(3, "received mmu notifier callout for [0x%lx, 0x%lx)\n", start, end);

    if (!blockable) {
        inv_err("received non-blockable mmu notifier callout\n");
        return -EAGAIN;
    }

    do_invirt_mmu_invalidate_range(mn, mm, start, end);
    return 0;
}
#endif
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,11,0)
static void
invirt_mmu_invalidate_page(struct mmu_notifier * mn,
                           struct mm_struct    * mm,
                           unsigned long         start)
{
    invirt_mmu_invalidate_range(mn, mm, start, start + PAGE_SIZE);
}
#endif

static void
invirt_mmu_release(struct mmu_notifier * mn,
                   struct mm_struct    * mm)
{
    struct invirt_thread_group * tg;
    unsigned long flags, tg_flags;
    int i;

    tg = invirt_tg_ref_by_tgid(invirt_get_tgid());
    if (!IS_ERR(tg)) {
        if (tg->mm == mm) {
            /* Normal case, process removing its own address space */
            invirt_teardown_tg(tg);
            return;
        } else {
            /* Abnormal case */
            invirt_tg_deref(tg);
        }
    }

    /* Although it is highly unlikely, an "outside" process could have
     * obtained a reference to this mm_struct and been the last to call
     * mmput().  In this case, we need to search all of the tgs to see
     * if one still matches with the mm passed to us from the MMU notifier
     * release callout.  If a match is found, that means the mmput()
     * occurred before the owning process has closed /dev/invirt and
     * we need to call invirt_teardown_tg() on behalf of the owning process
     * since the mm_struct mappings are being destroyed.
     */
    for (i = 0; i < INVIRT_TG_HASHTABLE_SIZE; i++) {
        read_lock_irqsave(&(gbl_tg_hashtable[i].lock), flags);

        list_for_each_entry(tg, &(gbl_tg_hashtable[i].list), gbl_hashnode) {
            if (tg->mm == mm) {
                spin_lock_irqsave(&(tg->lock), tg_flags);
                if (tg->flags & INVIRT_FLAG_DESTROYING) {
                    spin_unlock_irqrestore(&(tg->lock), tg_flags);
                    continue;
                }

                invirt_tg_ref(tg);
                spin_unlock_irqrestore(&(tg->lock), tg_flags);

                read_unlock_irqrestore(&(gbl_tg_hashtable[i].lock), flags);
                invirt_teardown_tg(tg);
                return;
            }
        }

        read_unlock_irqrestore(&(gbl_tg_hashtable[i].lock), flags);
    }
}

static struct mmu_notifier_ops invirt_mmuops =
{
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,11,0)
    .invalidate_page = invirt_mmu_invalidate_page
#endif
    .invalidate_range_start = invirt_mmu_invalidate_range,
    .release = invirt_mmu_release,
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,16,0) && LINUX_VERSION_CODE < KERNEL_VERSION(4,20,0)
    .flags = 0 /* MMU_INVALIDATE_DOES_NOT_BLOCK=0 */
#endif
};

int
invirt_init_tg_mmunot(struct invirt_thread_group * tg)
{
    tg->mmu_not.ops = &invirt_mmuops;
    return mmu_notifier_register(&(tg->mmu_not), tg->mm);
}

void
invirt_deinit_tg_mmunot(struct invirt_thread_group * tg)
{
    mmu_notifier_unregister(&(tg->mmu_not), tg->mm);
}

int
invirt_enable_mmunot(void)
{
    return invirt_connect_invh();
}

void
invirt_disable_mmunot(void)
{
    invirt_disconnect_invh();
}

int
invirt_release_page_range_invh(struct invirt_thread_group * tg,
                               unsigned long                start,
                               unsigned long                nr_pages,
                               bool                         destroying)
{
    struct invirt_vcall mmu_not = {
        .msg_type = INVIRT_VCALL_MMU_NOT,
        .tracer_tgid = tg->tgid,
        .mmu_not = {
            .msg_type = INVIRT_MMU_NOT_INVALIDATE,
            .vm_start = start,
            .nr_pages = nr_pages,
            .destroying = destroying
        }
    };
    int status;

    status = invirt_send_vcall_socket_with_uuid(invh_socket, &mmu_not, invirt_uuid);
    if (status != 0) {
        inv_err("failed to send INVIRT_VCALL_MMU_NOT message\n");
        return status;
    }

    status = invirt_recv_vcall_socket(invh_socket, &mmu_not, false, false);
    if (status != 0) {
        inv_err("failed to recv INVIRT_VCALL_MMU_NOT response\n");
        return status;
    }

    return mmu_not.mmu_not.rc;
}
