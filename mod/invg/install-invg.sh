#!/bin/bash

if [ ! -d $DIR ]; then
    echo "$DIR not present"
    exit 1
fi

set -e
set -x

#insmod /invg.ko invirt_debug_level=1 invirt_show_uuid=0
insmod /invg.ko invirt_debug_level=1 invirt_show_uuid=1

DIR=/sys/devices/virtual/misc/invg
DEV=$DIR/dev

if [ ! -f $DEV ]; then
    echo "$DEV not present"
    exit 1
fi

major=$(cat $DEV | cut -d ':' -f 1)
minor=$(cat $DEV | cut -d ':' -f 2)

mknod /dev/invg c $major $minor
chmod 666 /dev/invg
