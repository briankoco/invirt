/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

#ifndef __INVIRT_PRIV_H__
#define __INVIRT_PRIV_H__

#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/mm_types.h>
#include <linux/atomic.h>
#include <linux/sched.h>
#include <linux/list.h>
#include <linux/bitmap.h>
#include <linux/rbtree.h>
#include <linux/net.h>
#include <linux/file.h>

#include <invirt.h>
#include <invirt_common.h>

/*
 * We don't support kernels earlier than 4.8. fixup_user_fault() was not
 * exported before then, and in general it's a losing battle to provide
 * compatibility much further back
 */
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,8,0)
#error "Kernel must be version 4.8.0 or newer"
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,11,0)
#include <linux/sched/mm.h>
#include <linux/sched/task.h>
#else
#include <linux/mm.h>
#endif

#ifdef CONFIG_MMU_NOTIFIER
#include <linux/mmu_notifier.h>
#else
#error "Kernel needs to be configured with CONFIG_MMU_NOTIFIER"
#endif

/*
 * info we need to track for invirt programs
 */
struct invirt_thread_group {
    /* local shadow info */
    pid_t tgid;
    struct mm_struct * mm;

    /* target info */
    pid_t target_tgid;
    struct task_struct * target_task;
    struct mm_struct * target_mm;

    /* mmu notifier */
    struct mmu_notifier mmu_not;

    /* RB-tree of remote page extents we've faulted in */
    struct rb_root remote_extent_root;

    /* RB-tree of local page extents we've faulted in */
    struct rb_root local_extent_root;

    /* linkage on gbl_tg_hashtable */
    struct list_head gbl_hashnode;

    /*
     * - mutex protects modifications to data structures that can be
     *   concurrently modified
     * - lock protects flags
     */
    struct mutex mutex;
    spinlock_t lock;
    atomic_t refcnt;
    volatile int flags;
};

/*
 * Attribute and state flags
 */
#define INVIRT_FLAG_DESTROYING      0x00010 /* being destroyed */
#define INVIRT_FLAG_DESTROYED       0x00020 /* 'being destroyed' finished */
#define INVIRT_FLAG_FLUSHING        0x00040 /* being flushed */

/* defined in invirt_core.c */
extern struct invirt_hashlist * gbl_tg_hashtable;
extern char * invirt_uuid;
void invirt_teardown_tg(struct invirt_thread_group *);

/* defined in invirt_umem.c */
int invirt_setup_vma(struct invirt_thread_group *, struct vm_area_struct *);
int invirt_do_remote_fault(struct invirt_thread_group *, struct socket *,
    unsigned long, unsigned int, bool);
void invirt_release_local_extents(struct invirt_thread_group *, bool);
void invirt_release_remote_extents(struct invirt_thread_group *);
int invirt_release_page_range(struct invirt_thread_group *, unsigned long,
unsigned long);

/* defined in invirt_pts.c */
pte_t * invirt_vaddr_to_pte_offset(struct mm_struct *, unsigned long,
    unsigned long *);
unsigned long invirt_vaddr_to_pfn(struct mm_struct *, unsigned long);
#define invirt_vaddr_to_pte(mm, vaddr) \
    invirt_vaddr_to_pte_offset(mm, vaddr, NULL)

/* defined in invirt_mmunot.c */
int invirt_init_tg_mmunot(struct invirt_thread_group *);
void invirt_deinit_tg_mmunot(struct invirt_thread_group *);
int invirt_enable_mmunot(void);
void invirt_disable_mmunot(void);
int invirt_release_page_range_invh(struct invirt_thread_group *, unsigned long, unsigned long, bool);

struct invirt_hashlist {
    rwlock_t lock;
    struct list_head list;
} ____cacheline_aligned;

#define INVIRT_TG_HASHTABLE_SIZE 8

static inline int
invirt_tg_hashtable_index(pid_t tgid)
{
    return ((unsigned int)tgid % INVIRT_TG_HASHTABLE_SIZE);
}

static inline void
invirt_tg_ref(struct invirt_thread_group * tg)
{
    WARN_ON(atomic_read(&(tg->refcnt)) <= 0);
    atomic_inc(&(tg->refcnt));
}

static inline struct invirt_thread_group *
invirt_tg_ref_by_tgid_nolock_internal(pid_t tgid,
                                      int   index,
                                      int   return_destroying)
{
    struct invirt_thread_group * tg;

    list_for_each_entry(tg, &(gbl_tg_hashtable[index].list), gbl_hashnode) {
        if (tg->tgid == tgid) {
            if ((tg->flags & INVIRT_FLAG_DESTROYING)  &&
                (return_destroying == 0))
            {
                continue;
            }

            invirt_tg_ref(tg);
            return tg;
        }
    }

    return ERR_PTR(-ENOENT);
}

static inline void
invirt_tg_deref(struct invirt_thread_group * tg)
{
    WARN_ON(atomic_read(&(tg->refcnt)) <= 0);
    if (atomic_dec_return(&(tg->refcnt)) != 0)
        return;

    WARN_ON(!(tg->flags & INVIRT_FLAG_DESTROYED));

    inv_dbg(3, "last deref of tg %d\n", tg->tgid);

    /* last put of this tg .. tear it down */
    kfree(tg);
}

static inline struct invirt_thread_group *
__invirt_tg_ref_by_tgid(pid_t tgid,
                        int   return_destroying,
                        int   do_lock)
{
    struct invirt_thread_group * tg;
    int index;
    unsigned long flags;

    index = invirt_tg_hashtable_index(tgid);

    if (do_lock)
        read_lock_irqsave(&(gbl_tg_hashtable[index].lock), flags);

    tg = invirt_tg_ref_by_tgid_nolock_internal(
        tgid,
        index,
        return_destroying
    );

    if (do_lock)
        read_unlock_irqrestore(&(gbl_tg_hashtable[index].lock), flags);

    return tg;
}

#define invirt_tg_ref_by_tgid(t)            __invirt_tg_ref_by_tgid(t, 0, 1)
#define invirt_tg_ref_by_tgid_all(t)        __invirt_tg_ref_by_tgid(t, 1, 1)
#define invirt_tg_ref_by_tgid_nolock(t)     __invirt_tg_ref_by_tgid(t, 0, 0)
#define invirt_tg_ref_by_tgid_nolock_all(t) __invirt_tg_ref_by_tgid(t, 1, 0)


static inline void
invirt_tg_not_destroyable(struct invirt_thread_group * tg)
{
    atomic_set(&(tg->refcnt), 1);
}

static inline void
invirt_tg_destroyable(struct invirt_thread_group * tg)
{
    invirt_tg_deref(tg);
}

static inline pid_t
invirt_get_pid(void)
{
    return task_pid_vnr(current);
}

static inline pid_t
invirt_get_tgid(void)
{
    return task_tgid_vnr(current);
}


#endif /* __INVIRT_PRIV_H__ */
