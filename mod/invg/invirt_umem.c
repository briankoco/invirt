/*
 * This file is part of the invirt project.
 *
 * This is free software.  You are permitted to use, redistribute, and
 * modify it as specified in the file "LICENSE.md".
 */

/*
 * This file provides memory management support for shadow memory regions in
 * the guest shadow process.
 *
 * The shadow process as a whole has 2 broad classes of memory regions: those
 * allocated by the kernel during its own execution, and these invirt regions
 * that map the target's initial VMA regions. This file supports creation and
 * management of the latter.
 *
 * Shadow regions are created by mmap()'ing /dev/invirt. This allows us to
 * latch onto the VMAs (which are still created by the kernel, not us) and
 * register a custom page fault function on top of them. When page faults are
 * encountered, the general strategy is to first force fault the relevant
 * memory region in the target process, query the PA that was allocated for the
 * target, and then map that into the shadow.
 *
 * However, not all memory regions are created equally. There are two classes
 * of VMAs that we need to handle: those mapping "regular" memory that the
 * kernel manages with "struct page", and those mapping "raw" memory that the
 * kernel does not track in the same way. The latter class are really just
 * physical page frame numbers (PFNs) that can be mapped into page tables, but
 * for which lots of other kernel functionality is removed, including "page
 * following," whereby a process can walk the page tables and derive a list of
 * "struct page"s that cover the address range. Page following is commonly
 * performed by drivers, e.g., to pin memory for RDMA transfers.
 *
 * We handle both classes of memory here.  Depending on the type of VMA in the
 * target process, we have different mechanisms for walking the target's page
 * tables to query the phyiscal memory.
 *
 * All memory is inserted into the shadow's address space as raw PFNs.
 */

#include <linux/version.h>
#include <linux/mm.h>
#include <linux/memory.h>
#include <linux/vmalloc.h>
#include <linux/highmem.h>
#include <linux/slab.h>
#include <linux/atomic.h>
#include <linux/rbtree.h>

#include <asm/page.h>
#include <asm/io.h>

#include <invirt_priv.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,8,0)
#define mmap_read_lock(mm)    down_read(&(mm)->mmap_sem)
#define mmap_read_unlock(mm)  up_read(&(mm)->mmap_sem)
#endif

/*
 * Each memory mapping of /dev/invirt generates an invirt_local_extent
 *
 * This data structure is created on the call to mmap(/dev/invirt), and
 * is stored in vma->vm_private_data
 */
struct invirt_local_extent {
    atomic_t nr_pages_refed;
    atomic_t refcnt;

    struct invirt_thread_group * tg;
    struct vm_area_struct * vma;
    unsigned long vm_start; /* set to vma->vm_start on creation */
    unsigned long * pfn_list;
    unsigned long nr_pages;

    /* rb-tree linkage; queried via vma->vm_start, which we know will not
     * change throughout the lifetime of the VMA due to VM_DONTEXPAND in
     * vma->vm_flags
     */
    struct rb_node rb_node;
};

/*
 * Each remote fault request generates an invirt_remote_extent
 *
 * If the host is just faulting one page at a time, each extent is just one
 * page; otherwise, the extent likely covers the whole VMA (though not
 * necessarily -- the VMA could grow or shrink)
 *
 * Each time we create a new extent, we add it to an RB-tree for the thread
 * group, sorted by extent->vm_start
 *
 * We remove extents on MMU invalidations
 */

struct invirt_remote_extent {
    struct vm_area_struct * vma; /* VMA we cover */
    unsigned long vm_start; /* address in the VMA we cover from */
    unsigned long * pfn_list;  /* list of pfns we've followed (+ref'd) */
    unsigned long nr_pages; /* size of pfn_list */
    unsigned long pte_flags; /* how the memory is mapped */

    /* rb-tree linkage; queried via vm_start. There is one remote extent for
     * every fault request received from invh. We don't know whether or not
     * the VMA's own start/end are the same at teardown as they are on creation
     * of the remote_extent (i.e., at fault time), but that's ok, since we
     * don't actually need the VMA to find pages, which are stored in pfn_list
     */
    struct rb_node rb_node;
};

static struct vm_operations_struct invirt_vma_ops;

static inline bool
__is_local_extent_vma(struct vm_area_struct * vma)
{
    return (vma->vm_ops == &invirt_vma_ops);
}

static void
__invirt_release_local_extent(struct invirt_local_extent * local_extent,
                              bool                         from_mmu)
{
    unsigned long i, pfn, length;

    for (i = 0;
          (i < local_extent->nr_pages) &&
          (atomic_read(&(local_extent->nr_pages_refed)) > 0);
         i++)
    {
        pfn = local_extent->pfn_list[i];
        if (pfn && pfn_valid(pfn)) {
            put_page(pfn_to_page(pfn));
            atomic_dec(&(local_extent->nr_pages_refed));
            local_extent->pfn_list[i] = 0;
        }
    }

    length = (local_extent->nr_pages << PAGE_SHIFT);

    /* if this all started from an mmu notifier callout, we don't need
     * to zap the PTEs, since that will be done by the kernel's VM
     * system after we return
     */
    if (!from_mmu) {
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,18,0)
        WARN_ON(zap_vma_ptes(local_extent->vma, local_extent->vma->vm_start, length) != 0);
#else
        zap_vma_ptes(local_extent->vma, local_extent->vma->vm_start, length);
#endif

        inv_dbg(3, "zapped PTEs for [0x%lx, 0x%lx)\n",
            local_extent->vma->vm_start,
            local_extent->vma->vm_start + length
        );
    }

    WARN_ON(atomic_read(&(local_extent->nr_pages_refed)) != 0);

    inv_dbg(2, "released local_extent [0x%lx, 0x%lx)\n",
        local_extent->vma->vm_start,
        local_extent->vma->vm_start + length
    );
}


static void
__invirt_release_remote_extent(struct invirt_thread_group  * tg,
                               struct invirt_remote_extent * remote_extent,
                               bool                          send_invalidate)
{
    unsigned long i, pfn, start, end, nr_pages;
    int status, destroying;

    start    = remote_extent->vm_start;
    nr_pages = remote_extent->nr_pages;
    end      = start + (nr_pages << PAGE_SHIFT);

    destroying = (tg->flags & INVIRT_FLAG_DESTROYING);

    /* send invalidation message to invh */
    if (send_invalidate) {
        status = invirt_release_page_range_invh(tg, start, nr_pages, destroying);
        if (status != 0) {
            /* -ESRCH with a DESTROYING tg almost certainly means the host has already
             * killed its thread
             */
            if ((status != -ESRCH) || !destroying) {
                inv_err("remote MMU invalidation of [0x%lx, 0x%lx) failed (ret=%d)\n",
                    remote_extent->vm_start,
                    remote_extent->vm_start + (remote_extent->nr_pages << PAGE_SHIFT),
                    status
                );
            }
        }
    }

    for (i = 0; i < remote_extent->nr_pages; i++) {
        pfn = remote_extent->pfn_list[i];
        if (pfn_valid(pfn))
            put_page(pfn_to_page(pfn));
    }

    inv_dbg(2, "released remote_extent [0x%lx -- 0x%lx)\n", start, end);
}

#define __add_extent(root, type, new_extent) ({\
    struct rb_node ** new    = &((root)->rb_node);\
    struct rb_node *  parent = NULL;\
    \
    unsigned long new_start = new_extent->vm_start;\
    unsigned long new_end   = new_start + (new_extent->nr_pages << PAGE_SHIFT);\
    int ret;\
    \
    while (*new) {\
        type * extent = container_of(*new, type, rb_node);\
        unsigned long start = extent->vm_start;\
        unsigned long end   = start + (extent->nr_pages << PAGE_SHIFT);\
        \
        parent = *new;\
        if (new_end <= start)\
            new = &((*new)->rb_left);\
        else if (new_start >= end)\
            new = &((*new)->rb_right);\
        else {\
            ret = -EFAULT;\
            goto done;\
        }\
    }\
    \
    rb_link_node(&(new_extent->rb_node), parent, new);\
    rb_insert_color(&(new_extent->rb_node), root);\
    ret = 0;\
    \
done:\
    ret;\
})


#define __add_local_extent(root, new_extent) \
    __add_extent(root, struct invirt_local_extent, new_extent);

#define __add_remote_extent(root, new_extent) \
    __add_extent(root, struct invirt_remote_extent, new_extent);

#define __print_extents(root, type) ({\
    struct rb_node * node;\
    struct rb_node * next;\
    type           * extent;\
    \
    for (node = rb_first(root); node != NULL; node = next) {\
        next = rb_next(node);\
        extent = container_of(node, type, rb_node);\
        inv_dbg(3, "  [0x%lx, 0x%lx)\n", extent->vm_start, extent->vm_start + (extent->nr_pages << 12));\
    }\
})

/* Find the first extent that begins after the specified address */
#define __find_extent(root, type, start) ({\
    struct rb_node * node;\
    struct rb_node * next;\
    type           * extent;\
    type           * found = NULL;\
    \
    for (node = rb_first(root); node != NULL; node = next) {\
        next = rb_next(node);\
        extent = container_of(node, type, rb_node);\
        if (extent->start >= start) {\
            found = extent;\
            break;\
        }\
    }\
    found;\
})

#define __find_local_extent(root, start) \
    __find_extent(root, struct invirt_local_extent, start)

#define __find_remote_extent(root, start) \
    __find_extent(root, struct invirt_remote_extent, start)

static void
__print_local_extents(struct invirt_thread_group * tg)
{
    inv_dbg(3, "printing tg %d local extent tree\n", tg->tgid);
    __print_extents(&(tg->local_extent_root), struct invirt_local_extent);
}

static void
__print_remote_extents(struct invirt_thread_group * tg)
{
    inv_dbg(3, "printing tg %d remote extent tree\n", tg->tgid);
    __print_extents(&(tg->remote_extent_root), struct invirt_remote_extent);
}

static int
__invirt_follow_page(struct invirt_thread_group * tg,
                     struct vm_area_struct      * target_vma,
                     struct vm_area_struct      * vma,
                     unsigned long                fault_address,
                     unsigned long                vmf_flags,
                     struct page               ** page)
{
    unsigned long gup_flags;
    long rc;

    gup_flags = FOLL_POPULATE | FOLL_MLOCK | FOLL_GET;
    if (vma->vm_flags & VM_WRITE)
        gup_flags |= FOLL_WRITE;

    rc = get_user_pages_remote(
#if LINUX_VERSION_CODE < KERNEL_VERSION(5,9,0)
        tg->target_task,
#endif
        tg->target_mm,
        fault_address,
        1,
        gup_flags,
        page,
        NULL
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,10,0)
        , NULL
#endif
    );

    if (rc != 1)
        inv_err("get_user_pages failed (err=%ld)\n", rc);

    return (rc == 1) ? 0 : rc;
}

static int
__invirt_follow_pfn(struct task_struct    * task,
                    struct mm_struct      * mm,
                    struct vm_area_struct * vma,
                    unsigned long           fault_address,
                    unsigned long           vmf_flags,
                    unsigned long         * pfn)


{
    int status;

    status = follow_pfn(vma, fault_address, pfn);
    if (status != 0) {
        /* follow_pfn() just walks the PTs, it does not force faults -- so we
         * do so manually here
         */
        status = fixup_user_fault(
#if LINUX_VERSION_CODE < KERNEL_VERSION(5,9,0)
            task,
#endif
            mm,
            fault_address,
            (vmf_flags & FAULT_FLAG_WRITE) ? FAULT_FLAG_WRITE : 0,
            NULL
        );

        if (status != 0) {
            inv_err("fixup_user_fault() failed (err=%d)\n", status);
            return status;
        }

        status = follow_pfn(vma, fault_address, pfn);
        if (status != 0) {
            inv_err("follow_pfn() failed (err=%d)\n", status);
            return status;
        }
    }

    return 0;
}

static int
__invirt_map_memory(struct invirt_thread_group * tg,
                    struct vm_area_struct      * target_vma,
                    struct vm_area_struct      * vma,
                    unsigned long                fault_address,
                    unsigned long                vmf_flags)
{
    struct invirt_local_extent * extent = vma->vm_private_data;
    struct page * page;
    unsigned long pfn, pgoff;
    int status;

    /* Use the target's VMA to determine how to find the memory  */
    if (target_vma->vm_flags & (VM_IO | VM_PFNMAP)) {
        status = __invirt_follow_pfn(tg->target_task, tg->target_mm,
            target_vma, fault_address, vmf_flags, &pfn
        );

        if ((status == 0) && pfn_valid(pfn))
            get_page(pfn_to_page(pfn));

    } else {
        status = __invirt_follow_page(tg, target_vma, vma, fault_address,
            vmf_flags, &page
        );

        if (status == 0)
            pfn = page_to_pfn(page);
    }

    if (status != 0)
        return VM_FAULT_SIGBUS;

    /* we found memory, now map it in */
    status = remap_pfn_range(vma, fault_address, pfn, PAGE_SIZE,
        vma->vm_page_prot
    );
    if (status != 0) {
        inv_err("remap_pfn_range() failed (err=%d)\n", status);

        if (pfn_valid(pfn))
            put_page(pfn_to_page(pfn));

        return VM_FAULT_SIGBUS;
    }

    /* remember valid pages for future release */
    if (pfn_valid(pfn)) {
        pgoff = (fault_address - vma->vm_start) >> PAGE_SHIFT;
        WARN_ON(extent->pfn_list[pgoff] != 0);
        extent->pfn_list[pgoff] = pfn;
        atomic_inc(&(extent->nr_pages_refed));
    }

    inv_dbg(3, "[target VMA] mapped local fault VA 0x%lx to %s at PA 0x%lx\n",
        fault_address,
        pfn_valid(pfn) ? "[struct page]" : "[raw PFN]",
        pfn_valid(pfn) ? (unsigned long)page_to_phys(page) : (unsigned long)PFN_PHYS(pfn)
    );

    __print_local_extents(tg);

    return VM_FAULT_NOPAGE;
}


/*
 * Page fault handler
 *
 * See detailed comment at the top of this file for discussion on the
 * logic we use
 */
static int
__invirt_vma_fault(struct invirt_thread_group * tg,
                   unsigned long                vmf_flags,
                   struct vm_area_struct      * vma,
                   unsigned long                fault_address)
{
    struct vm_area_struct * target_vma;
    int status;

    status = VM_FAULT_SIGBUS;

    /* need find_extend_vma in case this fault should force a stack expansion */
    target_vma = find_extend_vma(tg->target_mm, fault_address);
    if (!target_vma || target_vma->vm_start > fault_address) {
        inv_err("could not find target VMA at VA 0x%lx\n", fault_address);
        goto out;
    }

    status = __invirt_map_memory(
        tg,
        target_vma,
        vma,
        fault_address,
        vmf_flags
    );

out:
    if (status != VM_FAULT_NOPAGE)
        inv_err("failed to handle page fault at 0x%lx\n", fault_address);

    return status;
}


#if LINUX_VERSION_CODE < KERNEL_VERSION(4,17,0)
typedef int vm_fault_t;
#endif

static vm_fault_t
invirt_vma_fault(
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,11,0)
                 struct vm_area_struct * vma,
#endif
                 struct vm_fault       * vmf)
{
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,11,0)
    struct vm_area_struct * vma = vmf->vma;
#endif

    unsigned long fault_address;
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,10,0)
    fault_address = PAGE_ALIGN(vmf->virtual_address);
#else
    fault_address = PAGE_ALIGN(vmf->address);
#endif

    {
        struct invirt_thread_group * tg;
        int ret;

        tg = invirt_tg_ref_by_tgid(invirt_get_tgid());
        if (IS_ERR(tg))
            return VM_FAULT_SIGBUS;

        ret = __invirt_vma_fault(tg, vmf->flags, vma, fault_address);

        invirt_tg_deref(tg);
        return ret;
    }
}

static void
invirt_vma_open(struct vm_area_struct * vma)
{
    struct invirt_local_extent * extent = vma->vm_private_data;
    atomic_inc(&(extent->refcnt));
}

static void
invirt_vma_close(struct vm_area_struct * vma)
{

    struct invirt_local_extent * extent = vma->vm_private_data;

    if (atomic_dec_return(&(extent->refcnt)) > 0)
        return;

    __invirt_release_local_extent(extent, false);

    rb_erase(&(extent->rb_node), &(extent->tg->local_extent_root));
    vfree(extent->pfn_list);

    invirt_tg_deref(extent->tg);
    kfree(extent);

    inv_dbg(2, "[target VMA] closed VMA [0x%lx -- 0x%lx)\n", vma->vm_start, vma->vm_end);
}

static struct vm_operations_struct
invirt_vma_ops =
{
    .open  = invirt_vma_open,
    .close = invirt_vma_close,
    .fault = invirt_vma_fault,
};

int
invirt_setup_vma(struct invirt_thread_group * tg,
                 struct vm_area_struct      * vma)
{
    struct invirt_local_extent * extent;
    unsigned long nr_pages;
    int status;

    mutex_lock(&(tg->mutex));

    /* ensure tg has attached to a target */
    if (tg->target_tgid == 0) {
        mutex_unlock(&(tg->mutex));
        return -ENODEV;
    }

    status = -ENOMEM;
    extent = kmalloc(sizeof(struct invirt_local_extent), GFP_KERNEL);
    if (extent == NULL)
        goto out_kmalloc;

    nr_pages = (vma->vm_end - vma->vm_start) >> PAGE_SHIFT;
    if ((vma->vm_end - vma->vm_start) & (PAGE_SIZE - 1))
        nr_pages++;

    extent->pfn_list = vzalloc(sizeof(unsigned long) * nr_pages);
    if (extent->pfn_list == NULL)
        goto out_vmalloc;

    extent->tg = tg;
    extent->vm_start = vma->vm_start;
    extent->vma = vma;
    extent->nr_pages = nr_pages;
    atomic_set(&(extent->nr_pages_refed), 0);
    atomic_set(&(extent->refcnt), 1);

    /* maintain an extra ref to protect vma->tg */
    invirt_tg_ref(tg);

    /* add to rb-tree */
    status = __add_local_extent(&(tg->local_extent_root), extent);
    if (status != 0) {
        inv_err("failed to track local extent (err=%d)\n", status);
        goto out_extent;
    }

    vma->vm_flags |= (VM_IO | VM_PFNMAP | VM_DONTCOPY | VM_DONTEXPAND);
    vma->vm_ops = &invirt_vma_ops;
    vma->vm_private_data = extent;

    mutex_unlock(&(tg->mutex));

    inv_dbg(2, "added local_extent [0x%lx, 0x%lx)\n",
        extent->vm_start,
        extent->vm_start + (nr_pages << PAGE_SHIFT)
    );

    return 0;

out_extent:
    vfree(extent->pfn_list);
out_vmalloc:
    kfree(extent);
out_kmalloc:
    mutex_unlock(&(tg->mutex));
    return status;
}

static int
__invirt_faultin_range(struct invirt_thread_group   * tg,
                       struct vm_area_struct        * vma,
                       unsigned long                  vm_start,
                       unsigned long                  nr_pages,
                       unsigned int                   vmf_flags,
                       struct invirt_remote_extent ** page_extent)
{
    unsigned long vaddr, gup_flags, nr_pinned, i, pfn;
    struct invirt_remote_extent * extent;
    struct page ** page_list;
    int status;
    bool do_gup;
    long rc;

    do_gup = !(vma->vm_flags & (VM_IO | VM_PFNMAP));

    if (do_gup) {
        gup_flags = FOLL_POPULATE | FOLL_MLOCK | FOLL_GET;
        if (vmf_flags & FAULT_FLAG_WRITE)
            gup_flags |= FOLL_WRITE;
    }

    extent = kmalloc(sizeof(struct invirt_remote_extent), GFP_KERNEL);
    if (extent == NULL) {
        inv_err("out of memory\n");
        return -ENOMEM;
    }

    extent->vma = vma;
    extent->vm_start = vm_start;
    extent->nr_pages = nr_pages;
    extent->pte_flags = 0; /* FIXME */
    extent->pfn_list = vmalloc(sizeof(unsigned long) * nr_pages);
    if (extent->pfn_list == NULL) {
        inv_err("out of memory\n");
        kfree(extent);
        return -ENOMEM;
    }

    page_list = vmalloc(sizeof(struct page *) * nr_pages);
    if (page_list == NULL) {
        inv_err("out of memory\n");
        vfree(extent->pfn_list);
        kfree(extent);
        return -ENOMEM;
    }

    vaddr = vm_start;
    nr_pinned = 0;

    do {
        if (do_gup) {
            rc = get_user_pages(
                vaddr + (nr_pinned * PAGE_SIZE),
                nr_pages - nr_pinned,
                gup_flags,
                page_list + nr_pinned,
                NULL
            );

            if (rc < 0) {
                inv_err("get_user_pages failed (err=%ld)\n", rc);
                status = rc;
                goto err_gup;
            }

            for (i = nr_pinned; i < (nr_pinned + rc); i++) {
                extent->pfn_list[i] = page_to_pfn(page_list[i]);
            }
        } else {
            status = __invirt_follow_pfn(
                current, current->mm, vma,
                vaddr + (nr_pinned * PAGE_SIZE),
                vmf_flags,
                &pfn
            );

            if (status != 0)
                goto err_follow;

            if (pfn_valid(pfn))
                get_page(pfn_to_page(pfn));

            extent->pfn_list[nr_pinned] = pfn;
            rc = 1;
        }

        nr_pinned += rc;
    } while (nr_pinned < nr_pages);

    /* add to rb-tree */
    mutex_lock(&(tg->mutex));

    /* first, find any remove extents that overlap with this VMA range */
    while (true) {
        unsigned long end, exist_end;
        struct invirt_remote_extent * exist;

        exist = __find_remote_extent(&(tg->remote_extent_root), vm_start);
        if (!exist)
            break;

        end = vm_start + (nr_pages << PAGE_SHIFT);
        exist_end = exist->vm_start + (exist->nr_pages << PAGE_SHIFT);

        if (exist->vm_start >= end)
            break;

        /* exist overlaps this range; ensure it does so completely, else BUG */
        BUG_ON(exist_end > end);

        /* remove it */
        __invirt_release_remote_extent(tg, exist, true);

        rb_erase(&(exist->rb_node), &(tg->remote_extent_root));
        vfree(exist->pfn_list);
        kfree(exist);
    }

    /* add the new extent */
    status = __add_remote_extent(&(tg->remote_extent_root), extent);
    mutex_unlock(&(tg->mutex));

    if (status != 0) {
        inv_err("failed to track remote extent [0x%lx, 0x%lx) (err=%d)\n",
            vm_start, vm_start + (nr_pages << PAGE_SHIFT), status);
        goto err_extent;
    }

    inv_dbg(1, "faulted in remote_extent [0x%lx, 0x%lx)\n", vm_start, vm_start + (nr_pages << PAGE_SHIFT));

    vfree(page_list);
    *page_extent = extent;
    return 0;

err_follow:
err_gup:
    if (nr_pinned > 0)
        inv_err("could only pin %lu pages out of %lu in VMA\n", nr_pinned, nr_pages);

err_extent:
    for (i = 0; i < nr_pinned; i++) {
        pfn = extent->pfn_list[i];
        if (pfn_valid(pfn))
            put_page(pfn_to_page(pfn));
    }

    vfree(page_list);
    vfree(extent->pfn_list);
    kfree(extent);

    return status;
}

static int
__invirt_faultin_vma(struct invirt_thread_group   * tg,
                     struct vm_area_struct        * vma,
                     unsigned int                   vmf_flags,
                     struct invirt_remote_extent ** extent)
{
    return __invirt_faultin_range(tg, vma, vma->vm_start,
        (vma->vm_end - vma->vm_start) >> PAGE_SHIFT,
        vmf_flags, extent
    );
}

static int
__invirt_faultin_page(struct invirt_thread_group   * tg,
                      struct vm_area_struct        * vma,
                      unsigned long                  address,
                      unsigned int                   vmf_flags,
                      struct invirt_remote_extent ** extent)
{
    return __invirt_faultin_range(tg, vma, address, 1, vmf_flags, extent);
}

static int
__invirt_send_remote_fault_response(struct invirt_thread_group   * tg,
                                    struct socket                * socket,
                                    struct invirt_remote_extent  * extent,
                                    int                            rc)
{
    int status, sent;
    unsigned long len;
    struct invirt_vcall vcall = {
        .msg_type = INVIRT_VCALL_PAGE_FAULT,
        .tracer_tgid = tg->tgid,
        .page_fault = {
            .hva = extent->vm_start,
            .nr_pages = extent->nr_pages,
            .pte_flags = extent->pte_flags,
            .rc = rc
        }
    };

    status = invirt_send_vcall_socket(socket, &vcall);
    if (status != 0) {
        inv_err("failed to send page_fault response (err=%d)\n", status);
        return status;
    }

    len = sizeof(unsigned long) * extent->nr_pages;
    sent = invirt_send_data_socket(socket, (void *)extent->pfn_list, len);
    if (sent != len) {
        inv_err("failed to send GPA list (sent %d out of %lu bytes)\n", sent, len);
        status = -EIO;
    } else
        status = 0;

    return status;
}

static int
__invirt_send_remote_fault_response_error(struct invirt_thread_group * tg,
                                          struct socket              * socket,
                                          int                          status)
{
    struct invirt_remote_extent extent = {
        .vm_start = 0,
        .nr_pages = 0,
        .pte_flags = 0
    };

    return __invirt_send_remote_fault_response(tg, socket, &extent, status);
}

int
invirt_do_remote_fault(struct invirt_thread_group * tg,
                       struct socket *              sock,
                       unsigned long                fault_vaddr,
                       unsigned int                 vmf_flags,
                       bool                         prefault_vma)
{
    struct vm_area_struct * vma;
    struct invirt_remote_extent * extent;
    int status;

    mmap_read_lock(current->mm);

    status = -EFAULT;
    vma = find_vma(current->mm, fault_vaddr);
    if (!vma || vma->vm_start > fault_vaddr) {
        inv_err("failed to handle fault at 0x%lx: no VMA for this address\n", fault_vaddr);
        goto out;
    }

    /* see if user wants to fault the whole vma in or just a page */
retry:
    if (prefault_vma)
        status = __invirt_faultin_vma(tg, vma, vmf_flags, &extent);
    else
        status = __invirt_faultin_page(tg, vma, fault_vaddr, vmf_flags, &extent);

    /*
     * It is possible to fail to prefault a whole VMA. For example, only a
     * subset of the 4 VVAR pages can be mapped based on the availability of
     * VDSO-capable clock sources.
     *
     * So, if we encounter a failure on attempt to prefault a whole VMA,
     * fallback to only faulting the specific vaddr before bailing altogether.
     */
    if (status && prefault_vma) {
        inv_dbg(1, "failed to prefault VMA [0x%lx, %lx). Retrying 0x%lx without prefault\n",
            vma->vm_start, vma->vm_end, fault_vaddr
        );
        prefault_vma = false;
        goto retry;
    }

    if (status == 0) {
        inv_dbg(2, "%s faulted %lu pages to VA [0x%lx -- 0x%lx)\n",
            __is_local_extent_vma(vma) ? "[target VMA]" : "[shadow VMA]",
            extent->nr_pages,
            extent->vm_start,
            extent->vm_start + (extent->nr_pages * PAGE_SIZE)
        );

        __print_remote_extents(tg);
    }

out:
    if (status)
        status = __invirt_send_remote_fault_response_error(tg, sock, status);
    else
        status = __invirt_send_remote_fault_response(tg, sock, extent, 0);

    mmap_read_unlock(current->mm);

    return status;
}

static void
__invirt_release_local_extents(struct invirt_thread_group * tg,
                               unsigned long                start,
                               unsigned long                end,
                               bool                         from_mmu)
{
    struct rb_node * node, * next;
    struct invirt_local_extent * extent;
    unsigned long local_start, local_end;

    for (node = rb_first(&(tg->local_extent_root)); node != NULL; node = next)
    {
        next = rb_next(node);
        extent = container_of(node, struct invirt_local_extent, rb_node);

        local_start = extent->vm_start;
        local_end   = local_start + (extent->nr_pages << PAGE_SHIFT);

         /* once we find an extent past the range, we're done */
        if (end && local_start >= end)
            break;
        else if (start && start >= local_end)
            continue;

        __invirt_release_local_extent(extent, from_mmu);

        /* don't free the local_extent -- that will be done in vma->vm_close */
#if 0
        rb_erase(&(extent->rb_node), &(tg->local_extent_root));
        vfree(extent->pfn_list);
        kfree(extent);
#endif
    }

#if 0
    for (vma = find_vma_intersection(current->mm, start, end);
         vma && vma->vm_start < end;
         vma = vma->vm_next)
    {
        if (vma->vm_start >= end)
            break;

        if (!__is_local_extent_vma(vma))
            continue;

        local_extent = (struct invirt_local_extent *)vma->vm_private_data;
        __invirt_release_local_extent(local_extent, true);
    }
#endif
}

void
invirt_release_local_extents(struct invirt_thread_group * tg,
                             bool                         from_mmu)
{
    inv_dbg(1, "releasing all tg %d local extents\n", tg->tgid);
    //mutex_lock(&(tg->mutex));
    __invirt_release_local_extents(tg, 0, 0, from_mmu);
    //mutex_unlock(&(tg->mutex));
}

static void
__invirt_release_remote_extents(struct invirt_thread_group * tg,
                                unsigned long                start,
                                unsigned long                end)
{
    struct rb_node * node, * next;
    struct invirt_remote_extent * extent;
    unsigned long remote_start, remote_end;

    for (node = rb_first(&(tg->remote_extent_root)); node != NULL; node = next)
    {
        next = rb_next(node);
        extent = container_of(node, struct invirt_remote_extent, rb_node);

        remote_start = extent->vm_start;
        remote_end   = remote_start + (extent->nr_pages << PAGE_SHIFT);

         /* once we find an extent past the range, we're done */
        if (end && remote_start >= end)
            break;
        else if (start && start >= remote_end)
            continue;

        __invirt_release_remote_extent(tg, extent, true);

        rb_erase(&(extent->rb_node), &(tg->remote_extent_root));
        vfree(extent->pfn_list);
        kfree(extent);
    }
}

void
invirt_release_remote_extents(struct invirt_thread_group * tg)
{
    inv_dbg(1, "releasing all tg %d remote extents\n", tg->tgid);
    //mutex_lock(&(tg->mutex));
    __invirt_release_remote_extents(tg, 0, 0);
    //mutex_unlock(&(tg->mutex));
}

/*
 * Called from mmu_notifier callout, mmap_sem already held
 *
 * Invalidate memory mappings for a particular virtual address range.
 * This range could cover either a local or remote extent, or both.
 *
 * Local extents:
 *   Local extents cover memory faulted via /dev/invirt. We need
 *   to both invalidate the PTEs and release refs to the underlying
 *   struct pages
 *
 * Remote extents:
 *   Remote extents cover memory faulted in response to external
 *   requests from invh. We drop references to struct page that
 *   cover this range, but don't manually modify PTEs, so no PTE
 *   zapping is necessary
 *
 * We make no assumptions about the invalidating range mapping cleanly to a
 * given extent. So instead what we do is, if we find an overlap, we invalidate
 * the entire extent. The extent will then be re-faulted in if necessary
 */
int
invirt_release_page_range(struct invirt_thread_group * tg,
                          unsigned long                start,
                          unsigned long                nr_pages)
{
    unsigned long end = start + (nr_pages << PAGE_SHIFT);

    if (WARN_ON(tg->mm != current->mm))
        return -EFAULT;

    mutex_lock(&(tg->mutex));
    __invirt_release_remote_extents(tg, start, end);
    __invirt_release_local_extents(tg, start, end, true);
    mutex_unlock(&(tg->mutex));

    return 0;
}
