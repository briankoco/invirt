#!/bin/bash

if [ -c /dev/invg ]; then
    rmmod invg
fi

insmod invg.ko invirt_debug_level=1 invirt_show_uuid=0
chmod 666 /dev/invg
